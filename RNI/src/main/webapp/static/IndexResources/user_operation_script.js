
function gotoOuterUrl(url)
{
	var win = window.open(url, '_blank');	
}

function showAttachType(attachType)
{
	if(attachType=="file")
	{
		$("#attachFile").slideDown();
		
		$("#attachUrl").slideUp();		
		
		$("#privilageDiv").slideDown();
	}
	else if(attachType=="url")
	{
		$("#attachUrl").slideDown();		
		
		$("#attachFile").slideUp();
		
		$("#privilageDiv").slideUp();
	}
	else
	{
		$("#attachUrl").slideUp();
		$("#attachFile").slideUp();
		
		$("#privilageDiv").slideUp();
	}
}


function uploadSelectedFile(fileFor)
{
	var flag = 'true';
	var fi = document.getElementById('file');
	
	if(fi.files.length>0)
	{	
		var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","mp4","MP4","webm","WEBM","ogg","OGG","PDF","pdf");	
	    
		var fsize = 0;
		
		var condition = "NotGranted";
		
		for (var f = 0; f <= fi.files.length - 1; f++) 
		{
			var fname = fi.files.item(f).name;
			var ext=fname.substring(fname.lastIndexOf('.')+1)
			
			for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
		    	{				    	    
		    	   condition="Granted";				    	    
		    	}			    
			}
			
			if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrId").show();
			    $("#fileErrId").html("Image , video(mp4,webm,ogg) and pdf files are allowed.");
				return false;				  
			}
			
			var fileSizeinBytes = fi.files.item(f).size;   
			var sizeinKB = +fileSizeinBytes / 1024;
	  		var sizeinMB = +sizeinKB / 1024;
	  		
	  		fsize = fsize + sizeinMB;		  		
		}
		
		if(fsize>40)
		{
			flag = 'false';
			$("#fileErrId").show();
			$("#fileErrId").html("choose a file less then 40 MB.");
			return false;	 		   		 
		}		
		
	}
	
	
	if(flag=='true')
	{
		$("#preloader").show();
		
		if(fileFor=="Post")
		{
			var formData = new FormData($("#postForm")[0]);
			
			formData.append("fileFor",fileFor);
			
			
				$.ajax({
					type : "POST",
					data : formData,
					url : "captureTempPostFiles",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response!="")
			      		{																	    		 			    		  
							var fileArray = response.split("/");
							
							$("#selectedFileList").html("");
							
							for(var i=0;i<fileArray.length;i++)
							{
								$("#selectedFileList").append("<li><i title='Remove the file' style='color:#333;cursor:pointer;' class='fa fa-trash' id='"+fileArray[i]+"' onclick='deletePostFile(this.id)'></i> &nbsp;"+fileArray[i]+"</li>");
								
								$("#fileListDiv").show();
							}
							
							$("#file").val("");
							
						}
			      	    else
			      		{
				      		alert("There is some problem arise,Please try again later.");			
				      		
				      		window.location = "postFeed";
			      		}							
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});
		}
		else
		{
			var formData = new FormData($("#commentForm")[0]);
			
			formData.append("fileFor",fileFor);
			
			$.ajax({
		
					type : "POST",
					data : formData,
					url : "captureTempCommentFiles",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response!="")
			      		{																	    		 			    		  
							var fileArray = response.split("/");
							
							$("#selectedFileList").html("");
							
							for(var i=0;i<fileArray.length;i++)
							{
								$("#selectedFileList").append("<li><i title='Remove the file' style='color:#333;cursor:pointer;' class='fa fa-trash' id='"+fileArray[i]+"' onclick='deletePostFile(this.id)'></i> &nbsp;"+fileArray[i]+"</li>");
								
								$("#fileListDiv").show();
							}
							
							$("#file").val("");
							
						}
			      	    else
			      		{
				      		alert("There is some problem arise,Please try again later.");			
				      		
				      		window.location = "userDashboard";
			      		}							
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});	
			
		}	
	}
	
	
}

function deletePostFile(fileName)
{
	$("#preloader").show();
	
	$.ajax({
		type : "POST",
		data : "fileName="+fileName,
		url : "deletePostFile",								
		success : function(response) {
           
			$("#preloader").hide();
			
			if(response!="")
      		{																	    		 			    		  
				var fileArray = response.split("/");
				
				$("#selectedFileList").html("");
				
				for(var i=0;i<fileArray.length;i++)
				{
					$("#selectedFileList").append("<li><i title='Remove the file' style='color:#333;' class='fa fa-trash' id='"+fileArray[i]+"' onclick='deletePostFile(this.id)'></i> &nbsp;"+fileArray[i]+"</li>");
				}
				
				$("#fileListDiv").show();
			}
			else if(response=="")
      		{
				$("#fileListDiv").hide();
				$("#selectedFileList").html("");
      		}
      	    else
      		{
	      		alert("There is some problem arise,Please try again later.");			
	      		window.location = "postFeed";
      		}
			
		},			
	});	
}




function saveMyPost()
{
	var flag = 'true';
	
	var title = $("#post_title").val();
	var fi = document.getElementById('file');
	var desc = $("#post_desc").val();
	var publish = "";
	var attachType = "";
	
	var radioboxes = document.getElementsByName('publish_type');
	
	for (var i=0; i<radioboxes.length; i++) {
	    if (radioboxes[i].checked) {
	    	
	    	publish = radioboxes[i].value;
	    }
	}
	
	var attachCheck = document.getElementsByName('attach_type');
	
	for (var i=0; i<attachCheck.length; i++) {
	    if (attachCheck[i].checked) {
	    	
	    	attachType = attachCheck[i].value;
	    }
	}
	
	if(title=="")
	{
		flag = 'false';
		$("#titleErrId").show();
		$("#titleErrId").html("Please mention a title for your post");
	}
	
	if(title!="")
	{
		$("#titleErrId").hide();
	}
	
	if(fi.files.length>0)
	{	
		var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","mp4","MP4","webm","WEBM","ogg","OGG","PDF","pdf");	
	    
		var fsize = 0;
		
		var condition = "NotGranted";
		
		for (var f = 0; f <= fi.files.length - 1; f++) 
		{
			var fname = fi.files.item(f).name;
			var ext=fname.substring(fname.lastIndexOf('.')+1)
			
			for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
		    	{				    	    
		    	   condition="Granted";				    	    
		    	}			    
			}
			
			if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrId").show();
			    $("#fileErrId").html("Image , video(mp4,webm,ogg) and pdf files are allowed.");
				return false;				  
			}
			
			var fileSizeinBytes = fi.files.item(f).size;   
			var sizeinKB = +fileSizeinBytes / 1024;
	  		var sizeinMB = +sizeinKB / 1024;
	  		
	  		fsize = fsize + sizeinMB;	
	  		
		}
		
		if(fsize>40)
		{
			flag = 'false';
			$("#fileErrId").show();
			$("#fileErrId").html("choose a file less then 40 MB.");
			return false;	 		   		 
		}		
		
	}
	
	
	if(flag=='true')
	{		
		$("#preloader").show();
			
		var formData = new FormData($("#postForm")[0]);
		
		formData.append("publish",publish);
		formData.append("attachType",attachType);
		
		  $.ajax({
					type : "POST",
					data : formData,
					url : "saveMyPost",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response=="success")
			      		{																	    		 			    		  
							alert("Your post successfully posted into the portal.");
							
							window.location = "postFeed";
						}
			      	    else
			      		{
				      		alert("There is some problem arise,Please try again later.");			
				      		
				      		window.location = "postFeed";
			      		}							
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}

function viewIndexUserPost(postId)
{
	$("#preloader").show();;
	
	$.ajax({
		type : "POST",
		data : "postId="+postId,
		url : "viewIndexUserPost",								
		success : function(response) {
           
			$("#preloader").hide();
			
			$("#gallerypopup").html(response);
			
			$("#gallerypopup").show();
			
			$('#gallerypopup')[0].scrollIntoView(true);						
		},			
	});	
}


function viewUserPost(postId,userId)
{
	$("#preloader").show();
	
	$.ajax({
		type : "POST",
		data : "postId="+postId+"&userId="+userId,
		url : "showUserPost",								
		success : function(response) {
           
			$("#preloader").hide();
			
			$("#postpopup").html(response);
			
			$("#postpopup").show();
			
			$('#postpopup')[0].scrollIntoView(true);						
		},			
	});	
}

function countNoOfFile(fileNames)
{
	/*alert("sdfsd");
	var count = fileNames.split(",");
	
	alert("sdfsd-"+count.length);
	
	if(count.length>0)
	{
		alert("sdfsd23232-"+count.length);
		$("#fileCountDiv").append(count+" file choosen");
	}
	else
	{
		$("#fileCountDiv").html("Choose file");
	}*/
}


function downloadPostFile(postId,userId)
{
	window.location ="downloadPostFile?postId="+postId+"&userId="+userId;
}

function postComment(postId)
{
	var flag = 'true';
	
	var comment = $("#post_comment").val();
	//var fi = document.getElementById('commentFiles');
	
	if(comment=="")
	{
		flag = 'false';
		alert("Please type some comment on this post.");
	}
	
	/*if(fi.files.length>0)
	{	
		var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","mp4","MP4","webm","WEBM","ogg","OGG","PDF","pdf");	
	    
		var fsize = 0;
		
		var condition = "NotGranted";
		
		for (var f = 0; f <= fi.files.length - 1; f++) 
		{
			var fname = fi.files.item(f).name;
			var ext=fname.substring(fname.lastIndexOf('.')+1)
			
			for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
		    	{				    	    
		    	   condition="Granted";				    	    
		    	}			    
			}
			
			if(condition=="NotGranted")
			{
			    flag = 'false';	
			    alert("Image , video(mp4,webm,ogg) and pdf files are allowed.");
				return false;				  
			}
			
			
			var fileSizeinBytes = fi.files.item(f).size;   
			var sizeinKB = +fileSizeinBytes / 1024;
	  		var sizeinMB = +sizeinKB / 1024;
	  		
	  		fsize = fsize + sizeinMB;	
	  		
		}
		
		
		if(fsize>40)
		{
			flag = 'false';
			alert("choose a file less then 40 MB.");
			return false;	 		   		 
		}		
		
	}*/
	
	
	if(flag=='true')
	{		
		$("#preloader").show();
			
		var formData = new FormData($("#commentForm")[0]);
		
		formData.append("postId",postId);
		
		  $.ajax({
					type : "POST",
					data : formData,
					url : "postComment",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response=="success")
			      		{																	    		 			    		  
							alert("Your comment has successfully saved.");
							
							viewUserPost(postId,0);
							
							updateCommentCount(postId);
						}
			      	    else
			      		{
				      		alert("There is some problem arise,Please try again later.");		
				      		
				      		viewUserPost(postId,0)				      		
			      		}	
						
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}	
}

function updateCommentCount(postId)
{
	$("#preloader").show();
	
	$.ajax({  
			
	     type : "post",   
	     url : "updateCommentCount", 
	     data :"&postId="+postId,	     	     	     
	     success : function(response) 
	     {  											    	 
	    	 $("#preloader").hide();
	    	 
	    	 $("#cmtDiv"+postId).html(response);
	    	 
	     },  
    });
}


function showCommentFile(event,fileCategory,fileName,userId,postId,fileType)
{
	if(event=="Open")
	{
		$("#preloader").show();
		
		$.ajax({  
				
		     type : "post",   
		     url : "showCommentFile", 
		     data :"fileCategory="+fileCategory+"&fileName="+fileName+"&userId="+userId+"&postId="+postId+"&fileType="+fileType,	     	     	     
		     success : function(response) 
		     {  											    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="failed")
		      	 {
		      		alert("Your session got expired please login again.");	
		      		window.location = "userLogout";
		      	 }
		    	 else 
		      	 {																	    		 	
	    		    $("#commentPopUp").html(response);
	    		    $("#commentPopUp").show();
		    	 }		      	
		     },  
	    });
	}
	else
	{
		$("#commentPopUp").html("");
	    $("#commentPopUp").hide();
	}
}


function showInterest(interest,postId)
{	
	
	$.ajax({
		type : "POST",
		data : "interest="+interest+"&postId="+postId,
		url : "showInterest",								
		success : function(response) {
           
			if(response=="failed")
			{
				alert("There might be a problem occure please try again later.");	
	      		window.location = "postFeed";
			}
			else if(response=="success")
			{
				if(interest=="Like")
				{
					var currentClass = $("#like"+postId).attr("class");
					
					if(currentClass=="fa fa-thumbs-o-up")
					{
						$("#like"+postId).removeClass("fa fa-thumbs-o-up");
						$("#like"+postId).addClass("fa fa-thumbs-up");
					}
					else
					{
						$("#like"+postId).addClass("fa fa-thumbs-o-up");
						$("#like"+postId).removeClass("fa fa-thumbs-up");
					}	
					
					countInterest(interest,postId);
				}
				else
				{
					var currentClass = $("#like"+postId).attr("class");
					
					if(currentClass=="fa fa-thumbs-o-down")
					{
						$("#disLike"+postId).removeClass("fa fa-thumbs-o-down");
						$("#disLike"+postId).addClass("fa fa-thumbs-down");
					}
					else
					{
						$("#disLike"+postId).addClass("fa fa-thumbs-o-down");
						$("#disLike"+postId).removeClass("fa fa-thumbs-down");
					}	
					
					countInterest(interest,postId);
				}
			}
			else
			{
				alert("Your session got expired please login again.");	
	      		window.location = "userDashboard";
			}
							
		},			
	});	

}

function countInterest(interest,postId)
{
	$.ajax({
		type : "POST",
		data : "interest="+interest+"&postId="+postId,
		url : "countInterest",								
		success : function(response) {
           
			if(response=="failed")
			{
				alert("Your session got expired please login again.");	
	      		window.location = "userLogout";
			}
			else if(response>=0)
			{
				if(interest=="Like")
				{
					$("#likeCount"+postId).html("<span>"+response+"</span>");	
					$("#likeCount"+postId)[0].scrollIntoView(true);
				}
				else
				{
					$("#disLikeCount"+postId).html("<span>"+response+"</span>");	
					$("#disLikeCount"+postId)[0].scrollIntoView(true);
				}
			}
			else
			{
				alert("Your session got expired please login again.");	
	      		window.location = "userDashboard";
			}						
		},			
	});	
}
	




function calculateView()
{
	var postId = $("#viewPostId").val();
	
	$.ajax({
		type : "POST",
		data : "postId="+postId,
		url : "calculateView",								
		success : function(response) {
           
			if(response=="failed")
			{
				alert("Your session got expired please login again.");	
	      		window.location = "userLogout";
			}
			else if(response>=0)
			{
				$("#viewPost"+postId).html(response);
			}
			else
			{
				alert("Your session got expired please login again.");	
	      		window.location = "userDashboard";
			}						
		},			
	});	
	
}

/// END OF POST SCRIPT 

///  START EVENT SCRIPT 


function viewEventImage(eventCode)
{
	$("#preloader").show();
	
	$.ajax({
		type : "POST",
		data : "eventCode="+eventCode,
		url : "viewEventImage",								
		success : function(response) {
           
			$("#preloader").hide();
			
			$("#postpopup").html(response);
			
			$("#postpopup").show();
			
			$('#postpopup')[0].scrollIntoView(true);						
		},			
	});	
}


function showVideo(where,fileCategory,fileName,userId,postId)
{	
	$.ajax({
		type : "POST",
		data : "fileCategory="+fileCategory+"&fileName="+fileName+"&userId="+userId+"&postId="+postId,
		url : "showVideo",								
		success : function(response) {
           
			if(response=="failed")
			{
				alert("Your session got expired please login first.")
				window.location = "userLogout";
			}
			else
			{
				if(where=="PopUp")
				{
					$("#renderImagePopUp"+postId).hide();
					$("#renderVideoPopUp"+postId).html(response);
					/*$("#renderVideoPopUp"+postId)[0].scrollIntoView(true);*/
				}
				else if(where=="NoPopUp")
				{
					$("#renderImage"+postId).hide();
					$("#renderVideo"+postId).html(response);
					/*$("#renderVideo"+postId)[0].scrollIntoView(true);*/
				}
				else
				{					
					$("#renderImageCmnt"+postId).hide();
					$("#renderVideoCmnt"+postId).html(response);
					/*$("#renderVideo"+postId)[0].scrollIntoView(true);*/
				}
				
			}
			
		},			
	});	
	
	
}

function showPdfFile(where,fileCategory,fileName,userId,postId)
{
	var win = window.open("showPdfFile?fileCategory="+fileCategory+"&fileName="+fileName+"&userId="+userId+"&postId="+postId+"", '_blank');
	win.focus();		
}


//////////////////////////////  END OF USER POST SCRIPT  //////////////////////////////////////////////



//////// USER SETTING SCRIPT  ////////////////////////////////////////////////////////////////////

var alfanumeric = /^[a-zA-Z0-9]*$/;        	
var nameWithOutSpace = /^[a-zA-Z]+$/;
var nameWithSpace = /^[a-zA-Z\s]*$/;	
var phNo = /^[0-9]{10}$/;	
var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
var poi= /^[a-zA-Z0-9]{6,20}$/;


function updateUserRegister()
{	
    var firstName = $("#first_name").val();
    var lastName = $("#last_name").val();
    var emailId = $("#email_id").val();
    var mobileNo = $("#mobile_no").val();
    var profession = $("#profession").val();
    
    var flag = 'true';
    
    if(firstName=="")
	{		
		flag = 'false';
		$("#fNameErrId").show();
        $("#fNameErrId").html("Please enter your first name.");	               
	}
    
    if(firstName!="")
	{
		if(!firstName.match(nameWithSpace))  
        {  		
			flag = 'false';
			$("#fNameErrId").show();
	        $("#fNameErrId").html("Only alphabets are allowed.");		           
        }
		else
		{
			$("#fNameErrId").hide();
		}
	}
    
    if(lastName=="")
	{		
		flag = 'false';
		$("#lNameErrId").show();
        $("#lNameErrId").html("Please enter your last name.");	               
	}
    
    if(lastName!="")
	{
		if(!lastName.match(nameWithSpace))  
        {  		
			flag = 'false';
			$("#lNameErrId").show();
	        $("#lNameErrId").html("Only alphabets are allowed.");		           
        }
		else
		{
			$("#lNameErrId").hide();
		}
	}
   
    if(emailId=="")
	{		
		flag = 'false';
		$("#emailErrId").show();
        $("#emailErrId").html("Please enter your email id.");                  
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			flag = 'false';
			$("#emailErrId").show();
	        $("#emailErrId").html("Please enter a valid email id.");		           
        }
		else
		{
			$("#emailErrId").hide();
		}
	}
       
    if(mobileNo=="")
	{		
		flag = 'false';
		$("#mobileErrId").show();
        $("#mobileErrId").html("Please enter your mobile no.");	
    }
    
    if(mobileNo!="")
	{
		if(!mobileNo.match(phNo))  
        {  			
			flag='false';
			$("#mobileErrId").show();
	        $("#mobileErrId").html("Please enter a valid mobile no.");
	    }
		else
		{
			$("#mobileErrId").hide();
		}
	}
    
    if(profession=="")
    {
    	flag = 'false';
    	$("#professionErrId").show();
    	$("#professionErrId").html("Please enter your profession.")
    }
    
    if(profession!="")
    {
    	$("#professionErrId").hide();
    }
    
    
    if(flag=='true')
    	{  	
	    	 $("#preloader").show();
	    	    	 
	    	 $.ajax({  
		 			
			     type : "post",   
			     url : "checkUserEmailIdOnUpdate", 
			     data :$('#userRegForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 
					 if(response=="Not Exist")
					 {			
						    
						     $.ajax({  
						 			
							     type : "post",   
							     url : "checkUserPhnoOnUpdate", 
							     data :$('#userRegForm').serialize(),	     	     	     
							     success : function(response) 
							     {  		
							    	 
									 if(response=="Not Exist")
										 {			
										 
										 $.ajax({  
									 			
										     type : "post",   
										     url : "updateUserRegister", 
										     data :$('#userRegForm').serialize(),	     	     	     
										     success : function(response) 
										     {  											    	 
										    	 $("#preloader").hide();
										    	 
										    	 if(response=="success")
										      		{																	    		 	
										    		   	alert("Your have been successfully updated the details.");
										    		   	window.location = "userSetting";
													}
										      	 else if(response=="failed")
										      		{
											      		alert("There is some problem arise,Please try again later.");	
											      		window.location = "userSetting";
										      		}
										      	 else
										      		{
										      		 	alert("Your session got expired please try to login !");
										      		 	window.location = "userSetting";
										      		}
										     },  
									     }); 
												    
									}
									else
									{		
										 $("#preloader").hide();
								    	 								    	 
										$("#mobileErrId").show();
								        $("#mobileErrId").html("Given mobile no is already register");
								    }
							     },  
						     }); 
						 }
					 else
						 {							 	
						 	$("#preloader").hide();
					    	 					    	 
							$("#emailErrId").show();
					        $("#emailErrId").html("Given emailId is already registered.");	
					     }
			     },  
		     }); 
	    	 
	    	 
    	}
    
}


function changeUserPassword()
{
	var curPassword = $("#curPassword").val();
	var newPassword = $("#newPassword").val();
	var cnfPassword = $("#cnfPassword").val();
	    
	var flag = 'true';
	    
    if(curPassword=="")
	{		
		flag = 'false';
		$("#curPwErrId").show();
        $("#curPwErrId").html("Please enter your current password.");	               
	}
    
    if(curPassword!="")
	{		
		$("#curPwErrId").hide();
    }
    
    if(newPassword=="")
	{		
		flag = 'false';
		$("#newPwErrId").show();
        $("#newPwErrId").html("Please enter your new password.");	               
	}
    
    if(newPassword!="")
	{		
		$("#newPwErrId").hide();
    }
    
    if(cnfPassword=="")
	{		
		flag = 'false';
		$("#cnfPwErrId").show();
        $("#cnfPwErrId").html("Please confirm your new password.");	               
	}
    
    if(cnfPassword!="")
	{		
    	$("#cnfPwErrId").hide();
    }
    
    if(cnfPassword!=newPassword)
	{
    	flag = 'false';
		$("#cnfPwErrId").show();
        $("#cnfPwErrId").html("New password is not matched.");
	}
    
    if(cnfPassword==newPassword)
	{
    	$("#cnfPwErrId").hide();
    }
    
    
    if(flag=='true')
    {
    	$("#preloader").show();
    	 
    	$.ajax({  
 			
		     type : "post",   
		     url : "changeUserPassword", 
		     data :$('#passwordForm').serialize(),	     	     	     
		     success : function(response) 
		     {  											    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="success")
		      		{																	    		 	
		    		   	alert("Your password successfully changed.");
		    		   	window.location = "userSetting";
					}
		      	 else if(response=="failed")
		      		{
			      		alert("There is some problem arise,Please try again later.");	
			      		window.location = "userSetting";
		      		}
		      	 else if(response=="incorrect")
		      		{
		      		 	$("#curPwErrId").show();
		      		 	$("#curPwErrId").html("Your current password is not correct.");	
		      		}
		    	 else
		    		 {
		    		 	alert("Your session got expired please login !");	
			      		window.location = "userSetting";
		    		 }
		     },  
	     }); 
    }
}

function showOtherInterest(value)
{
	if(value=="Other")
	{
		if(document.getElementById("interestBox").style.display=="none")
		{
			$("#interestBox").val("");
			$("#interestBox").slideDown();
		}
		else
		{
			$("#interestBox").val("");
			$("#interestBox").slideUp();
		}
	}	
}

function calculateUserAge(dob)
{
	$("#preloader").show();
	 
	$.ajax({  
			
	     type : "post",   
	     url : "calculateUserAge", 
	     data :"dob="+dob,	     	     	     
	     success : function(response) 
	     {  											    	 
	    	 $("#preloader").hide();
	    	 
	    	 $("#age").val(response);
	    	 
	    	 return true;
	     },  
     }); 
}


function saveProfileDetails(action)
{	
	var flag = 'true';
	var areaOfInterest = "";
	
	var dob = $("#dob").val();
	var gender = $("#gender").val();
	var underGraduation = $("#under_graduation").val();
	var postGraduation = $("#post_graduation").val();
	var fellowship = $("#fellowship").val();
	var addnQual = $("#additional_qualification").val();
	var practiceAt = $("#practice_at").val();
	var practiceAs = $("#practice_as").val();
	var yoe = $("#year_of_experience").val();
	var phone = $("#phone_number").val();
	var website = $("#website_url").val();
	var pincode = $("#pincode").val();
	var photo = $("#file").val();
	var otherInterest = "";
	
	if(dob=="")
	{
		flag = 'false';	
	    $("#dobErrId").show();
	    $("#dobErrId").html("enter your date of birth.");
	}
	
	if(dob!="")
	{
		$("#dobErrId").hide();
	}
	
	if(gender=="Select gender")
	{
		flag = 'false';	
	    $("#genderErrId").show();
	    $("#genderErrId").html("Choose your gender.");
	}
	
	if(gender!="Select gender")
	{
		$("#genderErrId").hide();
	}
	
	if(underGraduation=="")
	{
		flag = 'false';	
	    $("#graduatenErrId").show();
	    $("#graduatenErrId").html("Mention under graduation.");
	}
	
	if(underGraduation!="")
	{
		$("#graduatenErrId").hide();
	}
	
	
	if(yoe=="")
	{
		flag = 'false';	
	    $("#yoeErrId").show();
	    $("#yoeErrId").html("year of experience.");
	}
	
	if(yoe!="")
	{
		$("#yoeErrId").hide();
	}
	
	if(pincode=="")
	{
		flag = 'false';	
	    $("#pincodeErrId").show();
	    $("#pincodeErrId").html("mention pincode.");
	}
	
	if(pincode!="")
	{
		$("#pincodeErrId").hide();
	}
	
	if(photo!="")
	{		  
	   var fileDetails = photo;
	   var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);  
	   var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG");	
	   
	   var condition = "NotGranted";
	    
	    for(var m=0;m<extension.length;m++)
		{
		    if(ext==extension[m])
	    	{				    	    
	    	   condition="Granted";				    	    
	    	}				    
		}
	    
		if(condition=="NotGranted")
		{
		    flag = 'false';	
		    $("#fileErrId").show();
		    $("#fileErrId").html("Only image files are allowed.");
			return false;				  
		}
		
		 var fileDetails1 = document.getElementById("file");
		 var fileSize = fileDetails1.files[0];
		 var fileSizeinBytes = fileSize.size;
		 var sizeinKB = +fileSizeinBytes / 1024;
  		 var sizeinMB = +sizeinKB / 1024;
  		 
  		 if(sizeinMB>10)
		 {
			flag = 'false';
			$("#fileErrId").show();
			$("#fileErrId").html("choose a file less then 10 MB.");
			return false;	 		   		 
		 }  
  		 
	}
	
	
	var checkboxes = document.getElementsByName('area_of_interest');
	var selected = [];
	
	for (var i=0; i<checkboxes.length; i++) {
	   
		if (checkboxes[i].checked) {
	        
	    	if(checkboxes[i].value=="Other")
	    	{
	    		otherInterest = $("#interest").val();
	    		areaOfInterest += checkboxes[i].value + "/";
	    	}
	    	else
	    	{
	    		areaOfInterest += checkboxes[i].value + "/";
	    	}
	    	
	    }
	}
	
	
	
	if(flag=='true')
	{		
		$("#preloader").show();
			
		var formData = new FormData($("#profileForm")[0]);
		
		formData.append("areaOfInterest",areaOfInterest);
		formData.append("otherInterest",otherInterest);
		
		if(action=="Save")
		{
			$.ajax({
				type : "POST",
				data : formData,
				url : "saveUserProfileData",		
				success : function(response) {
		          
					$("#preloader").hide();
					
					if(response=="success")
		      		{																	    		 			    		  
						alert("Your profile details has successfully saved.");
						
						window.location = "userSetting";
					}
		      	    else if(response=="failed")
		      		{
			      		alert("There is some problem arise,Please try again later.");						      		
		      		}
		      	    else
		      	    {
		      	    	alert("Your Session got expired please login first.");	
		      	    	window.location = "userSetting";
		      	    }
					
				},
				cache: false,
		        contentType: false,
		        processData: false,
		    });	
		}
		else
		{			
			$.ajax({
				type : "POST",
				data : formData,
				url : "updateUserProfileData",		
				success : function(response) {
		           
					$("#preloader").hide();
					
					if(response=="success")
		      		{																	    		 			    		  
						alert("Your profile details has successfully saved.");
						
						window.location = "userSetting";
					}
		      	    else if(response=="failed")
		      		{
			      		alert("There is some problem arise,Please try again later.");						      		
		      		}
		      	    else
		      	    {
		      	    	alert("Your Session got expired please login first.");	
		      	    	window.location = "userSetting";
		      	    }
					
				},
				cache: false,
		        contentType: false,
		        processData: false,
		   });	
		}
				   	
	}	
}





////////////////////////////////////////////   END OF USER SETTING SCRIPT  //////////////////////////////////////////////////////



function showParticipateForm(divId,eventId)
{
	$("#preloader").show();
	
	$.ajax({
		type : "POST",
		data : "eventId="+eventId,
		url : "showParticipateForm",								
		success : function(response) {
           
			$("#preloader").hide();
			
			if(response=="failed")
			{
				alert("Your session got expired please login !");	
	      		window.location = "userEvent";
			}
			else
			{
				$("#"+divId).html(response);
				
				$("#"+divId).slideDown();
				
				$("#"+divId)[0].scrollIntoView(true);
				
				var d = new Date().getTime();
				document.getElementById("tid").value = d;
			}
								
		},			
	});	
}


function showAppliedForm(divId,eventId)
{
	$("#preloader").show();
	
	$.ajax({
		type : "POST",
		data : "eventId="+eventId,
		url : "showAppliedForm",								
		success : function(response) {
           
			$("#preloader").hide();
			
			if(response=="failed")
			{
				alert("Your session got expired please login !");	
	      		window.location = "userEvent";
			}
			else
			{
				$("#"+divId).html(response);
				
				$("#"+divId).slideDown();
				
				$("#"+divId)[0].scrollIntoView(true);	
			}
								
		},			
	});	
}


function closeParticipateForm(divId)
{
	$("#"+divId).slideUp();	
	$("#"+divId).html("");
}

function saveApplication(divId,formId,eventType)
{	
	var flag = 'true';
	
	var name= $("#fName").val();
	var address= $("#applicant_address").val();
	var city= $("#applicant_city").val();
	var state= $("#applicant_state").val();
	var pincode= $("#applicant_pincode").val();
	var email= $("#applicant_email").val();
	var mobile= $("#applicant_mobile").val();
	var curLocation= $("#applicant_affiliation").val();
	var councilNo= $("#applicant_council_no").val();
	
	if(name=="")
	{
		flag = 'false';
		$("#fNameErrId").html("Please enter first name");
		$("#fNameErrId").show();
	}
	if(name!="")
	{
		$("#fNameErrId").hide();
	}
	
	if(address=="")
	{
		flag = 'false';
		$("#addressErrId").html("Please enter your address");
		$("#addressErrId").show();
	}
	if(address!="")
	{
		$("#addressErrId").hide();
	}
	
	if(city=="")
	{
		flag = 'false';
		$("#cityErrId").html("Please enter city name");
		$("#cityErrId").show();
	}
	if(city!="")
	{
		$("#cityErrId").hide();
	}
	
	if(state=="")
	{
		flag = 'false';
		$("#stateErrId").html("Please enter state name");
		$("#stateErrId").show();
	}
	if(state!="")
	{
		$("#stateErrId").hide();
	}
	
	if(pincode=="")
	{
		flag = 'false';
		$("#pincodeErrId").html("Please enter area pincode");
		$("#pincodeErrId").show();
	}
	if(pincode!="")
	{
		$("#pincodeErrId").hide();
	}
	
	if(email=="")
	{
		flag = 'false';
		$("#emailErrId").html("Please enter your email id");
		$("#emailErrId").show();
	}
	if(email!="")
	{
		$("#emailErrId").hide();
	}
	
	if(mobile=="")
	{
		flag = 'false';
		$("#mobileErrId").html("Please enter mobile number");
		$("#mobileErrId").show();
	}
	if(mobile!="")
	{
		$("#mobileErrId").hide();
	}
	
	if(curLocation=="")
	{
		flag = 'false';
		$("#affErrId").html("Please enter affiliation / current practice Location");
		$("#affErrId").show();
	}
	if(curLocation!="")
	{
		$("#affErrId").hide();
	}
	
	if(councilNo=="")
	{
		flag = 'false';
		$("#councilErrId").html("Please enter council no");
		$("#councilErrId").show();
	}
	if(councilNo!="")
	{
		$("#councilErrId").hide();
	}
	
	if(flag=='true')
	{
		$("#preloader").show();
		
		if(eventType=="Before Reg")
		{			
			
			$.ajax({  
				 type : "post",   
			     url : "indexSaveApplicaitionPassToPayment", 
			     data :$('#'+formId).serialize(),	   		     	     	     
			     success : function(response) 
			     {  											    	 
			    	 $("#preloader").hide();
			    	 
			    	 if(response=="success")
			      	 {																	    		 		    		   
			    		window.location = "indexPaymentOverview";
			    	 }
			    	 else if(response=="No gateway")
			    	 {
			    		 alert("Your application successfully submitted,We will update you the details.");
			    		 
			    		 window.location = "partcipateOnEvent";
			    	 }
			      	 else if(response=="failed")
			      	 {
			      		alert("There is some problem arise,Please try again later.");	
			      		window.location = "partcipateOnEvent";
			      	 }
			      	 else
			      	 {
		      		 	alert("Your session got expired please try to login !");
		      		 	window.location = "partcipateOnEvent";
			      	 }
			     },
			});	
		}
		else if(eventType=="Pre Conference Workshop")
		{			
			
			$.ajax({  
				 type : "post",   
			     url : "indexSaveConfWorkShopPassToPayment", 
			     data :$('#'+formId).serialize(),	   		     	     	     
			     success : function(response) 
			     {  											    	 
			    	 $("#preloader").hide();
			    	 
			    	 if(response=="success")
			      	 {																	    		 		    		   
			    		window.location = "indexPreConferenceOverview";
			    	 }
			    	 else if(response=="No gateway")
			    	 {
			    		 alert("Your application successfully submitted,We will update you the details.");
			    		 
			    		 window.location = "partcipateOnEvent";
			    	 }
			      	 else if(response=="failed")
			      	 {
			      		alert("There is some problem arise,Please try again later.");	
			      		window.location = "partcipateOnEvent";
			      	 }
			      	 else
			      	 {
		      		 	alert("Your session got expired please try to login !");
		      		 	window.location = "partcipateOnEvent";
			      	 }
			     },
			});	
		}
		else
		{
			$.ajax({  
				
			     type : "post",   
			     url : "saveApplicaitionPassToPayment", 
			     data :$('#'+formId).serialize(),	     	     	     
			     success : function(response) 
			     {  											    	 
			    	 $("#preloader").hide();
			    	 
			    	 if(response=="success")
			      	 {																	    		 		    		   
			    		window.location = "saveApplicaitionPassToPayment";
			    	 }
			    	 else if(response=="No gateway")
			    	 {
			    		 alert("Your application successfully submitted,We will update you the details.");
			    		 
			    		 window.location = "userEvent";
			    	 }
			      	 else if(response=="failed")
			      	 {
			      		alert("There is some problem arise,Please try again later.");	
			      		window.location = "userEvent";
			      	 }
			      	 else
			      	 {
		      		 	alert("Your session got expired please try to login !");
		      		 	window.location = "userEvent";
			      	 }
			     },  
		    });
		}
		 
	}	
}

function changeRegCate(regCategory)
{
	$("#reg_category").val(regCategory);
	
	if(regCategory=="Double Occupancy + Registration")
	{
		$("#room_partner_name").val("");
		$("#room_partner_mobile").val("");
		$("#romSharingInfo").slideDown();
		$("#2ndPartnerInfo").slideUp();
	}
	else if(regCategory=="Triple Occupancy + Registration")
	{
		$("#room_partner_name").val("");
		$("#room_partner_mobile").val("");
		
		$("#room_partner_name_2").val("");
		$("#room_partner_mobile_2").val("");
		
		$("#romSharingInfo").slideDown();
		$("#2ndPartnerInfo").slideDown();
	}
	else
	{
		$("#romSharingInfo").slideUp();
	}
}

function saveAbstractSubmission()
{
	var flag  = 'true';
	
	var title= $("#abstract_title").val();
	var fName= $("#first_name").val();
	var lName= $("#last_name").val();
	var email= $("#email").val();
	var mobile= $("#mobile").val();
	
	if(title=="")
	{
		flag = 'false';
		$("#titleErrId").html("Please provide abstraction title.");
		$("#titleErrId").show();
	}
	if(title!="")
	{
		$("#titleErrId").hide();
	}
	
	if(fName=="")
	{
		flag = 'false';
		$("#fNameErrId").html("Please enter your first name");
		$("#fNameErrId").show();
	}
	if(fName!="")
	{
		$("#fNameErrId").hide();
	}
	
	if(email=="")
	{
		flag = 'false';
		$("#emailErrId").html("Please enter your email id");
		$("#emailErrId").show();
	}
	if(email!="")
	{
		$("#emailErrId").hide();
	}
	
	if(mobile=="")
	{
		flag = 'false';
		$("#mobileErrId").html("Please enter mobile number");
		$("#mobileErrId").show();
	}
	if(mobile!="")
	{
		$("#mobileErrId").hide();
	}
	
	var fi = document.getElementById('file');
	
	if(fi.files.length==0)
	{
		flag = 'false';	
	    $("#fileErrId").show();
	    $("#fileErrId").html("Select a file to submit.");
	}
	
	if(fi.files.length>0)
	{	
		var extension = new Array("mp4","MP4","doc","DOC","docx","DOCX","ppt","PPT","PDF","pdf");	
	    
		var fsize = 0;
		
		var condition = "NotGranted";
		
		for (var f = 0; f <= fi.files.length - 1; f++) 
		{
			var fname = fi.files.item(f).name;
			var ext=fname.substring(fname.lastIndexOf('.')+1)
			
			for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
		    	{				    	    
		    	   condition="Granted";				    	    
		    	}			    
			}
			
			if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrId").show();
			    $("#fileErrId").html("Only mp4,ppt,doc and pdf files are allowed.");
				return false;				  
			}
			
			var fileSizeinBytes = fi.files.item(f).size;   
			var sizeinKB = +fileSizeinBytes / 1024;
	  		var sizeinMB = +sizeinKB / 1024;
	  		
	  		fsize = fsize + sizeinMB;		  		
		}
		
		if(fsize>40)
		{
			flag = 'false';
			$("#fileErrId").show();
			$("#fileErrId").html("choose a file less then 40 MB.");
			return false;	 		   		 
		}				
	}
	
	
	
	if(flag=='true')
	{
		$("#preloader").show();
		
			var formData = new FormData($("#eventAbstactDocForm")[0]);
			
				$.ajax({
					type : "POST",
					data : formData,
					url : "saveAbstractSubmission",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response=="success")
			      		{																	    		 			    		  
							alert("Your details have been successfully submitted.");			
				      		
				      		window.location = "http://www.retnetindia.com";
						}
			      	    else
			      		{
				      		alert("There is some problem arise,Please try again later.");			
				      		
				      		window.location = "http://www.retnetindia.com";
			      		}							
					},
					cache: false,
			        contentType: false,
			        processData: false,			        
			});				
	}
	

}




function searchUser(searchValue)
{
	if(searchValue.length>4)
	{
		$("#preloader").show();
		
		$.ajax({  
				
		     type : "post",   
		     url : "searchUserProfile", 
		     data :"searchValue="+searchValue,	     	     	     
		     success : function(response) 
		     {  											    	 
		    	 $("#preloader").hide();
		    	 
		    	 $("#userSearchListDiv").html(response);
		    	 $("#userSearchListDiv").show();
		    	 
		     },  
	    }); 
	}
	
}

function searchUserByClick()
{
	var searchValue = $("#searchValueId").val();
	
	if(searchValue.length>4)
	{
		$("#preloader").show();
		
		$.ajax({  
				
		     type : "post",   
		     url : "searchUserProfile", 
		     data :"searchValue="+searchValue,	     	     	     
		     success : function(response) 
		     {  											    	 
		    	 $("#preloader").hide();
		    	 
		    	 $("#userSearchListDiv").html(response);
		    	 $("#userSearchListDiv").show();
		    	 
		     },  
	    }); 
	}
}


///////////////////////////////////  FEED FILTER SCRIPT ************************************************////////////////////////

function showFilterFeedOption(selectValue)
{		
	$("#preloader").show();
	
	if(selectValue=="Find feeds with duration")
	{
		$.ajax({  
			
		     type : "post",   
		     url : "showFilterFeedOption", 
		     data :"selectValue="+selectValue,	     	     	     
		     success : function(response) 
		     {  											    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response!="No Record")
		    	 {
		    		 var listArray = response.split("/");
					 
			    	 $("#yearSelect").html("");
			    	 
			    	 $("#yearSelect").append("<option data-icon='icon-User' value='Select Year'>Select Year</option>");
		    		 
					 for(var i=0;i<listArray.length;i++)
					 {
						 $("#yearSelect").append("<option data-icon='icon-User' value='"+listArray[i]+"'>"+listArray[i]+"</option>");
			    	 }
					
			    	 $("#yearDiv").show();
		    	 }
		    	 else
		    	 {
		    		 alert("No feeds found as per the filter.")
		    	 }		    	 
		    	 
		     },  
	    }); 
	}
	else
	{
		$.ajax({  
			
		     type : "post",   
		     url : "showFilterFeed", 
		     data :"selectValue="+selectValue,	     	     	     
		     success : function(response) 
		     {  											    	 
		    	 $("#preloader").hide();
		    	 
		    	 $("#postViewDiv").html(response);
				 $("#postViewDiv").show();
				 
				 
				 // CAN BE REMOVED IF NEEDED
				 $("#yearDiv").hide();
				 $("#monthDiv").hide();
		     },  
	    }); 
		
	}
	
	
}


function showMonth(year)
{
	if(year=="Select Year")
	{
		$("#monthDiv").hide();
	}
	else
	{
		$.ajax({  
			
		     type : "post",   
		     url : "showMonth", 
		     data :"year="+year,	     	     	     
		     success : function(response) 
		     {  											    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response!="No Record")
		    	 {
		    		 var listArray = response.split("/");
					 
			    	 $("#monthSelect").html("");
			    	 
			    	 $("#monthSelect").append("<option data-icon='icon-User' value='Select Month'>Select Month</option>");
		    		 
					 for(var i=0;i<listArray.length;i++)
					 {
						 $("#monthSelect").append("<option data-icon='icon-User' value='"+listArray[i]+"'>"+listArray[i]+"</option>");
			    	 }
					
			    	 $("#monthDiv").show();
		    	 }
		    	 else
		    	 {
		    		 alert("No feeds found as per the filter.")
		    	 }		    	 
		    	 
		     },  
	    }); 
	}
}

function showFeedViaMonth(month)
{
	if(month!="Select Month")
	{
		var filterFeed = $("#filterFeedId").val();
		var year = $("#yearSelect").val();
		
		$.ajax({  
			
		     type : "post",   
		     url : "showFeedViaMonth", 
		     data : "filterFeed="+filterFeed+"&year="+year+"&month="+month,	     	     	     
		     success : function(response) 
		     {  											    	 
		    	 $("#preloader").hide();
		    	 
		    	 $("#postViewDiv").html(response);
				 $("#postViewDiv").show();
		     },  
	    }); 		
	}
	else
	{
		
	}
}

/////*************************************************** CASE MANAGEMENT SCRIPT ****************************************/////

function uploadCaseCommentFile()
{
	var flag = 'true';
	var fi = document.getElementById('files');
	
	if(fi.files.length>0)
	{	
		var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","mp4","MP4","webm","WEBM","ogg","OGG","PDF","pdf");	
	    
		var fsize = 0;
		
		var condition = "NotGranted";
		
		for (var f = 0; f <= fi.files.length - 1; f++) 
		{
			var fname = fi.files.item(f).name;
			var ext=fname.substring(fname.lastIndexOf('.')+1)
			
			for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
		    	{				    	    
		    	   condition="Granted";				    	    
		    	}			    
			}
			
			if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrId").show();
			    $("#fileErrId").html("Image , video(mp4,webm,ogg) and pdf files are allowed.");
				return false;				  
			}
			
			var fileSizeinBytes = fi.files.item(f).size;   
			var sizeinKB = +fileSizeinBytes / 1024;
	  		var sizeinMB = +sizeinKB / 1024;
	  		
	  		fsize = fsize + sizeinMB;		  		
		}
		
		if(fsize>40)
		{
			flag = 'false';
			$("#fileErrId").show();
			$("#fileErrId").html("choose a file less then 40 MB.");
			return false;	 		   		 
		}				
	}
	
	
	if(flag=='true')
	{
		$("#preloader").show();
		
			var formData = new FormData($("#formBean")[0]);
			
				$.ajax({
					type : "POST",
					data : formData,
					url : "captureBrowseFiles",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response!="")
			      		{																	    		 			    		  
							var fileArray = response.split("/");
							
							$("#selectedFileList").html("");
							
							for(var i=0;i<fileArray.length;i++)
							{
								$("#selectedFileList").append("<li><i title='Remove the file' style='color:#333;cursor:pointer;' class='fa fa-trash' id='"+fileArray[i]+"' onclick='deleteBrowseFile(this.id)'></i> &nbsp;"+fileArray[i]+"</li>");
								
								$("#fileListDiv").show();
							}
							
							$("#file").val("");
							
						}
			      	    else
			      		{
				      		alert("There is some problem arise,Please try again later.");			
				      		
				      		window.location = "postFeed";
			      		}							
					},
					cache: false,
			        contentType: false,
			        processData: false,
			        
			});				
	}
	


}


function deleteBrowseFile(fileName)
{
	$("#preloader").show();
	
	$.ajax({
		type : "POST",
		data : "fileName="+fileName,
		url : "deleteBrowseFile",								
		success : function(response) {
           
			$("#preloader").hide();
			
			if(response!="")
      		{																	    		 			    		  
				var fileArray = response.split("/");
				
				$("#selectedFileList").html("");
				
				for(var i=0;i<fileArray.length;i++)
				{
					$("#selectedFileList").append("<li><i title='Remove the file' style='color:#333;cursor:pointer;' class='fa fa-trash' id='"+fileArray[i]+"' onclick='deleteBrowseFile(this.id)'></i> &nbsp;"+fileArray[i]+"</li>");
				}
				
				$("#fileListDiv").show();
			}
			else if(response=="")
      		{
				$("#fileListDiv").hide();
				$("#selectedFileList").html("");
      		}
      	    else
      		{
	      		alert("There is some problem arise,Please try again later.");			
	      		window.location = "postFeed";
      		}
			
		},			
	});	
}

function submitCaseComment()
{
	var flag = 'true';
	
	var caseId = $("#contentString2").val();
	
	var commentMessage = $("#contentString1").val();
	
	if(commentMessage=="")
	{
		flag = 'false';
		alert("Please comment a message before submit.");
	}
	
	if(flag=='true')
	{
		$("#preloader").show();
		
		var formData = new FormData($("#formBean")[0]);
		
			$.ajax({
				type : "POST",
				data : formData,
				url : "submitCaseComment",		
				success : function(response) {
		           
					$("#preloader").hide();
					
					if(response=="success")
		      		{		
						$("#contentString1").val("");
						$("#files").val("");
						$("#selectedFileList").html("");
						
						getUserCaseComment(caseId);
					}
		      	    else
		      		{
			      		alert("There is some problem arise,Please try again later.");			
			      	}							
				},
				cache: false,
		        contentType: false,
		        processData: false,
		        
		});	
	}
}

function getUserCaseComment(caseId)
{	
	$("#preloader").show();
	
		$.ajax({
			type : "POST",
			data : "caseId="+caseId,
			url : "getUserCaseComment",		
			success : function(response) {
	           
				$("#preloader").hide();
				
				$("#userCaseCommentDiv").html(response);
				
			},
	});
}

function showCaseFile(action,fileName,fileType,caseId)
{
	if(action=="Open")
	{
		$("#preloader").show();
		
		$.ajax({
			type : "POST",
			data : "fileType="+fileType+"&fileName="+fileName+"&caseId="+caseId,
			url : "showCaseFile",		
			success : function(response) {
	           
				$("#preloader").hide();
				
				$("#commentPopUp").html(response);
    		    $("#commentPopUp").show();
    		    
    		    /*$("#gallerypopup").html(response);
    		    $("#gallerypopup").show();*/
			},
	   });
	}
	else
	{
		$("#commentPopUp").html("");
	    $("#commentPopUp").hide();
	    
	    /*$("#gallerypopup").html("");
	    $("#gallerypopup").hide();*/
	}
}

function showCasePdfFile(CaseFile,fileName,caseId)
{
	var win = window.open("showPdfFile?fileCategory="+CaseFile+"&fileName="+fileName+"&caseId="+caseId+"", '_blank');
	win.focus();	
}


function showCaseCommentFile(action,fileCategory,fileType,fileName,caseId,userId)
{
	if(action=="Open")
	{
		$("#preloader").show();
		
		$.ajax({
			type : "POST",
			data : "fileCategory="+fileCategory+"&fileType="+fileType+"&fileName="+fileName+"&caseId="+caseId+"&userId="+userId,
			url : "showCaseCommentFile",		
			success : function(response) {
	           
				$("#preloader").hide();
				
				$("#commentPopUp").html(response);
    		    $("#commentPopUp").show();
    		    
    		    /*$("#gallerypopup").html(response);
    		    $("#gallerypopup").show();*/
			},
	   });
	}
	else
	{
		$("#commentPopUp").html("");
	    $("#commentPopUp").hide();
	    
	    /*$("#gallerypopup").html("");
	    $("#gallerypopup").hide();*/
	}
}

function showCaseComntPdfFile(fileCategory,fileName,caseId,userId)
{
	var win = window.open("showPdfFile?fileCategory="+fileCategory+"&fileName="+fileName+"&caseId="+caseId+"&userId="+userId+"", '_blank');
	win.focus();		
}
