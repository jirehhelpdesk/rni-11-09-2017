

function loadAlertBox(action)
{
	if(action=='Open')
	{
		// UPDATE AFTER WEBSITE LAUNCH
		
		//$('#demo').delay(1200).fadeIn();
		//$('#commentFade').delay(1200).fadeIn();
		
		// BEFORE WEBSITE LAUNCH
		
		$('#demo').fadeIn();
		$('#commentFade').fadeIn();
	}
	else
	{
		$("#demo").slideUp();
		$("#commentFade").slideUp();
	}	
}

function activeMenu(menuType,userType)
{ 			
	if(menuType=='menu1')
		{
			 if(userType=="Admin")
			 {
				 $("#menu1").attr('class', 'active');
			 }
			 else if(userType=="User")
			 {
				 $("#uMenu1").attr('class', 'userMenuActive');
			 }
			 else
			 {
				 $("#menu1").attr('class', 'active');
			 }		     
		}
	else if(menuType=='menu2')
		{
			 if(userType=="Admin")
			 {
				 $("#menu2").attr('class', 'active');
			 }
			 else if(userType=="User")
			 {
				 $("#uMenu2").attr('class', 'userMenuActive');
			 }
			 else
			 {
				 $("#menu2").attr('class', 'active');
			 }				
		}
	else if(menuType=='menu3')
		{		
			 if(userType=="Admin")
			 {
				 $("#menu3").attr('class', 'active');
			 }
			 else if(userType=="User")
			 {
				 $("#uMenu3").attr('class', 'userMenuActive');
			 }
			 else
			 {
				 $("#menu3").attr('class', 'active');
			 }	 
		}
	else if(menuType=='menu4')
		{		  
			 if(userType=="Admin")
			 {
				 $("#menu4").attr('class', 'active');
			 }
			 else if(userType=="User")
			 {
				 $("#uMenu4").attr('class', 'userMenuActive');
			 }
			 else
			 {
				 $("#menu4").attr('class', 'active');
			 }		  
		}
	else if(menuType=='menu5')
		{		  		 	 
			 if(userType=="Admin")
			 {
				 $("#menu5").attr('class', 'active');
			 }
			 else if(userType=="User")
			 {
				 $("#uMenu5").attr('class', 'userMenuActive');
			 }
			 else
			 {
				 $("#menu5").attr('class', 'active');
			 }			  
		}
	else if(menuType=='menu6')
		{						 
			 if(userType=="Admin")
			 {
				 $("#menu6").attr('class', 'active');
			 }
			 else if(userType=="User")
			 {
				 $("#uMenu6").attr('class', 'userMenuActive');
			 }
			 else
			 {
				 $("#menu6").attr('class', 'active');
			 }			 
		}
	else if(menuType=='menu7')
		{						 
			 if(userType=="Admin")
			 {
				 $("#menu7").attr('class', 'active');
			 }
			 else if(userType=="User")
			 {
				 $("#uMenu7").attr('class', 'userMenuActive');
			 }
			 else
			 {
				 $("#menu7").attr('class', 'active');
			 }		 
		}
	else if(menuType=='menu8')
		{						 
			 if(userType=="Admin")
			 {
				 $("#menu8").attr('class', 'active');
			 }
			 else if(userType=="User")
			 {
				 $("#uMenu8").attr('class', 'userMenuActive');
			 }
			 else
			 {
				 $("#menu8").attr('class', 'active');
			 }			 
		}
	else if(menuType=='menu9')
		{
			 if(userType=="Admin")
			 {
				 $("#menu9").attr('class', 'active');
			 }
			 else if(userType=="User")
			 {
				 $("#uMenu9").attr('class', 'userMenuActive');
			 }
			 else
			 {
				 $("#menu9").attr('class', 'active');
			 }		
		}	
	else if(menuType=='menu10')
		{
			 if(userType=="Admin")
			 {
				 $("#menu10").attr('class', 'active');
			 }
			 else if(userType=="User")
			 {
				 $("#uMenu10").attr('class', 'userMenuActive');
			 }
			 else
			 {
				 $("#menu10").attr('class', 'active');
			 }		
		}
	else
		{
			if(userType=="Admin")
			 {
				 $("#menu11").attr('class', 'active');
			 }
			 else if(userType=="User")
			 {
				 $("#uMenu11").attr('class', 'userMenuActive');
			 }
			 else
			 {
				 $("#menu11").attr('class', 'active');
			 }	
		}
}


////// LOGIN SCRIPT  ////////////////////////////////////////////


function rememberMe(id)
{
	var remember = document.getElementById(id);
	
	if(remember.checked==true)
	{
		$("#CheckRemember").val("Yes");
	}
	else
	{
		$("#CheckRemember").val("No");
	}
}


/////////  END OF LOGIN SCRIPT ///////////////////////////////////////


//////// USER REGISTRATION SCRIPT  ////////////////////////////////////////////////////////////////////

var alfanumeric = /^[a-zA-Z0-9]*$/;        	
var nameWithOutSpace = /^[a-zA-Z]+$/;
var nameWithSpace = /^[a-zA-Z\s]*$/;	
var phNo = /^[0-9]{10}$/;	
var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
var poi= /^[a-zA-Z0-9]{6,20}$/;


function showTermsBox()
{
	var agree = document.getElementById("terms");
	
	agree.checked=false;
	
	$("#myModal").show();
	$("#modelContent").show();
}

function actionOnTerms(condition)
{
	var agree = document.getElementById("terms");
	
	if(condition=="Accept")
	{
		agree.checked=true;
		
		$("#myModal").hide();
		$("#modelContent").hide();
	}
	else
	{
		agree.checked=false;
		
		$("#myModal").hide();
		$("#modelContent").hide();
	}
}


function saveUserRegister()
{	
    var firstName = $("#first_name").val();
    var lastName = $("#last_name").val();
    var emailId = $("#email_id").val();
    var mobileNo = $("#mobile_no").val();
    var password = $("#password").val();
    var confirmPassword = $("#cnfPassword").val();
    var profession = $("#profession").val();
    
    // PROFILE DETAILS
    
    var areaOfInterest = "";
    
    var dob = $("#dob").val();
	var gender = $("#gender").val();
	var underGraduation = $("#under_graduation").val();
	var postGraduation = $("#post_graduation").val();
	var fellowship = $("#fellowship").val();
	var addnQual = $("#additional_qualification").val();
	var practiceAt = $("#practice_at").val();
	var practiceAs = $("#practice_as").val();
	var yoe = $("#year_of_experience").val();
	var phone = $("#phone_number").val();
	var website = $("#website_url").val();
	var pincode = $("#pincode").val();
	var photo = $("#file").val();
	var otherInterest = "";
	
    
    
    
    //var agree = document.getElementById("terms");
  
    var flag = 'true';
    
    if(firstName=="")
	{		
		flag = 'false';
		$("#fNameErrId").show();
        $("#fNameErrId").html("Please enter your first name.");	               
	}
    
    if(firstName!="")
	{
		if(!firstName.match(nameWithSpace))  
        {  		
			flag = 'false';
			$("#fNameErrId").show();
	        $("#fNameErrId").html("Only alphabets are allowed.");		           
        }
		else
		{
			$("#fNameErrId").hide();
		}
	}
    
    if(lastName=="")
	{		
		flag = 'false';
		$("#lNameErrId").show();
        $("#lNameErrId").html("Please enter your last name.");	               
	}
    
    if(lastName!="")
	{
		if(!lastName.match(nameWithSpace))  
        {  		
			flag = 'false';
			$("#lNameErrId").show();
	        $("#lNameErrId").html("Only alphabets are allowed.");		           
        }
		else
		{
			$("#lNameErrId").hide();
		}
	}
   
    if(emailId=="")
	{		
		flag = 'false';
		$("#emailErrId").show();
        $("#emailErrId").html("Please enter your email id.");                  
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			flag = 'false';
			$("#emailErrId").show();
	        $("#emailErrId").html("Please enter a valid email id.");		           
        }
		else
		{
			$("#emailErrId").hide();
		}
	}
       
    if(mobileNo=="")
	{		
		flag = 'false';
		$("#mobileErrId").show();
        $("#mobileErrId").html("Please enter your mobile no.");	
    }
    
    if(mobileNo!="")
	{
		if(!mobileNo.match(phNo))  
        {  			
			flag='false';
			$("#mobileErrId").show();
	        $("#mobileErrId").html("Please enter a valid mobile no.");
	    }
		else
		{
			$("#mobileErrId").hide();
		}
	}
    
   
    if(password=="")
	{
    	flag='false';
		$("#pwErrId").show();
        $("#pwErrId").html("Please set a password for the account.");
    }
    
    if(password!="")
	{
		$("#pwErrId").hide();
	}
   
    if(confirmPassword=="")
	{
    	flag='false';
		$("#cnfPwdErrId").show();
        $("#cnfPwdErrId").html("Please confirm your password.");
    }
    
    if(confirmPassword!="")
	{
		$("#cnfPwdErrId").hide();	 
	}
   
    if(password!=confirmPassword)
	{
    	flag='false';
		$("#cnfPwdErrId").show();
        $("#cnfPwdErrId").html("Confirm password not matched with password.");
    }
    
    if(profession=="")
    {
    	flag = 'false';
    	$("#professionErrId").show();
    	$("#professionErrId").html("Please enter your profession.")
    }
    
    if(profession!="")
    {
    	$("#professionErrId").hide();
    }
    
    
    
    
    
    
    
    
    if(dob=="")
	{
		flag = 'false';	
	    $("#dobErrId").show();
	    $("#dobErrId").html("enter your date of birth.");
	}
	
	if(dob!="")
	{
		$("#dobErrId").hide();
	}
	
	if(gender=="Select gender")
	{
		flag = 'false';	
	    $("#genderErrId").show();
	    $("#genderErrId").html("Choose your gender.");
	}
	
	if(gender!="Select gender")
	{
		$("#genderErrId").hide();
	}
	
	if(underGraduation=="")
	{
		flag = 'false';	
	    $("#graduatenErrId").show();
	    $("#graduatenErrId").html("Mention under graduation.");
	}
	
	if(underGraduation!="")
	{
		$("#graduatenErrId").hide();
	}
	
	
	if(yoe=="")
	{
		flag = 'false';	
	    $("#yoeErrId").show();
	    $("#yoeErrId").html("year of experience.");
	}
	
	if(yoe!="")
	{
		$("#yoeErrId").hide();
	}
	
	if(pincode=="")
	{
		flag = 'false';	
	    $("#pincodeErrId").show();
	    $("#pincodeErrId").html("mention pincode.");
	}
	
	if(pincode!="")
	{
		$("#pincodeErrId").hide();
	}
	
	if(photo!="")
	{		  
	   var fileDetails = photo;
	   var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);  
	   var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG");	
	   
	   var condition = "NotGranted";
	    
	    for(var m=0;m<extension.length;m++)
		{
		    if(ext==extension[m])
	    	{				    	    
	    	   condition="Granted";				    	    
	    	}				    
		}
	    
		if(condition=="NotGranted")
		{
		    flag = 'false';	
		    $("#fileErrId").show();
		    $("#fileErrId").html("Only image files are allowed.");
			return false;				  
		}
		
		 var fileDetails1 = document.getElementById("file");
		 var fileSize = fileDetails1.files[0];
		 var fileSizeinBytes = fileSize.size;
		 var sizeinKB = +fileSizeinBytes / 1024;
  		 var sizeinMB = +sizeinKB / 1024;
  		 
  		 if(sizeinMB>10)
		 {
			flag = 'false';
			$("#fileErrId").show();
			$("#fileErrId").html("choose a file less then 10 MB.");
			return false;	 		   		 
		 }  
  		 
	}
	
	
	var checkboxes = document.getElementsByName('area_of_interest');
	var selected = [];
	
	for (var i=0; i<checkboxes.length; i++) {
	   
		if (checkboxes[i].checked) {
	        
	    	if(checkboxes[i].value=="Other")
	    	{
	    		otherInterest = $("#interest").val();
	    		areaOfInterest += checkboxes[i].value + "/";
	    	}
	    	else
	    	{
	    		areaOfInterest += checkboxes[i].value + "/";
	    	}
	    	
	    }
	}
	
    /*
    if(agree.checked==false)
    {
    	flag='false';
		$("#termErrId").show();
        $("#termErrId").html("Are you agree with the terms and conditions.");
       // return false;
    } 
    
    if(agree.checked==true)
    {
		$("#termErrId").hide();		
    }  */
    
    if(flag=='true')
    	{  	
	    	 $("#preloader").show();
	    	  
	    	 var formData = new FormData($("#userRegForm")[0]);
	 		
	 		 formData.append("areaOfInterest",areaOfInterest);
	 		 formData.append("otherInterest",otherInterest);
	 		
	    	 $.ajax({  
		 			
			     type : "post",   
			     url : "checkUserEmailId", 
			     data : formData,	     	     
			     success : function(response) 
			     {  					    	 
					 if(response=="Not Exist")
					 {									    
						     $.ajax({  
						 			
							     type : "post",   
							     url : "checkUserPhno", 
							     data : formData,	     	     	     	     
							     success : function(response) 
							     {  									    	 
									 if(response=="Not Exist")
									 {													 
										 $.ajax({  
									 			
										     type : "post",   
										     url : "saveUserRegister", 
										     data : formData,	     		     	     
										     success : function(response) 
										     {  											    	 
										    	 $("#preloader").hide();
										    	 
										    	 if(response=="success")
									      		 {																	    		 	
									    		   	alert("Your have been successfully registered,Please check the mail and activate your account.");
									    		   	
									    		   	$("#first_name").val("");
									    		    $("#last_name").val("");
									    		    $("#email_id").val("");
									    		    $("#mobile_no").val("");
									    		    $("#password").val("");
									    		    $("#cnfPassword").val("");
									    		    $("#profession").val("");
									    		    
									    		    // PROFILE DETAILS
									    		    
									    		    $("#dob").val("");
									    			$("#gender").val("");
									    			$("#under_graduation").val("");
									    			$("#post_graduation").val("");
									    			$("#fellowship").val("");
									    			$("#additional_qualification").val("");
									    			$("#practice_at").val("");
									    			$("#practice_as").val("");
									    			$("#year_of_experience").val("");
									    			$("#phone_number").val("");
									    			$("#website_url").val("");
									    			$("#pincode").val("");
									    			$("#file").val("");
									    			
									    		   	window.location = "userRegister";
												 }
										      	 else
									      		 {
										      		alert("There is some problem arise,Please try again later.");	
										      		window.location = "userRegister";
									      		 }											    	 
										     },
										     cache: false,
										     contentType: false,
										     processData: false,
									     }); 												    
									}
									else
									{		
										$("#preloader").hide();
								    	 								    	 
										$("#mobileErrId").show();
								        $("#mobileErrId").html("Given mobile no is already register");
								    }
							     }, 
							     cache: false,
							     contentType: false,
							     processData: false,
						     }); 
						 }
					 else
						 {							 	
						 	$("#preloader").hide();
					    	 					    	 
							$("#emailErrId").show();
					        $("#emailErrId").html("Given emailId is already registered.");	
					     }
			     },
			     cache: false,
			     contentType: false,
			     processData: false,
		     }); 
	    	 
	    	 
    	}
    
}


function subscribeNews()
{
	var flag = 'true';
	
	var emailId = $("#newsemailId").val();
	
	if(emailId=="")
	{		
		flag = 'false';
		$("#emailSubErrId").show();
        $("#emailSubErrId").html("Please enter your email id.");                  
	}
	    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			flag = 'false';
			$("#emailSubErrId").show();
	        $("#emailSubErrId").html("Please enter a valid email id.");		           
        }
		else
		{
			$("#emailSubErrId").hide();
		}
	}
    
    if(flag=='true')
    {
    	$("#preloader").show();
    	 
    	$.ajax({  
 			
		     type :"post",   
		     url : "saveSubscriptionRequest", 
		     data :"emailId="+emailId,	     	     	     
		     success : function(response) 
		     {  											    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="Exist")
		      	 {						
		    		 $("#emailSubErrId").show();
		    		 $("#emailSubErrId").html("Given email id already subscribed.");		    		 
		    	 }
		    	 else if(response=="success")
		      	 {																	    		 	
		    		 alert("Your have been successfully subscribed for update feeds.");
		    		 $("#newsemailId").val("");
		    		 $("#emailSubErrId").hide();
		    	 }
		      	 else
		      	 {
			         alert("There is some problem arise,Please try again later.");	
			     }											    	 
		     },  
	     }); 
    }
    
}



function viewGalleryFile(action,fileCategory,fileName,fileType)
{
	if(action=="Open")
	{
		$("#preloader").show();
		
		$.ajax({  
				
		     type : "post",   
		     url : "showGalleryFile", 
		     data :"fileCategory="+fileCategory+"&fileName="+fileName+"&fileType="+fileType,	     	     	     
		     success : function(response) 
		     {  											    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="failed")
		      	 {
		      		alert("Your session got expired please login again.");	
		      		window.location = "userLogout";
		      	 }
		    	 else 
		      	 {																	    		 	
	    		    $("#commentPopUp").html(response);
	    		    $("#commentPopUp").show();
		    	 }		      	
		     },  
	    });
	}
	else
	{
		$("#commentPopUp").html("");
	    $("#commentPopUp").hide();
	}
}


function sendLinkForForgetPw()
{
	var flag = 'true';
	var emailId = $("#forgetPwEmailId").val();
	
	if(emailId=="")
	{		
		flag = 'false';
		$("#linkEmailErrId").show();
        $("#linkEmailErrId").html("Please enter your email id.");                  
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			flag = 'false';
			$("#linkEmailErrId").show();
	        $("#linkEmailErrId").html("Please enter a valid email id.");		           
        }
		else
		{
			$("#linkEmailErrId").hide();
		}
	}
    
    if(flag=='true')
    {
    	$("#preloader").show();
    	 
    	$.ajax({  
			
   	     type : "post",   
   	     url : "sendLinkForForgetPw", 
   	     data :"forgetPwEmailId="+emailId,	     	     	     
   	     success : function(response) 
   	     {  											    	 
   	    	 $("#preloader").hide();
   	    	 
   	    	 if(response=="success")
   	      	 {
   	      		alert("We send link to this email id,Please follow the instruction.");	
   	      		window.location = "userRegister";
   	      	 }
   	    	 else if(response=="Not Exist") 
   	      	 {																	    		 	
       		    $("#forgetPwErrId").html("This email id is not registered yet.");
       		    $("#forgetPwErrId").show();
   	    	 }
   	    	 else
   	    	 {
   	    		 
   	    	 }
   	     },  
       });
    }
	
}

function saveResetedPassword()
{
	var flag = 'true';
	var requestedEmailID = $("#requestedEmailID").val();
	var newPassword = $("#newPassword").val();
	var cnfPassword = $("#cnfPassword").val();
	
	if(newPassword=="")
	{
		flag = 'false';
		$("#newPwErrId").html("Please enter new password.");
		$("#newPwErrId").show();
	}
	
	if(newPassword!="")
	{
		$("#newPwErrId").hide();
	}
	
	if(cnfPassword=="")
	{
		flag = 'false';
		$("#cnfPwErrId").html("Please confirm your password.");
		$("#cnfPwErrId").show();
	}
	
	if(newPassword!=cnfPassword)
	{
		flag = 'false';
		$("#cnfPwErrId").html("Not match with the password.");
		$("#cnfPwErrId").show();
	}
	
    if(flag=='true')
    {
    	$("#preloader").show();
    	 
    	$.ajax({  
			
   	     type : "post",   
   	     url : "saveUserResetPassword", 
   	     data :"password="+newPassword+"&requestedEmailID="+requestedEmailID,	     	     	     
   	     success : function(response) 
   	     {  											    	 
   	    	 $("#preloader").hide();
   	    	 
   	    	 if(response=="success")
   	      	 {
   	      		alert("Your password successfully changed,Goto signin page and sign in.");	
   	      		window.location = "userRegister";
   	      	 }
   	    	 else if(response=="failed") 
   	      	 {																	    		 	
   	    		alert("Due to some reason your request failed so please try again later.");
   	    	 }
   	    	 else
   	    	 {
   	    		 
   	    	 }
   	     },  
       });
    }
}
