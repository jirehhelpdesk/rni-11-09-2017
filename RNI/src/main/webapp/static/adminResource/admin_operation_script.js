

///////////////////////  ADMIN SCRIPT FOR MANAGE USER  ///////////////////////////////////////////////////////

function showUserSearchOption(option)
{
	if(option=="option1")
	{
		$("#searchValue").attr("placeholder", "Enter user's name to search");
		
		$("#textDiv").slideDown();
		$("#dateDiv").slideUp();
	}
	else if(option=="option2")
	{
		$("#searchValue").attr("placeholder", "Enter user's email id to search");
		
		$("#textDiv").slideDown();
		$("#dateDiv").slideUp();
	}
	else
	{
		$("#textDiv").slideUp();
		$("#dateDiv").slideDown();
	}
}


function searchUser()
{
	var flag = 'true';
	var searchType = "";
	var searchVal = "";
	var startDate = "";
	var endDate = "";
	
	if($('#optionsRadios1').is(':checked'))
	{
		searchType = "Name";
		
		searchVal = $("#post_title").val();
		
		if(searchVal=="")
		{
			flag = 'false';
			$("#valueErrorId").html("Please enter name to search.");
			$("#valueErrorId").slideDown();
		}
		if(searchVal!="")
		{
			$("#valueErrorId").slideUp();
		}
	}
	else if($('#optionsRadios2').is(':checked'))
	{
		searchType = "Email";
		
		searchVal = $("#post_title").val();
		
		if(searchVal=="")
		{
			flag = 'false';
			$("#valueErrorId").html("Please enter email id to search.");
			$("#valueErrorId").slideDown();
		}
		if(searchVal!="")
		{
			$("#valueErrorId").slideUp();
		}
	}
	else if($('#optionsRadios3').is(':checked'))
	{
		searchType = "Duration";
		
		startDate = $("#some_class_1").val();
		endDate = $("#some_class_2").val();
		
		if(startDate=="")
		{
			flag = 'false';
			$("#sDateErrorId").html("Please enter start date.");
			$("#sDateErrorId").slideDown();
		}
		if(startDate!="")
		{
			$("#sDateErrorId").slideUp();
		}
		
		if(endDate=="")
		{
			flag = 'false';
			$("#eDateErrorId").html("Please enter end date.");
			$("#eDateErrorId").slideDown();
		}
		if(endDate!="")
		{
			$("#eDateErrorId").slideUp();
		}
	}
	else
	{
		flag = 'false';
		alert("Please select your search type to search post.");
	}
	
	
	
	if(flag=='true')
	{		
		  $("#preloader").show();
		  
		  var formData = new FormData($("#userSearchForm")[0]);
		  
		  formData.append("searchType",searchType);
		  
		  $.ajax({
					type : "POST",
					data : formData,
					url : "searchUser",		
					success : function(response) {
			            
						$("#preloader").hide();
						
						if(response=="failed")
						{
							alert("Session got expired,Please login again !");
							window.location = "adminLogin";
						}
						else
						{
							$("#responseDiv").html(response);
							$("#responseDiv").slideDown();
							$('#responseDiv')[0].scrollIntoView(true);
						}
						
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}


function showUserFromAdmin(userId)
{
	$("#preloader").show();
	
	$.ajax({
		type : "POST",
		data : "userId="+userId,
		url : "showUserFromAdmin",								
		success : function(response) {
           
			$("#preloader").hide();
			
			if(response=="failed")
			{
				alert("Session got expired,Please login again !");
				window.location = "adminLogin";
			}
			else
			{				
				$("#userViewDiv").html(response);
				$("#userViewDiv").slideDown();
				$("#userViewDiv")[0].scrollIntoView(true);
			}						
		},			
	});	
}


function closeDivision(divId)
{
	$("#"+divId).slideUp();
}

function changeUserStatus(status,userId)
{
    $("#preloader").show();
	
	$.ajax({
		type : "POST",
		data : "status="+status+"&userId="+userId,
		url : "changeUserStatus",								
		success : function(response) {
           
			$("#preloader").hide();
			
			if(response=="failed")
			{
				alert("Session got expired,Please login again !");
				window.location = "adminLogin";
			}
			else if(response=="success")
			{				
				showUserFromAdmin(userId);
			}		
			else
			{
				alert("Due to some problem request can't proceed,Please try again later.");
			}
		},			
	});	
}


//////////////////////  ADMIN SCRIPT FOR MANAGE USER   ///////////////////////////////////////////////////////



///////////////////  ADMIN SCRIPT FOR MANAGE POST  //////////////////////////////////////////////////////////////

function showRelative(option){
	
	if(option=="option1")
	{
		$("#textDiv").slideDown();
		$("#dateDiv").slideUp();
	}
	else
	{
		$("#textDiv").slideUp();
		$("#dateDiv").slideDown();
	}
	
}

function searchPost()
{
	var flag = 'true';
	var searchType = "";
	var titleVal = "";
	var startDate = "";
	var endDate = "";
	
	if($('#optionsRadios1').is(':checked'))
	{
		searchType = "Title";
		
		titleVal = $("#post_title").val();
		
		if(titleVal=="")
		{
			flag = 'false';
			$("#titleErrorId").html("Please enter title to search.");
			$("#titleErrorId").slideDown();
		}
		if(titleVal!="")
		{
			$("#titleErrorId").slideUp();
		}
	}
	else if($('#optionsRadios2').is(':checked'))
	{
		searchType = "Duration";
		
		startDate = $("#some_class_1").val();
		endDate = $("#some_class_2").val();
		
		if(startDate=="")
		{
			flag = 'false';
			$("#sDateErrorId").html("Please enter start date.");
			$("#sDateErrorId").slideDown();
		}
		if(startDate!="")
		{
			$("#sDateErrorId").slideUp();
		}
		
		if(endDate=="")
		{
			flag = 'false';
			$("#eDateErrorId").html("Please enter end date.");
			$("#eDateErrorId").slideDown();
		}
		if(endDate!="")
		{
			$("#eDateErrorId").slideUp();
		}
	}
	else
	{
		flag = 'false';
		alert("Please select your search type to search post.");
	}
	
	if(flag=='true')
	{		
		  $("#preloader").show();
		  
		  var formData = new FormData($("#postSearchForm")[0]);
		  
		  formData.append("searchType",searchType);
		  
		  $.ajax({
					type : "POST",
					data : formData,
					url : "searchPost",		
					success : function(response) {
			            
						$("#preloader").hide();
						
						if(response=="failed")
						{
							alert("Session got expired,Please login again !");
							window.location = "adminLogin";
						}
						else
						{
							$("#responseDiv").html(response);
							$("#responseDiv").slideDown();
							$('#responseDiv')[0].scrollIntoView(true);
						}
						
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}


function showPostFromAdmin(postId,userId)
{
	$("#preloader").show();
	
	$.ajax({
		type : "POST",
		data : "postId="+postId+"&userId="+userId,
		url : "showPostFromAdmin",								
		success : function(response) {
           
			$("#preloader").hide();
			
			if(response=="failed")
			{
				alert("Session got expired,Please login again !");
				window.location = "adminLogin";
			}
			else
			{				
				$("#postDivId").html(response);
				$(".postDetails").slideDown();
				$("#postDivId")[0].scrollIntoView(true);
			}						
		},			
	});	
}


function closePostDiv(className)
{
	$("#postDivId").html("");
	$("."+className).slideUp();
}

function blockUserPost(postId,status)
{
	$("#preloader").show();
	
	$.ajax({
		type : "POST",
		data : "postId="+postId+"&status="+status,
		url : "blockUserPost",								
		success : function(response) {
           
			$("#preloader").hide();
			
			if(response=="failed")
			{
				alert("Session got expired,Please login again !");
				window.location = "adminLogin";
			}
			else if(response=="success")
			{	
				alert("User post status got successfully updated.");
				
				$("#postDivId").html("");
				$("."+className).slideUp();
				
				searchPost();
			}
			else
			{
				alert("Due to some problem request can't proceed,Please try again later.");
				window.location = "manPost";
			}
		},			
	});	
}


function manageCaseOfDiscussion(action,postId)
{
	if(action=="Yes")
	{
		if(confirm("Are you sure you want to mark this as a feed of discussion?"))
		{
			 $("#preloader").show();
			
			 $.ajax({  
					
			     type : "post",   
			     url : "manageCaseOfDiscussion", 
			     data :"action="+action+"&postId="+postId,	
			     success : function(response) 
			     {  			    	 
			    	 $("#preloader").hide();
			    	 
			    	 if(response=="failed")
			    	 {
			    		 alert("Session got expired,Please login again !");
						 window.location = "adminLogin";
			    	 }
			    	 else
			    	 {
			    		 alert("Feed marked as a feed of disscussion");
			    	 }
			    	 	 
			     }, 	     
		    });		
		}
		else
		{
			alert("Its ok mistake happen.");
		}
	}
	else
	{
		if(confirm("Are you sure you want to unmark this as a feed of discussion?"))
		{
			$("#preloader").show();
			
			 $.ajax({  
					
			     type : "post",   
			     url : "manageCaseOfDiscussion", 
			     data :"action="+action+"&postId="+postId,	
			     success : function(response) 
			     {  			    	 
			    	 $("#preloader").hide();
			    	 
			    	 if(response=="failed")
			    	 {
			    		 alert("Session got expired,Please login again !");
						 window.location = "adminLogin";
			    	 }
			    	 else
			    	 {
			    		 alert("Feed unmarked as a feed of disscussion");
			    	 }
			    	 	 
			     }, 	     
		    });	
		}
		else
		{
			alert("Its ok mistake happen.");
		}
	}	
}

/////////////////// END OF ADMIN SCRIPT FOR MANAGE POST  //////////////////////////////////////////////////////////////







///////////////////  ADMIN SCRIPT FOR MANAGE EVENT  //////////////////////////////////////////////////////////////

function showEventForm(action)
{
	if(action=="Open")
	{
		$("#preloader").show();
		
		 $.ajax({  
				
		     type : "post",   
		     url : "showEventForm", 
		     success : function(response) 
		     {  			    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="failed")
		    	 {
		    		 alert("Session got expired,Please login again !");
					 window.location = "adminLogin";
		    	 }
		    	 else
		    	 {
		    		 alert(response);
		    		 
		    		 $("#eventFormDiv").html(response);
		    		 $("#eventFormDiv").slideDown();
		    		 $("#eventFormDiv").show();
		    		 $('#eventFormDiv')[0].scrollIntoView(true);
		    	 }
		    	 	 
		     }, 	     
	    });		 		
	}
	else
	{
		$("#eventFormDiv").html("");
		$("#eventFormDiv").slideUp();
		$('#eventFormDiv')[0].scrollIntoView(true);
	}
}

function saveEventFile()
{
	var flag = 'true';
	var file = $("#file").val();
	
	if(file=="")
	{
		flag = 'false';	
	    $("#fileErrorId").show();
	    $("#fileErrorId").html("Browse a file to upload.");
	}
	
	if(file!="")
	{	
	  
	   var fileDetails = file;
	   var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);  
	   var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","mp4","MP4");	
	   
	   var condition = "NotGranted";
	    
	    for(var m=0;m<extension.length;m++)
		{
		    if(ext==extension[m])
	    	{				    	    
	    	   condition="Granted";				    	    
	    	}				    
		}
	    
		if(condition=="NotGranted")
		{
		    flag = 'false';	
		    $("#fileErrorId").show();
		    $("#fileErrorId").html("Image and video files are allowed.");
			return false;				  
		}
		
		 var fileDetails1 = document.getElementById("file");
		 var fileSize = fileDetails1.files[0];
		 var fileSizeinBytes = fileSize.size;
		 var sizeinKB = +fileSizeinBytes / 1024;
  		 var sizeinMB = +sizeinKB / 1024;
  		 
  		 if(sizeinMB>10)
		 {
			flag = 'false';
			$("#fileErrorId").show();
			$("#fileErrorId").html("choose a file less then 10 MB.");
			return false;	 		   		 
		 }  
  		 
	}
	
	
	
	
	if(flag=='true')
	{		
		  $("#preloader").show();
		  
		  var formData = new FormData($("#eventForm")[0]);
		 
		  $.ajax({
					type : "POST",
					data : formData,
					url : "saveEventFile",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response=="No file")
						{
							alert("No file added till now.");
						}
						else
						{
							var fileArray = response.split("/"); 
							
							$("#imageList").html("");
							
							for(var i=0;i<fileArray.length;i++)
			    		 	{								
								$("#imageList").append("<div>");
								$("#imageList").append("<i class='fa fa-window-close' aria-hidden='true' id='"+fileArray[i]+"' onclick='deleteTempFile(this.id)'></i>");
								$("#imageList").append("<label for='optionsRadios2'>'"+fileArray[i]+"'</label>");
								$("#imageList").append("</div>");
			    		 	}
							
							$("#file").val("");
						}
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}


function deleteTempFile(fileName)
{
	$("#preloader").show();
	
	 $.ajax({  
			
	     type : "post",   
	     url : "deleteTempFile", 
	     data :"fileName="+fileName,	     	     	     
	     success : function(response) 
	     {  			    	 
	    	 $("#preloader").hide();
	    	 
	    	 if(response=="No file")
			 {
	    		 $("#imageList").html("");
			 }
			 else
			 {
				var fileArray = response.split("/"); 
				
				$("#imageList").html("");
				
				for(var i=0;i<fileArray.length;i++)
    		 	{								
					$("#imageList").append("<div>");
					$("#imageList").append("<i class='fa fa-window-close' aria-hidden='true' id='"+fileArray[i]+"' onclick='deleteTempFile(this.id)'></i>");
					$("#imageList").append("<label for='optionsRadios2'>'"+fileArray[i]+"'</label>");
					$("#imageList").append("</div>");
    		 	}							
			 }		    	 
	     }, 	     
    }); 
}

function saveEventDetail(action)
{
	var flag = 'true';
	
	var title = $("#event_title").val();
	var desc = $("#aboutUs").val();
	var publish = "";
	var fee = "";
	
	var radioboxes = document.getElementsByName('eventCharge');
	
	for (var i=0; i<radioboxes.length; i++) {
	    if (radioboxes[i].checked) {
	    	
	    	publish = radioboxes[i].value;	    	
	    }
	}
	
	if(title=="")
	{
		flag = 'false';
		$("#titleErrorId").show();
		$("#titleErrorId").html("Please mention a title for your post");
	}
	
	if(title!="")
	{
		$("#titleErrorId").hide();
	}
	
	if(flag=='true')
	{	 
		$("#preloader").show();
		
		 var formData = new FormData($("#eventForm")[0]);
		
		 var divvalue = tinymce.get("event_desc").getContent();
		 
		 formData.append("textAreaContent",divvalue);
		  
		 formData.append("publish",publish);
		 
		 if(publish=="Yes")
		 {
			 fee = $("#eventFee").val();
			 formData.append("fee",fee); 
		 }
		 
		 
		 if(action=='save')
		 {
			 $.ajax({
					type : "POST",
					data : formData,
					url : "saveEventDetail",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response=="success")
			      		{																	    		 			    		  
							alert("Event details saved successfully into the portal.");
							
							window.location = "manEvent";
						}
			      	    else
			      		{
				      		alert("There is some problem arise,Please try again later.");			
				      		
				      		window.location = "manEvent";
			      		}	
						
					},
					cache: false,
				    contentType: false,
				    processData: false,
			});
		 }
		 else
		 {
			 $.ajax({
					type : "POST",
					data : formData,
					url : "updateEventDetail",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response=="success")
			      		{																	    		 			    		  
							alert("Event details saved successfully into the portal.");
							
							window.location = "manEvent";
						}
			      	    else
			      		{
				      		alert("There is some problem arise,Please try again later.");			
				      		
				      		window.location = "manEvent";
			      		}	
						
					},
					cache: false,
				    contentType: false,
				    processData: false,
			});
		 }
		 
		  			   	
	}
}


function manageFeeBox(choice)
{
	if(choice=="Yes")
	{
		$("#feeBox").show();
	}
	else
	{
		$("#feeBox").hide();
	}
}


function searchEvent()
{
	var flag = 'true';
	var searchType = "";
	var searchVal = "";
	var startDate = "";
	var endDate = "";
	
	if($('#optionsRadios1').is(':checked'))
	{
		searchType = "Title";
		
		searchVal = $("#searchValue").val();
		
		if(searchVal=="")
		{
			flag = 'false';
			$("#valueErrorId").html("Please enter name to search.");
			$("#valueErrorId").slideDown();
		}
		if(searchVal!="")
		{
			$("#valueErrorId").slideUp();
		}
	}	
	else if($('#optionsRadios3').is(':checked'))
	{
		searchType = "Duration";
		
		startDate = $("#some_class_1").val();
		endDate = $("#some_class_2").val();
		
		if(startDate=="")
		{
			flag = 'false';
			$("#sDateErrorId").html("Please enter start date.");
			$("#sDateErrorId").slideDown();
		}
		if(startDate!="")
		{
			$("#sDateErrorId").slideUp();
		}
		
		if(endDate=="")
		{
			flag = 'false';
			$("#eDateErrorId").html("Please enter end date.");
			$("#eDateErrorId").slideDown();
		}
		if(endDate!="")
		{
			$("#eDateErrorId").slideUp();
		}
	}
	else
	{
		flag = 'false';
		alert("Please select your search type to search post.");
	}
	
	
	
	if(flag=='true')
	{		
		  $("#preloader").show();
		  
		  var formData = new FormData($("#eventSearchForm")[0]);
		  
		  formData.append("searchType",searchType);
		  
		  $.ajax({
					type : "POST",
					data : formData,
					url : "searchEvent",		
					success : function(response) {
			            
						$("#preloader").hide();
						
						if(response=="failed")
						{
							alert("Session got expired,Please login again !");
							window.location = "adminLogin";
						}
						else
						{
							$("#eventSearchDiv").html(response);
							$("#eventSearchDiv").slideDown();
							$('#eventSearchDiv')[0].scrollIntoView(true);
						}
						
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}


function detailsOnEvent(eventId)
{
	$("#preloader").show();
	
	 $.ajax({  
			
	     type : "post",   
	     url : "detailsOnEvent", 
	     data :"eventId="+eventId,	     	     	     
	     success : function(response) 
	     {  			    	 
	    	 $("#preloader").hide();
	    	 
	    	 if(response=="failed")
	    	 {
	    		 alert("Session got expired,Please login again !");
				 window.location = "adminLogin";
	    	 }
	    	 else
	    	 {
	    		 $("#eventFormDiv").html(response);
	    		 $("#eventFormDiv").slideDown();
	    		 $('#eventFormDiv')[0].scrollIntoView(true);
	    	 }
	    	 	 
	     }, 	     
  });
}


function showEventReport(eventId)
{
	$("#preloader").show();
	
	 $.ajax({  
			
	     type : "post",   
	     url : "showEventReport", 
	     data :"eventId="+eventId,	     	     	     
	     success : function(response) 
	     {  			    	 
	    	 $("#preloader").hide();
	    	 
	    	 if(response=="failed")
	    	 {
	    		 alert("Session got expired,Please login again !");
				 window.location = "adminLogin";
	    	 }
	    	 else
	    	 {
	    		 $("#eventReportDiv").html(response);
	    		 $("#eventReportDiv").slideDown();
	    		 $('#eventReportDiv')[0].scrollIntoView(true);
	    	 }
	    	 	 
	     }, 	     
 });
}

//////********************************************** END OF *********************************************************////

///// *********************************************** START OF SETTING SCRIPT **************************************//////



function saveAdminDetails()
{
	var flag = 'true';
	
	var name = $("#admin_name").val();
	var userName = $("#admin_user_id").val();
	var designation = $("#admin_designation").val();
	
	if(name=="")
	{
		flag = 'false';
		$("#nameErrorId").show();
		$("#nameErrorId").html("Please mention name of the admin");
	}
	
	if(name!="")
	{
		$("#nameErrorId").hide();
	}
	
	if(userName=="")
	{
		flag = 'false';
		$("#usernameErrorId").show();
		$("#usernameErrorId").html("Please mention user name of the admin");
	}
	
	if(userName!="")
	{
		$("#usernameErrorId").hide();
	}
	
	if(designation=="")
	{
		flag = 'false';
		$("#designationErrorId").show();
		$("#designationErrorId").html("Please mention a designation of the admin.");
	}
	
	if(designation!="")
	{
		$("#designationErrorId").hide();
	}
	
	if(flag=='true')
	{		
		 $("#preloader").show();
			
		 var formData = new FormData($("#adminForm")[0]);
		
		 $.ajax({
					type : "POST",
					data : formData,
					url : "saveAdminDetails",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response=="failed")
						{
							alert("Session got expired,Please login again !");
							window.location = "adminLogin";
						}
						else if(response=="success")
			      		{																	    		 			    		  
							alert("Admin details saved successfully into the portal.");
							
							window.location = "manSetting";
						}
			      	    else
			      		{
				      		alert("There is some problem arise,Please try again later.");			
				      		
				      		window.location = "manEvent";
			      		}	
						
					},
					cache: false,
				    contentType: false,
				    processData: false,
			});			   	
	}
}


function updateAdminPassword()
{
	var flag = 'true';
	
	var curPassword = $("#curPassword").val();
	var newPassword = $("#newPassword").val();
	var cnfPassword = $("#cnfPassword").val();
	
	if(curPassword=="")
	{
		flag = 'false';
		$("#curErrorId").show();
		$("#curErrorId").html("Please enter your current password.");
	}
	
	if(curPassword!="")
	{
		$("#curErrorId").hide();
	}
	
	
	if(flag=='true')
	{
		$("#preloader").show();
		
		 $.ajax({  
				
		     type : "post",   
		     url : "updateAdminPassword", 
		     data :"curPassword="+curPassword+"&newPassword="+newPassword,	     	     	     
		     success : function(response) 
		     {  			    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="failed")
		    	 {
		    		 alert("Session got expired,Please login again !");
					 window.location = "adminLogin";
		    	 }
		    	 else if(response=="No Match")
		    	 {
		    		 $("#curErrorId").show();
		    		 $("#curErrorId").html("Incorrect current password.");
		    	 }
		    	 else if(response=="success")
		    	 {
		    		 alert("Admin password details updated successfully into the portal.");
						
				     window.location = "manSetting";
		    	 }
		    	 else
		    	 {
		    		 alert("There is some problem arise,Please try again later.");			
		    	 }		    	 	 
		     }, 	     
	   });
	}
}




///// **********************************************  END OF SETTING SCRIPT ***************************************/////


/////// *****************************************   PORTAL MANAGE SCRIPT  **************************************/////

function saveFaculty()
{
	var flag = 'true';
	
	var name = $("#faculty_name").val();
	var emailId = $("#faculty_email_id").val();
	var phone = $("#faculty_phone").val();
	
	if(name=="")
	{
		flag = 'false';
		$("#nameErrorId").show();
		$("#nameErrorId").html("Enter full name");
	}
	
	if(name!="")
	{
		$("#nameErrorId").hide();
	}
	
	if(emailId=="")
	{
		flag = 'false';
		$("#emailErrorId").show();
		$("#emailErrorId").html("Enter email id");
	}
	
	if(emailId!="")
	{
		$("#emailErrorId").hide();
	}
	
	
	
	if(flag=='true')
	{		
		  $("#preloader").show();
		  
		  var formData = new FormData($("#facultyForm")[0]);
		  
		  $.ajax({
					type : "POST",
					data : formData,
					url : "saveFaculty",		
					success : function(response) {
			            
						$("#preloader").hide();
						
						if(response=="failed")
						{
							alert("Session got expired,Please login again !");
							window.location = "adminLogin";
						}
						else
						{
							$("#facultyDiv").html(response);
							$("#facultyDiv").slideDown();
							$('#facultyDiv')[0].scrollIntoView(true);
						}
						
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}


function showEditFormFaculty(facultyId)
{
	$("#preloader").show();
	
	 $.ajax({  
			
	     type : "post",   
	     url : "showEditFormFaculty", 
	     data :"facultyId="+facultyId,	     	     	     
	     success : function(response) 
	     {  			    	 
	    	 $("#preloader").hide();
	    	 
	    	 if(response=="failed")
	    	 {
	    		 alert("Session got expired,Please login again !");
				 window.location = "adminLogin";
	    	 }
	    	 else
	    	 {
	    		 $("#facultyFormDiv").html(response);
	    		 $('#facultyFormDiv')[0].scrollIntoView(true);
	    	 }
	    	 	 
	     }, 	     
   });
}


function deleteFaculty(facultyId)
{
	if(confirm("Are you sure you want to delete this?"))
	{
		$("#preloader").show();
		
		 $.ajax({  
				
		     type : "post",   
		     url : "deleteFaculty", 
		     data :"facultyId="+facultyId,	     	     	     
		     success : function(response) 
		     {  			    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="failed")
		    	 {
		    		 alert("Session got expired,Please login again !");
					 window.location = "adminLogin";
		    	 }
		    	 else
		    	 {
		    		 $("#facultyDiv").html(response);
					 $("#facultyDiv").slideDown();
					 $('#facultyDiv')[0].scrollIntoView(true);
		    	 }	    	 	 
		     }, 	     
	   });
	}
	else
	{
		alert("To delete a record be sure on your work.");
	}
	
}



function saveMember()
{
	var flag = 'true';
	
	var name = $("#member_name").val();
	var emailId = $("#member_email_id").val();
	var phone = $("#member_phone").val();
	
	if(name=="")
	{
		flag = 'false';
		$("#cnameErrorId").show();
		$("#cnameErrorId").html("Enter full name");
	}
	
	if(name!="")
	{
		$("#cnameErrorId").hide();
	}
	
	if(emailId=="")
	{
		flag = 'false';
		$("#cemailErrorId").show();
		$("#cemailErrorId").html("Enter email id");
	}
	
	if(emailId!="")
	{
		$("#cemailErrorId").hide();
	}
	
	
	
	if(flag=='true')
	{		
		  $("#preloader").show();
		  
		  var formData = new FormData($("#memberForm")[0]);
		  
		  $.ajax({
					type : "POST",
					data : formData,
					url : "saveComMember",		
					success : function(response) {
			            
						$("#preloader").hide();
						
						if(response=="failed")
						{
							alert("Session got expired,Please login again !");
							window.location = "adminLogin";
						}
						else
						{
							$("#memberDiv").html(response);
							$("#memberDiv").slideDown();
							$('#memberDiv')[0].scrollIntoView(true);
						}
						
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}


function showEditFormMember(memberId)
{
	$("#preloader").show();
	
	 $.ajax({  
			
	     type : "post",   
	     url : "showEditFormMember", 
	     data :"memberId="+memberId,	     	     	     
	     success : function(response) 
	     {  			    	 
	    	 $("#preloader").hide();
	    	 
	    	 if(response=="failed")
	    	 {
	    		 alert("Session got expired,Please login again !");
				 window.location = "adminLogin";
	    	 }
	    	 else
	    	 {
	    		 $("#memberFormDiv").html(response);
	    		 $('#memberFormDiv')[0].scrollIntoView(true);
	    	 }
	    	 	 
	     }, 	     
  });
}

function deleteMember(memberId)
{
	if(confirm("Are you sure you want to delete this?"))
	{
		$("#preloader").show();
		
		 $.ajax({  
				
		     type : "post",   
		     url : "deleteMember", 
		     data :"memberId="+memberId,	     	     	     
		     success : function(response) 
		     {  			    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="failed")
		    	 {
		    		 alert("Session got expired,Please login again !");
					 window.location = "adminLogin";
		    	 }
		    	 else
		    	 {
		    		 $("#memberDiv").html(response);
					 $("#memberDiv").slideDown();
					 $('#memberDiv')[0].scrollIntoView(true);
		    	 }	    	 	 
		     }, 	     
	   });
	}
	else
	{
		alert("To delete a record be sure on your work.");
	}
}


function portalMngFormAndRecord(manageType)
{
	if(manageType=="Content")
	{
		$("#conFormDiv").slideDown();
		
		$("#fucFormDiv").slideUp();
		$("#commFormDiv").slideUp();
		
		
		$("#conButnId").addClass("managePrtlActive");
		
		$("#commButnId").removeClass("managePrtlActive");
		$("#fucButnId").removeClass("managePrtlActive");		
	}
	else if(manageType=="Committee")
	{
		$("#commFormDiv").slideDown();
		
		$("#fucFormDiv").slideUp();
		$("#conFormDiv").slideUp();
		
		$("#commButnId").addClass("managePrtlActive");
		
		$("#conButnId").removeClass("managePrtlActive");
		$("#fucButnId").removeClass("managePrtlActive");
	}
	else
	{
		$("#fucFormDiv").slideDown();
		
		$("#commFormDiv").slideUp();
		$("#conFormDiv").slideUp();
		
		$("#fucButnId").addClass("managePrtlActive");
		
		$("#conButnId").removeClass("managePrtlActive");
		$("#commButnId").removeClass("managePrtlActive");
	}
}

function saveContentDetails(action)
{
	var flag = 'true';
	
	var contentFor = $("#content_for").val();
	var heading = $("#content_heading").val();
	
	if(contentFor=="Select content for")
	{
		flag = 'false';
		$("#conForErrorId").html("Select content heading for which page.");
		$("#conForErrorId").slideDown();
	}
	
	if(contentFor!="Select content for")
	{
		$("#conForErrorId").slideUp();
	}
	
	if(heading=="")
	{
		flag = 'false';
		$("#headErrorId").html("Enter a heading message.");
		$("#headErrorId").slideDown();
	}
	
	if(heading!="")
	{
		$("#headErrorId").slideUp();
	}
	
	
	
	if(action=='Save')
	{		
		if(flag=='true')
		{		
			  $("#preloader").show();
			  
			  var formData = new FormData($("#contentForm")[0]);
			  
			  formData.append("actionRequest",action);
			  
			  $.ajax({
						type : "POST",
						data : formData,
						url : "savePortalContent",		
						success : function(response) {
				            
							$("#preloader").hide();
							
							if(response=="failed")
							{
								alert("Session got expired,Please login again !");
								window.location = "adminLogin";
							}
							else
							{
								alert("Content title message details saved successfully.");
								
								$("#contentDiv").html(response);
								$("#contentDiv").slideDown();
								$('#contentDiv')[0].scrollIntoView(true);
							}
							
						},
						cache: false,
				        contentType: false,
				        processData: false,
				});			   	
		}
	}
	else
	{
		
	}
}

function showEditFormContent(contentId)
{
	$("#preloader").show();
	
	 $.ajax({  
			
	     type : "post",   
	     url : "showEditFormContent", 
	     data :"contentId="+contentId,	     	     	     
	     success : function(response) 
	     {  			    	 
	    	 $("#preloader").hide();
	    	 
	    	 if(response=="failed")
	    	 {
	    		 alert("Session got expired,Please login again !");
				 window.location = "adminLogin";
	    	 }
	    	 else
	    	 {	    		
	    		 $("#contentFormDiv").html(response);
	    		 $('#contentFormDiv')[0].scrollIntoView(true);
	    	 }	    	 	 
	     }, 	     
   });
}

function deleteContent(contentId)
{
	if(confirm("Are you sure you want to delete this?"))
	{
		$("#preloader").show();
		
		 $.ajax({  
				
		     type : "post",   
		     url : "deleteContent", 
		     data :"contentId="+contentId,	     	     	     
		     success : function(response) 
		     {  			    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="failed")
		    	 {
		    		 alert("Session got expired,Please login again !");
					 window.location = "adminLogin";
		    	 }
		    	 else
		    	 {
		    		 alert("Content details deleted successfully.");
		    		 
		    		 $("#contentDiv").html(response);
					 $("#contentDiv").slideDown();
					 $('#contentDiv')[0].scrollIntoView(true);
		    	 }	    	 	 
		     }, 	     
	   });
	}
	else
	{
		alert("To delete a record be sure on your work.");
	}
}

////// ***************************************** END OF PORTAL MANAGE SCRIPT ***********************************/////



/////  ***************************************** START OF MANAGE NEWSLETTER SCRIPT ******************************//////

function showSTypeOpton(value)
{
	if(value=="Registred User")
	{
		$("#sTypeId").slideDown();
		$("#user_count").val("");
	}
	else if(value=="Nonregistred User")
	{
		$("#sTypeId").slideUp();
		showUserCount(value);
	}
	else
	{
		$("#sTypeId").slideUp();
		$("#user_count").val("");
	}
}

function showUserCount(value)
{
	$("#preloader").show();
	
	 $.ajax({  
			
	     type : "post",   
	     url : "showUserCount", 
	     data :"value="+value,	     	     	     
	     success : function(response) 
	     {  			    	 
	    	 $("#preloader").hide();
	    	 
	    	 if(response=="failed")
	    	 {
	    		 alert("Session got expired,Please login again !");
				 window.location = "adminLogin";
	    	 }
	    	 else
	    	 {
	    		 $("#user_count").val(response);
			 }	    	 	 
	     }, 	     
    });
}


function sendNewsLetter()
{
	var flag = 'true';
	
	var fi = document.getElementById('files');
	
	if(fi.files.length>0)
	{	
		var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","mp4","MP4","webm","WEBM","ogg","OGG","PDF","pdf");	
	    
		var fsize = 0;
		
		var condition = "NotGranted";
		
		for (var f = 0; f <= fi.files.length - 1; f++) 
		{
			var fname = fi.files.item(f).name;
			var ext=fname.substring(fname.lastIndexOf('.')+1)
			
			for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
		    	{				    	    
		    	   condition="Granted";				    	    
		    	}			    
			}
			
			if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrorId").show();
			    $("#fileErrorId").html("Image , video(mp4,webm,ogg) and pdf files are allowed.");
				return false;				  
			}
			
			var fileSizeinBytes = fi.files.item(f).size;   
			var sizeinKB = +fileSizeinBytes / 1024;
	  		var sizeinMB = +sizeinKB / 1024;
	  		
	  		fsize = fsize + sizeinMB;	
	  		
		}
		
		/*if(fsize>40)
		{
			flag = 'false';
			$("#fileErrorId").show();
			$("#fileErrorId").html("choose a file less then 40 MB.");
			return false;	 		   		 
		}	*/	
		
	}
	
	
	if(flag=='true')
	{		
		  $("#preloader").show();
		  
		  var formData = new FormData($("#newsForm")[0]);
		  
		  $.ajax({
					type : "POST",
					data : formData,
					url : "sendNewsLetter",		
					success : function(response) {
			            
						$("#preloader").hide();
						
						if(response=="failed")
						{
							alert("Session got expired,Please login again !");
							window.location = "adminLogin";
						}
						if(response=="error")
						{
							alert("There might be an problem arise so please try again later !");
						}
						else
						{
							alert("Sending process starts system will send mail one by one to all user.");
							window.location = "manNewsLetter";
						}
						
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}

////// ******************************************* END OF MANAGE NEWSLETTER SCRIPT *****************************/////


///// ****************************************** START OF MANAGE GALLERY SCRIPT ******************************* //////

function saveGalleryRecord()
{
	var flag = 'true';
	
	var fi = document.getElementById('files');
	
	if(fi.files.length>0)
	{	
		var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","mp4","MP4","webm","WEBM","ogg","OGG","PDF","pdf");	
	    
		var fsize = 0;
		
		var condition = "NotGranted";
		
		for (var f = 0; f <= fi.files.length - 1; f++) 
		{
			var fname = fi.files.item(f).name;
			var ext=fname.substring(fname.lastIndexOf('.')+1)
			
			for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
		    	{				    	    
		    	   condition="Granted";				    	    
		    	}			    
			}
			
			if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrorId").show();
			    $("#fileErrorId").html("Image , video(mp4,webm,ogg) and pdf files are allowed.");
				return false;				  
			}
			
			var fileSizeinBytes = fi.files.item(f).size;   
			var sizeinKB = +fileSizeinBytes / 1024;
	  		var sizeinMB = +sizeinKB / 1024;
	  		
	  		fsize = fsize + sizeinMB;	
	  		
		}
		
		/*if(fsize>40)
		{
			flag = 'false';
			$("#fileErrorId").show();
			$("#fileErrorId").html("choose a file less then 40 MB.");
			return false;	 		   		 
		}	*/	
		
	}
	
	if(flag=='true')
	{		
		  $("#preloader").show();
		  
		  var formData = new FormData($("#galleryForm")[0]);
		  
		  $.ajax({
					type : "POST",
					data : formData,
					url : "saveGalleryRecord",		
					success : function(response) {
			            
						$("#preloader").hide();
						
						if(response=="failed")
						{
							alert("Session got expired,Please login again !");
							window.location = "adminLogin";
						}
						if(response=="error")
						{
							alert("There might be an problem arise so please try again later !");
						}
						else
						{
							alert("Gallery files saved successfully.");
							window.location = "manGallary";
						}
						
					},
					cache: false,
			        contentType: false,
			        processData: false,
			});			   	
	}
}


function deleteGallery(galleryId)
{
	$("#preloader").show();
	
	 $.ajax({  
			
	     type : "post",   
	     url : "deleteGallery", 
	     data :"galleryId="+galleryId,	     	     	     
	     success : function(response) 
	     {  			    	 
	    	 $("#preloader").hide();
	    	 
	    	 if(response=="failed")
	    	 {
	    		 alert("Session got expired,Please login again !");
				 window.location = "adminLogin";
	    	 }
	    	 else
	    	 {
	    		 alert("Gallery files deleted successfully.");
				 window.location = "manGallary";
			 }	    	 	 
	     }, 	     
   });
}

////************************************ END OF SCRIPT FOR GALLERY **************************************/////////////////////////





/////************************************* START OF SCRIPT FOR CASE MANAGEMENT *************************////////////////////////////////////

	function showCaseForm(action)
	{
		if(action=="show")
		{
			$("#caseFormDiv").slideDown();
		}
		else
		{
			$("#caseFormDiv").slideUp();
		}		
	}
	
	
	
	function saveCaseDetail(action)
	{	
		var flag = 'true';
		
		
		if(flag=='true')
		{			
			$("#preloader").show();
			
			var formData = new FormData($("#formBean")[0]);
			
			var caseDesc = CKEDITOR.instances['ckeditor'].getData();
			
			formData.append("action",action);
			formData.append("caseDesc",caseDesc);
			
				$.ajax({
					type : "POST",
					data : formData,
					url : "saveCaseDetail",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response=="success")
			      		{				
							alert("Case details saved successfully.");			
							
							window.location = "manCase";
						}
			      	    else
			      		{
				      		alert("There is some problem arise,Please try again later.");			
				      		
			      		}							
					},
					cache: false,
			        contentType: false,
			        processData: false,
			        
			});	
			
		}
	}
	
	function deleteCaseDetails(caseId)
	{
		if(confirm("Are you sure you want to delete this?"))
		{
			$("#preloader").show();
			
			 $.ajax({  
					
			     type : "post",   
			     url : "deleteCaseDetails", 
			     data :"caseId="+caseId,	     	     	     
			     success : function(response) 
			     {  			    	 
			    	 $("#preloader").hide();
			    	 
			    	 if(response=="failed")
			    	 {
			    		 alert("Session got expired,Please login again !");
						 window.location = "adminLogin";
			    	 }
			    	 else
			    	 {
			    		 alert("Case details deleted successfully.");
			    		 window.location = "manCase";
					 }	    	 	 
			     }, 	     
		   });
		}
		else
		{
			alert("Its ok mistake happens.");
		}
		

		
	}
	
	function viewCaseDetails(caseId)
	{
		$("#preloader").show();
		
		 $.ajax({  
				
		     type : "post",   
		     url : "viewCaseDetails", 
		     data :"caseId="+caseId,	     	     	     
		     success : function(response) 
		     {  			    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="failed")
		    	 {
		    		 alert("Session got expired,Please login again !");
					 window.location = "adminLogin";
		    	 }
		    	 else
		    	 {
		    		 $("#caseDetails").html(response);
		    		 $("#caseDetails").slideDown();
				 }	    	 	 
		     }, 	     
	   });
	}
	
	function closeCaseDiv()
	{
		$("#caseDetails").html("");
		$("#caseDetails").slideUp();
	}

	function changeCaseStatus(caseStatus,caseId)
	{
		$("#preloader").show();
		
		 $.ajax({  
				
		     type : "post",   
		     url : "changeCaseStatus", 
		     data :"caseStatus="+caseStatus+"&caseId="+caseId,	     	     	     
		     success : function(response) 
		     {  			    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="failed")
		    	 {
		    		 alert("Session got expired,Please login again !");
					 window.location = "adminLogin";
		    	 }
		    	 else
		    	 {
		    		 alert("Case status successfully update.");
		    		 
		    		 viewCaseDetails(caseId);
				 }	    	 	 
		     }, 	     
	   });
	}
	
/////************************************* END OF SCRIPT FOR CASE MANAGEMENT *************************////////////////////////////////////





/////************************************* START OF SCRIPT FOR BANNER MANAGEMENT *************************////////////////////////////////////




function uploadBannerFile()
{
	var flag = 'true';
	var fi = document.getElementById('files');
	
	if(fi.files.length>0)
	{	
		var extension = new Array("JPG","jpg","jpeg","JPEG","gif","GIF","png","PNG","mp4","MP4","webm","WEBM","ogg","OGG","PDF","pdf");	
	    
		var fsize = 0;
		
		var condition = "NotGranted";
		
		for (var f = 0; f <= fi.files.length - 1; f++) 
		{
			var fname = fi.files.item(f).name;
			var ext=fname.substring(fname.lastIndexOf('.')+1)
			
			for(var m=0;m<extension.length;m++)
			{
			    if(ext==extension[m])
		    	{				    	    
		    	   condition="Granted";				    	    
		    	}			    
			}
			
			if(condition=="NotGranted")
			{
			    flag = 'false';	
			    $("#fileErrId").show();
			    $("#fileErrId").html("Image , video(mp4,webm,ogg) and pdf files are allowed.");
				return false;				  
			}
			
			var fileSizeinBytes = fi.files.item(f).size;   
			var sizeinKB = +fileSizeinBytes / 1024;
	  		var sizeinMB = +sizeinKB / 1024;
	  		
	  		fsize = fsize + sizeinMB;		  		
		}
		
		if(fsize>40)
		{
			flag = 'false';
			$("#fileErrId").show();
			$("#fileErrId").html("choose a file less then 40 MB.");
			return false;	 		   		 
		}				
	}
	
	
	if(flag=='true')
	{
		$("#preloader").show();
		
			var formData = new FormData($("#formBean")[0]);
			
				$.ajax({
					type : "POST",
					data : formData,
					url : "captureBrowseFiles",		
					success : function(response) {
			           
						$("#preloader").hide();
						
						if(response!="")
			      		{																	    		 			    		  
							var fileArray = response.split("/");
							
							$("#selectedFileList").html("");
							
							for(var i=0;i<fileArray.length;i++)
							{
								$("#selectedFileList").append("<li><i title='Remove the file' style='color:#333;cursor:pointer;' class='fa fa-trash' id='"+fileArray[i]+"' onclick='deleteBrowseFile(this.id)'></i> &nbsp;"+fileArray[i]+"</li>");
								
								$("#fileListDiv").show();
							}
							
							$("#file").val("");
							
						}
			      	    else
			      		{
				      		alert("There is some problem arise,Please try again later.");			
				      		
				      		window.location = "postFeed";
			      		}							
					},
					cache: false,
			        contentType: false,
			        processData: false,
			        
			});				
	}
	


}


function deleteBrowseFile(fileName)
{
	$("#preloader").show();
	
	$.ajax({
		type : "POST",
		data : "fileName="+fileName,
		url : "deleteBrowseFile",								
		success : function(response) {
           
			$("#preloader").hide();
			
			if(response!="")
      		{																	    		 			    		  
				var fileArray = response.split("/");
				
				$("#selectedFileList").html("");
				
				for(var i=0;i<fileArray.length;i++)
				{
					$("#selectedFileList").append("<li><i title='Remove the file' style='color:#333;cursor:pointer;' class='fa fa-trash' id='"+fileArray[i]+"' onclick='deleteBrowseFile(this.id)'></i> &nbsp;"+fileArray[i]+"</li>");
				}
				
				$("#fileListDiv").show();
			}
			else if(response=="")
      		{
				$("#fileListDiv").hide();
				$("#selectedFileList").html("");
      		}
      	    else
      		{
	      		alert("There is some problem arise,Please try again later.");			
	      		window.location = "postFeed";
      		}
			
		},			
	});	
}


function saveBannerDetail()
{
	var flag = 'true';
	var fi = document.getElementById('files');
	
	if(flag=='true')
	{
		$("#preloader").show();
		
		var formData = new FormData($("#formBean")[0]);
		
			$.ajax({
				type : "POST",
				data : formData,
				url : "saveBannerFiles",		
				success : function(response) {
		           
					$("#preloader").hide();
					
					if(response=="success")
		      		{																	    		 			    		  
						alert("Banner files successfully added to the portal.");			
			      		
			      		window.location = "manBanner";
					}
		      	    else
		      		{
			      		alert("There is some problem arise,Please try again later.");			
			      	}							
				},
				cache: false,
		        contentType: false,
		        processData: false,
		        
		});	
	}
	
}

	function viewBanner(bannerId)
	{
		$("#preloader").show();
		
		 $.ajax({  
				
		     type : "post",   
		     url : "viewBanner", 
		     data :"bannerId="+bannerId,	     	     	     
		     success : function(response) 
		     {  			    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="failed")
		    	 {
		    		 alert("Session got expired,Please login again !");
					 window.location = "adminLogin";
		    	 }
		    	 else
		    	 {
		    		 $("#bannerDetails").html(response);
		    		 $("#bannerDetails").slideDown();
				 }	    	 	 
		     }, 	     
	   });
	}

	function deleteBanner(bannerId)
	{
		if(confirm("Are you sure you want to delete this?"))
		{
			$("#preloader").show();
			
			 $.ajax({  
					
			     type : "post",   
			     url : "deleteBanner", 
			     data :"bannerId="+bannerId,	     	     
			     success : function(response) 
			     {  			    	 
			    	 $("#preloader").hide();
			    	 
			    	 if(response=="failed")
			    	 {
			    		 alert("Session got expired,Please login again !");
						 window.location = "adminLogin";
			    	 }
			    	 else
			    	 {
			    		 alert("Banner deleted successfully");
			    		 
			    		 window.location = "manBanner";
					 }	    	 	 
			     }, 	     
		   });
		}
		else
		{
			alert("Its ok mistake happen !");
		}
		
	}
	
	function changeBannerStatus(bannerStatus,bannerId)
	{
		$("#preloader").show();
		
		 $.ajax({  
				
		     type : "post",   
		     url : "changeBannerStatus", 
		     data :"bannerStatus="+bannerStatus+"&bannerId="+bannerId,	     	     	     
		     success : function(response) 
		     {  			    	 
		    	 $("#preloader").hide();
		    	 
		    	 if(response=="failed")
		    	 {
		    		 alert("Session got expired,Please login again !");
					 window.location = "adminLogin";
		    	 }
		    	 else
		    	 {
		    		 alert("Banner status successfully update.");
		    		 
		    		 viewBanner(bannerId);
				 }	    	 	 
		     }, 	     
	   });
	}
	
	
/////************************************* END OF SCRIPT FOR BANNER MANAGEMENT *************************////////////////////////////////////

