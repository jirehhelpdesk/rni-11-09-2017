
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:if test="${empty facultyData}">	

	<form:form  id="facultyForm" modelAttribute="faculty" method="post" action="saveFaculty">
	                        
	  <div class="form-group">
	    <label class="form-control-label">Full Name</label>
	    <input type="text" placeholder="Email Address" name="faculty_name" path="faculty_name" id="faculty_name" class="form-control">
	    <span id="nameErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group">
	    <label class="form-control-label">Email id</label>
	    <input type="email" placeholder="Email Address" name="faculty_email_id" path="faculty_email_id" id="faculty_email_id" class="form-control">
	    <span id="emailErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group">       
	    <label class="form-control-label">Phone no</label>
	    <input type="text" maxlength="10" placeholder="Phone number" name="faculty_phone" path="faculty_phone" id="faculty_phone" class="form-control">
	    <span id="phoneErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group row">
	  	<div class="offset-sm-10">       
	   	    <input type="button" value="Save" class="btn btn-primary" onclick="saveFaculty()">
	   	</div>
	  </div>
	  
	</form:form>

</c:if>


<c:if test="${!empty facultyData}">	

   <c:forEach items="${facultyData}" var="val">
   			
   			<form:form  id="facultyForm" modelAttribute="faculty" method="post" action="saveFaculty">
	                        
			  <div class="form-group">
			    <label class="form-control-label">Full Name</label>
			    <input type="text" placeholder="Email Address" name="faculty_name" path="faculty_name" id="faculty_name" value="${val.faculty_name}" class="form-control">
			    <span id="nameErrorId" class="help-block-none"></span>
			  </div>
			  
			  <div class="form-group">
			    <label class="form-control-label">Email id</label>
			    <input type="email" placeholder="Email Address" name="faculty_email_id" path="faculty_email_id" id="faculty_email_id" value="${val.faculty_email_id}" class="form-control">
			    <span id="emailErrorId" class="help-block-none"></span>
			  </div>
			  
			  <div class="form-group">       
			    <label class="form-control-label">Phone no</label>
			    <input type="text" maxlength="10" placeholder="Phone number" name="faculty_phone" path="faculty_phone" id="faculty_phone" value="${val.faculty_phone}" class="form-control">
			    <span id="phoneErrorId" class="help-block-none"></span>
			  </div>
			  
			  
			  <input type="hidden" name="faculty_id" path="faculty_id" value="${val.faculty_id}" />
			  
			  <div class="form-group row">
			  	<div class="offset-sm-10">       
			   	    <input type="button" value="Save" class="btn btn-primary" onclick="saveFaculty()">
			   	</div>
			  </div>
			  
			</form:form>
   			
   </c:forEach>
	
</c:if>