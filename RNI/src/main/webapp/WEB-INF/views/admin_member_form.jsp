
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:if test="${empty memberData}">	

	<form:form class="form-horizontal"  id="memberForm" modelAttribute="member" method="post" action="saveComMember" >
                      
                        
        <div class="form-group">
          <label class="form-control-label">Full Name</label>
          <input type="text" placeholder="Enter member full name" name="member_name" path="member_name" id="member_name" class="form-control">
          <span id="cnameErrorId" class="help-block-none"></span>
        </div>
        
        <div class="form-group">
          <label class="form-control-label">Email id</label>
          <input type="email" placeholder="Email Address" name="member_email_id" path="member_email_id" id="member_email_id" class="form-control">
          <span id="cemailErrorId" class="help-block-none"></span>
        </div>
        
        <div class="form-group">       
          <label class="form-control-label">Phone no</label>
          <input type="text" maxlength="10" placeholder="Phone number" name="member_phone" path="member_phone" id="member_phone" class="form-control">
          <span id="cphoneErrorId" class="help-block-none"></span>
        </div>
        
        <div class="form-group row">       
          <div class="offset-sm-10">
            <input type="button" value="Save" class="btn btn-primary" onclick="saveMember()">
          </div>
        </div>
        
      </form:form>

</c:if>


<c:if test="${!empty memberData}">	

   <c:forEach items="${memberData}" var="val">
   			
   			<form:form class="form-horizontal"  id="memberForm" modelAttribute="member" method="post" action="saveComMember" >
                       
	              <div class="form-group">
	                <label class="form-control-label">Full Name</label>
	                <input type="text" placeholder="Enter member full name" name="member_name" path="member_name" id="member_name" value="${val.member_name}" class="form-control">
	                <span id="cnameErrorId" class="help-block-none"></span>
	              </div>
	              
	              <div class="form-group">
	                <label class="form-control-label">Email id</label>
	                <input type="email" placeholder="Email Address" name="member_email_id" path="member_email_id" id="member_email_id" value="${val.member_email_id}" class="form-control">
	                <span id="cemailErrorId" class="help-block-none"></span>
	              </div>
	              
	              <div class="form-group">       
	                <label class="form-control-label">Phone no</label>
	                <input type="text" maxlength="10" placeholder="Phone number" name="member_phone" path="member_phone" id="member_phone" value="${val.member_phone}" class="form-control">
	                <span id="cphoneErrorId" class="help-block-none"></span>
	              </div>
	              
	              <input type="hidden" name="committee_member_id" path="committee_member_id" value="${val.committee_member_id}" />
	              
	              <div class="form-group row">       
	                <div class="offset-sm-10">
	                  <input type="button" value="Save" class="btn btn-primary" onclick="saveMember()">
	                </div>
	              </div>
              
            </form:form>
   			
   </c:forEach>
	
</c:if>