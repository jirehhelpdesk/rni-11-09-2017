
<%@include file="index_common_style.jsp"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<aside class="widget widget_similar_event">
	<div class="widget-title">
		<span class="icon icon-Blog"></span>
		<h3>User Menu</h3>
	</div>

	<ul>
		
		
		<%-- <li class="trli">
			
			<ul id="tree1">
				
				<c:if test="${!empty feedTree}">	
						
				   		<li>
				   		    <a href="#">View Feed</a>
	
							<ul>
							    
							    <%int tempYear = 0; %>
								
								<c:forEach items="${feedTree}" var="val">
									
									<c:set var="date" value="${val}"/>
									<% Date date = (Date)pageContext.getAttribute("date");%>
									
									<%int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(date)); %>
									
									<li><%=year%></li>
									
								</c:forEach>
								
							</ul>
							
						</li>
				
				</c:if>
					   
				<li><a href="#">View Feed</a>

					<ul>
						<li>2017</li>
						<li>2016
							<ul>
								<li>December</li>
								<li>November</li>
							</ul>
						</li>
						<li>2015</li>
					</ul>
				
				</li>
				
			</ul>
			
		</li> --%>

		
		<li id="uMenu1"><a title="View Feed"
			href="userDashboard"><span class="icon icon-Blog"></span>
			<h4>View Feed</h4></a>
		</li>
		
		<li id="uMenu2"><a title="Post Feed"
			href="postFeed"><span class="icon icon-Notes"></span>
			<h4>Post Feed</h4></a>
		</li>
		
		<li id="uMenu3"><a title="Events"
			href="userEvent"><span class="icon icon-ClipboardText"></span>
			<h4>Events</h4></a>
		</li>
		
		<li id="uMenu4"><a title="Case of the month"
			href="userCaseMonth"><span class="icon icon-Briefcase"></span>
			<h4>Case of the month</h4></a>
		</li>
		
		<li id="uMenu5"><a title="Settings"
			href="userSetting"><span class="icon icon-Settings"></span>
			<h4>Setting</h4></a>
		</li>
		
	</ul>
</aside>



<!-- <aside class="widget">
	
	<div class="widget-title">
		<span class="icon icon-PaperClip"></span>
		<h3>Arrange feeds</h3>
	</div>
	
	<div class="event-categories" style="margin-bottom: 12px;">
						
		<div class="event-select-option">
			<select class="selectpicker" title="Choose one of the following..." id="filterFeedId" onchange="showFilterFeedOption(this.value)">
				<option data-icon="icon-Folder" value="Filter feeds">Filter feeds</option>
				<option data-icon="icon-Files" value="Most viewed feeds">Most viewed feeds</option>
				<option data-icon="icon-Floppy" value="Most liked feeds">Most liked feeds</option>
				<option data-icon="icon-File" value="Find feeds with duration">Find feeds with duration</option>
			</select>
		</div>
		
		<div class="event-select-option" id="yearDiv" style="display:none;">
			<select class="FilterDropdown" id="yearSelect" onchange="showMonth(this.value)">
				<option data-icon="icon-User" value="Select Year">Select Year</option>
			</select>
		</div>
		
		<div class="event-select-option" id="monthDiv" style="display:none;">
			<select class="FilterDropdown" id="monthSelect" onchange="showFeedViaMonth(this.value)">
				<option data-icon="icon-Tag" value="Select Month">Select Month</option>			
			</select>
		</div> 
		
		<a href="#" title="Filter">Filter</a>
		
	</div>
	
</aside> -->
					
					
   




<%-- <%@include file="index_common_script.jsp"%> --%>

<script src="static/IndexResources/js/tree_menu.js"></script>


