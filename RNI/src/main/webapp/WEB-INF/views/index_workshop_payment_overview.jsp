<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Pre Conference Workshop Participant Overview</title>
																												
	<%@include file="index_common_style.jsp" %>
																																	
</head>

<%@ page import = "java.io.*,java.util.*,com.ccavenue.security.*" %>

<script>
	window.onload = function() {
		var d = new Date().getTime();
		document.getElementById("tid").value = d;
	};
</script>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">
	
	<header class="header-main container-fluid no-padding"> <!-- Menu Block -->
	
	<div class="menu-block container-fluid no-padding affix-top" data-spy="affix" data-offset-top="100">
		<!--   -->
		<!-- Container -->
		<div class="container">
			<!-- User -->
			<div class="col-md-12">
				<a href="#"> <img src="static/IndexResources/images/logo.png"
					alt="logo" class="retnet center-block" width="170">
				</a>
			</div>
		</div>
		<!-- Container /- -->
	</div>
	
	<!-- Menu Block /- --> 
	</header>
	
	
	<!-- Schedule Section -->
	<div class="container-fluid no-padding schedule-section">
		<div class="section-padding"></div>
		<div class="container">
			<div class="section-header">
				<h3>Registration overview</h3>
				<span>Please pay now to get confirmation</span>
			</div>	
			
			<div class="schedule-block">
				
				<!-- <img src="images/schedule.jpg" alt="schedule"/> -->
				
				<div class="col-md-11">
					
					<div class="tab-content">
						
						<div role="tabpanel" class="tab-pane fade in active" id="schedule_1" >
							<div class="panel-group schedule-accordion" id="accordion" role="tablist" aria-multiselectable="true">
								
								<form method="post" name="customerData" action="testCCavenueRequest">
								
										<div class="panel panel-default">
											
											
											<c:forEach items="${applicantDetails}" var="applicant">
								
													<div class="">
															
															<div class="col-md-12">
																<div class="form-group">
																	<div class="row">
																						
																	    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																		   
																		   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Name</b> </div>
																		   
																		   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																		   
																		   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${applicant.applicant_name}</span> </div>
																			
																		</div>
																		
																	</div>
																</div>
															</div>
															
															<div class="col-md-12">
																<div class="form-group">
																	<div class="row">
																						
																	    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																		   
																		   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Email id</b> </div>
																		   
																		   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																		   
																		   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${applicant.applicant_email}</span> </div>
																			
																		</div>
																		
																	</div>
																</div>
															</div>
															
															<div class="col-md-12">
																<div class="form-group">
																	<div class="row">
																						
																	    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																		   
																		   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Mobile</b> </div>
																		   
																		   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																		   
																		   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${applicant.applicant_mobile}</span> </div>
																			
																		</div>
																		
																	</div>
																</div>
															</div>
												
															<div class="col-md-12">
																<div class="form-group">
																	<div class="row">
																						
																	    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																		   
																		   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Address</b> </div>
																		   
																		   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																		   
																		   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor">${applicant.applicant_address}</span> </div>
																			
																		</div>
																		
																	</div>
																</div>
															</div>
															
															<div class="col-md-12">
																<div class="form-group">
																	<div class="row">
																						
																	    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																		   
																		   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Location</b> </div>
																		   
																		   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																		   
																		   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor">${applicant.applicant_city},${applicant.applicant_state} - ${applicant.applicant_pincode}</span> </div>
																			
																		</div>
																		
																	</div>
																</div>
															</div>
															
															<div class="col-md-12">
																<div class="form-group">
																	<div class="row">
																						
																	    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																		   
																		   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Amount</b> </div>
																		   
																		   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																		   
																		   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> Rs. ${applicant.applicant_amount} /- ${applicant.registration_category}</span> </div>
																			
																		</div>
																		
																	</div>
																</div>
															</div>
															
															<div class="col-md-12">
																<div class="form-group">
																	<div class="row">
																		 <div class="col-md-5 col-sm-5 col-xs-5"></div>
																		
																		 <div class="col-md-7 col-sm-7 col-xs-5">
																		     
																					<%
																						ResourceBundle fileResource = ResourceBundle.getBundle("resources/paymentGateway");
																					    String url = fileResource.getString("url");
																					    String merchant_id = fileResource.getString("merchant_id");																					    
																				     %>
																															       
																			        <input type="hidden" name="tid" id="tid" readonly />
																					<input type="hidden" name="merchant_id" id="merchant_id" value="<%=merchant_id%>" />
																					<input type="hidden" name="order_id" value="${applicant.applicant_order_id}" />
																					<input type="hidden" name="currency" value="INR" />
																					<input type="hidden" name="amount" value="${applicant.applicant_amount}" /> 
																					
																					<input type="hidden" name="redirect_url" value="<%=url%>indexWorkPaymentStatus" />
																					<input type="hidden" name="cancel_url" value="<%=url%>indexWorkPaymentStatus" />
																					
																					<input type="hidden" name="language" id="language" value="EN" />
																					
																					<input type="hidden" name="billing_name" value="${applicant.applicant_name}" />
																					<input type="hidden" name="billing_address" value="${applicant.applicant_address}" />
																					<input type="hidden" name="billing_city" value="${applicant.applicant_city}" />
																					<input type="hidden" name="billing_state" value="${applicant.applicant_state}" />
																					<input type="hidden" name="billing_zip" value="${applicant.applicant_pincode}" />
																					<input type="hidden" name="billing_country" value="India" />
																					<input type="hidden" name="billing_tel" value="${applicant.applicant_mobile}" />
																					<input type="hidden" name="billing_email" value="${applicant.applicant_email}" />
														     																     
																			        <input type="submit" class="viewmre buttoncs btn mrgnknowmore" value="Pay Now" />
																			   
															             </div>
															        </div>
																</div>
															</div>
															
													</div>
												
										</c:forEach>
											
										</div>
										
								</form>
										
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- Schedule Section /- -->
	
	
	
	
	<footer class="footer-main container-fluid no-padding"> <!-- Container -->
	<div class="container">
		<div class="footer-menu">
			<!-- Copyrights -->
			<div class="copyrights ow-pull-left">
				<p>� 2018 | All rights reserved by Retnetindia</p>
			</div>
			<!-- Copyrights /- -->
			<!-- Navigation -->
			<nav class="ow-pull-right foltleft">
			<div class="">
				<ul class="nav navbar-nav">
					<li><span class="dgnpra">Design &amp; Developed by <a
							target="_" href="http://www.jirehsol.co.in/" class="dvby">Jireh</a></span></li>
				</ul>
			</div>
			</nav>
			<!-- Navigation /- -->
		</div>
		<!-- Footer Menu /- -->
	</div>
	<!-- Container /- --> </footer>
	
	<div id="commentPopUp" style="display:none;"></div>
	
	
	<div id="preloader" style="display: none;">
	
		<div class="cssload-container">
			<div class="cssload-whirlpool"></div>
		</div>
		
	</div>
	
	<%@include file="index_common_script.jsp" %>
	
</body>
</html>