<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Manage Post</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    
    
    <%@include file="admin_common_style.jsp" %>
    
    
  </head>
  
  
  <body onload="activeMenu('menu4','Admin')">
    
    
    
    <div class="page form-page">
     
     
      <!-- Main Navbar-->
      <%@include file="admin_header.jsp" %>
      
      
      
      <div class="page-content d-flex align-items-stretch"> 
        
        
         <%@include file="admin_side_bar.jsp" %>
        
        
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Manage Feed </h2>
            </div>
          </header>
          
          
           <div id="page-wrapper">
          <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                
                
               
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    
                    
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Search User's Feed</h3>
                    </div>
                    
                    <div class="card-body">
                      
                      <form class="form-horizontal" id="postSearchForm">
                        
                        
                        <div class="form-group row">
                          
                          <label class="col-sm-3 form-control-label">Search via <br><small class="text-primary">Result depends upon criteria</small></label>
                          
                          <div class="col-sm-9">
                            
                            <div>
                              	<input id="optionsRadios1" type="radio"  value="option1" name="optionsRadios" onclick="showRelative(this.value)">
                              	<label for="optionsRadios1">Post title</label>
                            </div>
                            
                            <div>
                              	<input id="optionsRadios2" type="radio" value="option2" name="optionsRadios" onclick="showRelative(this.value)">
                              	<label for="optionsRadios2">As per posted duration</label>
                            </div>
                            
                            <div id="textDiv" style="display:none;">
                            	<input type="text" placeholder="Enter post title to search (Min 5 Character)" name="post_title" id="post_title" class="form-control">
                            	<span id="titleErrorId" class="help-block-none"></span>
							</div>
                            
                             <div id="dateDiv" class="form-group row" style="display:none;">
		                          <div class="col-sm-5">
		                          	 <input type="text" id="some_class_1" class="form-control datepickerWithoutTime" name="start_date"  placeholder="Opening Date">
									 <span id="sDateErrorId" class="help-block-none"></span>
		                          </div>
		                          <div class="col-sm-5">
		                            <input type="text" id="some_class_2" class="form-control datepickerWithoutTime" name="end_date"  placeholder="Ending Date">
									<span id="eDateErrorId" class="help-block-none"></span>
		                          </div>
	                        </div>
                        
                          
                          </div>
                          
                        </div>
                        
                        
                        <div class="line"></div>
                        <div class="form-group row">
                          <div class="col-sm-5 offset-sm-3">
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <button type="button" class="btn btn-primary" onclick="searchPost()">Search</button>
                          </div>
                        </div>                        
                        
                      </form>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          
          
          <section class="forms postDetails" style="display:none;"> 
            <div class="container-fluid">
              <div class="row">
              	
              	<div class="col-lg-12" id="postDivId">
                  
                  
                  
                </div>
              
              
              </div>
              
             </div>
              
         </section>
          
          
          <section id="responseDiv" class="tables">   
            
            
          </section>
          </div>
         <!-- Footer Main -->
			<%@include file="admin_footer.jsp" %>
		  <!-- END Footer Main -->
          
          
          
          
        </div>
      </div>
    </div>
    
    <div id="postpopup" class="modal fade postpage" role="dialog">
  
  
    </div>
    
    
    <%@include file="admin_common_script.jsp" %>
    
    
    
  </body>
</html>