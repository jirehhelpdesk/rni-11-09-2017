<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
 
 
 <div class="container-fluid">
              
              <div class="row">
                	
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Banner Record</h3>
                    </div>
                    <div class="card-body">
                     
                     
                    <c:if test="${!empty bannerRecord}">	
                      
		                   
                   		   <table class="table table-striped table-hover" data-toggle="table" data-pagination="true">
	                       
		                        <thead>
		                        
		                          <tr>
		                            <th>Sl.No</th>
		                            <th>File name</th>
		                            <th>Added date</th>
		                            <th>Status</th>
		                            <th>Manage</th>
		                          </tr>
		                          
		                        </thead>
	                        
	                        
		                        <tbody>
		                         
		                           <%int i = 1; %> 
		                           <c:forEach items="${bannerRecord}" var="val">
													
										
										<tr>
				                            
				                            <td scope="row"><%=i++%></td>
				                            <td><span class="contentHide">${val.banner_file_name}</span></td>
				                            <td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.banner_cr_date}" /></td>
				                            <td>${val.banner_status}</td>
				                           
				                            <td>
				                            	
				                            	<a href="#" onclick="viewBanner('${val.banner_id}')">View</a>
				                            	&nbsp;&nbsp;
				                            	<a href="#" onclick="deleteBanner('${val.banner_id}')">Delete</a>
				                            	
				                            </td>
				                            
				                        </tr>
										 	  
				                   </c:forEach>
		                          
		                          
		                        </tbody>
	                        
	                      </table>
                   			
                     </c:if>
                     
                     <c:if test="${empty bannerRecord}">	
                     
                     		<div style="color:red;text-align:center;font-size:18px;">
								As of now nothing added to the banner section of portal,So default image will appear.
							</div>
                     
                     </c:if> 
                      
                      
                      
                      
                      
                      
                    </div>
                  </div>
                </div>
                	
                	
              </div>
              
 </div>