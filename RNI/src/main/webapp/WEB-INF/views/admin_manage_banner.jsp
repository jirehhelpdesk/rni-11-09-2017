<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Manage Banner</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    
    
    <%@include file="admin_common_style.jsp" %>
    
    



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

  </head>
  
  
  <body onload="activeMenu('menu10','Admin')">
    
    
    
    <div class="page form-page">
     
     
      <!-- Main Navbar-->
      <%@include file="admin_header.jsp" %>
      
      
      
      <div class="page-content d-flex align-items-stretch"> 
        
        
         <%@include file="admin_side_bar.jsp" %>
        
        
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Banner Management</h2>
            </div>
          </header>
          
          
          <div id="page-wrapper">
          
          
          
          <!-- Forms Section-->
          
                    
          <section class="forms" id="caseFormDiv" style="display:block;"> 
            
             	<div class="container-fluid">
              
		              <div class="row">
		                
		                <!-- Form Elements -->
		                <div class="col-lg-12">
		                  <div class="card">
		                    
		                    <div class="card-header d-flex align-items-center">
		                      <h3 class="h4">Add banner to the portal</h3>
		                    </div>
		                    <div class="card-body">
		                      
		                      
		                      
		                      <form:form  class="form-horizontal" id="formBean" modelAttribute="formBean" method="post" action="saveBannerDetails"  enctype="multipart/form-data">
								
		                        
		                        <div class="form-group row">
		                          
		                          <label class="col-sm-4 form-control-label">Add Banner (width x height) of (1920 x 770)</label>
		                          <div class="col-sm-6">
		                                
		                                <input type="file" name="files" id="files" multiple class="form-control" onchange="uploadBannerFile()">  
		                                <span id="fileErrId" class="help-block-none"></span>
		                                
		                          </div>
		                          
		                        </div>
		                        
		                        <div class="form-group row">
		                          
		                          <label class="col-sm-2 form-control-label"><br><small class="text-primary"></small></label>
		                          
		                          <div class="col-sm-10">
		                            	
		                            	<ul id="selectedFileList" style="list-style: none;">

										
										</ul>
		
		                          </div>
		                          
		                        </div>
		                        
		                       
		                       
		                        <div class="line"></div>
		                        <div class="form-group row">
		                          <div class="col-sm-4 offset-sm-3">
		                            <button type="reset" class="btn btn-secondary">Cancel</button>
		                            <button type="button" class="btn btn-primary" onclick="saveBannerDetail()">Save</button>
		                          </div>
		                        </div>
		                        
		                        
		                      </form:form>
		                      
		                    </div>
		                  </div>
		                </div>
		                
		                
		              </div>
		            </div>
          
          </section>
          
          <section class="forms" id="bannerDetails" style="display:none;"> 
            
          </section>
          
          
         <section id="tableDiv" class="tables" style="margin-top:-50px;">   
            
            	<%@include file="admin_banner_table.jsp" %>
            
          </section>
          
          
          </div>
          
         <!-- Footer Main -->
			<%@include file="admin_footer.jsp" %>
		  <!-- END Footer Main -->
          
          
          
          
        </div>
      </div>
    </div>
    
    
    
    
    <%@include file="admin_common_script.jsp" %>
    
    
    
  </body>
</html>