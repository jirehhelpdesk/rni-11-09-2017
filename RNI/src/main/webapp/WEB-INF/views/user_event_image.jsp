
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:forEach items="${eventDetails}" var="val">
								
		<c:set var="id" value="${val.event_id}"/>
		<% int id = (Integer)pageContext.getAttribute("id"); %>
		
		<div class="eventslider eventpopimg">
		<div class="modal-dialog modal-md">
	
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">${val.event_title}</h4>
				</div>
	
				<div class="modal-body">
	
					<div class="row">
						<div class="col-md-12">
							
							
									<div class="slideshow">
												
											<input type="radio" name="ss1" id="ss1-item-1" class="slideshow--bullet" checked="checked" />
											
											<% int i = 0; %>
											<c:forEach items="${val.image}" var="image">
													
													<% i = i + 1; %>
													
													<c:set var="file" value="${image.file_name}"/>
													<% String file = (String)pageContext.getAttribute("file"); %>
											
													
													<%if(i==1){ %>
													
														<div class="slideshow--item">
															<img src="${pageContext.request.contextPath}<%="/previewEventMedia?fileName="+file+"&eventId="+id+""%>" class="img-responsive"/>
															<label for="ss1-item-3" class="slideshow--nav slideshow--nav-previous">Go to slide 3</label>
															<label for="ss1-item-2" class="slideshow--nav slideshow--nav-next">Go to slide 2</label>
														</div>
													
													<%}else{ %>
													
														
														<input type="radio" name="ss1" id="ss1-item-<%=i%>" class="slideshow--bullet" />
														<div class="slideshow--item">
															<img src="${pageContext.request.contextPath}<%="/previewEventMedia?fileName="+file+"&eventId="+id+""%>"  class="img-responsive"/>
															<label for="ss1-item-<%=i-1%>" class="slideshow--nav slideshow--nav-previous">Go to slide <%=i-1%></label>
															<label for="ss1-item-<%=i+1%>" class="slideshow--nav slideshow--nav-next">Go to slide <%=i+1%></label>
														</div>
											
													<%} %>
											
											
											</c:forEach>		
													
									</div>
	
						</div>
	
	
					</div>
				</div>
	
			</div>
	
		</div>
		</div>
		
		
		




		

</c:forEach>

