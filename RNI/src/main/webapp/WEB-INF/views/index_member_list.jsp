<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
 
 <div class="total-upcomming-event col-md-12 col-sm-12 col-xs-12  no-padding">
            
     <c:forEach items="${memberList}" var="val">

		<div class="col-md-4">
			<div class="single-upcomming shadow-box">
				
				
				<div class="col-md-3 hidden-sm col-xs-12">
					<div class="sue-pic">
						<div class="wrap-feature-icon">
							<div class="feature-icon">
								<span class="icon  icon-User"></span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-9 col-sm-5 col-xs-12">
					<div class="uc-event-title">
						<h3>${val.member_name}</h3>
						<p>Mob : +91 - ${val.member_phone}</p>
						<p>
							Email : <a style="font-size: 13px;color: #21a5b2;text-decoration: none;" href="mailto:${val.member_email_id}" target="_blank">${val.member_email_id}</a>
						</p>
					</div>
				</div>
				
				
			</div>
		</div>

	</c:forEach>        
          
</div>