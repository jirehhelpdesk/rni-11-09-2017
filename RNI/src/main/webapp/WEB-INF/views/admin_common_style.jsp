<!-- Bootstrap CSS-->
    <link rel="stylesheet" href="static/adminResource/css/bootstrap.min.css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="static/adminResource/css/googleapi.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="static/adminResource/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="static/adminResource/css/custom.css">
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="static/IndexResources/images/favicon.ico">
    <!-- Font Awesome CDN-->
    <!-- you can replace it by local Font Awesome-->
    <script src="static/adminResource/js/fontawesome.js"></script>
    <!-- Font Icons CSS-->
    <link rel="stylesheet" href="static/adminResource/css/icons.css">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        
        
    <link rel="stylesheet" href="static/loader/style.css">  
    
    <link rel="stylesheet" href="static/datePicker/jquery.datetimepicker.css">  
    
    <style type="text/css">

		.custom-date-style {
			background-color: red !important;
		}
	
	</style>
	
	<link rel="stylesheet" href="static/adminResource/css/custom_ui.css">
	<link rel="stylesheet" href="static/adminResource/css/bootstrap-table.css">  
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	
	