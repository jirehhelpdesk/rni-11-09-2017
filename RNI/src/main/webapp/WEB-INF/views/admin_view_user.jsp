<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
<c:set var="userId" value="${userBean.user_id}"/>
<% int userIden = (Integer)pageContext.getAttribute("userId"); %>
						
<c:set var="status" value="${userBean.status}"/>
<% String status = (String)pageContext.getAttribute("status"); %>
                    							

 
<div class="container-fluid">
  
  <div class="row">
    <!-- Recent Updates-->
    
    <!-- Daily Feeds -->
    <div class="col-lg-12">
      <div class="daily-feeds card"> 
        
        <div class="card-header">
          <h3 class="h4">${userBean.first_name}'s Profile</h3>
        </div>
        <div class="card-body no-padding">
          <!-- Item-->
          
          <div class="item">
            <div class="feed d-flex justify-content-between">
              <div class="feed-body d-flex justify-content-between">
                  
                  
                    <c:if test="${empty userBean.profileBean}">
                       
                       <a href="#" class="feed-profile profileimg">
                        	
                        	<img style="border-radius:0%" src="static/adminResource/img/userDemo.png" alt="person" class="img-fluid rounded-circle">
                       
                       </a>
                       
                    </c:if>	 
                    
                    
                    <c:if test="${!empty userBean.profileBean}">
                    	
                    	<c:set var="profilePhoto" value="${userBean.profileBean.profile_photo}"/>
						<% String profilePhoto = (String)pageContext.getAttribute("profilePhoto"); %>
                    	
                    	<a href="#" class="feed-profile profileimg">
                        	
                        	<%if(profilePhoto.equals("")){%>
                        		
                        		<img style="border-radius:0%" src="static/adminResource/img/userDemo.png" alt="person" class="img-fluid rounded-circle">
                    		
                    		<%}else{%>
                    		
                    			<img style="border-radius:0%" src="${pageContext.request.contextPath}<%="/previewProfilePhoto?fileName="+profilePhoto+"&userId="+userIden+""%>" alt="person" class="img-fluid rounded-circle">
                    		
                    		<%}%>
                    	</a>
                    	
                    </c:if>


                
                <div class="content" style="float:left;width:100%;">
                  
                  <p>Account Details</p>
                  
                  <ul>	
                  	   <li><span><b>Name     :</b> ${userBean.first_name} ${userBean.last_name} </span></li>
                  	   <li><span><b>Email id :</b> ${userBean.email_id} </span></li>
                  	   <li><span><b>Phone    :</b> ${userBean.mobile_no} </span></li>
                  	   <li>
                  	   		<%if(status.equals("Active")){ %>
                  	   			
                  	   			<span><b>Status    :</b> <i style="color:green;font-weight:bold;">${userBean.status}</i> </span>
                  	   			
                  	   		<%}else{%>
                  	   			
                  	   			<span><b>Status    :</b> <i style="color:red;font-weight:bold;">${userBean.status}</i> </span>
                  	   			
                  	   		<%} %>
                  	   </li>
                  </ul>
                  
                  <p>Profile Details</p>
                  
                  <ul>
                  		<li><span><b>Under Graduation &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.under_graduation} </span></li>
                  		<li><span><b>Post Graduation  &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.post_graduation} </span></li>
                  		<li><span><b>Additional       &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.additional_qualification} </span></li>
                  		<li><span><b>Practice as      &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.practice_as} </span></li>
                  		<li><span><b>Practice at      &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.practice_at} </span></li>
                  		<li><span><b>Year of Exp      &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.year_of_experience} </span></li>
                  		<li><span><b>Practice as      &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.practice_as} </span></li>
                  		<li><span><b>Area of interest &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.area_of_interest} </span></li>
                  		<li><span><b>Fellowship       &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.fellowship} </span></li>
                  		<li><span><b>Website          &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.website_url} </span></li>
                  		<li><span><b>Gender           &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.gender} </span></li>
                  		<li><span><b>Date of Birth    &nbsp;&nbsp;&nbsp; :</b>    <fmt:formatDate pattern="dd/MM/yyyy" value="${userBean.profileBean.dob}" /> </span></li>
                  		<li><span><b>Age              &nbsp;&nbsp;&nbsp; :</b>    ${userBean.profileBean.age} </span></li>
                  		<li></li>
                  		<li></li>
                  		<li></li>
                  </ul>
                  
                  
                </div>
                
              </div>
              
              <div class="date text-right">Registered Date &nbsp;<small><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${userBean.cr_date}" /></small></div>
              
            </div>
          </div>
          
        	<div class="form-group row" >
               <div class="col-md-4 offset-md-8 col-sm-5 offset-sm-7 col-xs-9 offset-xs-3" style="margin-top:10px;">
                      
                      
                    <button type="button" class="btn btn-secondary" onclick="closeDivision('userViewDiv')">Close</button>
                    
                    <%if(status.equals("Active")){ %>
                  	   			
        	   			<button type="button" class="btn btn-primary" onclick="changeUserStatus('Deactive','${userBean.user_id}')">Block</button>
        	   			
        	   		<%}else{%>
        	   			
        	   			<button type="button" class="btn btn-primary" onclick="changeUserStatus('Active','${userBean.user_id}')">Unblock</button>
        	   			
        	   		<%} %>
                 
               </div>
             </div>
                        
          
        </div>
      </div>
    </div>
    <!-- Recent Activities -->
    
  </div>
  
</div>

