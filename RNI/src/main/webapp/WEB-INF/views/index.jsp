<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>RetNetIndia</title>

	
	<%@include file="index_common_style.jsp" %>
	
</head>

<%@ page import = "java.io.*,java.util.*" %>
<%@page import="java.text.SimpleDateFormat"%>

<%
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	Date today = new Date();
	Date lastDate = sdf.parse("2018-06-01");
%>	

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation" onload="activeMenu('menu1','index'),loadAlertBox('Open')">
	
	
	
	<!-- Loader And Header /- -->
	
	<%@include file="index_header.jsp" %>
	
	<!-- END of Loader and Header -->
	
	<!-- Slider Section -->
	
	
	
	<div id="slider-section" class="slider-section container-fluid no-padding">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			
			
			
		  <c:if test="${empty bannerRecord}">	
			
					<div class="carousel-inner" role="listbox">
						
						
						<div class="item active">
							 <img src="static/IndexResources/images/slider1.jpg" alt="slide1" width="100%" />
							<!-- <div class="carousel-caption">
								<div class="container">
									<div class="col-md-5 col-sm-6 col-xs-9 pull-right">
										<div class="slider-content-box">
											<div class="col-md-12 col-sm-12 col-xs-6 no-padding">
												<h3 class="slider-title">We conduct conference on medical </h3>
												<span>January 03-07</span>
												<p>09, Design Street, New York, NY- 10001, United States </p>
											</div>
											<div class="slider-author col-md-12 col-sm-12 col-xs-6 no-padding">
												<img src="images/slider-thumb1.jpg" alt="slider-thumb" width="74" height="74"/>
												<h3>Daniel Lesner<span>Public Speaker</span></h3>
											</div>
											<div class="col-md-12 col-sm-12 col-xs-6 no-padding">
												<a href="#" title="Register now">Register Now</a>
											</div>
										</div>
									</div>
								</div>
							</div> -->
						</div>
						
						
						<div class="item">
							<img src="static/IndexResources/images/slider2.jpg" alt="slide2" width="100%"/>
							<!-- <div class="carousel-caption">
								<div class="container">
									<div class="col-md-5 col-sm-6 col-xs-9 pull-right">
										<div class="slider-content-box">
											<div class="col-md-12 col-sm-12 col-xs-6 no-padding">
												<h3 class="slider-title">International Academic Business Conference Orlando</h3>
												<span>January 15-19</span>
												<p>09, Design Street, New York, NY- 10001, United States</p>
											</div>
											<div class="slider-author col-md-12 col-sm-12 col-xs-6 no-padding">
												<img src="images/slider-thumb2.jpg" alt="slider-thumb" width="74" height="74"/>
												<h3>Dwayne Johnson<span>Business Speaker</span></h3>
											</div>
											<div class="col-md-12 col-sm-12 col-xs-6 no-padding">
												<a href="#" title="Register now">Register Now</a>
											</div>
										</div>
									</div>
								</div>
							</div> -->
						</div>
						
						
						<div class="item">
							<img src="static/IndexResources/images/slider3.jpg" alt="slide3" width="100%"/>
							<!-- <div class="carousel-caption">
								<div class="container">
									<div class="col-md-5 col-sm-6 col-xs-9 pull-right">
										<div class="slider-content-box">
											<div class="col-md-12 col-sm-12 col-xs-6 no-padding">
												<h3 class="slider-title">Well the first thing you know ol' Jeds a millionaire</h3>
												<span>January 27-30</span>
												<p>09, Design Street, New York, NY- 10001, United States </p>
											</div>
											<div class="slider-author col-md-12 col-sm-12 col-xs-6 no-padding">
												<img src="images/slider-thumb3.jpg" alt="slider-thumb" width="74" height="74"/>
												<h3>David Warner<span>Public Speaker</span></h3>
											</div>
											<div class="col-md-12 col-sm-12 col-xs-6 no-padding">
												<a href="#" title="Register now">Register Now</a>
											</div>
										</div>
									</div>
								</div>
							</div> -->
						</div>
						
						
						
					</div>
			</c:if>
			
			<c:if test="${!empty bannerRecord}">	
					
				<div class="carousel-inner" role="listbox">
					
					<%int i = 1; %> 
					<c:forEach items="${bannerRecord}" var="val">
						
						<%
							String fileType = "";
							String fileName = "";
						%>
	
						<c:set var="fileType" value="${val.banner_file_type}" />
						<%
							fileType = (String) pageContext.getAttribute("fileType");
						%>
	
						<c:set var="fileName" value="${val.banner_file_name}" />
						<%
							fileName = (String) pageContext.getAttribute("fileName");
						%>
									
					   <%if(i==1){ %>
							
							<div class="item active">
								 
								 <img src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=BannerFile&fileName="+fileName+""%>" alt="slide<%=i%>" width="1920" height="770"/>
							
							</div>
							
					   <%}else{%>
						
							<div class="item">
								
								<img src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=BannerFile&fileName="+fileName+""%>" alt="slide<%=i%>" width="1920" height="770"/>
							
							</div>
						
						<%}%>
						
						<%=i++%>
						 
					</c:forEach>
					
				</div>
			
			</c:if>
			
			
			
			
			
			
			
			<!-- Controls -->
			<div class="container">
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<i class="fa fa-angle-left"></i>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<i class="fa fa-angle-right"></i>
				</a>
			</div>
		</div>
	</div><!-- Slider Section /- -->
	
	
	
	<!-- Howwecan Section -->
	<div class="container-fluid no-padding howwecan-section">
		<div class="section-padding"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="howwecan-left">
						<div class="img-box">
							<img src="static/IndexResources/images/howwecan2.jpg" alt="howwecan1" width="149" height="149"/>
						</div>
						<div class="img-box">
							<img src="static/IndexResources/images/howwecan1.jpg" alt="howwecan2" width="149" height="149"/>
						</div>
						<div class="img-box">
							<img src="static/IndexResources/images/howwecan3.jpg" alt="howwecan3" width="149" height="149"/>
						</div>
					</div>
				</div>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<div class="howwecan-right">
						<div class="section-header">
							<h3>How RetNet India Conference will helpfull</h3>
						</div>
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation">
								<a href="#howwecan_1" aria-controls="howwecan_1" role="tab" data-toggle="tab">
									<span class="icon icon-WorldWide"></span>
								</a>
							</li>
							<li role="presentation">
								<a href="#howwecan_2" aria-controls="howwecan_2" role="tab" data-toggle="tab">
									<span class="icon icon-Briefcase"></span>
								</a>
							</li>
							<li role="presentation" class="active">
								<a href="#howwecan_3" aria-controls="howwecan_3" role="tab" data-toggle="tab">
									<span class="icon icon-Files"></span>
								</a>
							</li>
							<li role="presentation">
								<a href="#howwecan_4" aria-controls="howwecan_4" role="tab" data-toggle="tab">
									<span class="icon icon-Book"></span>
								</a>
							</li>
							<li role="presentation">
								<a href="#howwecan_5" aria-controls="howwecan_5" role="tab" data-toggle="tab">
									<span class="icon icon-Folder"></span>
								</a>
							</li>
							<li role="presentation">
								<a href="#howwecan_6" aria-controls="howwecan_6" role="tab" data-toggle="tab">
									<span class="icon icon-ClipboardChart"></span>
								</a>
							</li>
						</ul>  
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade" id="howwecan_1">
								<h3>Presentations 1</h3>
								<p>Believe it or not I'm walking on air. I never thought I could feel so free! Doin' it our way. There's nothing we wont try. Never heard the word impossible. This time there's no stopping us. And they knew it was much more than a hunch.</p>
								<a href="#" title="Learn more" class="buttoncs btn">Learn more</a>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="howwecan_2">
								<h3>Presentations 2</h3>
								<p>Believe it or not I'm walking on air. I never thought I could feel so free! Doin' it our way. There's nothing we wont try. Never heard the word impossible. This time there's no stopping us. And they knew it was much more than a hunch.</p>
								<a href="#" title="Learn more" class="buttoncs btn">Learn more</a>
							</div>
							<div role="tabpanel" class="tab-pane fade in active" id="howwecan_3">
								<h3>Presentations</h3>
								<p>Believe it or not I'm walking on air. I never thought I could feel so free! Doin' it our way. There's nothing we wont try. Never heard the word impossible. This time there's no stopping us. And they knew it was much more than a hunch.</p>
								<a href="#" title="Learn more" class="buttoncs btn">Learn more</a>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="howwecan_4">
								<h3>Presentations 3</h3>
								<p>Believe it or not I'm walking on air. I never thought I could feel so free! Doin' it our way. There's nothing we wont try. Never heard the word impossible. This time there's no stopping us. And they knew it was much more than a hunch.</p>
								<a href="#" title="Learn more" class="buttoncs btn">Learn more</a>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="howwecan_5">
								<h3>Presentations 4</h3>
								<p>Believe it or not I'm walking on air. I never thought I could feel so free! Doin' it our way. There's nothing we wont try. Never heard the word impossible. This time there's no stopping us. And they knew it was much more than a hunch.</p>
								<a href="#" title="Learn more" class="buttoncs btn">Learn more</a>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="howwecan_6">
								<h3>Presentations 5</h3>
								<p>Believe it or not I'm walking on air. I never thought I could feel so free! Doin' it our way. There's nothing we wont try. Never heard the word impossible. This time there's no stopping us. And they knew it was much more than a hunch.</p>
								<a href="#" title="Learn more" class="buttoncs btn">Learn more</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- Howwecan Section /- -->
	
	
	
	
	
	<!-- Introduction Section -->
	<!-- Introduction Section /- -->
	
	
	
	<!-- Testimonial Section -->
	<!-- <div class="container-fluid testimonial-section no-padding">
			<div class="container">
				<div class="section-header">
					<h3>What our core concept of conference</h3>
					<span>Feedback From Our Members</span>
				</div>
				<div class="testimonial-carousel">
					<div class="testimonial-block row">	
						<div class="col-md-5 col-sm-12 no-padding">
							<div class="testimonial-carousel-left">
								<div class="testimonial-box testimonial-left">
									<div class="testimonial-content">
										<p>Feels so right it cant be wrong. Rockin' and rollin' all week long. Then one day he was shootin' at some food..</p>										
										<span>hendry smith villa</span>
									</div>
									<img src="static/IndexResources/images/testimonial1.jpg" alt="testimonial1" width="144" height="136"/>
								</div>
								<div class="testimonial-box testimonial-left">
									<div class="testimonial-content">
										<p>Feels so right it cant be wrong. Rockin' and rollin' all week long. Then one day he was shootin' at some food..</p>										
										<span>hendry smith villa</span>
									</div>
									<img src="static/IndexResources/images/testimonial1.jpg" alt="testimonial1" width="144" height="136"/>
								</div>
								<div class="testimonial-box testimonial-left">
									<div class="testimonial-content">
										<p>Feels so right it cant be wrong. Rockin' and rollin' all week long. Then one day he was shootin' at some food..</p>										
										<span>hendry smith villa</span>
									</div>
									<img src="static/IndexResources/images/testimonial1.jpg" alt="testimonial1" width="144" height="136"/>
								</div>
							</div>
						</div>
						<div class="col-md-2 col-sm-12">
							<div class="testimonial-blockquote-circle">	
								<span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
							</div>
						</div>
						<div class="col-md-5 col-sm-12 no-padding">
							<div class="testimonial-carousel-right">
								<div class="testimonial-box testimonial-right">
									<div class="testimonial-content">
										<p>These Happy Days are yours and mine Happy Days. So get a witch's shawl on a broomstick you can crawl on. </p>										
										<span>Mike Davidson</span>
									</div>
									<img src="static/IndexResources/images/testimonial2.jpg" alt="testimonial1" width="144" height="136"/>
								</div>
								<div class="testimonial-box testimonial-right">
									<div class="testimonial-content">
										<p>These Happy Days are yours and mine Happy Days. So get a witch's shawl on a broomstick you can crawl on. </p>										
										<span>Mike Davidson</span>
									</div>
									<img src="static/IndexResources/images/testimonial2.jpg" alt="testimonial1" width="144" height="136"/>
								</div>
								<div class="testimonial-box testimonial-right">
									<div class="testimonial-content">
										<p>These Happy Days are yours and mine Happy Days. So get a witch's shawl on a broomstick you can crawl on. </p>
										
										<span>Mike Davidson</span>
									</div>
									<img src="static/IndexResources/images/testimonial2.jpg" alt="testimonial1" width="144" height="136"/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div> --><!-- Testimonial Section /- -->
	
	
	
	
	<!-- Footer Main -->
	<%@include file="index_footer.jsp" %>
	<!-- END Footer Main -->
	
	
	
	<!-- AFTER LAUNCH ALERT BOX -->
	
		<!-- <div id="demo" class="modal fade in" role="dialog" style="display: none;padding-right: 17px;"> 
		  <div class="modal-dialog">
		    Modal content 
		    <div class="modal-content indexcont">
		      <div class="modal-header" style="border: 0;">
		        <button type="button" class="close indexmodal" data-dismiss="modal" onclick="loadAlertBox('Close')">&times;</button>
		      </div>
		      <div class="modal-body">
		        <p class="indxpopuppra">To view post and interesting case discussion kindly sign up/ sign in</p>
		        <div class="text-center">
		        <a href="userSignUp" class="btn btn-default lobtn">Register Now</a>
		        </div>
		      </div>	      
		    </div>
		
		  </div>
		</div>
		
		<div id="commentFade" style="z-index: 999;display: none;" onclick="loadAlertBox('Close')"></div>
		 -->
	<!-- EOF AFTER LAUNCH ALERT BOX -->
	
	
	
	
	<!-- BEFORE LAUNCH ALERT BOX -->
	
		<div id="demo" class="modal fade in" role="dialog" style="display:block;padding-right: 17px;"> <!--  -->
		  <div class="modal-dialog">
		    <!-- Modal content--> 
		    <div class="modal-content indexcont" style="margin-top:4%;">
		      <div class="modal-header" style="border: 0;">
		        <!-- <button type="button" class="close indexmodal" data-dismiss="modal" onclick="loadAlertBox('Close')">&times;</button> -->
		      </div>
		      <div class="modal-body">
		        <p class="indxpopuppra">10th RETNET MEETING </p>
		        <p class="indxpopuppra">9th & 10th June 2018,</p>
		        <p class="indxpopuppra">Venue- Clarks Exotica,Bangalore</p>
		        <div class="text-center">
		        <a href="partcipateOnEvent" class="btn btn-default lobtn">Register Now</a>
		        </div>
		        
		        <%  if (today.before(lastDate)) {%>
		        
			        <div class="text-center">
			        	<a href="preConferenceWorkShop" class="btn btn-default lobtn">Pre conference workshop</a>
			        </div>
		       
		       <% }%> 
		       
		         <div class="text-center">
		        	<a href="abstractSubmission" class="btn btn-default lobtn">Abstract Submission</a>
		        </div>
		      </div>	      
		    </div>
		
		  </div>
		</div>
		
		<div id="commentFade" style="z-index: 999;display:block;" ></div>
		
	<!-- EOF BEFORE LAUNCH ALERT BOX -->
	
	
	
	
	
	<%@include file="index_common_script.jsp" %>
    
    <!--  <script src='static/IndexResources/js/bootstrapcdn.3.3.js'></script>
	
	<script>
			$(window).load(function(){
			    setTimeout(function() { $('#demo').modal('show'); },500);
			});					
	</script> -->
	
	
</body>
</html>