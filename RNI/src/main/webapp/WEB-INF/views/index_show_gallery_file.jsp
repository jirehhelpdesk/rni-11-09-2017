<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<%          
      String fileCategory = (String)request.getAttribute("fileCategory");
	  String fileName = (String)request.getAttribute("fileName");
	  String fileType = (String)request.getAttribute("fileType");		
%>


<%if(fileCategory.equals("GalleryFile")){ %>

		<div id="commentLight">
		
			<a href="#">
			
			   <%if(fileType.equals("Image")){ %>
			
			  		 <img src="${pageContext.request.contextPath}<%="/previewGallaryFile?fileCategory=GalleryFile&fileName="+fileName+""%>"  alt="" style="width:100%;height:auto;">
			  
			   <%}else{ %>
					
					<video controls style="width:100%;height:100%;" >
					  	<source src="${pageContext.request.contextPath}<%="/previewGallaryFile?fileCategory=GalleryFile&fileName="+fileName+""%>" type="video/mp4" style="width:100%;height:100%;" >
					  	<source src="${pageContext.request.contextPath}<%="/previewGallaryFile?fileCategory=GalleryFile&fileName="+fileName+""%>" type="video/ogg" style="width:100%;height:100%;">
					</video>
				
			   <%} %>
			   	
			</a>
		
		</div>
		
		
		<div id="commentFade" onClick="viewGalleryFile('Close','GalleryFile','<%=fileName%>','<%=fileType%>')"></div> 

<%}else if(fileCategory.equals("PostFile")){ %>


<%}else if(fileCategory.equals("CaseFile")){ %>
		
		<%int caseId =  (Integer)request.getAttribute("caseId");%>
		
		<div id="commentLight">
		
			<a href="#">
			
			   <%if(fileType.equals("Image")){ %>
			
			  		 <img src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+fileName+"&caseId="+caseId+""%>"  alt="" style="width: 100%;height: 577px;">
			  
			   <%}else{ %>
					
					<video controls style="width:100%;height:100%;" >
					  	<source src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+fileName+"&caseId="+caseId+""%>" type="video/mp4" style="width:100%;height:100%;" >
					  	<source src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+fileName+"&caseId="+caseId+""%>" type="video/ogg" style="width:100%;height:100%;">
					</video>
				
			   <%} %>
			   	
			</a>
		
		</div>
		
		
		<div id="commentFade" onclick="showCaseFile('Close','<%=fileName%>','<%=fileType%>','<%=caseId%>')"></div> 
		
<%}else if(fileCategory.equals("CaseCommentFile")){ %>
		
		<%int caseId =  (Integer)request.getAttribute("caseId");%>
		<%int userId =  (Integer)request.getAttribute("commentUserid");%>
		
		<div id="commentLight">
		
			<a href="#">
			
			   <%if(fileType.equals("Image")){ %>
			
			  		 <img src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseCommentFile&fileName="+fileName+"&caseId="+caseId+"&userId="+userId+""%>"  alt="" style="width: 100%;height: 577px;">
			  
			   <%}else{ %>
					
					<video controls style="width:100%;height:100%;" >
					  	<source src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseCommentFile&fileName="+fileName+"&caseId="+caseId+"&userId="+userId+""%>" type="video/mp4" style="width:100%;height:100%;" >
					  	<source src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseCommentFile&fileName="+fileName+"&caseId="+caseId+"&userId="+userId+""%>" type="video/ogg" style="width:100%;height:100%;">
					</video>
				
			   <%} %>
			   	
			</a>
		
		</div>
		
		
		<div id="commentFade" onclick="showCaseCommentFile('Close','CaseCommentFile','Video','<%=fileName%>','<%=caseId%>','<%=userId%>')"></div> 
		

<%}else{} %>		







