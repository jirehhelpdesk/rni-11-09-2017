<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
 
 <div class="container-fluid">
              
              <div class="row">
                	
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">User result</h3>
                    </div>
                    <div class="card-body">
                     
                     
                    <c:if test="${!empty userSearchResult}">	
                      
		                   
                   		   <table class="table table-striped table-hover" data-toggle="table" data-pagination="true">
	                       
	                        <thead>
	                        
	                          <tr>
	                            <th>Sl.No</th>
	                            <th>Name</th>
	                            <th>Email Id</th>
	                            <th>Mobile</th>
	                            <th>Register Date</th>
	                            <th>Status</th>
	                            <th>Manage</th>
	                          </tr>
	                          
	                        </thead>
	                        
	                        
	                        <tbody>
	                         
	                           <%int i = 1; %> 
	                           <c:forEach items="${userSearchResult}" var="val">
												
									
									<c:set var="userId" value="${val.user_id}"/>
									<% int userId = (Integer)pageContext.getAttribute("userId"); %>
									
									<tr>
			                            
			                            <td scope="row"><%=i++%></td>
			                            <td>${val.first_name} ${val.last_name}</td>
			                            <td><span class="contentHide">${val.email_id}</span></td>
			                            <td>${val.mobile_no}</td>
			                            <td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.cr_date}" /></td>
			                            <td>${val.status}</td>
			                            
			                            <td><a href="#" onclick="showUserFromAdmin('${val.user_id}')">View</a></td>
			                            
			                        </tr>
									 	  
			                   </c:forEach>
	                          
	                          
	                        </tbody>
	                        
	                      </table>
                   			
                     </c:if>
                     
                     <c:if test="${empty userSearchResult}">	
                     
                     		<div style="color:red;text-align:center;font-size:18px;">
								As per the search criteria no result found.
							</div>
                     
                     </c:if> 
                      
                      
                      
                      
                      
                      
                    </div>
                  </div>
                </div>
                	
                	
              </div>
              
 </div>
 <script src="static/adminResource/js/bootstrap-table.js"></script>