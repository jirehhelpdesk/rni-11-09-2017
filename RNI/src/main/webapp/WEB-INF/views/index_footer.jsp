   
   
   
   
   
   <footer class="footer-main container-fluid no-padding">
		<!-- Container -->
		<div class="container">
			<div class="row">
			<aside class="col-md-5 col-sm-6 col-xs-6 widget widget_contactus">
			<a href="index">
				<img src="static/IndexResources/images/logo.png" alt="logo" class="footerlogo" width="200"></a>
				
				<p class="footerabout">RetNetIndia is a network of the retina specialists in india who help each other learn, hone their clinical and surgical skills for the greater benefit of patients and the field of vitreo retinal diseases and surgery.</p>
			
			</aside>
				<!-- Customer Service Widget -->
				<!-- <aside class="col-md-2 col-sm-6 col-xs-6 widget widget_customer_services">
					<h3 class="block-title">Member services</h3>
					<ul>
						<li><a title="Online Earning" href="#">Become a member</a></li>
						<li><a title="Materials" href="#">Materials</a></li>
						<li><a title="Presentation" href="#">Presentation</a></li>
						<li><a title="Printed materials" href="#">Printed materials</a></li>
						<li><a title="Curriculum" href="#">Concept documents</a></li>
						<li><a title="Literature" href="#">Service -1 </a></li>
						<li><a title="Help & Faq" href="#">Help & Faq</a></li>
					</ul>
				</aside> --><!-- Customer Service Widget /- -->
				
			
				<!-- ContactUs Widget -->
				<aside class="col-md-3 col-sm-6 col-xs-6 widget widget_contactus">
					<h3 class="block-title">Contact Us</h3>
					<div class="contactinfo-box">
						<i class="fa fa-map-marker"></i>
							<p>09 Design Street, RetNet, Bangalore, India.</p>
					</div>
					<div class="contactinfo-box">
						<i class="fa fa-phone"></i>
						<p>
							<a title="0112345678" href="tel:+0112345678">+080 888888888</a>
							<a title="0112355689" href="tel:+0112355689">+080 888888888</a>
						</p>
					</div>
					<div class="contactinfo-box">
						<i class="fa fa-envelope"></i>
						<p>
							<a href="mailto:Info@OurDomain.Com" title="Info@OurDomain.Com">Info@retnetind.Com</a>
							<a href="mailto:Support@OurDomain.Com" title="Support@OurDomain.Com">Support@retnetind.Com</a>
						</p>
					</div>
				</aside><!-- ContactUs Widget /- -->
				
				<!-- NewsLetter Widget -->
				<aside class="col-md-4 col-sm-12 col-xs-12 widget widget_newsletter">
					<h3 class="block-title">News Letter</h3>
					<p>Got a dream and we just know now we're gonna make our dream come true</p>
					<div class="input-group">
						<input type="text" name="newsemailId" id="newsemailId" placeholder="Enter email id to get latest news" class="form-control">
						<div id="emailSubErrId" class="errorStyle"></div>
						<span class="input-group-btn">
							<button type="button" title="Subscribe" class="btn" onclick="subscribeNews()">Go</button>
						</span>
					</div>
					<!-- <ul>
						<li><a title="Facebook" data-toggle="tooltip" href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a title="Twitter" data-toggle="tooltip" href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a title="Google Plus" data-toggle="tooltip" href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a title="Linkedin" data-toggle="tooltip" href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a title="Pinterest" data-toggle="tooltip" href="#"><i class="fa fa-pinterest-p"></i></a></li>
						<li><a title="Dribbble" data-toggle="tooltip" href="#"><i class="fa fa-dribbble"></i></a></li>
					</ul> -->
				</aside><!-- NewsLetter Widget /- -->
			</div>
		</div><!-- Container /- -->	
		
		<!-- Container -->
		<div class="container">
			<div class="footer-menu">
				<!-- Copyrights -->
				<div class="copyrights ow-pull-left">
					<p>&copy; 2018 | All rights reserved by Retnetindia</p>
				</div><!-- Copyrights /- -->
				<!-- Navigation -->
				<nav class="ow-pull-right foltleft">
					
					<div class="">
						<ul class="nav navbar-nav">
							<li><span class="dgnpra">Design &amp; Developed by <a target="_" href="http://www.jirehsol.co.in/" class="dvby">Jireh</a></span></li>
						</ul>
					</div>
				</nav><!-- Navigation /- -->
			</div><!-- Footer Menu /- -->
		</div><!-- Container /- -->
	</footer><!-- Footer Main /- -->
	
	
	
	
	<div id="commentPopUp" style="display:none;"></div>
	
	
	<div id="preloader" style="display: none;">
	
		<div class="cssload-container">
			<div class="cssload-whirlpool"></div>
		</div>
		
	</div>
	
	<div class="searchlist" id="userSearchListDiv" style="display:none;">
		<%@include file="index_user_search_list.jsp" %>
	</div>
	