<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Setting</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    
    
    <%@include file="admin_common_style.jsp" %>
    
    
  </head>
  
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
  
  
  <body onload="activeMenu('menu5','Admin')">
    
    
    
    <div class="page form-page">
     
     
      <!-- Main Navbar-->
      <%@include file="admin_header.jsp" %>
      
      
      
      <div class="page-content d-flex align-items-stretch"> 
        
        
         <%@include file="admin_side_bar.jsp" %>
        
        
        <div class="content-inner">
          <!-- Page Header-->
          
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Setting</h2>
            </div>
          </header>
          
         
         
          <div id="page-wrapper">
          <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                
                
                
                
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Update admin details</h3>
                    </div>
                    <div class="card-body">
                      
                    
                      
	                      <form:form class="form-horizontal" id="adminForm" modelAttribute="admin" method="post" action="saveAdmin">
	                        
	                        <div class="row">
	                          <label class="col-sm-3 form-control-label">Profile Inputs</label>
	                          <div class="col-sm-9">
	                            <div class="form-group-material">
	                              <input id="admin_name" name="admin_name" path="admin_name" type="text" value="${adminData.admin_name}" required class="input-material" placeholder="Admin full name">
	                              <i id="nameErrorId" class="help-block-none"></i>
	                            </div>
	                            
	                            <div class="form-group-material">
	                              <input id="admin_user_id" name="admin_user_id" path="admin_user_id"  type="text" value="${adminData.admin_user_id}"  required class="input-material" placeholder="Admin user name to access admin portal">
	                              <i id="usernameErrorId" class="help-block-none"></i>
	                            </div>
	                            <div class="form-group-material">
	                              <input id="admin_designation" name="admin_designation" path="admin_designation" value="${adminData.admin_designation}" type="text" required class="input-material" placeholder="Admin designation">
	                              <i id="designationErrorId" class="help-block-none"></i>
	                            </div>
	                            
	                          </div>
	                        </div>
	                        
	                        <input type="hidden" name="admin_id" path="admin_id" value="${adminData.admin_id}" />
	                        
	                        <div class="line"></div>
	                        <div class="form-group row">
	                          <div class="col-sm-4 offset-sm-3">
	                            <button type="reset" class="btn btn-secondary">Cancel</button>
	                            <button type="button" class="btn btn-primary" onclick="saveAdminDetails()">Save changes</button>
	                          </div>
	                        </div>
	                        
	                        
	                      </form:form>
                      
                      
                      
                      
                    </div>
                  </div>
                </div>
                
                
                
                
                
                <!-- Inline Form-->
                <div class="col-lg-12">                           
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Change password</h3>
                    </div>
                    <div class="card-body">
                     
                      <form class="form-inline">
                        
                        <div class="form-group">
                          <input id="curPassword" type="password" placeholder="Current Password" class="mx-sm-3 form-control">
                       	  <span  id="curErrorId" class="help-block-none"></span>
                        </div>
                        
                        <div class="form-group">
                          <input id="newPassword" type="password" placeholder="New Password" class="mx-sm-3 form-control">
                          <span  id="newErrorId" class="help-block-none"></span>
                        </div>
                        
                        <div class="form-group">
                          <input id="cnfPassword" type="password" placeholder="Confirm new Password" class="mx-sm-3 form-control form-control">
                       	  <span  id="cnfErrorId" class="help-block-none"></span>
                        </div>
                        
                        <div class="form-group">
                          <input type="button" value="Change password" class="mx-sm-3 btn btn-primary" onclick="updateAdminPassword()">
                        </div>
                        
                      </form>
                    </div>
                  </div>
                </div>
                
                
                
                
                
                
                
                
              </div>
            </div>
          </section>
          
          </div>
          
         <!-- Footer Main -->
			<%@include file="admin_footer.jsp" %>
		  <!-- END Footer Main -->
          
          
          
          
        </div>
      </div>
    </div>
    
    
    
    
    <%@include file="admin_common_script.jsp" %>
    
    
    
  </body>
</html>