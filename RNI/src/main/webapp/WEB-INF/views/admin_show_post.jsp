	<!-- 
	<link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/default.css">
	<link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/reset.css">
	<script src="static/IndexResources/js/jquery-3.2.1.min.js"></script>
	 -->
	<link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/slideshow.css">
	


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="card">
      
	  <div class="card-body" >
	  
	  <% int postId = 0;%>
	  <% String postStatus = "";%>
	  <%String feedOfDiscussion = "";%>  
	    		<c:forEach items="${showPost}" var="val">
										
					<c:set var="postId" value="${val.post_id}"/>
					<% postId = (Integer)pageContext.getAttribute("postId"); %>
					  
					<c:set var="userId" value="${val.user.user_id}"/>
					<% int userId = (Integer)pageContext.getAttribute("userId"); %>
					
					<c:set var="postStatus" value="${val.post_status}"/>
					<% postStatus = (String)pageContext.getAttribute("postStatus"); %>
					
					<c:if test = "${val.feed_of_discussion ne null}">							
							
							<c:set var="feedOfDiscussion" value="${val.feed_of_discussion}"/>
					        <% feedOfDiscussion = (String)pageContext.getAttribute("feedOfDiscussion"); %>
						
					</c:if>
					
					<input type="hidden" id="viewPostId" value="<%=postId%>"/> 										
					 
					<div class="">
				
					    <!-- Modal content-->
					    <div class="">
					      <div class="modal-header">
					        <h4 class="modal-title">${val.post_title}</h4>
					      </div>
					      
					      <div class="modal-body">
					        
					        <div class="row">
					        <div class="col-md-12">
						        
						        
						     <!-- LEFT SIDE DIV -->   
						        
						     <c:if test="${!empty val.postFile}">	   
						        
								      <div class="col-md-6 popupimg">
								        
								        
								        <!-- POST IMAGE SLIDER IF MULTIPLE IS AVAILABLE -->
								        
								        <div class="slideshow manageMulti">
																		
											<input type="radio" name="ss1" id="ss1-item-1" class="slideshow--bullet" checked="checked" />
											
											<% int i = 0; %>
											<c:forEach items="${val.postFile}" var="fileVal">
													
													<% i = i + 1; %>
													
													<c:set var="fileType" value="${fileVal.post_file_type}"/>
													<% String fileType = (String)pageContext.getAttribute("fileType"); %>
													
													<c:set var="fileName" value="${fileVal.post_file_name}"/>
													<% String fileName = (String)pageContext.getAttribute("fileName"); %>
							
													<%if(i==1){ %>
													
														<div class="slideshow--item manageMulti">
															
															<%if(fileType.equals("Image")) {%>
								        
													        	<img class="img-responsive" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>">
													        
													        <%}else if(fileType.equals("Video")) { %>
													        
													        	<span class="thickbox play-button-link">
														        	
														        	<span id="renderImagePopUp<%=postId%>" >
																								
																			<%
																				int pos = fileName.lastIndexOf(".");
																			    String renderfileName = fileName.substring(0, pos)+".png";
																            %>
																			<img  alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>">
																			<img class="play-button alignButtonOnPopUp" alt="" src="static/IndexResources/images/Play1Pressed.png" onclick="showVideo('PopUp','PostFile','<%=fileName%>','<%=userId%>','<%=postId%>')"/>
																	
																	</span>
																	
																	<span id="renderVideoPopUp<%=postId%>"> </span>
																	
																</span>
													        
													        <%}else if(fileType.equals("Pdf")) { %>
													        
													        	<a title="Load to click on the pdf" href="#" onclick="showPdfFile('NoPopUp','PostFile','<%=fileName%>','<%=userId%>','<%=postId%>')">
																					
																	<%
																		int pos = fileName.lastIndexOf(".");
																		String renderfileName = fileName.substring(0, pos)+".png";
																    %>
																	<img alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>" /> 
																	
																</a>
																
															<%}else if(fileType.equals("URL")) { %>
																
																<a class="viewmore" target="_"  title="Click to know more" href="<%=fileName%>">
																			
																	<iframe style="height:430px;width:471px;" src="<%=fileName%>" frameborder="0" allowfullscreen></iframe>
																	
																</a>
																			
													        <%}else{ %>		
													        
													        
													        <%} %>
															
															<label for="ss1-item-3" class="slideshow--nav slideshow--nav-previous leftLebelButton">Go to slide 3</label>
															<label for="ss1-item-2" class="slideshow--nav slideshow--nav-next rightLebelButton">Go to slide 2</label>
															
														</div>
													
													<%}else{ %>
													
														
														<input type="radio" name="ss1" id="ss1-item-<%=i%>" class="slideshow--bullet" />
														<div class="slideshow--item manageMulti">
															
															<%if(fileType.equals("Image")) {%>
								        
													        	<img class="img-responsive" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>">
													        
													        <%}else if(fileType.equals("Video")) { %>
													        	
													        	<span class="thickbox play-button-link">
														        	
														        	<span id="renderImagePopUp<%=postId%>" >
																								
																			 <%
																				int pos = fileName.lastIndexOf(".");
																			    String renderfileName = fileName.substring(0, pos)+".png";
																             %>
																			<img  alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>">
																			<img class="play-button alignButtonOnPopUp" alt="" src="static/IndexResources/images/Play1Pressed.png" onclick="showVideo('PopUp','PostFile','<%=fileName%>','<%=userId%>','<%=postId%>')"/>
																	
																	</span>
																	
																	<span id="renderVideoPopUp<%=postId%>"> </span>
																	
																</span>
																
													        <%}else if(fileType.equals("Pdf")) { %>
													        
													        	<a title="Load to click on the pdf" href="#" onclick="showPdfFile('PostFile','<%=fileName%>','<%=userId%>','<%=postId%>')">
																					
																	<%
																		int pos = fileName.lastIndexOf(".");
																		String renderfileName = fileName.substring(0, pos)+".png";
																    %>
																	<img alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>" height="100px"/> 
																	
																</a>
																	
													        <%}else{ %>		
													        
													        <%} %>
															
															<label for="ss1-item-<%=i-1%>" class="slideshow--nav slideshow--nav-previous leftLebelButton" >Go to slide <%=i-1%></label>
															<label for="ss1-item-<%=i+1%>" class="slideshow--nav slideshow--nav-next rightLebelButton" >Go to slide <%=i+1%></label>
														</div>
											
													<%} %>
											
											
											</c:forEach>		
													
											
										</div>
								        
								     
								    </div>
							        
					        
					        </c:if>
					        
					        <!-- END OF LEFT SIDE DIV -->
					        
					        
					        <!-- RIGHT SIDE CONTENT DIV -->
					        
					        <c:if test="${!empty val.postFile}">	
					        
					        <div class="col-md-6 popupdecs boxscroll">
							        
							      <div class="simplebar myElement">
							
							</c:if>	 
							
							<c:if test="${empty val.postFile}">	
					        
					        <div class="col-md-12 popupdecs boxscroll">
							        
							      <div class="simplebar myElement" style="width:100%;">
							
							</c:if>	 
										  
										  
										   <div class="parent">
									       
									        <div class="popst-desc">
									        	<p>${val.post_desc}</p>
									        </div>
									        
									        
									        
									        <div class="post-comment">
									        
									        <c:if test="${!empty showComment}">	
									        
										        <section class="comment-list">
										        
										        <c:forEach items="${showComment}" var="cmt">
										          
											          <article class="row">
											            
											            
											            <div class="col-md-12 col-sm-12">
											              <div class="panel panel-default">
											                <div class="panel-body">
											                  <header>
											                    
											                    
											                    
											                    <div class="comment-user">
											                    
													                      <c:set var="userId" value="${cmt.user.user_id}"/>
																 		  <% int userIden = (Integer)pageContext.getAttribute("userId"); %>
												 						  <% String profilePhoto = ""; %>
												 						  
													                      
													                      <c:if test="${!empty cmt.user.profile}">
													                      		<c:set var="profilePhoto" value="${cmt.user.profile.profile_photo}"/>
																 				<% profilePhoto = (String)pageContext.getAttribute("profilePhoto"); %>
																 				
																 				<%if(!profilePhoto.equals("")){ %>
													                      			<img src="${pageContext.request.contextPath}<%="/previewProfilePhoto?fileName="+profilePhoto+"&userId="+userIden+""%>" style="width:34px;height:34px;border-radius:5px;"/>
													                     		<%}else{ %>
													                     			<!-- <i class="fa fa-user"></i>  -->
													                     			<img src="static/IndexResources/images/user.png" style="width:34px;height:34px;border-radius:5px;background-color:#21a5b2;padding: 2px;"/>
													                     		<%} %>
													                      </c:if>
													                      
													                      <c:if test="${empty cmt.user.profile}">
													                      		<img src="static/IndexResources/images/user.png" style="width:34px;height:34px;border-radius:5px;background-color:#21a5b2;padding: 2px;"/>
													                      </c:if>
													                      
													                      ${cmt.user.first_name} ${cmt.user.last_name}
													                      
											                    </div>
											                    
											                    
											                    <time class="comment-date" ><i class="fa fa-clock-o"></i> <fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${cmt.comment_cr_date}" /></time>
											                 
											                  </header>
											                  
											                  
											                  
											                  
											                  <div class="col-md-12 no-padding">
												                  	
												                  	<c:set var="commentContain" value="${cmt.comment_contain}"/>
																	<% String commentContain = (String)pageContext.getAttribute("commentContain"); %>
																							
												                  		
																	<%if(!commentContain.equals("No")) {%>
																			
												                      		  
												                      		  <!--Start Comment Image  -->
												                      		  
														                      <div class="col-md-4 no-padding">
																                  
																                   <!-- Design for Comment File is available  -->
															                  
															                     
																	                    <div class="commentFile commntimg">
																	                  	 
																		                  	<c:forEach items="${cmt.commentFile}" var="cmtFile"> 	
																		                  		
																		                  		<c:set var="commentFileType" value="${cmtFile.comment_file_type}"/>
																								<% String commentFileType = (String)pageContext.getAttribute("commentFileType"); %>
																								
																								<c:set var="commentFile" value="${cmtFile.comment_file}"/>
																								<% String commentFile = (String)pageContext.getAttribute("commentFile"); %>
																								
																		                  	    <%if(commentFileType.equals("Image")) {%>
																	        
																						        	<img  src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=CommentFile&fileName="+commentFile+"&userId="+userId+"&postId="+postId+""%>"   onclick="showCommentFile('Open','CommentFile','<%=commentFile%>','<%=userId%>','<%=postId%>','<%=commentFileType%>')">
																						        
																						        <%}else if(commentFileType.equals("Video")) { %>
																						            
																						            <span class="thickbox play-button-link" onclick="showCommentFile('Open','CommentFile','<%=commentFile%>','<%=userId%>','<%=postId%>','<%=commentFileType%>')">
																						            
																							            <span id="renderImageCmnt<%=postId%>" >
																												
																												<%
																													int pos = commentFile.lastIndexOf(".");
																												    String renderfileName = commentFile.substring(0, pos)+".png";
																									            %>
																									            
																												<img  alt="<%=commentFile%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=CommentFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>">
																												<img class="play-button alignPlayInPopUp" alt="" src="static/IndexResources/images/Play1Pressed.png" onclick="showVideo('Comment','CommentFile','<%=commentFile%>','<%=userId%>','<%=postId%>')"/>
																												
																										</span>
																										
																										<span id="renderVideoCmnt<%=postId%>">
																												
																										
																										</span>
																								
																									</span>
																						        
																						        <%}else if(commentFileType.equals("Pdf")) { %>
																						        
																						        	<a title="Load to click on the pdf" href="#"  onclick="showPdfFile('NoPopUp','CommentFile','<%=commentFile%>','<%=userId%>','<%=postId%>')"> 
																							
																										<%
																											int pos = commentFile.lastIndexOf(".");
																											String renderfileName = commentFile.substring(0, pos)+".png";
																									    %>
																										<img alt="<%=commentFile%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=CommentFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>" /> 
																										
																									</a>
																										
																						        <%}else{ %>		
																						        
																						        
																						        <%} %>
																	        				
																	        				     <br></br>
																	        				
																	        				</c:forEach>
																        				
																	                   </div>
															                     
														                      </div>
														                      
														                      <!--End Comment Image  -->
														                      
														                      <!--Start Comment Post  -->
														                      
														                      <div class="col-md-8">
																                  
																	                  <div class="comment-post">
																	                    <p>
																	                      ${cmt.comment_message}
																	                    </p>
																	                  </div>
																                  
														                     </div>
														                     
														                     <!--End Comment Post  -->
														                 
														                 
														                  
												                        <%}else{%>
												                  
														                  
														                  <div class="row">
												                      		  
														                      <!--Start Comment Post  -->
														                      
														                      <div class="col-md-12">
																                  
																                  <div class="comment-post">
																                    <p>
																                      ${cmt.comment_message}
																                    </p>
																                  </div>
																                  
														                     </div>
														                     
														                     <!--End Comment Post  -->
														                 
														                 </div>
												                  
												                  
												                      <%} %>
												                  
												                  
											                </div>
											                  
											                </div>
											                
											                
											              </div>
											            </div>
											          </article>
										          
										          </c:forEach>
										          
										        </section>
										     
										    </c:if>
										    
										    <c:if test="${empty val.comment}">	
										    
										    		<div class="recent-content" style="color:red;text-align:center;font-size:18px;">
														As of now no one didn't comment on your post.
													</div>
										    		
										    </c:if> 
										        
									        </div>
									        
									        
									        
									        
									        </div>
							        </div>
					        </div>
					        
					        
					        <!-- END OF LEFT SIDE DIV -->
					        
					        </div>
					        
					        </div>
					      </div>
					      
					    </div>
					
					  </div>										
					 										
				</c:forEach>
	    		
	  				
	  				<div class="line"></div>
	  				
	  				<div class="form-group row">
                      
                      <div class="col-sm-12 offset-sm-0">
                      	
                      	<%if(feedOfDiscussion.equals("Yes")){ %>
                      			
                      			<label><input type="checkbox" id="feedOfDiscussion" checked name="feedOfDiscussion" onclick="manageCaseOfDiscussion('No','<%=postId%>')"/> Mark as Feed of the discussion</label>	
                      	
                      	<%}else{ %>
                      		
                      			<label><input type="checkbox" id="feedOfDiscussion" name="feedOfDiscussion" onclick="manageCaseOfDiscussion('Yes','<%=postId%>')"/> Mark as Feed of the discussion</label>	
                      	
                      	<%} %>
                      	
                      </div>
                      
                      <div class="col-md-3 offset-md-9 col-sm-5 offset-sm-7">
                        <button type="button" class="btn btn-secondary" onclick="closePostDiv('postDetails')">Close</button>
                        
                        <%if(postStatus.equals("Active")){ %>
                        	<button type="button" class="btn btn-primary" onclick="blockUserPost('<%=postId%>','Deactive')">Block</button>
                     	<%}else{ %>
                     		<button type="button" class="btn btn-primary" onclick="blockUserPost('<%=postId%>','Active')">Unblock</button>
                     	<%} %>
                      </div>
                      
                    </div>
	  				
	  </div>
  
</div>






<script src="static/IndexResources/js/custome_scrollbar.js"></script>

