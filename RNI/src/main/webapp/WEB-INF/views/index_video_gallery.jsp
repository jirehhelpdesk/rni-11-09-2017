<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Video Gallery</title>


<%@include file="index_common_style.jsp"%>
<style>
</style>
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">



	<!-- Loader And Header /- -->

	<%@include file="index_header.jsp"%>

	<!-- END of Loader and Header -->
	<div class="container-fluid page-banner about no-padding">
		<div class="container">
			<div class="banner-content-block">
				<div class="banner-content">
					<h3>Our Videos</h3>
				</div>
			</div>
		</div>
		<!-- 
		<div class="section-padding"></div> -->
	</div>
	<!-- Schedule Section -->
	<div class="imgallery">
		<div class="container-fluid no-padding schedule-section gallerybg">
			<div class="container">
				<div class="schedule-block">
					<div id="container" class="clearfix">
						<div id="content" class="defaults">
							<!-- navigation holder -->
							<div class="holder"></div>


							<!-- item container -->
							 <ul id="itemContainer">
      
							      <c:forEach items="${videoPost}" var="val">
																	
																	
											<% String fileType = "";%>
											<% String fileName = "";%>
						
											<% int n = 1;%>
							
											<c:forEach items="${val.postFile}" var="filVal">
												
												<%if(n==1){ %>
												
													<c:set var="fileType" value="${filVal.post_file_type}"/>
													<% fileType = (String)pageContext.getAttribute("fileType"); %>
													
													<c:set var="fileName" value="${filVal.post_file_name}"/>
													<% fileName = (String)pageContext.getAttribute("fileName"); %>
												    
											    <%} %>
												
												<%n = n + 1; %>
												
											</c:forEach>
											
											
											<c:set var="postId" value="${val.post_id}"/>
											<% int postId = (Integer)pageContext.getAttribute("postId"); %>
											  
											<c:set var="userId" value="${val.user.user_id}"/>
											<% int userId = (Integer)pageContext.getAttribute("userId"); %>
											
							      
							     			<div class="col-md-3">
							     					
							     					<li>
							     					  <div class="thumbnail">
	     						
								     						<div class="caption">
											                    <p class="titl">${val.post_title}</p>
											                    <p class="text-center"><a href="" class="label label-default vimore" data-toggle="modal" data-target="#gallerypopup" href="#" onclick="viewIndexUserPost('${val.post_id}')">View more</a>
											                    </p>
											                </div>
								     						<video width="" height="" controls="">
															  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/mp4">
															  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/ogg">
															</video>
	     					                            </div>
							     					
							     					</li>
							     			
							     			</div>
							     			
							      </c:forEach>
							      
							    
						   </ul>

						</div>
						<!--! end of #content -->
					</div>
					<!--! end of #container -->
				</div>
			</div>
			<div class="section-padding"></div>
		</div>
		<!-- Schedule Section /- -->
	</div>








	<!-- Footer Main -->
	<%@include file="index_footer.jsp"%>
	<!-- END Footer Main -->


	<%@include file="index_common_script.jsp"%>

	<!-- Image Popup Modal -->
	
	
	<div id="gallerypopup" class="modal fade postpage" role="dialog">
		
	</div>


</body>
</html>