<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<%          
  String fileCategory = (String)request.getAttribute("fileCategory");
  String fileName = (String)request.getAttribute("fileName");
  String userId = (String)request.getAttribute("userId");
  String postId = (String)request.getAttribute("postId");
  String fileType = (String)request.getAttribute("fileType");		
%>

<%if(fileCategory.equals("PostFile")) {%>

<div id="commentLight">

	<a href="#">
	
	   <%if(fileType.equals("Image")){ %>
	
	  		 <img src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>"  alt="" class="commentimg">
	  
	   <%}else{ %>
			
			<video controls  class="commentimg" >
			  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/mp4" class="commentimg" >
			  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/ogg" class="commentimg">
			</video>
		
	   <%} %>
	   	
	</a>

</div>


<div id="commentFade" onClick="showCommentFile('Close','CommentFile','<%=fileName%>','<%=userId%>','<%=postId%>')"></div> 


<%}%>

<%if(fileCategory.equals("CommentFile")) {%>

<div id="commentLight">

	<a href="#">
	
	   <%if(fileType.equals("Image")){ %>
	
	  		 <img src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=CommentFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>"  alt="" class="commentimg">
	  
	   <%}else{ %>
			
			<video controls  class="commentimg" >
			  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=CommentFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/mp4" class="commentimg" >
			  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=CommentFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/ogg" class="commentimg">
			</video>
		
	   <%} %>
	   	
	</a>

</div>


<div id="commentFade" onClick="showCommentFile('Close','CommentFile','<%=fileName%>','<%=userId%>','<%=postId%>')"></div> 


<%}%>