<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Reset Password</title>

<%@include file="index_common_style.jsp"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="static/textEditor/css/style.css" rel="stylesheet"
	type="text/css" />
<script language="javascript" src="static/textEditor/js/calender.js"></script>
<link href="static/textEditor/css/calender_style.css" rel="stylesheet"
	type="text/css">


</head>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">

	<!-- Loader And Header /- -->

	<%@include file="index_header.jsp"%>

	<!-- END of Loader and Header -->

	<div class="container-fluid  no-padding">
		<div class="container">
			<div class="section-padding">
				<div class="cd-tabs">
					<nav>
					<h4 class="cd-tabs-navigation resetHeading">

						Reset Password

					</h4>
					</nav>

				
					<ul class="cd-tabs-content">

						<li class="selected">
							
							<form:form class="booking-form-block" name="login-form" id="resetPasswordForm" >

								<div class="col-md-12 no-padding">
									<div class="col-md-12 no-padding">
										<div class="form-fild">
											<input type="password" name="newPassword" id="newPassword" placeholder="New Password">
											<div id="newPwErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-12 no-padding">
										<div class="form-fild">
											<input type="password" name="cnfPassword" id="cnfPassword" placeholder="Confirm Password">
											<div id="cnfPwErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>
								
								<input type="hidden" value="<%=(String)request.getAttribute("requestedEmailID")%>" id="requestedEmailID" name="requestedEmailID" />

								<div class="col-md-12 no-padding">

									<div class="col-md-7">
										<span class="error"></span>
										<button type="button" class="btn loginbtn" onclick="saveResetedPassword()">Submit</button>
									</div>

									<div class="col-md-5">
										<div class="form-fild forgotpsw">
											<a href="userRegister">Sign In</a>
										</div>
									</div>

								</div>


							</form:form>
						</li>

					</ul>
				</div>
				<!-- end cd-tabs -->

			</div>
		</div>
	</div>







	<!-- Footer Main -->
	<%@include file="index_footer.jsp"%>
	<!-- END Footer Main -->









	<%@include file="index_common_script.jsp"%>

	<script>
		'use strict';

		window.addEventListener('load', windowLoaded, false);

		function windowLoaded() {
			var tabs = document.querySelectorAll('.cd-tabs')[0], login = document
					.querySelectorAll('a[data-content=\'login\']')[0], signup = document
					.querySelectorAll('a[data-content=\'signup\']')[0], tabContentWrapper = document
					.querySelectorAll('ul.cd-tabs-content')[0], currentContent = document
					.querySelectorAll('li.selected')[0];

			login.addEventListener('click', clicked, false);
			signup.addEventListener('click', clicked, false);

			function clicked(event) {
				event.preventDefault();

				var selectedItem = event.currentTarget;
				if (selectedItem.className === 'selected') {
					// ...       
				} else {
					var selectedTab = selectedItem.getAttribute('data-content'), selectedContent = document
							.querySelectorAll('li[data-content=\''
									+ selectedTab + '\']')[0];

					if (selectedItem == login) {
						signup.className = '';
						login.className = 'selected';
					} else {
						login.className = '';
						signup.className = 'selected';
					}

					currentContent.className = '';
					currentContent = selectedContent;
					selectedContent.className = 'selected';

				}
			}

			var inputs = document.querySelectorAll('input');
			for (var i = 0; i < inputs.length; i++) {
				inputs[i].addEventListener('focus', inputFocused, false);
			}

			function inputFocused(event) {
				var label = document.querySelectorAll('label[for=\''
						+ this.name + '\']')[0];
				label.className = 'focused';
			}
		}
	</script>


</body>
</html>