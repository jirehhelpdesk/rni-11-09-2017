<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Case of discussion</title>


<%@include file="index_common_style.jsp"%>


<script>
		
		$(document).ready(function(){
		    $(".participate").click(function(){
		        $(".participateform").slideToggle("");
		    });
		});
		
	</script>

</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<body data-offset="200" data-spy="scroll" data-target=".ow-navigation" onload="activeMenu('menu4','User')">



	<!-- Loader And Header /- -->

	<%@include file="index_header.jsp"%>

	<!-- END of Loader and Header -->
	<div class="container-fluid page-banner about no-padding">
		<div class="container">
			<div class="banner-content-block">
				<div class="banner-content">
					<h3>View Case's</h3>
				</div>
			</div>
		</div>
		<!-- 
		<div class="section-padding"></div> -->
	</div>

	<div class="eventlist">
		<div
			class="container-fluid eventlist upcoming-event latest-blog no-padding">

			<div class="manageSectionPadding section-padding"></div>

			<div class="container">


				<div class="row">

					<div class="col-md-3 col-sm-12 col-xs-12 widget-area">

						<%@include file="user_side_menu.jsp"%>

					</div>


					<div class="col-md-9 col-sm-12 col-xs-12 content-area">
					
					<div class="widget-area mypostfeed" style="width:100%;">
					
						<aside class="widget widget_recent user-eventmrgn">
						
							<div class="widget-title">
								<span class="icon icon-Briefcase"></span>
								<h3>Case of the month
								</h3>
								
							</div>
							
							<div class="recent-block user-eventpadng">
							
						
						    <c:if test="${!empty caseList}">		
							
								<c:forEach items="${caseList}" var="val">
										
										<c:set var="caseId" value="${val.case_id}"/>
										<% int caseId = (Integer)pageContext.getAttribute("caseId"); %>
																		 
										<article class="type-post">
							
										  <!-- <div class="entry-meta">
											<div class="" style="background-color:#21a5b2;color:#fff;">
												<a href="#" title=""><i class="fa fa-calendar" aria-hidden="true"></i><span>Case of November</span></a>
											</div>
										  </div> -->
											
										  <div class="entry-cover">
											 
											 <div class="portfolio_block columns3 pretty" data-animated="fadeIn">
												
												<% int i = 0; %>
												<c:forEach items="${val.caseFile}" var="caseFile">
													
													<% i = i + 1; %>
													
													<c:set var="fileName" value="${caseFile.file_name}"/>
													<% String fileName = (String)pageContext.getAttribute("fileName"); %>
													
													<c:set var="fileType" value="${caseFile.file_type}"/>
													<% String fileType = (String)pageContext.getAttribute("fileType"); %>
													
													<%if(i==1){ %>
													
														<div class="element gall branding">
														
															 <%if(fileType.equals("Image")) {%>
															 	
																<a href="caseDetails?caseId=<%=caseId%>" class="viewmore" title="View Case images" >
																
																    <img  class="usereventlistimg" src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+fileName+"&caseId="+caseId+""%>" alt="history" width="310" height="246" />
																
																</a>
															
															 <%} %>	
															 
															 <%if(fileType.equals("Video")) {%>
															 	
															 	<%
																	int pos = fileName.lastIndexOf(".");
																    String renderfileName = fileName.substring(0, pos)+".png";
													            %>
													            
																<a href="caseDetails?caseId=<%=caseId%>" class="viewmore" title="View Case images" >
																
																    <img  class="usereventlistimg" src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+renderfileName+"&caseId="+caseId+""%>" alt="history" width="310" height="246" />
																
																</a>
															
															 <%} %>	
															 
															 <%if(fileType.equals("Pdf")) {%>
															 	
															 	 <%
																	int pos = fileName.lastIndexOf(".");
																    String renderfileName = fileName.substring(0, pos)+".png";
													             %>
																		            
																 <a href="caseDetails?caseId=<%=caseId%>" class="viewmore" title="View Case images" >
																
																    <img  class="usereventlistimg" src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+renderfileName+"&caseId="+caseId+""%>" alt="history" width="310" height="246" />
																
																 </a>
															
															 <%} %>	
															 
														</div>
													
													<%}%>
													
												</c:forEach>
												
											</div>
			
										</div>
										
										
										<div class="entry-block knowmore">
										<div class="show-more-div-height">
											<div class="entry-title"><h3>${val.case_title}</h3>
											</div>
											<div class="entry-meta">
												
												<span>Case Added date : <fmt:formatDate pattern="dd/MM/yyyy" value="${val.case_cr_date}" /></span>
												
											</div>
											
											<div class="entry-content" style="height:100px;">
												<p>${val.case_desc}</p>
											</div>
											</div>
											<a href="caseDetails?caseId=<%=caseId%>" class="viewmre buttoncs btn mrgnknowmore" title="Learn More" onclick="">Read More</a>
											
										</div>
										
									</article>
								
								</c:forEach>
								
							</c:if>
							
							
							<c:if test="${empty caseList}">	
							
									<div class="recent-content" style="color:red;text-align:center;font-size:18px;">
										As of now RetNet India didn't added any case.
									</div>
								
							</c:if>
							
						</div>
							</aside>
							</div>
					</div>



				</div>
			</div>
			<div class="section-padding"></div>
		</div>

	</div>


	<!-- Footer Main -->
	<%@include file="index_footer.jsp"%>
	<!-- END Footer Main -->

	
	<div id="postpopup" class="modal fade postpage" role="dialog">
  
  
  
	</div>
	
	

	<%@include file="index_common_script.jsp"%>



<script src="static/adminResource/js/jquery.nicescroll.min.js"></script>


<script>

  //Hide Overflow of Body on DOM Ready //

     $(document).ready(
		function() {
		$(".popupdecs").niceScroll({cursorcolor:"#fff",autohidemode:false, zindex: 999});
		}
		);
		$(function() {  
		$('.reference-long').click(function(){
		    $(".popupdecs").getNiceScroll().resize();
		    $(".popupdecs").niceScroll({cursorcolor:"#fff",autohidemode:false, zindex: 999});
		  });
		});


</script>


</body>
</html>