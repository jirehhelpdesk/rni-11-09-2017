<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Event Overview</title>

	
	<%@include file="index_common_style.jsp" %>
	
	<style>
	.eventoverview .carousel-inner>.item>a>img, .carousel-inner>.item>img,
	.img-responsive, .thumbnail a>img, .thumbnail>img {
	display: block;
	max-width: 100%;
    height: 400px !important;
    width: 100%;
}
	</style>
	
	<link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/default.css">
	<link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/reset.css">
	<link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/slideshow.css">
	
</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
   
   
<body data-offset="200" data-spy="scroll" data-target=".ow-navigation" onclick="activeMenu('menu3','User')">
	
	
	
	<!-- Loader And Header /- -->
	
	<%@include file="index_header.jsp" %>
	
	<!-- END of Loader and Header -->
	
	
	
	
	
	
	<!-- PageBanner -->
<div class="container-fluid page-banner about no-padding">
			<div class="container">
				<div class="banner-content-block">
					<div class="banner-content">
						<h3>Event Overview</h3>
					</div>
				</div>
			</div><!-- 
		<div class="section-padding"></div> -->
	</div>
<!-- PageBanner /- --> 

<!-- Main Section -->
<div class="eventoverview">
		<div
			class="container-fluid eventsingle eventlist upcoming-event latest-blog our-history no-padding">

			<div class="manageSectionPadding section-padding"></div>

			<div class="container">


				<div class="row">

					<div class="col-md-3 col-sm-12 col-xs-12 widget-area">

						<%@include file="user_side_menu.jsp"%>

					</div>
					
					

					<c:forEach items="${eventDetails}" var="val">
								
							<c:set var="eventId" value="${val.event_id}"/>
							<% int eventId = (Integer)pageContext.getAttribute("eventId"); %>
							
							<c:set var="eventCode" value="${val.event_code}"/>
							<% String eventCode = (String)pageContext.getAttribute("eventCode"); %>
							
					
							
							<div class="col-md-9 col-sm-12 content-area">
								
								<article class="type-post">
									
									<div class="entry-cover overimgslider">
									
										<div class="slideshow">
												
												<input type="radio" name="ss1" id="ss1-item-1" class="slideshow--bullet" checked="checked" />
												
												<% int i = 0; %>
												<c:forEach items="${val.image}" var="image">
														
														<% i = i + 1; %>
														
														<c:set var="fileName" value="${image.file_name}"/>
														<% String fileName = (String)pageContext.getAttribute("fileName"); %>
												
														
														<%if(i==1){ %>
														
															<div class="slideshow--item">
																<img src="${pageContext.request.contextPath}<%="/previewEventMedia?fileName="+fileName+"&eventId="+eventId+""%>" class="img-responsive"/>
																<label for="ss1-item-3" class="slideshow--nav slideshow--nav-previous">Go to slide 3</label>
																<label for="ss1-item-2" class="slideshow--nav slideshow--nav-next">Go to slide 2</label>
															</div>
														
														<%}else{ %>
														
															
															<input type="radio" name="ss1" id="ss1-item-<%=i%>" class="slideshow--bullet" />
															<div class="slideshow--item">
																<img src="${pageContext.request.contextPath}<%="/previewEventMedia?fileName="+fileName+"&eventId="+eventId+""%>"  class="img-responsive"/>
																<label for="ss1-item-<%=i-1%>" class="slideshow--nav slideshow--nav-previous">Go to slide <%=i-1%></label>
																<label for="ss1-item-<%=i+1%>" class="slideshow--nav slideshow--nav-next">Go to slide <%=i+1%></label>
															</div>
												
														<%} %>
												
												
												</c:forEach>		
														
												
											</div>
	
									</div>
									
									
									<div class="entry-block">
										<div class="entry-title">
											<h3>${val.event_title}</h3>
										</div>
										<div class="entry-meta">
											<div class="post-date eventdate">
												Date<span>Location</span>
											</div>
											<div class="post-metablock" style="width:100%;">
												<div class="post-time">
													<span class="icon icon-Time"></span>
													<span><fmt:formatDate pattern="dd/MM/yyyy" value="${val.event_start_date}" /> - <fmt:formatDate pattern="dd/MM/yyyy" value="${val.event_end_date}" /></span>
													
												</div>
												<div class="post-location">
													<span class="icon icon-Pointer"></span>
													<span>${val.event_location}</span>
												</div>
											</div>
																		
										</div>
										<div class="entry-content">
											<p>${val.event_desc}</p>
										</div>
									</div>
									
									
									
								</article>
								
								
								<!-- Map -->
								
								
								
								<div class="map" style="width:100%;">
	
									${val.event_location_map}
									
								</div><!-- Map /- -->
								
								
								
							</div>
							
							
							
					</c:forEach>
					
					
				</div>
			</div>
			<div class="section-padding"></div>
		</div>

	</div>
	
	
	
	
	<!-- Footer Main -->
	<%@include file="index_footer.jsp" %>
	<!-- END Footer Main -->
	
	
	<%@include file="index_common_script.jsp" %>

	
	
</body>
</html>