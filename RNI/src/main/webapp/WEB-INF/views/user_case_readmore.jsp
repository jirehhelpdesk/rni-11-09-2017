<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Case Details</title>

	
	<%@include file="index_common_style.jsp" %>
	
	
	<style>
	.eventoverview .carousel-inner>.item>a>img, .carousel-inner>.item>img,
	.img-responsive, .thumbnail a>img, .thumbnail>img {
	display: block;
	max-width: 100%;
    height: 400px !important;
    width: 100%;
}
	</style>
	
	<link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/default.css">
	<link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/reset.css">
	<link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/slideshow.css">
</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
   
   
<body data-offset="200" data-spy="scroll" data-target=".ow-navigation" onclick="activeMenu('menu4','User')">
	
	
	
	<!-- Loader And Header /- -->
	
	<%@include file="index_header.jsp" %>
	
	<!-- END of Loader and Header -->
	
	
	
	
    <!-- PageBanner -->
	
	<div class="container-fluid page-banner about no-padding">
			<div class="container">
				<div class="banner-content-block">
					<div class="banner-content">
						<h3>Case Details</h3>
					</div>
				</div>
			</div><!-- 
		<div class="section-padding"></div> -->
	</div>
	
  <!-- PageBanner /- --> 

  
  <!-- Main Section -->
  <div class="eventoverview">
		
		<div class="container-fluid eventsingle eventlist upcoming-event latest-blog our-history no-padding">

			<div class="manageSectionPadding section-padding"></div>

			<div class="container">

				<div class="row">
				
					<div class="col-md-3 col-sm-12 col-xs-12 widget-area">

						<%@include file="user_side_menu.jsp"%>

					</div>
					
					<c:forEach items="${caseDetails}" var="val">
								
							<c:set var="caseId" value="${val.case_id}"/>
							<% int caseId = (Integer)pageContext.getAttribute("caseId"); %>
							
					
							<div class="col-md-9 col-sm-12 col-xs-12 content-area caseof-overview">
								
								<article class="type-post">
									
									<c:if test="${!empty val.caseFile}">		
									
										<div class="entry-cover overimgslider">
										
											<div class="slideshow">
													
													<input type="radio" name="ss1" id="ss1-item-1" class="slideshow--bullet" checked="checked" />
													
													<% int i = 0; %>
													<c:forEach items="${val.caseFile}" var="caseFile">
															
															<% i = i + 1; %>
															
															<c:set var="fileName" value="${caseFile.file_name}"/>
															<% String fileName = (String)pageContext.getAttribute("fileName"); %>
															
															<c:set var="fileType" value="${caseFile.file_type}"/>
															<% String fileType = (String)pageContext.getAttribute("fileType"); %>
													
															
															<%if(i==1){ %>
															
																		<%if(fileType.equals("Image")) {%>
																		
																			<div class="slideshow--item">
																				<img src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+fileName+"&caseId="+caseId+""%>" class="img-responsive" onclick="showCaseFile('Open','<%=fileName%>','<%=fileType%>','<%=caseId%>')"/>
																				<label for="ss1-item-3" class="slideshow--nav slideshow--nav-previous">Go to slide 3</label>
																				<label for="ss1-item-2" class="slideshow--nav slideshow--nav-next">Go to slide 2</label>
																			</div>
																		
																		<%}%>
																		
																		<%if(fileType.equals("Video")) { %>
																			
																			<div class="slideshow--item">
																			
																				<span class="thickbox play-button-link" onclick="showCaseFile('Open','<%=fileName%>','<%=fileType%>','<%=caseId%>')">
								            
																					<%
																						int pos = fileName.lastIndexOf(".");
																					    String renderfileName = fileName.substring(0, pos)+".png";
																		            %>
																		            
																					<img class="img-responsive" alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+renderfileName+"&caseId="+caseId+""%>">
																					<img class="play-button alignPlayInPopUp commentplbtn" alt="" src="static/IndexResources/images/Play1Pressed.png" onclick="showCaseFile('Open','<%=fileName%>','<%=fileType%>','<%=caseId%>')"/>
																					
																					
																				</span> 
																				<label for="ss1-item-3" class="slideshow--nav slideshow--nav-previous">Go to slide 3</label>
																				<label for="ss1-item-2" class="slideshow--nav slideshow--nav-next">Go to slide 2</label>
																			</div>
																			
																		<%} %>
																		
																		<%if(fileType.equals("Pdf")) { %>
																			
																			<div class="slideshow--item">
																			
																				  <%
																					 int pos = fileName.lastIndexOf(".");
																				     String renderfileName = fileName.substring(0, pos)+".png";
																	               %>
																	        	  <a title="Load to click on the pdf" href="#" onclick="showCasePdfFile('CaseFile','<%=fileName%>','<%=caseId%>')"> 
																		
																					<img  class="img-responsive" alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+renderfileName+"&caseId="+caseId+""%>" /> 
																					
																				  </a>
																				  
																				  <label for="ss1-item-3" class="slideshow--nav slideshow--nav-previous">Go to slide 3</label>
																				  <label for="ss1-item-2" class="slideshow--nav slideshow--nav-next">Go to slide 2</label>
																			</div>
																			
																		<%} %>
															<%}else{ %>
															
																		<%if(fileType.equals("Image")) {%>
																		
																			<input type="radio" name="ss1" id="ss1-item-<%=i%>" class="slideshow--bullet" />
																			<div class="slideshow--item">
																				<img src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+fileName+"&caseId="+caseId+""%>"  class="img-responsive" onclick="showCaseFile('Open','<%=fileName%>','<%=fileType%>','<%=caseId%>')"/>
																				<label for="ss1-item-<%=i-1%>" class="slideshow--nav slideshow--nav-previous">Go to slide <%=i-1%></label>
																				<label for="ss1-item-<%=i+1%>" class="slideshow--nav slideshow--nav-next">Go to slide <%=i+1%></label>
																			</div>
																		
																		<%} %>
																		
																		<%if(fileType.equals("Video")) { %>
																			
																			<input type="radio" name="ss1" id="ss1-item-<%=i%>" class="slideshow--bullet" />
																			<div class="slideshow--item">
																			
																				<span class="thickbox play-button-link" onclick="showCaseFile('Open','<%=fileName%>','<%=fileType%>','<%=caseId%>')">
								            
																					<%
																						int pos = fileName.lastIndexOf(".");
																					    String renderfileName = fileName.substring(0, pos)+".png";
																		            %>
																		            
																					<img  class="img-responsive"  alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+renderfileName+"&caseId="+caseId+""%>">
																					<img class="play-button alignPlayInPopUp commentplbtn" alt="" src="static/IndexResources/images/Play1Pressed.png" onclick="showCaseFile('Open','<%=fileName%>','<%=fileType%>','<%=caseId%>')"/>
																			
																				</span> 
																				
																				<label for="ss1-item-<%=i-1%>" class="slideshow--nav slideshow--nav-previous">Go to slide <%=i-1%></label>
																				<label for="ss1-item-<%=i+1%>" class="slideshow--nav slideshow--nav-next">Go to slide <%=i+1%></label>
																				
																			</div>
																			
																		<%} %>
																		
																		<%if(fileType.equals("Pdf")) { %>
																			 
																			 <input type="radio" name="ss1" id="ss1-item-<%=i%>" class="slideshow--bullet" />
																			 <div class="slideshow--item">
																			
																				  <%
																					 int pos = fileName.lastIndexOf(".");
																				     String renderfileName = fileName.substring(0, pos)+".png";
																	               %>
																	        	  <a title="Load to click on the pdf" href="#" onclick="showCasePdfFile('CaseFile','<%=fileName%>','<%=caseId%>')"> 
																		
																					<img  class="img-responsive" alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+renderfileName+"&caseId="+caseId+""%>" /> 
																					
																				  </a>
																			 	  
																			 	  <label for="ss1-item-<%=i-1%>" class="slideshow--nav slideshow--nav-previous">Go to slide <%=i-1%></label>
																				  <label for="ss1-item-<%=i+1%>" class="slideshow--nav slideshow--nav-next">Go to slide <%=i+1%></label>
																				
																			 </div>
																			 
																		<%} %>
																		
															<%} %>
													
													
													</c:forEach>		
															
													
												</div>
		
										</div>
										
									</c:if>
									
									<div class="entry-block">
										
										
										<div class="entry-title">
											<h3>${val.case_title}</h3>
										</div>
																	
										<div class="entry-content">
											<p>${val.case_desc}</p>
											
											<%-- <blockquote>
												${val.case_desc}
											</blockquote> --%>
											
										</div>	
																
									</div>
								</article>
								
								
								
								<!-- Case Comment -->
								
								
								<div class="post-comment manageCaseComment" id="userCaseCommentDiv">

								 <div class="content casecommentdivheight">

									<%@include file="user_case_comment.jsp" %>
									
								 </div>
								
								</div> 
								 
								<div  class="comment-form manageCommentForm"  style="background-color:#fff;">
								
									<form:form id="formBean" modelAttribute="formBean" >
									
										<h3>Comment on this case</h3>
										
										<div class="row">
											
											<div class="form-group col-md-12">
												<textarea placeholder="Your Comment*" rows="8" name="contentString1" path="contentString1" id="contentString1" class="form-control"></textarea>
											</div>
											
											<div class="form-group col-md-12">
												<input type="file" name="files" id="files" multiple onchange="uploadCaseCommentFile()"/>
											</div>
											
											<div class="form-group col-md-12">
												
												<!-- If file will browse the file list will appear here.  -->
												<ul id="selectedFileList" style="list-style: none;">
												
												</ul>
												
											</div>
											<div class="col-md-12 knowmore">
											<input type="hidden" value="${val.case_id}" name="contentString2" path="contentString2" id="contentString2"  />
											
											<!-- <input type="button" class="btn buttoncs postbtn"  title="Submit Comment" onclick="submitCaseComment()" value="Submit">  -->
											
											<button  type="button" class="btn buttoncs postbtn"  title="Submit Comment" onclick="submitCaseComment()" value="Submit">Submit</button> 
											
										</div>
										</div>
									
									</form:form>
								
								</div>
											
							</div>
							
					</c:forEach>
					
					
				</div>
			</div>
			<div class="section-padding"></div>
		</div>

	</div>
	
	
	
	
	<!-- Footer Main -->
	<%@include file="index_footer.jsp" %>
	<!-- END Footer Main -->
	
	
	<%@include file="index_common_script.jsp" %>

	<script src="static/IndexResources/js/custome_scrollbar.js"></script>
	
	<div id="gallerypopup" class="modal postpage fade" role="dialog">
  
  	
  
     </div>

</body>
</html>