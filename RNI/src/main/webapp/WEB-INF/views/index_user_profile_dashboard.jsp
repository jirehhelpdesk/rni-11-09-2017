<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="">
<!--<![endif]-->
<head>
<!-- dfgdgfg -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>User Profile</title>


<%@include file="index_common_style.jsp"%>

</head>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!-- View Post Page -->

<!-- UI update View Post Page -->

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">



	<!-- Loader And Header /- -->

	<%@include file="index_header.jsp"%>

	<c:set var="userIdentityId" value="${userBean.user_id}" />
	<%
		int userIdentityId = (Integer) pageContext.getAttribute("userIdentityId");
	%>

	<!-- END of Loader and Header -->

	<div class="container-fluid page-banner about no-padding">
		<div class="container">
			<div class="banner-content-block">
				<div class="banner-content">
					<h3>View Feeds</h3>
				</div>
			</div>
		</div>
		<!-- 
		<div class="section-padding"></div> -->
	</div>

	<div class="viewpostbg">
		<div
			class="container-fluid eventlist upcoming-event latest-blog no-padding">

			<div class="manageSectionPadding section-padding"></div>

			<div class="container">

				<div class="row">

					<div class="col-md-3 col-sm-12 col-xs-12 widget-area">

						<%@include file="user_side_menu.jsp"%>

					</div>





					<div class="col-md-9 col-sm-12 col-xs-12 content-area">

						<div class="widget-area mypostfeed" style="width: 100%;">

							<aside class="widget widget_recent">

								<div class="widget-title">
									<span class="icon icon-User"></span>
									<%
										String userType = (String) request.getAttribute("userType");
									%>
									<%
										if (userType.equals("diffUser")) {
									%>
									<h3>User Profile</h3>
									<%
										} else {
									%>
									<h3>My Profile</h3>
									<%
										}
									%>
								</div>

								<c:set var="useraccessId" value="${searchedUserDetails.user_id}" />
								<%
									int useraccessId = (Integer) pageContext.getAttribute("useraccessId");
								%>

								<div class="recent-block userprofiledis">

									<div class="col-md-12 col-sm-12 col-xs-12 mypostview  no-padding">
										<div class="user-profile-bg">
										<div class="row">
										<div class="col-md-12">
											<div class="col-md-4">
												<div class="userprofileimg">
													<c:if test="${!empty searchedUserDetails.profileBean}">

														<c:set var="userProfilePhoto"
															value="${searchedUserDetails.profileBean.profile_photo}" />
														<%
															String userProfilePhoto = (String) pageContext.getAttribute("userProfilePhoto");
														%>

														<%
															if (userProfilePhoto.equals("")) {
														%>

														<img src="static/IndexResources/images/user_pr.png"
															class="center-block">
														<!-- static/IndexResources/images/user.png -->

														<%
															} else {
														%>

														<img class="center-block"
															src="${pageContext.request.contextPath}<%="/previewProfilePhoto?fileName="+userProfilePhoto+"&userId="+useraccessId+""%>">

														<%
															}
														%>

													</c:if>

													<c:if test="${empty searchedUserDetails.profileBean}">

														<img src="static/IndexResources/images/user_pr.png"
															class="center-block">
														<!-- static/IndexResources/images/user.png -->

													</c:if>
												</div>
											</div>
											<div class="col-md-7">
												<h4>
													<span> ${searchedUserDetails.first_name}
														${searchedUserDetails.last_name}</span>
												</h4>
												<p>
													<span class="fa fa-envelope proiconcolor"></span>&nbsp;
													<span class="applicolor">
														${searchedUserDetails.email_id} </span>
												</p>
												<p>
													<span class="fa fa-phone proiconcolor"></span>&nbsp;
													<span class="applicolor">
														${searchedUserDetails.mobile_no}</span>
												</p>
												<p>
													<span class="fa fa-suitcase proiconcolor"></span>&nbsp;
													<span class="applicolor">
														${searchedUserDetails.profession}</span>
												</p>
												<p>
													<span class="fa fa-link proiconcolor"></span>&nbsp;
													<a href="${searchedUserDetails.profileBean.website_url}"
														target="_blank"><span class="applicolor">
															${searchedUserDetails.profileBean.website_url}</span></a>
												</p>
												<p><span class="fa fa-calendar proiconcolor"></span>&nbsp;
												<span class="applicolor"> <fmt:formatDate
																	pattern="dd/MM/yyyy"
																	value="${searchedUserDetails.profileBean.dob}" /></span>
												</p>
												<p><span class="fa fa-venus-mars proiconcolor"></span>&nbsp;
												<span class="applicolor">
																${searchedUserDetails.profileBean.gender}</span>
												</p>
											</div>
										</div>
											</div>
											</div>
									  <div class="user-profile-bg">
										<div class="row">
										<div class="col-md-12">
											<div class="col-md-12 no-padding">
												<h4 class="userprofileunderheader"><span class="fa fa-university"></span>&nbsp;Under Graduation</h4>
											</div>
											
											<div class="col-md-12 no-padding">
												<div class="">

													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5">
															<b>Graduation in</b>
														</div>

														<div
															class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>

														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">
																${searchedUserDetails.profileBean.under_graduation}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>Institute</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.under_graduation_institute}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>University</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.under_graduation_university}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>Place</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.under_graduation_place}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>State</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.under_graduation_state}</span>
														</div>

													</div>

												</div>
											</div>
											</div>
											</div>
											</div>
											<div class="user-profile-bg">
										<div class="row">
										<div class="col-md-12">
											<div class="col-md-12 no-padding">
												<h4 class="userprofileunderheader"><span class="fa fa-university"></span>&nbsp;Post Graduation</h4>
											</div>
											
											<div class="col-md-12 no-padding">
												<div class="">

													<div class="row">


														<div class="col-md-3 col-sm-3 col-xs-5">
															<b>Post Graduation in </b>
														</div>

														<div
															class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>

														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">
																${searchedUserDetails.profileBean.post_graduation}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>Institute</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.post_graduation_institute}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>University</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.post_graduation_university}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>Place</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.post_graduation_place}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>State</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.post_graduation_state}</span>
														</div>

													</div>

												</div>
											</div>
											</div>
											</div>
											</div>
											<div class="user-profile-bg">
										<div class="row">
										<div class="col-md-12">
											<div class="col-md-12 no-padding">
												<h4 class="userprofileunderheader"><span class="fa fa-university"></span>&nbsp;Fellowship</h4>
											</div>
											
											
											<div class="col-md-12 no-padding">
												<div class="">

													<div class="row">


														<div class="col-md-3 col-sm-3 col-xs-5">
															<b>Fellowship in</b>
														</div>

														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>

														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">
																${searchedUserDetails.profileBean.fellowship}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>Sub Speciality</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.fellowship_sub_speciality}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>Institute</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.fellowship_institute}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>University</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.fellowship_university}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>Place</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.fellowship_place}</span>
														</div>

													</div>
													
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5"><b>State</b></div>
														
														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>
														
														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">${searchedUserDetails.profileBean.fellowship_state}</span>
														</div>

													</div>
													

												</div>
											</div>
											</div>
											</div>
											</div>
											
											
											<div class="user-profile-bg user-eventmrgn">
										<div class="row">
										<div class="col-md-12">
											
											<div class="col-md-12 no-padding">

													<div class="row">


														<div class="col-md-3 col-sm-3 col-xs-5">
															<b>Addition Qualification</b>
														</div>

														<div
															class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>

														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">
																${searchedUserDetails.profileBean.additional_qualification}</span>
														</div>

													</div>

											</div>
											<div class="col-md-12 no-padding">

													<div class="row">


														<div class="col-md-3 col-sm-3 col-xs-5">
															<b>Practicing at</b>
														</div>

														<div
															class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>

														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">
																${searchedUserDetails.profileBean.practice_at}</span>
														</div>

													</div>

											</div>
											
											
											<%-- <div class="col-md-12 no-padding">
												<div class="form-group">

													<div class="row">


														<div class="col-md-3 col-sm-3 col-xs-5">
															<b>Practice as</b>
														</div>

														<div class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>

														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">
																${searchedUserDetails.profileBean.practice_as}</span>
														</div>

													</div>


												</div>
											</div> --%>
											
											
											<div class="col-md-12 no-padding">

													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5">
															<b>Year of Experience</b>
														</div>

														<div
															class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>

														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">
																${searchedUserDetails.profileBean.year_of_experience}</span>
														</div>

													</div>

											</div>
											<div class="col-md-12 no-padding">
													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5">
															<b>Phone</b>
														</div>

														<div
															class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>

														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">
																${searchedUserDetails.profileBean.phone_number}</span>
														</div>

													</div>
											</div>
											<div class="col-md-12 no-padding">

													<div class="row">

														<div class="col-md-3 col-sm-3 col-xs-5">
															<b>Pincode</b>
														</div>

														<div
															class="gapMaintain col-md-1 col-sm-1 col-xs-1 no-padding">:</div>

														<div class="col-md-7 col-sm-7 col-xs-5 no-padding">
															<span class="applicolor">
																${searchedUserDetails.profileBean.pincode}</span>
														</div>

													</div>
											</div>
											</div>
											</div>
											</div>
										</div>



									</div>

								</div>

							</aside>

						</div>

					</div>








				</div>
			</div>
			<div class="section-padding"></div>
		</div>
	</div>






	<!-- Footer Main -->
	<%@include file="index_footer.jsp"%>
	<!-- END Footer Main -->

	<div id="postpopup" class="modal fade postpage" role="dialog"></div>



	<!--end view details Popup  -->

	<%@include file="index_common_script.jsp"%>


	<script src="static/adminResource/js/jquery.nicescroll.min.js"></script>



</body>
</html>