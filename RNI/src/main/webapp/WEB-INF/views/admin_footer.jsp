




<footer class="main-footer">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <p>RetNet India &copy; 2017-2019</p>
      </div>
      <div class="col-sm-6 text-right">
        <p>Design and Developed by <a href="http://jirehsol.co.in/" target="_" class="external">Jireh</a></p>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
      </div>
    </div>
  </div>
</footer>



<div id="preloader" style="display: none;">
	
	<div class="cssload-container">
		<div class="cssload-whirlpool"></div>
	</div>
	
</div>