<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
 
 
 <div class="container-fluid">
              
              <div class="row">
                	
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Gallery Record</h3>
                    </div>
                    <div class="card-body">
                     
                     
                    <c:if test="${!empty galleryRecord}">	
                      
		                   
                   		   <table class="table table-striped table-hover" data-toggle="table" data-pagination="true">
	                       
	                        <thead>
	                        
	                          <tr>
	                            <th>Sl.No</th>
	                            <th>Gallery Title</th>
	                            <th>No. of file</th>
	                            <th>Added date</th>
	                            <th>Manage</th>
	                          </tr>
	                          
	                        </thead>
	                        
	                        
	                        <tbody>
	                         
	                         <%int i = 1; %> 
	                           <c:forEach items="${galleryRecord}" var="val">
												
									
									<c:set var="galleryId" value="${val.gallery_id}"/>
									<% int galleryId = (Integer)pageContext.getAttribute("galleryId"); %>
									
									<tr>
			                            
			                            <td scope="row"><%=i++%></td>
			                            <td><span class="contentHide">${val.gallery_text}</span></td>
			                            
										<td>
											 <c:set var = "galleryFile" value = "${val.galleryFile}" />
											 ${fn:length(galleryFile)}
										</td>
										
			                            <td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.cr_date}" /></td>
			                           
			                            <td><a href="#" onclick="deleteGallery('${val.gallery_id}')">Delete</a></td>
			                            
			                        </tr>
									 	  
			                   </c:forEach>
	                          
	                          
	                        </tbody>
	                        
	                      </table>
                   			
                     </c:if>
                     
                     <c:if test="${empty galleryRecord}">	
                     
                     		<div style="color:red;text-align:center;font-size:18px;">
								As of now nothing added to the gallery.
							</div>
                     
                     </c:if> 
                      
                      
                      
                      
                      
                      
                    </div>
                  </div>
                </div>
                	
                	
              </div>
              
 </div>