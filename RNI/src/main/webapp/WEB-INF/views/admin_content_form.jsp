
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:if test="${empty contentData}">	

	<form:form  id="contentForm" modelAttribute="prtlContent" method="post" action="saveFaculty">
	                        
	  <div class="form-group">
	    <label class="form-control-label">Content For</label>
	    <select name="content_for" path="content_for" id="content_for" class="form-control" style="padding: 5px 10px;">
	   			<option value="Select content for">Select content for</option>
	   			<option value="All">All</option>
	   			<option value="Home">Home</option>
	   			<option value="About us">About us</option>
	   			<option value="Sponsorship">Sponsorship</option>
	   			<option value="Gallery">Gallery</option>
	   			<option value="Events">Events</option>
	   			<option value="Contact">Contact</option>
	   	</select>
	    <span id="conForErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group">
	    <label class="form-control-label">Heading</label>
	    <input type="email" placeholder="Heading title which will display in the page" name="content_heading" path="content_heading" id="content_heading" class="form-control">
	    <span id="headErrorId" class="help-block-none"></span>
	  </div>
	  
	  <!-- <div class="form-group">       
	    <label class="form-control-label">Description</label>
	    <textarea placeholder="Description" name="content_desc" path="content_desc" id="content_desc" class="form-control"></textarea>
	    <span id="descErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group">       
	    <label class="form-control-label">Image</label>
	    <input type="file" name="file" id="file" class="form-control">
	    <span id="fileErrorId" class="help-block-none"></span>
	  </div> -->
	  
	  <div class="form-group row">
	  	<div class="offset-sm-10">       
	   	    <input type="button" value="Save" class="btn btn-primary" onclick="saveContentDetails('Save')">
	   	</div>
	  </div>
	  
	</form:form>

</c:if>


<c:if test="${!empty contentData}">	

   <c:forEach items="${contentData}" var="val">
   			
   			<form:form  id="contentForm" modelAttribute="prtlContent" method="post" action="saveFaculty">
	                        
				  <div class="form-group">
				    <label class="form-control-label">Content For</label>
				    
				    <c:set var="contentFor">All,Home,About us,Sponsorship,Gallery,Events,Contact</c:set>
				    <c:set var = "choosenContentFor" value ="${val.content_for}"/>
				    
				    <select name="content_for" path="content_for" id="content_for" class="form-control" style="padding: 5px 10px;">
			   			<option value="Select content for">Select content for</option>
			   			
			   			<c:forTokens items="${contentFor}" delims="," var="contFor">
			   					
		   					<c:if test = "${fn:contains(choosenContentFor, contFor)}">
		   						<option selected value="${contFor}">${contFor}</option>
		   					</c:if>
		   					
		   					<c:if test = "${not fn:contains(choosenContentFor, contFor)}">
		   						<option value="${contFor}">${contFor}</option>
		   					</c:if>
			   			
			   			</c:forTokens>				   							   			
				   	</select>
				   	
				    <span id="conForErrorId" class="help-block-none"></span>
				  </div>
				  
				  <div class="form-group">
				    <label class="form-control-label">Heading</label>
				    <input type="email" placeholder="Email Address" name="content_heading" path="content_heading" id="content_heading" class="form-control" value="${val.content_heading}">
				    <span id="headErrorId" class="help-block-none"></span>
				  </div>
				  
				  <%-- <div class="form-group">       
				    <label class="form-control-label">Description</label>
				    <textarea placeholder="Phone number" name="content_desc" path="content_desc" id="content_desc" class="form-control">value="${val.content_desc}"</textarea>
				    <span id="descErrorId" class="help-block-none"></span>
				  </div>
				  
				  <div class="form-group">       
				    <label class="form-control-label">Image</label>
				    <input type="file" name="file" id="file" class="form-control">
				    <span id="fileErrorId" class="help-block-none"></span>
				  </div> --%>
				  
				  <div class="form-group row">
				  	<div class="offset-sm-10">       
				   	    <input type="button" value="Update" class="btn btn-primary" onclick="saveContentDetails('Update')">
				   	</div>
				  </div>
				  
			</form:form>
   			
   </c:forEach>
	
</c:if>