<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="description" content="">
<meta name="author" content="">
<title>Abstract document submission</title>
<%@include file="index_common_style.jsp"%>
<style>
.accommodation-header {
	font-size: 16px !important;
	color: #222 !important;
	font-weight: 400 !important;
	font-family: Open Sans, sans-serif;
	letter-spacing: 0.39px;
	text-align: left;
}

.table>thead:first-child>tr:first-child>th {
	border-top: 0;
	font-size: 16px !important;
	color: #222 !important;
	font-weight: 500 !important;
	font-family: Open Sans, sans-serif;
	letter-spacing: 0.39px;
	text-align: left;
	border: 1px solid #ddd;
}

.pleas-fill {
	border-top: 0;
	font-size: 16px !important;
	color: #222 !important;
	font-weight: 500 !important;
	font-family: Open Sans, sans-serif;
	letter-spacing: 0.39px;
	text-align: left;
}

.ulist {
	padding: 0 30px;
}

.no-margin-bm {
	margin-bottom: 0;
}

.table-bordered {
	border: 0px solid #ddd;
}

.eventlistimg {
	width: 100%;
	height: auto;
	margin-bottom: 11px;
}

.popupimg {
	padding: 5px;
	margin-bottom: 13px;
	text-align: center;
}

.popupimg img {
	max-height: 436px;
}
</style>
</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<body>
	<header class="header-main container-fluid no-padding"> <!-- Menu Block -->
	<div class="menu-block container-fluid no-padding affix-top"
		data-spy="affix" data-offset-top="100">
		<!--   -->
		<!-- Container -->
		<div class="container">
			<!-- User -->
			<div class="col-md-12">
				<a href="#"> <img src="static/IndexResources/images/logo.png"
					alt="logo" class="retnet center-block" width="170">
				</a>
			</div>
		</div>
		<!-- Container /- -->
	</div>
	<!-- Menu Block /- --> </header>
	<div class="container-fluid page-banner about no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="banner-content-block">
				<div class="banner-content">
					<h3 style="color: #21a5b2;font-size:22px;font-weight: 500;">Abstract Submission for 10th RETNET Meeting</h3>
				</div>
			</div>
		</div>
	</div>
	<div class="eventlist">
		<div
			class="container-fluid eventlist upcoming-event latest-blog no-padding">
			<div class="container indexevent">
				<div class="row">
					<div class="col-md-12">
						<article class="type-post shadow-box">
						
						
								<div class="section-header">				
									<span>Kindly provide the complete presentation/Videos (12 to 14 slides / 3 Mins Videos) or the skeleton of the presentation with images and 2 or 3 points of discussion or queries.</span>
								</div>	
			
								<div class="entry-block knowmore" style="left: 136px;margin-top:12px;margin-bottom:12px;">
									<div class="col-md-12">
										
										<div id="participateform10" class="participateform">
											<div class="contact-form-section knowmore">
												
												<div class="">
													
													<form:form  id="eventAbstactDocForm" >
														
														<div class="col-md-12">
															<div class="form-group">
																<input type="text" class="form-control" name="abstract_title" id="abstract_title" placeholder="Abstract title">
																<div id="titleErrId" class="errorStyle"></div>
															</div>
														</div>
														
														<div class="col-md-12">
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<input type="text" class="form-control" placeholder="First Name" value="" name="first_name" id="first_name" required="">
																		<div id="fNameErrId" class="errorStyle"></div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<input type="text" class="form-control" placeholder="Last Name" value="" name="last_name" id="last_name" required="">
																		<div id="lNameErrId" class="errorStyle"></div>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="col-md-12">
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<input type="text" class="form-control" placeholder="Email id" value="" name="email" id="email" required="">
																		<div id="emailErrId" class="errorStyle"></div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="form-group">
																		<input type="text" class="form-control" placeholder="Mobile number" value="" name="mobile" id="mobile" required="">
																		<div id="mobileErrId" class="errorStyle"></div>
																	</div>
																</div>
															</div>
														</div>
														
														
														<div class="col-md-12">
															<div class="form-group">
																<input type="file" name="file" id="file" >
																
																<span style="color:blue;font-size:10px;">File type - PPT,PDF,MP4(Max file size - 40 MB)</span>
															</div>
															<div id="fileErrId" class="errorStyle"></div>
														</div>
														
														
														<div class="row">
															<div class="col-md-12">
																<button type="button" class="buttoncs btn postbtn"
																	value="" style="float: right; margin-top: 0;"
																	onclick="saveAbstractSubmission()">Submit</button>
															</div>
														</div>
														
													</form:form>
													
													
												</div>
											</div>
										</div>
										
										
										
									</div>
								</div>
						
						</article>
					</div>
				</div>
			</div>
			<div class="section-padding"></div>
		</div>
	</div>
	
	<footer class="footer-main container-fluid no-padding"> <!-- Container -->
	<div class="container">
		<div class="footer-menu">
			<!-- Copyrights -->
			<div class="copyrights ow-pull-left">
				<p>� 2018 | All rights reserved by Retnetindia</p>
			</div>
			<!-- Copyrights /- -->
			<!-- Navigation -->
			<nav class="ow-pull-right foltleft">
			<div class="">
				<ul class="nav navbar-nav">
					<li><span class="dgnpra">Design &amp; Developed by <a
							target="_" href="http://www.jirehsol.co.in/" class="dvby">Jireh</a></span></li>
				</ul>
			</div>
			</nav>
			<!-- Navigation /- -->
		</div>
		<!-- Footer Menu /- -->
	</div>
	<!-- Container /- --> </footer>
	
	
	
	<div id="commentPopUp" style="display:none;"></div>
	
	
	<div id="preloader" style="display: none;">
	
		<div class="cssload-container">
			<div class="cssload-whirlpool"></div>
		</div>
		
	</div>
	
	<%@include file="index_common_script.jsp"%>
</body>
</html>