<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:forEach items="${caseDetails}" var="val">
								
	<c:set var="caseId" value="${val.case_id}"/>
	<% int caseId = (Integer)pageContext.getAttribute("caseId"); %>

					
		<div class="container-fluid">
             
              <div class="row">
                
                <!-- Recent Activities -->
                <div class="col-lg-12">
                  <div class="recent-activities card">
                    
                    <div class="card-close">
                      <div class="dropdown">
                        
                         <button type="button" id="closeCard" class="dropdown-toggle" onclick="closeCaseDiv()">Close</button>
                        
                      </div>
                    </div>
                    
                    <div class="card-header">
                      <h3 class="h4">${val.case_title}</h3>
                    </div>
                    
                    <div class="card-body no-padding">
                      
                      <div class="item">
                        <div class="row">
                          
                          <div class="col-4 date-holder text-right">
                            <div class="icon"><i class="icon-clock"></i></div>
                            <div class="date"> 
                            
                            	<c:forEach items="${val.caseFile}" var="caseFile">
											
										<c:set var="fileName" value="${caseFile.file_name}"/>
										<% String fileName = (String)pageContext.getAttribute("fileName"); %>
								
										<div class="slideshow--item">
											<img src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseFile&fileName="+fileName+"&caseId="+caseId+""%>" class="img-responsive"/>
										</div>
										
										<br></br>
										
								</c:forEach>
                            
                            </div>
                          </div>
                          
                          <div class="col-8"> <!-- content -->
                           
                            <span>${val.case_desc}</span>
                          </div>
                          
                          
                        </div>
                      </div>
                      
                      <div class="form-group row">
			               <div class="col-sm-10 offset-sm-9" style="margin-top:10px;">
			                
			                <c:if test = "${val.case_status == 'Active'}">     
			                   
			                    <button type="button" class="btn btn-primary" onclick="changeCaseStatus('Deactive','${val.case_id}')">Deactive</button>
			        	   	
			        	   	</c:if>
			        	   	
			        	   	<c:if test = "${val.case_status == 'Deactive'}">
			        	   		  
			        	   		  <button type="button" class="btn btn-primary" onclick="changeCaseStatus('Active','${val.case_id}')">Activate</button>
			        	   	
			        	   	</c:if>
			        	   		
			               </div>
			           </div>
                      
                    </div>
                  </div>
                </div>
                
                
                
              </div>
            </div>


</c:forEach>
