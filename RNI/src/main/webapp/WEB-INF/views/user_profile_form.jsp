<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<link href="static/textEditor/css/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="static/textEditor/js/calender.js"></script>
<link href="static/textEditor/css/calender_style.css" rel="stylesheet" type="text/css">






	<div class="panel-heading" role="tab" id="headingTwo">
	   
	   <h4 class="panel-title">
	       <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span class="icon icon-User"></span>&nbsp;Profile Details
	       </a>
	   </h4>
	                            
	</div>
   
   
   
   <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
       <div class="panel-body">


		<c:if test="${empty userBean.profileBean}">
				
			
			<form:form class="booking-form-block" id="profileForm" modelAttribute="userProfile">	
				
				
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="form-group">
						<input type="text" name="dob" readonly path="dob" id="dob" class="form-control datepickerWithoutTime" placeholder="Date of birth *" onchange="calculateUserAge(this.value)"/>
					</div>
					<div id="dobErrId" class="errorStyle"></div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="form-group">
						<input type="text" readonly name="age" title="your age as per the date of birth" path="age" id="age" class="form-control" placeholder="Age *" />
					</div>
					<div id="ageErrId" class="errorStyle"></div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="form-group">
						
						<select name="gender" id="gender" class="form-control">
							<option value="Select gender">Select gender</option>
							<option value="Female">Female</option>
							<option value="Male">Male</option>
							<option value="Other">Other</option>
						</select>
					</div>
					<div id="genderErrId" class="errorStyle"></div>
				</div>


				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<input type="text" name="under_graduation" id="under_graduation" class="form-control" placeholder="Undergraduation *" />
					</div>
					<div id="graduatenErrId" class="errorStyle"></div>
				</div>
				
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 no-padding">
						<div class="form-group">
							<input type="text" required class="form-control" path="under_graduation_institute" name="under_graduation_institute" id="under_graduation_institute" placeholder="Institute name" />
						</div>
						<div id="graInstErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 no-padding">
						<div class="form-group">
							<input type="text" class="form-control" required path="under_graduation_university" name="under_graduation_university" id="under_graduation_university" placeholder="University name" />
						</div>
						<div id="graUniverErrId" class="errorStyle"></div>
					</div>
					
				</div>

								
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 no-padding">
						<div class="form-group">
							<input type="text" required class="form-control" path="under_graduation_place" name="under_graduation_place" id="under_graduation_place" placeholder="Place" />
						</div>
						<div id="grePlaceErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 no-padding">
						<div class="form-group">
							<input type="text" class="form-control" required path="under_graduation_state" name="under_graduation_state" id="under_graduation_state" placeholder="State" />
						</div>
						<div id="graStateErrId" class="errorStyle"></div>
					</div>
					
				</div>
						
								
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<input type="text" name="post_graduation" id="post_graduation" class="form-control" placeholder="Post graduation" />
					</div>
					<div id="pgErrId" class="errorStyle"></div>
				</div>
				
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 no-padding">
						<div class="form-group">
							<input type="text" required class="form-control" path="post_graduation_institute" name="post_graduation_institute" id="post_graduation_institute" placeholder="Institute name" />
						</div>
						<div id="pgInstErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 no-padding">
						<div class="form-group">
							<input type="text" class="form-control" required path="post_graduation_university" name="post_graduation_university" id="post_graduation_university" placeholder="University name" />
						</div>
						<div id="pgUniverErrId" class="errorStyle"></div>
					</div>
					
				</div>

								
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 no-padding">
						<div class="form-group">
							<input type="text" required class="form-control" path="post_graduation_place" name="post_graduation_place" id="post_graduation_place" placeholder="Place" />
						</div>
						<div id="pgPlaceErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 no-padding">
						<div class="form-group">
							<input type="text" class="form-control" required path="post_graduation_state" name="post_graduation_state" id="post_graduation_state" placeholder="State" />
						</div>
						<div id="pgStateErrId" class="errorStyle"></div>
					</div>
					
				</div>
				
				
				
				<div class="col-md-12 no-padding">
					<div class="col-md-12 no-padding">
						<span style="color: #21a5b2; margin-left: 14px;">Fellowship</span>
					</div>
				</div>

				
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" name="fellowship" id="fellowship" class="form-control" placeholder="Fellowship"  />
						</div>
						<div id="fellowErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<select class="form-control" path="fellowship_sub_speciality" name="fellowship_sub_speciality" id="fellowship_sub_speciality">
												
								<option value="Select sub speciality">Select sub speciality</option>
								<option value="Surgical retina">Surgical retina</option>
								<option value="Medical retina">Medical retina</option>
								<option value="Uvea">Uvea</option>
								<option value="Ocular oncology">Ocular oncology</option>
								<option value="Neuro ophthalmology">Neuro ophthalmology</option>
								
							</select>
						</div>
						<div id="felSpeErrId" class="errorStyle"></div>
					</div>
					
				</div>
				
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" required class="form-control" path="fellowship_institute" name="fellowship_institute" id="fellowship_institute" placeholder="Institute name" />
						</div>
						<div id="felInstErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control" required path="fellowship_university" name="fellowship_university" id="fellowship_university" placeholder="University name"  />
						</div>
						<div id="felUniverErrId" class="errorStyle"></div>
					</div>
					
				</div>
				
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" required class="form-control" path="fellowship_place" name="fellowship_place" id="fellowship_place" placeholder="Place" />
						</div>
						<div id="felPlaceErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control" required path="fellowship_state" name="fellowship_state" id="fellowship_state" placeholder="State"  />
						</div>
						<div id="felStateErrId" class="errorStyle"></div>
					</div>
					
				</div>
				
				
				
				
				
				
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<input type="text" name="additional_qualification" id="additional_qualification" class="form-control" placeholder="Additional qualification" />
					</div>
					<div id="addQuaErrId" class="errorStyle"></div>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<input type="text" name="practice_at" id="practice_at" class="form-control" placeholder="Practicing at (address)" />
					</div>
					<div id="praAtErrId" class="errorStyle"></div>
				</div>

				<!-- <div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<input type="text" name="practice_as" id="practice_as" class="form-control" placeholder="Practice as" />
					</div>
					<div id="praAsErrId" class="errorStyle"></div>
				</div> -->

				<div class="col-md-12 no-padding">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" name="year_of_experience" id="year_of_experience" class="form-control" placeholder="Year of experience" />
						</div>
						<div id="yoeErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="Phone no" />
						</div>
						<div id="phoneErrId" class="errorStyle"></div>
					</div>
				</div>
				
				<div class="col-md-12 no-padding">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<input type="text" name="website_url" id="website_url" class="form-control" placeholder="Website URL" />
					</div>
					<div id="websiteErrId" class="errorStyle"></div>
				</div>
				
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<input type="text" name="pincode" id="pincode" class="form-control" placeholder="Pincode" />
					</div>
					<div id="pincodeErrId" class="errorStyle"></div>
				</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 text-left">

					<span style="color: #21a5b2;">Area of interest</span>

					<div class="form-group">
						
						<ul style="list-style: none;">

							<li><input type="checkbox" name="area_of_interest" id="area_of_interest" value="Vitreo Retina" class="" onclick="showOtherInterest(this.value)"/> Vitreo Retina</li>
							<li><input type="checkbox" name="area_of_interest" id="area_of_interest" value="Medical Retina" class="" onclick="showOtherInterest(this.value)"/> Medical Retina</li>
							<li><input type="checkbox" name="area_of_interest" id="area_of_interest" value="Uvea" class="" onclick="showOtherInterest(this.value)"/> Uvea</li>
							<li><input type="checkbox" name="area_of_interest" id="area_of_interest" value="Ocular oncology" class="" onclick="showOtherInterest(this.value)"/> Ocular oncology</li>
							<li><input type="checkbox" name="area_of_interest" id="area_of_interest" value="Neuro ophthalmology" class="" onclick="showOtherInterest(this.value)"/> Neuro ophthalmology</li>
							<li><input type="checkbox" name="area_of_interest" id="area_of_interest" value="Other" class="" onclick="showOtherInterest(this.value)"/> Other</li>

						</ul>

						<div id="interestBox" style="display: none;">
							<input type="text" name="interest" id="interest" class="form-control" placeholder="Other Interest" />
						</div>

					</div>
					<div id="interestErrId" class="errorStyle"></div>

				</div>


				<div class="col-md-6 col-sm-6 col-xs-12  text-left">
					<span style="color: #21a5b2;">Profile Picture</span>
					<div class="form-group">

						<input type="file" id="file" name="file" class="form-control">
						<div id="fileErrId" class="errorStyle"></div>

					</div>
				</div>


				<div class="col-md-12 col-sm-12 col-xs-12">
						<button style="float: right;"  class="btn buttoncs postbtn" type="button" value="" id="" title="Save profile record" onclick="saveProfileDetails('Save')">Save</button>
					</div>

				<div id="alert-msg" class="alert-msg"></div>

			
			</form:form>
			
			
		</c:if>
		
		
		
		
		
		
		
		
		
		
		
		
		
		<c:if test="${!empty userBean.profileBean}">
		
		   <form:form class="booking-form-block" id="profileForm" modelAttribute="userProfile">

				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="form-group">
						
						<input type="text" name="dob" path="dob" readonly id="dob" class="form-control datepickerWithoutTime" placeholder="Date of birth *" value="<fmt:formatDate pattern="dd/MM/yyyy" value="${userBean.profileBean.dob}"/>" onchange="calculateUserAge(this.value)"/>
					</div>
					<div id="dobErrId" class="errorStyle"></div>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="form-group">
						<input type="text" name="age" path="age" readonly title="your age as per the date of birth" id="age" class="form-control" placeholder="Age *" value="${userBean.profileBean.age}" />
					</div>
					<div id="ageErrId" class="errorStyle"></div>
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="form-group">
						
						<c:if test = "${userBean.profileBean.gender == 'Female'}">
							
							<select name="gender" id="mobile_no" class="form-control">
								<option value="Select gender">Select gender</option>
								<option selected value="Female">Female</option>
								<option value="Male">Male</option>
								<option value="Other">Other</option>
							</select>
						
						</c:if>
						
						<c:if test = "${userBean.profileBean.gender == 'Male'}">
								
								<select name="gender" id="mobile_no" class="form-control">
									<option value="Select gender">Select gender</option>
									<option value="Female">Female</option>
									<option selected value="Male">Male</option>
									<option value="Other">Other</option>
								</select>
						</c:if>
						
						
					</div>
					<div id="genderErrId" class="errorStyle"></div>
				</div>

				<div class="col-md-12 no-padding">
					<div class="col-md-12 no-padding">
						<span style="color: #21a5b2; margin-left: 14px;">Under graduation</span>
					</div>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<input type="text" name="under_graduation" id="under_graduation" class="form-control" placeholder="Undergraduation *" value="${userBean.profileBean.under_graduation}" />
					</div>
					<div id="graduatenErrId" class="errorStyle"></div>
				</div>
				
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" required class="form-control" path="under_graduation_institute" name="under_graduation_institute" id="under_graduation_institute" placeholder="Institute name" value="${userBean.profileBean.under_graduation_institute}"/>
						</div>
						<div id="graInstErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control" required path="under_graduation_university" name="under_graduation_university" id="under_graduation_university" placeholder="University name" value="${userBean.profileBean.under_graduation_university}" />
						</div>
						<div id="graUniverErrId" class="errorStyle"></div>
					</div>
					
				</div>

								
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" required class="form-control" path="under_graduation_place" name="under_graduation_place" id="under_graduation_place" placeholder="Place" value="${userBean.profileBean.under_graduation_place}"/>
						</div>
						<div id="grePlaceErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control" required path="under_graduation_state" name="under_graduation_state" id="under_graduation_state" placeholder="State" value="${userBean.profileBean.under_graduation_state}" />
						</div>
						<div id="graStateErrId" class="errorStyle"></div>
					</div>
					
				</div>
				
				
				<div class="col-md-12 no-padding">
					<div class="col-md-12 no-padding">
						<span style="color: #21a5b2; margin-left: 14px;">Post graduation</span>
					</div>
				</div>
								
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<input type="text" name="post_graduation" id="post_graduation" class="form-control" placeholder="Post graduation" value="${userBean.profileBean.post_graduation}" />
					</div>
					<div id="pgErrId" class="errorStyle"></div>
				</div>

				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" required class="form-control" path="post_graduation_institute" name="post_graduation_institute" id="post_graduation_institute" placeholder="Institute name" value="${userBean.profileBean.post_graduation_institute}" />
						</div>
						<div id="pgInstErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control" required path="post_graduation_university" name="post_graduation_university" id="post_graduation_university" placeholder="University name" value="${userBean.profileBean.post_graduation_university}" />
						</div>
						<div id="pgUniverErrId" class="errorStyle"></div>
					</div>
					
				</div>

								
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" required class="form-control" path="post_graduation_place" name="post_graduation_place" id="post_graduation_place" placeholder="Place" value="${userBean.profileBean.post_graduation_place}" />
						</div>
						<div id="pgPlaceErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control" required path="post_graduation_state" name="post_graduation_state" id="post_graduation_state" placeholder="State" value="${userBean.profileBean.post_graduation_state}" />
						</div>
						<div id="pgStateErrId" class="errorStyle"></div>
					</div>
					
				</div>
				
				

				<div class="col-md-12 no-padding">
					<div class="col-md-12 no-padding">
						<span style="color: #21a5b2; margin-left: 14px;">Fellowship</span>
					</div>
				</div>

				
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" name="fellowship" id="fellowship" class="form-control" placeholder="Fellowship" value="${userBean.profileBean.fellowship}" />
						</div>
						<div id="fellowErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<select class="form-control" path="fellowship_sub_speciality" name="fellowship_sub_speciality" id="fellowship_sub_speciality">
												
								<option value="Select sub speciality">Select sub speciality</option>
								<option value="Surgical retina">Surgical retina</option>
								<option value="Medical retina">Medical retina</option>
								<option value="Uvea">Uvea</option>
								<option value="Ocular oncology">Ocular oncology</option>
								<option value="Neuro ophthalmology">Neuro ophthalmology</option>
								
							</select>
						</div>
						<div id="felSpeErrId" class="errorStyle"></div>
					</div>
					
				</div>
				
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" required class="form-control" path="fellowship_institute" name="fellowship_institute" id="fellowship_institute" placeholder="Institute name" value="${userBean.profileBean.fellowship_institute}"/>
						</div>
						<div id="felInstErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control" required path="fellowship_university" name="fellowship_university" id="fellowship_university" placeholder="University name" value="${userBean.profileBean.fellowship_university}" />
						</div>
						<div id="felUniverErrId" class="errorStyle"></div>
					</div>
					
				</div>
				
				<div class="col-md-12 no-padding">
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" required class="form-control" path="fellowship_place" name="fellowship_place" id="fellowship_place" placeholder="Place" value="${userBean.profileBean.fellowship_place}" />
						</div>
						<div id="felPlaceErrId" class="errorStyle"></div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control" required path="fellowship_state" name="fellowship_state" id="fellowship_state" placeholder="State" value="${userBean.profileBean.fellowship_state}" />
						</div>
						<div id="felStateErrId" class="errorStyle"></div>
					</div>
					
				</div>
				
				


				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<input type="text" name="additional_qualification" id="additional_qualification" class="form-control" placeholder="Addition qualification" value="${userBean.profileBean.additional_qualification}" />
					</div>
					<div id="addQuaErrId" class="errorStyle"></div>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<input type="text" name="practice_at" id="practice_at" class="form-control" placeholder="Practicing at (address)" value="${userBean.profileBean.practice_at}" />
					</div>
					<div id="praAtErrId" class="errorStyle"></div>
				</div>

				<%-- <div class="col-md-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<input type="text" name="practice_as" id="practice_as" class="form-control" placeholder="Practice as" value="${userBean.profileBean.practice_as}" />
					</div>
					<div id="praAsErrId" class="errorStyle"></div>
				</div> --%>
	    
	    
	    
	            <div class="col-md-12 no-padding">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<input type="text" name="year_of_experience" id="year_of_experience" class="form-control" placeholder="Year of experience" value="${userBean.profileBean.year_of_experience}" />
					</div>
					<div id="yoeErrId" class="errorStyle"></div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="Phone no" value="${userBean.profileBean.phone_number}" />
					</div>
					<div id="phoneErrId" class="errorStyle"></div>
				</div>
				</div>
		
		
				<div class="col-md-12 no-padding">
				
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<input type="text" name="website_url" id="website_url" class="form-control" placeholder="Website URL" value="${userBean.profileBean.website_url}" />
							</div>
							<div id="websiteErrId" class="errorStyle"></div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<input type="text" name="pincode" id="pincode" class="form-control" placeholder="Pincode" value="${userBean.profileBean.pincode}" />
							</div>
							<div id="pincodeErrId" class="errorStyle"></div>
						</div>
				</div>
				
				


				<div class="col-md-12 col-sm-12 col-xs-12 text-left">

					<span style="color: #21a5b2;">Area of interest</span>

					<div class="form-group">
						
						<ul style="list-style: none;">
						
						<c:set var="areaInterest">Vitreo Retina,Medical Retina,Uvea,Ocular oncology,Neuro ophthalmology,Other</c:set>
						
						<c:set var = "choosenInterest" value ="${userBean.profileBean.area_of_interest}"/>
						
						<c:forTokens items="${areaInterest}" delims="," var="interest">
						    
						    <c:if test = "${fn:contains(choosenInterest, interest)}">
				         		<li><input type="checkbox" checked name="area_of_interest" id="area_of_interest" value="${interest}" class="" onclick="showOtherInterest(this.value)"/> ${interest}</li>
				            </c:if>
				            
				            <c:if test = "${not fn:contains(choosenInterest, interest)}">
				         		<li><input type="checkbox" name="area_of_interest" id="area_of_interest" value="${interest}" class="" onclick="showOtherInterest(this.value)"/> ${interest}</li>
				            </c:if>
				            
						</c:forTokens>
						
						</ul>
						
						<c:if test = "${fn:contains(choosenInterest, 'Other')}">
							
							<div id="interestBox" style="display: block;">
								<input type="text" name="interest" id="interest" class="form-control" placeholder="Other Interest" value="${userBean.profileBean.other_area_of_interest}"/>
							</div>
						
						</c:if>
						
						<c:if test = "${not fn:contains(choosenInterest, 'Other')}">
							
							<div id="interestBox" style="display: none;">
								<input type="text" name="interest" id="interest" class="form-control" placeholder="Other Interest" />
							</div>
						
						</c:if>
						
						

					</div>
					
					<div id="interestErrId" class="errorStyle"></div>

				</div>



				<div class="col-md-6 col-sm-6 col-xs-12 text-left">
					<span style="color: #21a5b2;">Profile Picture</span>
					<div class="form-group">

						<input type="file" id="file" name="file" class="">
						<div id="fileErrId" class="errorStyle"></div>

					</div>
				</div>

				<input type="hidden" name="profileId"
					value="${userBean.profileBean.profile_id}" />

				<div class="col-md-12 col-sm-12 col-xs-12"> 
						<button style="float: right;" type="button" class="btn buttoncs postbtn" value="" id="" title="Update profile record" onclick="saveProfileDetails('Update')">Update</button>
					</div>

				<div id="alert-msg" class="alert-msg"></div>

		  </form:form>

		
		</c:if>
		
		
	</div>
       
  </div>
                            