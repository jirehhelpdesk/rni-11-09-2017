<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link href="static/textEditor/css/style.css" rel="stylesheet" type="text/css" />
<script type="javascript" src="static/textEditor/js/calender.js"></script>
<link href="static/textEditor/css/calender_style.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="static/textEditor/tiny_mce.js"></script>
<script type="text/javascript">
 tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",
		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",
		// Replace values for the template plugin
		template_replace_values : 
		{
		username : "Some User",
		staffid : "991234"
		}
	}); 
	
	
	function showContent()
	{
	   var des = document.getElementById("event_desc").value;
	   alert(des);
	}
	
	
</script>


<c:if test="${empty eventData}">	

       <div class="container-fluid">
              
              <div class="row">
                
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                           <a href="#" class="dropdown-item edit" onclick="showEventForm('Close')"> <i class="fa fa-times"></i>Close Form</a>
                           
                         </div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Create Event</h3>
                    </div>
                    <div class="card-body">
                      
                      
                      
                      <form:form  class="form-horizontal" id="eventForm" modelAttribute="eventDetail" method="post" action="saveEventDetails" >
						
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Event Name</label>
                          <div class="col-sm-10">
                            <input type="text" placeholder="Event title or name" name="event_title" path="event_title" id="event_title" class="form-control">
                            <span id="titleErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                                                
                        <div class="line"></div>
                        
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Event Date</label>
                          <div class="col-sm-5">
                          	 <input type="text" id="event_start_date" class="form-control datepickerWithoutTime" name="event_start_date" path="event_start_date"  placeholder="Opening Date">
							 <span id="sDateErrorId" class="help-block-none"></span>
                          </div>
                          <div class="col-sm-5">
                            <input type="text" id="event_end_date" class="form-control datepickerWithoutTime" name="event_end_date" path="event_end_date"  placeholder="Ending Date">
							<span id="eDateErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                        
                        <div class="line"></div>
                        
                        
                        <div class="form-group row">
                          
                          <label class="col-sm-2 form-control-label">Add event images</label>
                          <div class="col-sm-5">
                                
                                <input class="form-control" name='file' id="file" type=file multiple>  
                                <!-- <input type="file" name="file" id="file" class="form-control"> -->
		                        <span id="fileErrorId" class="help-block-none"></span>
                          </div>
                          
                          <div class="col-sm-5">
                                  
		                       <input type="button" value="Add Image" class="btn btn-primary" onclick="saveEventFile()">
		                        
                          </div>
                          
                        </div>
                        
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label"><br><small class="text-primary"></small></label>
                          
                          <div class="col-sm-10" id="imageList">
                            
                           
                          </div>
                          
                        </div>
                        
                        
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Event Charge</label>
                          
                          <div class="col-sm-2">
                            <label class="checkbox-inline">
                              <input type="radio" name="eventCharge" value="Yes" onclick="manageFeeBox(this.value)"> Yes
                            </label>
                            <label class="checkbox-inline">
                              <input type="radio" name="eventCharge" value="No" onclick="manageFeeBox(this.value)"> No
                            </label>
                            
                          </div>
                          
                          <div class="col-sm-8" id="feeBox" style="display:none;">
                            
                             <input type="text" class="form-control" placeholder="Event Fee" name="eventFee" id="eventFee">
                             <span id="feeErrorId" class="help-block-none"></span>
                             
                          </div>
                          
                        </div>
                        
                        
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Location Address</label>
                          <div class="col-sm-10">
                            <input type="text" placeholder="Location address" name="event_location" path="event_location" id="event_location" class="form-control">
                          	<span id="locationErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                        
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Location Map iFrame</label>
                          <div class="col-sm-10">
                            <input type="text" placeholder="Paste the location iframe" name="event_location_map" path="event_location_map" id="event_location_map" class="form-control">
                          	<span id="locationMapErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                        
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Event Description</label>
                          <div class="col-sm-10">
                            <textarea name="event_desc" path="event_desc"  id="event_desc" ></textarea>
                            <span id="descErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                        
                       
                        <div class="line"></div>
                        <div class="form-group row">
                          <div class="col-sm-5 offset-sm-3">
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="saveEventDetail('save')">Save</button>
                          </div>
                        </div>
                        
                        
                      </form:form>
                      
                    </div>
                  </div>
                </div>
                
                
              </div>
            </div>
    
</c:if>





<c:if test="${!empty eventData}">	

<c:forEach items="${eventData}" var="val">

	<c:set var="charge" value="${val.event_charge}"/>
	<% String charge = (String)pageContext.getAttribute("charge"); %>
											


	<div class="container-fluid">
              
              <div class="row">
                
                <!-- Form Elements -->
                <div class="col-lg-12">
                  
                  <div class="card">
                    
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                           <a href="#" class="dropdown-item edit" onclick="showEventForm('Close')"> <i class="fa fa-times"></i>Close Form</a>
                           
                         </div>
                      </div>
                    </div>
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Create Event</h3>
                    </div>
                    
                    <div class="card-body">
                      
                      
                      <form:form  class="form-horizontal" id="eventForm" modelAttribute="eventDetail" method="post" action="saveEventDetails" >
						
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Event Name</label>
                          <div class="col-sm-10">
                            <input type="text" placeholder="Event title or name" name="event_title" path="event_title" id="event_title" value="${val.event_title}" class="form-control">
                            <span id="titleErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                                                
                        <div class="line"></div>
                        
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Event Date</label>
                          <div class="col-sm-5">
                          	 <input type="text" id="some_class_1" class="form-control datepickerWithoutTime" name="event_start_date" path="event_start_date"  placeholder="Opening Date" value="<fmt:formatDate pattern="dd/MM/yyyy" value="${val.event_start_date}" />">
							 <span id="sDateErrorId" class="help-block-none"></span>
                          </div>
                          <div class="col-sm-5">
                            <input type="text" id="some_class_2" class="form-control datepickerWithoutTime" name="event_end_date" path="event_end_date"  placeholder="Ending Date" value="<fmt:formatDate pattern="dd/MM/yyyy" value="${val.event_end_date}" />">
							<span id="eDateErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                        
                        <div class="line"></div>
                        
                        
                        <div class="form-group row">
                          
                          <label class="col-sm-2 form-control-label">Add event images</label>
                          <div class="col-sm-5">
                                
                                <input class="form-control" name='file' id="file" type=file multiple>  
                                <!-- <input type="file" name="file" id="file" class="form-control"> -->
		                        <span id="fileErrorId" class="help-block-none"></span>
                          </div>
                          
                          <div class="col-sm-5">
                                  
		                       <input type="button" value="Add Image" class="btn btn-primary" onclick="saveEventFile()">
		                        
                          </div>
                          
                        </div>
                        
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label"><br><small class="text-primary"></small></label>
                          
                          <div class="col-sm-10" id="imageList">
                            
                           
                          </div>
                          
                        </div>
                        
                        
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Event Charge</label>
                          
                          
                          <%if(charge.equals("Yes")){ %> 
                          			
                          		<div class="col-sm-2">
		                            
		                            <label class="checkbox-inline">
		                              <input type="radio" name="eventCharge" checked value="Yes" onclick="manageFeeBox(this.value)"> Yes
		                            </label>
		                            <label class="checkbox-inline">
		                              <input type="radio" name="eventCharge" value="No" onclick="manageFeeBox(this.value)"> No
		                            </label>
		                            
		                        </div>	
                          		
                          		<div class="col-sm-8" id="feeBox" style="display:block;">
                            
		                             <input type="text" class="form-control" placeholder="Event Fee" name="eventFee" id="eventFee" value="${val.event_charge_fee}">
		                             <span id="feeErrorId" class="help-block-none"></span>
		                             
		                         </div>
                          
                          <%}else{ %>
                          
                          		<div class="col-sm-2">
		                            <label class="checkbox-inline">
		                              <input type="radio" name="eventCharge" value="Yes" onclick="manageFeeBox(this.value)"> Yes
		                            </label>
		                            <label class="checkbox-inline">
		                              <input type="radio" name="eventCharge" checked value="No" onclick="manageFeeBox(this.value)"> No
		                            </label>
		                            
		                          </div>
		                          
		                          <div class="col-sm-8" id="feeBox" style="display:none;">
                            
		                             <input type="text" class="form-control" placeholder="Event Fee" name="eventFee" id="eventFee" value="${val.event_charge_fee}">
		                             <span id="feeErrorId" class="help-block-none"></span>
		                             
		                          </div>
                          
                          <%}%>
                          
                          
                          
                        </div>
                        
                        
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Location Address</label>
                          <div class="col-sm-10">
                            <input type="text" placeholder="Location address" name="event_location" path="event_location" id="event_location" class="form-control" value="${val.event_location}">
                          	<span id="locationErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                        
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Location Map iFrame</label>
                          <div class="col-sm-10">
                            <input type="text" placeholder="Paste the location iframe" name="event_location_map" path="event_location_map" id="event_location_map" class="form-control" value="${val.event_location_map}" />
                          	<span id="locationMapErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                        
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Event Description</label>
                          <div class="col-sm-10">
                            <textarea name="event_desc" path="event_desc"  id="event_desc" >${val.event_desc}</textarea>
                            <span id="descErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                        
                       <input type="hidden" name="event_id" path="event_id" value="${val.event_id}" />
                       
                       
                        <div class="line"></div>
                        <div class="form-group row">
                          <div class="col-sm-5 offset-sm-3">
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="saveEventDetail('update')">Save</button>
                          </div>
                        </div>
                        
                        
                      </form:form>
                      
                    </div>
                  </div>
                </div>
                
                
              </div>
            </div>


</c:forEach>
	
	
	
</c:if>        
             
    
    <script src="static/datePicker/jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="static/datePicker/jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="static/datePicker/datepicker_custom.js"></script>
            