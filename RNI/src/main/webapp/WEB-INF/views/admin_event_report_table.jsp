<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
 
 <div class="container-fluid">
              
              <div class="row">
                	
                <div class="col-lg-12">
                  <div class="card">
                    
                    <c:if test="${!empty eventReportData}">	
	                    <div class="card-close">
	                      <div class="dropdown">
	                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
	                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
	                           <a href="downloadEventReport?eventId=<%=request.getAttribute("eventId")%>" class="dropdown-item remove"> <i class="fa fa-file-text"></i>Export for more details</a>
	                        </div>
	                      </div>
	                    </div>
                    </c:if>
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Event participation list</h3>
                    </div>
                    <div class="card-body">
                     
                     
                    <c:if test="${!empty eventReportData}">	
                      
		                   <div class="table-responsive">
                   		   <table class="table table-striped table-hover" data-toggle="table" data-pagination="true">
	                       
	                        <thead>
	                        
	                          <tr>
	                            <th>Sl.No</th>
	                            <th>Name</th>
	                            <th>Email Id</th>
	                            <th>Mobile</th>
	                            <th>City</th>
	                            <th>State</th>
	                            <th>Participated Date</th>
	                            <th>Status</th>
	                            
	                          </tr>
	                          
	                        </thead>
	                        
	                        
	                        <tbody>
	                         
	                         <%int i = 1; %> 
	                           <c:forEach items="${eventReportData}" var="val">
											
									<tr>
			                            
			                            <td scope="row"><%=i++%></td>
			                            <td>${val.applicant_name}</td>
			                            <td>${val.applicant_email}</td>
			                            <td>${val.applicant_mobile}</td>
			                            <td>${val.applicant_city}</td>
			                            <td>${val.applicant_state}</td>
			                            <td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.applied_date}" /></td>
			                            <td>${val.applicant_status}</td>
			                           
			                        </tr>
									 	  
			                   </c:forEach>
	                          
	                          
	                        </tbody>
	                        
	                      </table>
                   			</div>
                     </c:if>
                     
                     <c:if test="${empty eventReportData}">	
                     
                     		<div style="color:red;text-align:center;font-size:18px;">
								No one is participated in this event.
							</div>
                     
                     </c:if> 
                      
                      
                      
                      
                      
                      
                    </div>
                  </div>
                </div>
                	
                	
              </div>
              
 </div>