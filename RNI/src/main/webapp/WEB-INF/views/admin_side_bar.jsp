



<!-- Side Navbar -->
        <nav class="side-navbar">
          
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="static/adminResource/img/admin-2.png" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4">${adminData.admin_name}</h1>
              <p>${adminData.admin_designation}</p>
            </div>
          </div>
          <!-- Sidebar Navidation Menus-->
          
         
         
         
          <ul class="list-unstyled">
            
            
            
            <!-- <li><a href="#dashvariants" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Dropdown </a>
              <ul id="dashvariants" class="collapse list-unstyled">
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
              </ul>
            </li> -->
            
            <li id="menu1"> <a href="adminHome"><i class="fa fa-tachometer"></i>Dashboard</a></li>
            <li id="menu2"> <a href="manUser"> <i class="fa fa-users"></i>Manage User </a></li>
            <li id="menu3"> <a href="manEvent"> <i class="fa fa-calendar-check-o"></i>Manage Event </a></li>
            <li id="menu4"> <a href="manPost"> <i class="fa fa-pencil-square-o"></i>Manage Feed </a></li>
            <li id="menu5"> <a href="manSetting"> <i class="fa fa-cogs"></i>Setting</a></li>
            <li id="menu6"> <a href="manPortal"> <i class="fa fa-clone"></i>Manage Portal</a></li>
            <li id="menu7"> <a href="manNewsLetter"> <i class="fa fa-newspaper-o"></i>Manage Newsletter</a></li>
            <li id="menu8"> <a href="manGallary"> <i class="fa fa-picture-o"></i>Manage Gallery</a></li>
            <li id="menu9"> <a href="manCase"> <i class="fa fa-briefcase"></i>Manage Case</a></li>
            <li id="menu10"> <a href="manBanner"> <i class="fa fa-object-group"></i>Manage Banner</a></li>
            <li id="menu11"> <a href="masterLaunch"> <i class="fa fa-object-group"></i>Event Before launch</a></li>
          </ul>
          
          
          <!-- <span class="heading">Extras</span>
          
          <ul class="list-unstyled">
            <li> <a href="#"> <i class="icon-flask"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-screen"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-mail"></i>Demo </a></li>
            <li> <a href="#"> <i class="icon-picture"></i>Demo </a></li>
          </ul> -->
          
          
        </nav>