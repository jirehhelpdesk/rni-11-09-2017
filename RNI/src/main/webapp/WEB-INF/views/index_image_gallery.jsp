<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Image Gallery</title>

	
	<%@include file="index_common_style.jsp" %>
	<style>
	


	</style>
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<body data-offset="200" data-spy="scroll" data-target=".ow-navigation" onload="activeMenu('menu4','index')">
	
	
	
	<!-- Loader And Header /- -->
	
	<%@include file="index_header.jsp" %>
	
	<!-- END of Loader and Header -->
	<div class="container-fluid page-banner about no-padding">
			<div class="container">
				<div class="banner-content-block">
					<div class="banner-content">
						<h3>Our Gallery</h3>
					</div>
				</div>
			</div><!-- 
		<div class="section-padding"></div> -->
	</div>
	<!-- Schedule Section -->



	<div class="imgallery">
		<div class="container-fluid no-padding schedule-section gallerybg">
			<div class="container">
				<div class="schedule-block">
					<div id="container" class="clearfix">




						<div id="content" class="defaults">


							<!-- navigation holder -->


							<div class="holder"></div>


							<!-- item container -->
							<ul id="itemContainer">

								<c:forEach items="${galleryFileRecord}" var="val">

									<%
										String fileType = "";
										String fileName = "";
										int n = 1;
									%>

									<c:set var="fileType" value="${val.file_type}" />
									<%
										fileType = (String) pageContext.getAttribute("fileType");
									%>

									<c:set var="fileName" value="${val.file_name}" />
									<%
										fileName = (String) pageContext.getAttribute("fileName");
									%>


									<div class="col-md-3 col-sm-3">

										<li>
											<div class="thumbnail">


												<div class="caption">
													<p class="titl">
														<%-- ${val.file_name} --%>
													</p>
													<p class="text-center">
														<a href="" class="label label-default vimore"
															data-toggle="modal" data-target="#gallerypopup" href="#"
															onclick="viewGalleryFile('Open','GalleryFile','${val.file_name}','${val.file_type}')">View</a>
													</p>
												</div>

												<img src="${pageContext.request.contextPath}<%="/previewGallaryFile?fileCategory=GalleryFile&fileName="+fileName+""%>"
													 alt="image" class="img-responsive" onclick="viewGalleryFile('Open','GalleryFile','${val.file_name}','${val.file_type}')">

											</div>

										</li>

									</div>

								</c:forEach>




							</ul>



						</div>
						<!--! end of #content -->
					</div>
					<!--! end of #container -->
				</div>
			</div>
		</div>
		<!-- Schedule Section /- -->
	</div>







	<!-- Footer Main -->
	<%@include file="index_footer.jsp" %>
	<!-- END Footer Main -->
	
	
	<%@include file="index_common_script.jsp" %>





<!-- Image Popup Modal -->

<div id="gallerypopup" class="modal postpage fade" role="dialog">
  
  	
  
</div>



	
	
</body>
</html>