
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
   
   
<div class="contact-form-section knowmore">

	<div class="row">
	<div class="col-md-12">
	<h4 class="participatehdr">Applied Application</h4>
	</div>
	</div>
	<c:forEach items="${eventDetails}" var="val">
								
			
			<div class="">

					<div class="col-md-12">
						<div class="form-group">
							<div class="row">
												
							    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
								   
								   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Name</b> </div>
								   
								   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
								   
								   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${val.applicant_name}</span> </div>
									
								</div>
								
							</div>
						</div>
					</div>
		
					<div class="col-md-12">
						<div class="form-group">
							<div class="row">
												
							    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
								   
								   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Address</b> </div>
								   
								   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
								   
								   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${val.applicant_address}</span> </div>
									
								</div>
								
							</div>
						</div>
					</div>
		
					<div class="col-md-12">
						<div class="form-group">
							<div class="row">
								
								<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
									   
								   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Location</b></div>
								   
								   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
								   
								   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${val.applicant_city},${val.applicant_state},${val.applicant_pincode}</span>  </div>
									
								</div>
								
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							
							<div class="row">
							
								<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
									   
								   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Email id</b> </div>
								   
								   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
								   
								   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${userBean.email_id} ${userBean.mobile_no}</span> </div>
									
								</div>
								
							</div>
						
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							
							<div class="row">
							
								<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
									   
								   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Mobile no</b> </div>
								   
								   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
								   
								   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${userBean.mobile_no}</span> </div>
									
								</div>
								
							</div>
						
						</div>
					</div>
		
							
					<div class="col-md-12">
						<div class="form-group">
							<div class="row">
												
							    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
								   
								   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Affiliation</b> </div>
								   
								   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
								   
								   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${val.applicant_affiliation}</span> </div>
									
								</div>
								
							</div>
						</div>
					</div>
		
					<div class="col-md-12">
						<div class="form-group">
							<div class="row">
												
							    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
								   
								   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Council no</b> </div>
								   
								   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
								   
								   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${val.applicant_council_no}</span> </div>
									
								</div>
								
							</div>
						</div>
					</div>
					
					<c:set var="applicationamount" value="${val.applicant_amount}"/>
					<%float applicationamount = (Float)pageContext.getAttribute("applicationamount"); %>
					
					<%if(applicationamount!=0) {%>
						
							<div class="col-md-12">
								<div class="form-group">
									<div class="row">
														
									    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
										   
										   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Fee</b> </div>
										   
										   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
										   
										   <div class="col-md-9 col-sm-9 col-xs-5"> <span class="applicolor">Rs. <%=applicationamount%> /- Registration fee </span> </div>
											
										</div>
										
									</div>
								</div>
							</div>
					
					<%} %>			
					
					
					<div class="col-md-12">
						<div class="form-group">
							<div class="row">
												
							    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
								   
								   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Date</b> </div>
								   
								   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
								   
								   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${val.applied_date}</span> </div>
									
								</div>
								
							</div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<div class="row">
												
							    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
								   
								   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Status</b> </div>
								   
								   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
								   
								   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> Success</span> </div>
									
								</div>
								
							</div>
						</div>
					</div>
					
					
					<c:set var="appliedEvntId" value="${val.event.event_id}"/>
					<% int appliedEvntId = (Integer)pageContext.getAttribute("appliedEvntId"); %>
			
						<div class="row">
							<div class="col-md-12">
								<button type="button" class="buttoncs btn postbtn" value="Close" style="float: left; margin-top: 0;" onclick="closeParticipateForm('participateform<%=appliedEvntId%>')">Close</button>
							</div>
						</div>
		
				
			</div>
	
	</c:forEach>
	
	
	
	

</div>