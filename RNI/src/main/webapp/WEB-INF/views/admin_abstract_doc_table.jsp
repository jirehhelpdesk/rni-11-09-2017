<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
 
 
 <div class="container-fluid">
              
              <div class="row">
                	
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Abstract Submission</h3>
                    </div>
                    <div class="card-body">
                     
                     
                    <c:if test="${!empty abstractDocRecord}">	
                      
                   		   <table class="table table-striped table-hover" data-toggle="table" data-pagination="true">
	                            
		                        <thead>
		                        
		                          <tr>
		                            <th>Sl.No</th>
		                            <th>Title</th>
		                            <th>Name</th>
		                            <th>Email id</th>
		                            <th>Mobile</th>
		                            <th>File name</th>
		                            <th>Uploaded date</th>
		                            <th>Download</th>
		                          </tr>
		                          
		                        </thead>
	                            
		                        <tbody>
		                         
		                           <%int i = 1; %> 
		                           <c:forEach items="${abstractDocRecord}" var="val">
											
										<tr>
				                            <td scope="row"><%=i++%></td>
				                            <td>${val.abstract_title}</td>
				                            <td>${val.sender_name}</td>
				                            <td>${val.sender_email}</td>
				                            <td>${val.sender_mobile}</td>
				                            <td>${val.sender_file_name}</td>
				                            <td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.uploaded_date}"/></td>
				                            <td>
				                            	<a href="downloadAbstractDoc?fileName=${val.sender_file_name}" title="Download the document"><i class="fa fa-download"></i></a>
				                            </td>
				                        </tr>
										 	  
				                   </c:forEach>
		                          
		                        </tbody>
	                        
	                      </table>
                   	 		
                     </c:if>
                     
                     <c:if test="${empty abstractDocRecord}">	
                     
                     		<div style="color:red;text-align:center;font-size:18px;">
								As of now no one submitted abstract document.
							</div>
                     
                     </c:if> 
                      
                    </div>
                  </div>
                </div>
                	
                	
              </div>
              
 </div>