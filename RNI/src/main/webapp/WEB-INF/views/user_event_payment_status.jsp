<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import = "java.io.*,java.util.*,com.ccavenue.security.*" %>

<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
	
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Payment Status</title>

	
	<%@include file="index_common_style.jsp" %>
	
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">
	
	
	<!-- Loader And Header /- -->
	
	<%@include file="index_header.jsp" %>
	
	<!-- END of Loader and Header -->
	
	
	<!-- Schedule Section -->
	<div class="container-fluid no-padding schedule-section">
		<div class="section-padding"></div>
		<div class="container">
			<div class="section-header">
				<h3>Payment Status</h3>
				
			</div>	
			
			<div class="schedule-block">
				
				<!-- <img src="images/schedule.jpg" alt="schedule"/> -->
				
				<div class="col-md-11">
					
					<div class="tab-content">
						
						<div role="tabpanel" class="tab-pane fade in active" id="schedule_1" >
							<div class="panel-group schedule-accordion" id="accordion" role="tablist" aria-multiselectable="true">
								
								
										<div class="panel panel-default">
											
											
											<c:forEach items="${applicantDetails}" var="applicant">
								
			
												<div class="">
									
														<div class="col-md-12">
															<div class="form-group">
																<div class="row">
																					
																    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																	   
																	   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Name</b> </div>
																	   
																	   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																	   
																	   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${applicant.applicant_name}</span> </div>
																		
																	</div>
																	
																</div>
															</div>
														</div>
														
														<div class="col-md-12">
															<div class="form-group">
																<div class="row">
																					
																    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																	   
																	   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Email id</b> </div>
																	   
																	   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																	   
																	   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${applicant.applicant_email}</span> </div>
																		
																	</div>
																	
																</div>
															</div>
														</div>
														
														<div class="col-md-12">
															<div class="form-group">
																<div class="row">
																					
																    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																	   
																	   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Mobile</b> </div>
																	   
																	   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																	   
																	   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${applicant.applicant_mobile}</span> </div>
																		
																	</div>
																	
																</div>
															</div>
														</div>
											
														<div class="col-md-12">
															<div class="form-group">
																<div class="row">
																					
																    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																	   
																	   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Address</b> </div>
																	   
																	   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																	   
																	   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor">${applicant.applicant_address}</span> </div>
																		
																	</div>
																	
																</div>
															</div>
														</div>
														
														<div class="col-md-12">
															<div class="form-group">
																<div class="row">
																					
																    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																	   
																	   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Location</b> </div>
																	   
																	   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																	   
																	   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor">${applicant.applicant_city},${applicant.applicant_state} - ${applicant.applicant_pincode}</span> </div>
																		
																	</div>
																	
																</div>
															</div>
														</div>
														
														<div class="col-md-12">
															<div class="form-group">
																<div class="row">
																					
																    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																	   
																	   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Amount</b> </div>
																	   
																	   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																	   
																	   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${applicant.applicant_amount}</span> </div>
																		
																	</div>
																	
																</div>
															</div>
														</div>
														
														<div class="col-md-12">
															<div class="form-group">
																<div class="row">
																					
																    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																	   
																	   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Status</b> </div>
																	   
																	   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																	   
																	   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${applicant.applicant_status}</span> </div>
																		
																	</div>
																	
																</div>
															</div>
														</div>
														
														<div class="col-md-12">
															<div class="form-group">
																<div class="row">
																					
																    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
																	   
																	   <div class="col-md-2 col-sm-2 col-xs-4"> <b>Transaction Id</b> </div>
																	   
																	   <div class="gapMaintain col-md-1 col-sm-1 col-xs-1">:</div>
																	   
																	   <div class="col-md-9 col-sm-9 col-xs-5"><span class="applicolor"> ${applicant.applicant_transaction_id}</span> </div>
																		
																	</div>
																	
																</div>
															</div>
														</div>
														
														<div class="col-md-12">
															<div class="form-group">
																<div class="row">
																	 <div class="col-md-5 col-sm-5 col-xs-5"></div>
																	
																	 <div class="col-md-7 col-sm-7 col-xs-5">
																	     							     
																	        <input type="button" class="viewmre buttoncs btn mrgnknowmore" value="Print" />
																	        
														             </div>
														        </div>
															</div>
														</div>
														
												</div>
										
										</c:forEach>
											
										</div>
										
								
										
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- Schedule Section /- -->
	
	
	
	
	<!-- Footer Main -->
	<%@include file="index_footer.jsp" %>
	<!-- END Footer Main -->
	
	
	<%@include file="index_common_script.jsp" %>
	
</body>
</html>