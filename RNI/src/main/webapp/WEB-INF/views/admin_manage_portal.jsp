<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
 
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Manage Portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    
    
    <%@include file="admin_common_style.jsp" %>
    
    <style>
    .mngPortal strong
    {
    	font-size: 0.8em;
    }
    </style>
  </head>
  
  
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
  
  
  <body onload="activeMenu('menu6','Admin')">
    
    
    
    <div class="page form-page">
     
     
      <!-- Main Navbar-->
      <%@include file="admin_header.jsp" %>
      
      
      
      <div class="page-content d-flex align-items-stretch"> 
        
        
         <%@include file="admin_side_bar.jsp" %>
        
        
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Poral Management</h2>
            </div>
          </header>
          <div id="page-wrapper">
         
         
         <!-- Dashboard Header Section    -->
          <section class="dashboard-header">
            <div class="container-fluid">
              <div class="row">
                
                <!-- Statistics -->
                <div class="statistics col-lg-3 col-12 mngPortal">
                  
                  <div class="statistic d-flex align-items-center bg-white has-shadow managePrtlActive" id="conButnId" style="padding: 10px 10px;cursor:pointer;" onclick="portalMngFormAndRecord('Content')">
                    <div class="icon bg-red"><i class="fa fa-tasks"></i></div>
                    <div class="text"><strong>Add Content</strong></div>
                  </div>
                  
                  <div class="statistic d-flex align-items-center bg-white has-shadow" id="commButnId" style="padding: 10px 10px;cursor:pointer;" onclick="portalMngFormAndRecord('Committee')">
                    <div class="icon bg-green"><i class="fa fa-calendar-o"></i></div>
                    <div class="text"><strong>Add committee member</strong></div>
                  </div>
                  
                  <div class="statistic d-flex align-items-center bg-white has-shadow" id="fucButnId" style="padding: 10px 10px;cursor:pointer;" onclick="portalMngFormAndRecord('Faculty')">
                    <div class="icon bg-orange"><i class="fa fa-paper-plane-o"></i></div>
                    <div class="text"><strong>Add faculty</strong></div>
                  </div>
                  
                </div>
                
                
                <!-- Line Chart -->
               
                <div class="chart col-lg-9 col-12">
                  
                  
                  <div class="card" style="display:block;" id="conFormDiv" >
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Add Content</h3>
                    </div>
                    
                    <div class="card-body">
                      <p>Content means only heading is customize.</p>
                      
                         <div id="contentFormDiv"><%@include file="admin_content_form.jsp" %></div> 
                      
                    </div>
                    
                  </div>
                  
                  
                  
                  
                  
                  <div class="card" style="display:none;" id="fucFormDiv">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Add Faculty</h3>
                    </div>
                    <div class="card-body">
                      <p>Details will appear in portal.</p>
                      
                         <div id="facultyFormDiv"><%@include file="admin_faculty_form.jsp" %></div> 
                      
                    </div>
                  </div>
                  
                  
                  
                  
                  <div class="card" style="display:none;" id="commFormDiv">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Add committee member</h3>
                    </div>
                    <div class="card-body">
                      
                      <p>Details will appear in portal.</p>
                      
                        <div id="memberFormDiv"><%@include file="admin_member_form.jsp" %></div>
                      
                    </div>
                  </div>
                  
                  
                  
                  
                  
                  
                </div>
                
              </div>
            </div>
          </section>
          <!-- Projects Section-->
         
         
         
         
         
         
         
         
          <!-- Forms Section-->
          <%-- <section class="forms"> 
            <div class="container-fluid">
              
              <div class="row">
               
               
               
                <!-- Basic Form-->
                <div class="col-lg-6">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Add Faculty</h3>
                    </div>
                    <div class="card-body">
                      <p>Details will appear in portal.</p>
                      
                         <div id="facultyFormDiv"><%@include file="admin_faculty_form.jsp" %></div> 
                      
                    </div>
                  </div>
                </div>
                
                
                <!-- Horizontal Form-->
                
                <div class="col-lg-6">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Add committee member</h3>
                    </div>
                    <div class="card-body">
                      
                      <p>Details will appear in portal.</p>
                      
                        <div id="memberFormDiv"><%@include file="admin_member_form.jsp" %></div>
                      
                    </div>
                  </div>
                </div>
                
                
               
              </div>
              
              
              
              
              
            </div>
          </section> --%>
          
          <section id="contentDiv" class="tables" >   
            	
            	<%@include file="admin_content_table.jsp" %>
            
          </section>
          
          <section id="memberDiv" class="tables" >   
            	
            	<%@include file="admin_member_table.jsp" %>
            
          </section>
          
          <section id="facultyDiv" class="tables" >   
            
            	<%@include file="admin_faculty_table.jsp" %>
            
          </section>
          
          
          
          
          </div>
          <!-- Footer Main -->
			<%@include file="admin_footer.jsp" %>
		  <!-- END Footer Main -->
          
          
          
        </div>
      </div>
    </div>
    
    
    
    
    <%@include file="admin_common_script.jsp" %>
    
    
    
  </body>
</html>