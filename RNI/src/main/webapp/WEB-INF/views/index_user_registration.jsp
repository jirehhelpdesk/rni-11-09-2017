<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>

<%@include file="index_common_style.jsp"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="static/textEditor/css/style.css" rel="stylesheet"
	type="text/css" />
<script language="javascript" src="static/textEditor/js/calender.js"></script>
<link href="static/textEditor/css/calender_style.css" rel="stylesheet"
	type="text/css">


</head>

<%
	String layOutMessage = (String) request.getAttribute("layOutMessage");
%>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">



	<!-- Loader And Header /- -->

	<%@include file="index_header.jsp"%>

	<!-- END of Loader and Header -->

	<%
		String actionMsg = (String) request.getAttribute("actionMessage");
	%>

	<%
		String userName = "", asdasd = "", check = "";
		Cookie cookie = null;
		Cookie[] cookies = null;

		// Get an array of Cookies associated with this domain
		cookies = request.getCookies();

		if (cookies != null) {
			System.out.println("Hello world=" + userName);
			for (int i = 0; i < cookies.length; i++) {

				cookie = cookies[i];

				if (cookie.getName().equals("CheckRemember")) {
					check = cookie.getValue();
				}

				if (cookie.getName().equals("loginEmail")) {
					userName = cookie.getValue();
				}

				if (cookie.getName().equals("loginpassword")) {
					asdasd = cookie.getValue();
				}
			}
		}
	%>


	<div class="container-fluid  no-padding">
		<div class="container">
				<div class="cd-tabs">
					<nav>
					<ul class="cd-tabs-navigation">

						<%
							if (layOutMessage.equals("SignIn")) {
						%>

						<li><a href="#" data-content="login" class="selected">Sign
								In</a></li>

						<li><a href="#" data-content="signup">Sign Up</a></li>

						<%
							} else {
						%>

						<li><a href="#" data-content="login">Sign In</a></li>

						<li><a href="#" data-content="signup" class="selected">Sign
								Up</a></li>

						<%
							}
						%>

					</ul>
					</nav>

					<%
						if (actionMsg.equals("Active")) {
					%>

					<p class="activateMessagePara">Your Account activated
						successfully.</p>

					<%
						} else if (actionMsg.equals("Not a User")) {
					%>

					<p class="errorMessagePara">This email id is not registered
						yet.</p>

					<%
						} else if (actionMsg.equals("Deactive")) {
					%>

					<p class="errorMessagePara">Your account was Deactivated,Please
						activate first.</p>

					<%
						} else if (actionMsg.equals("Failed")) {
					%>

						<p class="errorMessagePara">Given credential was wrong.</p>

					<%} else if (actionMsg.equals("Link Expired")) {%>
						
						<p class="errorMessagePara">Given Link is expired please try again for reset password.</p>	
							
					<%}else{}%>

					<ul class="cd-tabs-content">


						<%
							if (layOutMessage.equals("SignIn")) {
						%>
						<li data-content="login" class="selected">
							<%
								} else {
							%>
						
						<li data-content="login">
							<%
								}
							%> <form:form class="booking-form-block" name="login-form"
								id="userLoginForm" modelAttribute="userRegDetails" method="post"
								action="authUserCredential">

								<div class="col-md-12 no-padding">
									<div class="col-md-12 no-padding">
										<div class="form-fild">
											<label for="loginEmail"></label> <input type="text"
												path="email_id" name="loginEmail" id="loginEmail"
												value="<%=userName%>" placeholder="Emailid (username)">
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-12 no-padding">
										<div class="form-fild">
											<label for="loginpassword"></label> <input type="password"
												name="loginpassword" id="loginpassword" path="password"
												value="<%=asdasd%>" placeholder="Password">
										</div>
									</div>
								</div>

								<%
									if (check.equals("Yes")) {
								%>

								<input type="hidden" name="CheckRemember" id="CheckRemember"
									value="Yes" />
								<label class="remeberMe"><input type="checkbox" checked
									name=rememberChkBox id="remember" onclick="rememberMe(this.id)">Remember
									me </label>

								<%
									} else {
								%>

								<input type="hidden" name="CheckRemember" id="CheckRemember"
									value="No" />
								<label class="remeberMe"><input type="checkbox"
									name=rememberChkBox id="remember" onclick="rememberMe(this.id)">Remember
									me </label>

								<%
									}
								%>

								<div class="col-md-12 no-padding">

									

									<div class="col-md-5 col-xs-7">
										<div class=" forgotpsw">
											<a href="#" data-toggle="modal" data-target="#myModal">Forgot
												Password?</a>
										</div>
									</div>
								<div class="col-md-7 col-xs-5">
									<div class="">
										<span class="error"></span>
										<button type="submit" class="loginbtn buttoncs btn">LogIn</button>
									</div>
								</div>

								</div>


							</form:form>
						</li>


						<%
							if (layOutMessage.equals("SignIn")) {
						%>
						<li data-content="signup">
							<%
								} else {
							%>
						
						<li data-content="signup" class="selected">
							<%
								}
							%> <form:form class="booking-form-block" id="userRegForm"
								modelAttribute="userRegDetails" method="post"
								action="saveUserRegister">


								<!-- ACCOUNT DETAILS -->
								<div class="col-md-12 no-padding">
									<div class="col-md-12 no-padding">
										<span style="color: #21a5b2; margin-left: 14px;">Account
											Details</span>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="first_name"></label> <input type="text" required
												class="" path="first_name" name="first_name" id="first_name"
												placeholder="First Name" />
											<div id="fNameErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="last_name"></label> <input type="text" class=""
												required path="last_name" name="last_name" id="last_name"
												placeholder="Last Name" />
											<div id="lNameErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="email_id"></label> <input type="email" class=""
												path="email_id" name="email_id" id="email_id"
												placeholder="Emailid (username)" />
											<div id="emailErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="mobile_no"></label> <input type="text" class=""
												maxlenght="10" path="mobile_no" name="mobile_no"
												id="mobile_no" placeholder="Mobile" />
											<div id="mobileErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="password"></label> <input type="password"
												class="" path="password" name="password" id="password"
												placeholder="Password" />
											<div id="pwErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="cnfPassword"></label> <input type="password"
												class="" name="cnfPassword" id="cnfPassword"
												placeholder="Confirm Password" />
											<div id="cnfPwErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-12 col-sm-12 no-padding">
										<div class="form-fild">
											<label for="profession"></label> <input type="text"
												name="profession" path="profession" id="profession"
												placeholder="Designation">
											<div id="professionErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>


								<div class="col-md-12 no-padding interestList">
									<div class="col-md-12">
										<label for="subscription_type">Get news letter</label>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label><input type="radio" name="subscription_type"
												path="subscription_type" id="subscription_type"
												value="Weekly">Weekly</label>
										</div>
									</div>

									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label><input type="radio" name="subscription_type"
												path="subscription_type" id="subscription_type"
												value="Monthly">Monthly</label>
										</div>
									</div>

									<div id="notifyErrId" class="errorStyle"></div>

								</div>


								<div class="col-md-12 no-padding interestList">
									<div class="col-md-12">
										<label for="subscription_type">Get Feeds update</label>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label><input type="radio" name="feeds_update"
												path="feeds_update" id="feeds_update" value="Yes">Yes</label>
										</div>
									</div>

									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label><input type="radio" name="feeds_update"
												path="feeds_update" id="feeds_update" value="No">No</label>
										</div>
									</div>

									<div id="fedUpdateErrId" class="errorStyle"></div>

								</div>

								<!-- PROFILE DETAILS -->



								<div class="col-md-12 no-padding">
									<div class="col-md-12 no-padding">
										<span style="color: #21a5b2; margin-left: 14px;">Profile
											Details</span>
									</div>
								</div>



								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<input type="text" name="dob" readonly path="dob" id="dob"
												class="datepickerWithoutTime" placeholder="Date of birth *" />
											<div id="dobErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<select name="gender" id="gender">
												<option value="Select gender">Select gender</option>
												<option value="Female">Female</option>
												<option value="Male">Male</option>
												<option value="Other">Other</option>
											</select>
											<div id="genderErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-12 no-padding">
										<span style="color: #21a5b2; margin-left: 14px;">Under
											graduation</span>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-12 col-sm-12 no-padding">
										<div class="form-fild">
											<label for="under_graduation"></label> <input type="text"
												name="under_graduation" id="under_graduation" class=""
												placeholder="Under graduation *" />
											<div id="graduatenErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="under_graduation_institute"></label> <input
												type="text" required class=""
												path="under_graduation_institute"
												name="under_graduation_institute"
												id="under_graduation_institute" placeholder="Institute name" />
											<div id="graInstErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="under_graduation_university"></label> <input
												type="text" class="" required
												path="under_graduation_university"
												name="under_graduation_university"
												id="under_graduation_university"
												placeholder="University name" />
											<div id="graUniverErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="under_graduation_place"></label> <input
												type="text" required class="" path="under_graduation_place"
												name="under_graduation_place" id="under_graduation_place"
												placeholder="Place" />
											<div id="grePlaceErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="under_graduation_state"></label> <input
												type="text" class="" required path="under_graduation_state"
												name="under_graduation_state" id="under_graduation_state"
												placeholder="State" />
											<div id="graStateErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>


								<div class="col-md-12 no-padding">
									<div class="col-md-12 no-padding">
										<span style="color: #21a5b2; margin-left: 14px;">Post graduation</span>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-12 col-sm-12 no-padding">
										<div class="form-fild">
											<label for="post_graduation"></label> <input type="text"
												name="post_graduation" id="post_graduation" class=""
												placeholder="Post graduation *" />
											<div id="pgErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="post_graduation_institute"></label> <input
												type="text" required class=""
												path="post_graduation_institute"
												name="post_graduation_institute"
												id="post_graduation_institute" placeholder="Institute name" />
											<div id="pgInstErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="post_graduation_university"></label> <input
												type="text" class="" required
												path="post_graduation_university"
												name="post_graduation_university"
												id="post_graduation_university"
												placeholder="University name" />
											<div id="pgUniverErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="post_graduation_place"></label> <input
												type="text" required class="" path="post_graduation_place"
												name="post_graduation_place" id="post_graduation_place"
												placeholder="Place" />
											<div id="pgPlaceErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="post_graduation_state"></label> <input
												type="text" class="" required path="post_graduation_state"
												name="post_graduation_state" id="post_graduation_state"
												placeholder="State" />
											<div id="pgStateErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>


								<div class="col-md-12 no-padding">
									<div class="col-md-12 no-padding">
										<span style="color: #21a5b2; margin-left: 14px;">Fellowship</span>
									</div>
								</div>
								

								<div class="col-md-12 no-padding">
									
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="fellowship"></label> <input type="text"
												name="fellowship" id="fellowship" class=""
												placeholder="Fellowship" />
											<div id="fellowErrId" class="errorStyle"></div>
										</div>
									</div>
									
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="fellowship_sub_speciality"></label> 
											<select required path="fellowship_sub_speciality" name="fellowship_sub_speciality" id="fellowship_sub_speciality">
												
												<option value="Select sub speciality">Select sub speciality</option>
												<option value="Surgical retina">Surgical retina</option>
												<option value="Medical retina">Medical retina</option>
												<option value="Uvea">Uvea</option>
												<option value="Ocular oncology">Ocular oncology</option>
												<option value="Neuro ophthalmology">Neuro ophthalmology</option>
												
											</select>	
											<div id="felSpeErrId" class="errorStyle"></div>
										</div>
									</div>
									
								</div>
								
								
								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="fellowship_institute"></label> 
											<input type="text" required class="" path="fellowship_institute" name="fellowship_institute" id="fellowship_institute" placeholder="Institute name" />
											<div id="felInstErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="fellowship_university"></label> 
											<input type="text" class="" required path="fellowship_university" name="fellowship_university" id="fellowship_university" placeholder="University name" />
											<div id="felUniverErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="fellowship_place"></label> 
											<input type="text" required class="" path="fellowship_place" name="fellowship_place" id="fellowship_place" placeholder="Place" />
											<div id="felPlaceErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="fellowship_state"></label> <input
												type="text" class="" required path="fellowship_state"
												name="fellowship_state" id="fellowship_state"
												placeholder="State" />
											<div id="felStateErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>
								
								
								
								
								

								<div class="col-md-12 no-padding">
									<div class="col-md-12 col-sm-12 no-padding">
										<div class="form-fild">
											<label for="additional_qualification"></label> <input
												type="text" name="additional_qualification"
												id="additional_qualification" class=""
												placeholder="Addition qualification" />
											<div id="addQuaErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-12 col-sm-12 no-padding">
										<div class="form-fild">
											<label for="practice_at"></label> <input type="text"
												name="practice_at" id="practice_at" class=""
												placeholder="Practicing  at (address)" />
											<div id="praAtErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<!-- <div class="col-md-12 no-padding">
									<div class="col-md-12 col-sm-12 no-padding">
										<div class="form-fild">
											<label for="practice_as"></label> <input
												type="text" name="practice_as" id="practice_as" class=""
												placeholder="Practice as" />
											<div id="praAsErrId" class="errorStyle"></div>
										</div>
									</div>
								</div> -->

								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="year_of_experience"></label> <input type="text"
												name="year_of_experience" id="year_of_experience" class=""
												placeholder="Year of experience" />
											<div id="yoeErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="phone_number"></label> <input type="text"
												name="phone_number" id="phone_number" class=""
												placeholder="Phone no" />
											<div id="phoneErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>


								<div class="col-md-12 no-padding">
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="website_url"></label> <input type="text"
												name="website_url" id="website_url" class=""
												placeholder="Website URL" />
											<div id="websiteErrId" class="errorStyle"></div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 no-padding">
										<div class="form-fild">
											<label for="pincode"></label> <input type="text"
												name="pincode" id="pincode" class="" placeholder="Pincode" />
											<div id="pincodeErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12 no-padding interestList">
									
									<div class="col-md-12">
										<label for="subscription_type">Area of interest</label>
									</div>
									
									<div class="col-md-4 col-sm-4 no-padding">
										<div class="form-fild">
											<label class="remeberMe"><input type="checkbox"
												name="area_of_interest" id="area_of_interest"
												value="Vitreo Retina" class=""
												onclick="showOtherInterest(this.value)" /> Vitreo Retina</label>
										</div>
									</div>
									
									<div class="col-md-4 col-sm-4 no-padding">
										<div class="form-fild">
											<label class="remeberMe"><input type="checkbox"
												name="area_of_interest" id="area_of_interest"
												value="Medical Retina" class=""
												onclick="showOtherInterest(this.value)" /> Medical Retina</label>
										</div>
									</div>
									
									<div class="col-md-4 col-sm-4 no-padding">
										<div class="form-fild">
											<label class="remeberMe"><input type="checkbox"
												name="area_of_interest" id="area_of_interest" value="Uvea"
												class="" onclick="showOtherInterest(this.value)" /> Uvea</label>
										</div>
									</div>
									
									<div class="col-md-4 col-sm-4 no-padding">
										<div class="form-fild">
											<label class="remeberMe"><input type="checkbox"
												name="area_of_interest" id="area_of_interest" value="Ocular oncology"
												class="" onclick="showOtherInterest(this.value)" /> Ocular oncology</label>
										</div>
									</div>
									
									<div class="col-md-4 col-sm-4 no-padding">
										<div class="form-fild">
											<label class="remeberMe"><input type="checkbox"
												name="area_of_interest" id="area_of_interest" value="Neuro ophthalmology"
												class="" onclick="showOtherInterest(this.value)" /> Neuro ophthalmology</label>
										</div>
									</div>
									
									<div class="col-md-4 col-sm-4 no-padding">
										<div class="form-fild">
											<label class="remeberMe"><input type="checkbox"
												name="area_of_interest" id="area_of_interest" value="Other"
												class="" onclick="showOtherInterest(this.value)" /> Other</label>
										</div>
									</div>


									<div class="form-fild">


										<div id="interestBox" style="display: none;">
											<input type="text" name="interest" id="interest" class=""
												placeholder="Other Interest" />
										</div>

										<div id="interestErrId" class="errorStyle"></div>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-12 col-sm-12 no-padding">
										<span style="color: #21a5b2; margin-left: 14px;">Profile
											Picture</span>
										<div class="form-fild">
											<input type="file" id="file" name="file" class="">
											<div id="fileErrId" class="errorStyle"></div>
										</div>
									</div>
								</div>



								<div class="col-md-12 no-padding">
									<span class="error"></span>
									<button type="button" class="buttoncs btn" onclick="saveUserRegister()">Register
										Now</button>
								</div>
							</form:form>
						</li>
					</ul>
				</div>
				<!-- end cd-tabs -->
		</div>
	</div>







	<!-- Footer Main -->
	<%@include file="index_footer.jsp"%>
	<!-- END Footer Main -->


	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content forgot">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Forgot Password</h4>
				</div>
				<div class="modal-body">
					<form name="login-form" class="booking-form-block">
						<div class="form-fild">
							<input type="text" id="forgetPwEmailId" name="forgetPwEmailId" placeholder="Enter registered email id">
							<div id="linkEmailErrId" class="errorStyle"></div>
						</div>
						<div class="row">
						<div class="col-md-12">
						<button type="button" class="buttoncs btn" onclick="sendLinkForForgetPw()">Submit</button>
						</div>
						</div>
					</form>
				</div>
				<!--<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>-->
			</div>

		</div>
	</div>








	<%@include file="index_common_script.jsp"%>

	<script>
		'use strict';

		window.addEventListener('load', windowLoaded, false);

		function windowLoaded() {
			var tabs = document.querySelectorAll('.cd-tabs')[0], login = document
					.querySelectorAll('a[data-content=\'login\']')[0], signup = document
					.querySelectorAll('a[data-content=\'signup\']')[0], tabContentWrapper = document
					.querySelectorAll('ul.cd-tabs-content')[0], currentContent = document
					.querySelectorAll('li.selected')[0];

			login.addEventListener('click', clicked, false);
			signup.addEventListener('click', clicked, false);

			function clicked(event) {
				event.preventDefault();

				var selectedItem = event.currentTarget;
				if (selectedItem.className === 'selected') {
					// ...       
				} else {
					var selectedTab = selectedItem.getAttribute('data-content'), selectedContent = document
							.querySelectorAll('li[data-content=\''
									+ selectedTab + '\']')[0];

					if (selectedItem == login) {
						signup.className = '';
						login.className = 'selected';
					} else {
						login.className = '';
						signup.className = 'selected';
					}

					currentContent.className = '';
					currentContent = selectedContent;
					selectedContent.className = 'selected';

				}
			}

			var inputs = document.querySelectorAll('input');
			for (var i = 0; i < inputs.length; i++) {
				inputs[i].addEventListener('focus', inputFocused, false);
			}

			function inputFocused(event) {
				var label = document.querySelectorAll('label[for=\''
						+ this.name + '\']')[0];
				label.className = 'focused';
			}
		}
	</script>


</body>
</html>