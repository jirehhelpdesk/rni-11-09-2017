<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>



	
	<div class="simplebar">
		
	<div class="searchhight boxscroll">
		
		<ul>
		
			<c:forEach items="${userSearchList}" var="val">
										
										
				<c:set var="userId" value="${val.user_id}"/>
				<% int userIden = (Integer)pageContext.getAttribute("userId"); %>
		 		
		 		<c:if test="${!empty val.profile}">								
		 				
		 				<c:set var="profilePhoto" value="${val.profile.profile_photo}"/>
						<% String profilePhoto = (String)pageContext.getAttribute("profilePhoto"); %>
									
						<li>
					
						<a href="inViewUserProfile?userId=<%=userIden%>">
							
							<%if(profilePhoto.equals("")){ %>
								
								<div class="prof_img" style="background: #21a5b2; padding: 5px;">
									<img src="static/IndexResources/images/user.png">
								</div>
							
							<%}else{ %>
							
								<div class="prof_img">
									<img src="${pageContext.request.contextPath}<%="/previewProfilePhoto?fileName="+profilePhoto+"&userId="+userIden+""%>">
								</div>
							
							<%}%>
								
								<div class="content">
									<h4>${val.first_name} ${val.last_name}</h4>
									<p>${val.email_id}</p>
									<p><b>Practice As :</b> ${val.profile.practice_as}</p>
									<p><b>Practice At :</b> ${val.profile.practice_at}</p>
								</div>
						</a>
		 		
		 		</c:if>
		 		
		 		<c:if test="${empty val.profile}">								
		 				
						<li>
					
						<a href="inViewUserProfile?userId=<%=userIden%>">
							
								<div class="prof_img" style="background: #21a5b2; padding: 5px;">
									<img src="static/IndexResources/images/user.png">
								</div>
							
								<div class="content">
									<h4>${val.first_name} ${val.last_name}</h4>
									<p>${val.email_id}</p>
									<p><b>Practice As :</b> ${val.profile.practice_as}</p>
									<p><b>Practice At :</b> ${val.profile.practice_at}</p>
								</div>
						</a>
						
		 		</c:if>
		 										
				
				
				</li>
			
			</c:forEach>
				
			
			
			
		</ul>
	</div>
</div>

<script src="static/IndexResources/js/custome_scrollbar.js"></script>
<script src="static/IndexResources/js/jasny-bootstrap.js"></script>
<link href="static/IndexResources/css/jasny-bootstrap.css" rel="stylesheet"></link>