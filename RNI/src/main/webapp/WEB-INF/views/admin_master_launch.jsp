<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Manage Event Record</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    
    
    <%@include file="admin_common_style.jsp" %>
    
    

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

  </head>
  
  
  <body onload="activeMenu('menu11','Admin')">
    
    
    
    <div class="page form-page">
     
     
      <!-- Main Navbar-->
      <%@include file="admin_header.jsp" %>
      
      
      
      <div class="page-content d-flex align-items-stretch"> 
        
        
         <%@include file="admin_side_bar.jsp" %>
        
        
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Event before launch</h2>
            </div>
          </header>
          
          
          <div id="page-wrapper">
          
          
          
          <!-- Forms Section-->
          
          
          <section id="tableDiv" class="tables">   
            
            	<%@include file="admin_launch_event_table.jsp" %>
            
          </section>
          
          <section id="tableDiv" class="tables" style="margin-top:-50px;">   
            
            	<%@include file="admin_pre_conf_workshop_table.jsp" %>
            
          </section>
          
          <section id="tableDiv" class="tables" style="margin-top:-50px;">   
            
            	<%@include file="admin_abstract_doc_table.jsp" %>
            
          </section>
          
          
          </div>
          
         <!-- Footer Main -->
			<%@include file="admin_footer.jsp" %>
		  <!-- END Footer Main -->
          
          
          
          
        </div>
      </div>
    </div>
    
    
    
    
    <%@include file="admin_common_script.jsp" %>
    
    
    
  </body>
</html>