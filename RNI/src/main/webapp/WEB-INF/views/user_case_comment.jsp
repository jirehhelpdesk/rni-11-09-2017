<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

									
<c:if test="${!empty caseComment}">		
	
	<!-- <h3><span>3</span> Comments</h3> -->
	
	<c:forEach items="${caseComment}" var="caseCmnt">
		
		  <c:set var="userId" value="${caseCmnt.user.user_id}"/>
		  <% int userIden = (Integer)pageContext.getAttribute("userId"); %>
		  
		  <c:set var="caseId" value="${caseCmnt.caseModel.case_id}"/>
		  <% int caseIdCmtFile = (Integer)pageContext.getAttribute("caseId"); %>
		  
		  <% String profilePhoto = ""; %>
		  
		            	                      
		  <div class="media">
												
			<div class="media-left">
				
				<a href="#" title="${caseCmnt.user.first_name}">
					
					 <c:if test="${!empty caseCmnt.user.profile}">
                 		
                 		<c:set var="profilePhoto" value="${caseCmnt.user.profile.profile_photo}"/>
						<% profilePhoto = (String)pageContext.getAttribute("profilePhoto"); %>
						
						<%if(!profilePhoto.equals("")){ %>
                 			<img src="${pageContext.request.contextPath}<%="/previewProfilePhoto?fileName="+profilePhoto+"&userId="+userIden+""%>" class="media-object" width="97" height="97"/>
                		<%}else{ %>
                			<!-- <i class="fa fa-user"></i>  -->
                			<img src="static/IndexResources/images/user.png" class="media-object" width="97" height="97"/>
                		<%} %>
                		
	                 </c:if>
	                 
	                 <c:if test="${empty caseCmnt.user.profile}">
	                 		<img src="static/IndexResources/images/user.png" class="media-object" width="97" height="97"/>
	                 </c:if>
					
				</a>
				
			</div>
			
			<div class="media-body">
				
				<div class="media-content">
					
					<h4 class="media-heading casecommentheading">
						${caseCmnt.user.first_name} ${caseCmnt.user.last_name}<span><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${caseCmnt.case_comment_cr_date}" /></span>
					</h4>
					<div class="row">
					<div class="col-md-12">
					<c:if test="${!empty caseCmnt.caseCommentFile}">
					
						<div class="commentFile commntimg cashcomtimg">
	                  	 <ul>
	                  	
		                  	<c:forEach items="${caseCmnt.caseCommentFile}" var="caseCmtFile"> 	
		                  		
		                  		<li>
			                  		<c:set var="commentFileType" value="${caseCmtFile.case_comment_file_type}"/>
									<% String commentFileType = (String)pageContext.getAttribute("commentFileType"); %>
									
									<c:set var="commentFile" value="${caseCmtFile.case_comment_file}"/>
									<% String commentFile = (String)pageContext.getAttribute("commentFile"); %>
									
									<c:set var="casCmtFileId" value="${caseCmtFile.case_comment_file_id}"/>
									<% int casCmtFileId = (Integer)pageContext.getAttribute("casCmtFileId"); %>
									
			                  	    <%if(commentFileType.equals("Image")) {%>
		        
							        	<img  src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseCommentFile&fileName="+commentFile+"&caseId="+caseIdCmtFile+"&userId="+userIden+""%>" width="70"   onclick="showCaseCommentFile('Open','CaseCommentFile','Image','<%=commentFile%>','<%=caseIdCmtFile%>','<%=userIden%>')">
							        
							        <%}else if(commentFileType.equals("Video")) { %>
							            
							             <span class="thickbox play-button-link" onclick="showCaseCommentFile('Open','CaseCommentFile','Video','<%=commentFile%>','<%=caseIdCmtFile%>','<%=userIden%>')">
							            
								            <span id="renderImageCmnt<%=casCmtFileId%>" >
													
													<%
														int pos = commentFile.lastIndexOf(".");
													    String renderfileName = commentFile.substring(0, pos)+".png";
										            %>
										            
													<img  width="70"  alt="<%=commentFile%>" src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseCommentFile&fileName="+renderfileName+"&caseId="+caseIdCmtFile+"&userId="+userIden+""%>">
													<img class="play-button alignPlayInPopUp commentplbtn" alt="" src="static/IndexResources/images/Play1Pressed.png" onclick="showCaseCommentFile('Open','CommentFile','Video','<%=commentFile%>','<%=caseIdCmtFile%>','<%=userIden%>')"/>
													
											</span>
											
											<span id="renderVideoCmnt"> </span>
									
										</span> 
							        
							        <%}else if(commentFileType.equals("Pdf")) { %>
							        
							        	<%
											int pos = commentFile.lastIndexOf(".");
											String renderfileName = commentFile.substring(0, pos)+".png";
									     %>
							        	<a title="Load to click on the pdf" href="#" onclick="showCaseComntPdfFile('CaseCommentFile','<%=commentFile%>','<%=caseIdCmtFile%>','<%=userIden%>')"> 
								
											<img  width="70" alt="<%=commentFile%>" src="${pageContext.request.contextPath}<%="/previewFile?fileCategory=CaseCommentFile&fileName="+renderfileName+"&caseId="+caseIdCmtFile+"&userId="+userIden+""%>" /> 
											
										</a>
											
							        <%}else{ %>		
							        
							        
							        <%} %>
	        				  </li>
	        				  
	        				</c:forEach>
	       				
	       				 </ul>
	       				 
	                   </div>
					
					</c:if>
					</div>
					</div>
					<div class="row">
					<div class="col-md-12">
					<p class="casecommentpra">${caseCmnt.case_comment_message}</p>
					</div>
					   </div>
				</div>											
			</div>
			
		</div>
		
		
		
	
	</c:forEach>
	
</c:if>

<c:if test="${empty caseComment}">		

	<div class="recent-content" style="color:red;text-align:center;font-size:18px;">
		As of now nobody commented on this case !
	</div>
	
</c:if>
	
	
<script src="static/IndexResources/js/custome_scrollbar.js"></script>
<script src="static/IndexResources/js/jasny-bootstrap.js"></script>
<link href="static/IndexResources/css/jasny-bootstrap.css" rel="stylesheet"></link>