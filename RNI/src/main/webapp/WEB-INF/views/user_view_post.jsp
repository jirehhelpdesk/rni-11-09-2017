<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
<!-- dfgdgfg -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>View Post</title>

	
	<%@include file="index_common_style.jsp" %>
	
</head>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<!-- View Post Page -->

<!-- UI update View Post Page -->

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation" onload="activeMenu('menu1','User')">
	
	
	
	<!-- Loader And Header /- -->
	
	<%@include file="index_header.jsp" %>
	
	
	<!-- END of Loader and Header -->
	
	<div class="container-fluid page-banner about no-padding">
			<div class="container">
				<div class="banner-content-block">
					<div class="banner-content">
						<h3>View Feeds</h3>
					</div>
				</div>
			</div>
	</div>
	
	<div class="viewpostbg">
		<div class="container-fluid eventlist upcoming-event latest-blog no-padding">
		
		<div class="manageSectionPadding section-padding"></div>
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-3 col-sm-12 col-xs-12 widget-area">
					
					<%@include file="user_side_menu.jsp" %>
					
					
					
					<aside class="widget">
	
						<div class="widget-title">
							<span class="icon icon-PaperClip"></span>
							<h3>Arrange feeds</h3>
						</div>
						
						<div class="event-categories" style="margin-bottom: 12px;">
											
							<div class="event-select-option">
								<select class="selectpicker" title="Choose one of the following..." id="filterFeedId" onchange="showFilterFeedOption(this.value)">
									<option data-icon="icon-Folder" value="Filter feeds">Filter feeds</option>
									<option data-icon="icon-Files" value="Most viewed feeds">Most viewed feeds</option>
									<!-- <option data-icon="icon-Floppy" value="Most liked feeds">Most liked feeds</option> -->
									<option data-icon="icon-Floppy" value="Feed of the discussion">Feed of the discussion</option>
									<option data-icon="icon-File" value="Find feeds with duration">Find feeds with duration</option>
								</select>
							</div>
							
							<div class="event-select-option" id="yearDiv" style="display:none;">
								<select class="FilterDropdown" id="yearSelect" onchange="showMonth(this.value)">
									<option data-icon="icon-User" value="Select Year">Select Year</option>
								</select>
							</div>
							
							<div class="event-select-option" id="monthDiv" style="display:none;">
								<select class="FilterDropdown" id="monthSelect" onchange="showFeedViaMonth(this.value)">
									<option data-icon="icon-Tag" value="Select Month">Select Month</option>			
								</select>
							</div> 
							
							<!-- <a href="#" title="Filter">Filter</a> -->
							
						</div>
						
					</aside>
					
					
					
				</div>
				
				

				<div class="col-md-9 col-sm-12 col-xs-12 content-area" id="postViewDiv">
					<div class="widget-area mypostfeed" style="width:100%;">
					
						<aside class="widget widget_recent">
						
							<div class="widget-title">
								<span class="icon icon-Blog"></span>
								<h3>View Feed
								</h3>
								
							</div>
							
							<div class="recent-block">
						
					
					<c:set var="userIdentityId" value="${userBean.user_id}"/>
					<% int userIdentityId = (Integer)pageContext.getAttribute("userIdentityId"); %>
	
				    
					<c:if test="${!empty myAllPost}">	
									
					   <c:forEach items="${myAllPost}" var="val">
							
							<% String fileType = "";%>
							<% String fileName = "";%>
						
							<% int n = 1;%>
							
							<c:forEach items="${val.postFile}" var="filVal">
								
								<%if(n==1){ %>
								
									<c:set var="fileType" value="${filVal.post_file_type}"/>
									<% fileType = (String)pageContext.getAttribute("fileType"); %>
									
									<c:set var="fileName" value="${filVal.post_file_name}"/>
									<% fileName = (String)pageContext.getAttribute("fileName"); %>
								    
							    <%} %>
								
								<%n = n + 1; %>
								
							</c:forEach>
							
							<c:set var="postId" value="${val.post_id}"/>
							<% int postId = (Integer)pageContext.getAttribute("postId"); %>
							  
							<c:set var="userId" value="${val.user.user_id}"/>
							<% int userId = (Integer)pageContext.getAttribute("userId"); %>
							
							
							
							
							<div class="col-md-12 col-sm-12 col-xs-12 mypostview viewpost">
							
							
								<article class="">
								
									<%long like = 0;long dislike = 0;String userInterest = ""; %>
									
								    <div class="entry-meta">
										
										<div class="post-date">
											<a href="#" title=""><i class="fa fa-calendar" aria-hidden="true"></i><span><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.post_cr_date}" /></span></a>
										</div>
										
										<div class="comment">
											
											<span title="View" ><i class="fa fa-eye" aria-hidden="true"></i><small id="viewPost<%=postId%>">${val.post_view}</small></span>
											&nbsp;
											
											<!-- START OF LIKE DISLIKE SECTION -->
											
											<c:if test="${!empty postInterest}">	
												
												<c:forEach items="${postInterest}" var="postInterest">
												
														<c:set var="interestPostId" value="${postInterest.post.post_id}"/>
														<%int interestPostId = (Integer)pageContext.getAttribute("interestPostId"); %>
														
														<%if(interestPostId==postId){%>  
															
															<c:set var="interest" value="${postInterest.interest}"/>
															<%String interest = (String)pageContext.getAttribute("interest"); %>
															
															<%if(interest.equals("Like")){ %>
																<%like = like + 1; %>
														    <%}else{ %>
														        <%dislike = dislike + 1; %>
														    <%} %>
															
															<c:set var="interestUserId" value="${postInterest.user.user_id}"/>
															<% int interestUserId = (Integer)pageContext.getAttribute("interestUserId"); %>
																
																<%if(userIdentityId==interestUserId){     
																	
																	userInterest = interest;
																}%>
														
														<%}%>
														
												</c:forEach>
												
											</c:if>
											
											
											
											
											<%if(userInterest.equals("")){ %>
											
											    <span title="Like" onclick="showInterest('Like','<%=postId%>')">
												     <i id="like<%=postId%>" class="fa fa-thumbs-o-up" aria-hidden="true"></i>
												     <small id="likeCount<%=postId%>"><%=like%><%-- ${val.post_like} --%></small>
												</span>
												
												&nbsp;
												
												<span title="Dislike" onclick="showInterest('Dislike','<%=postId%>')">
													<i id="disLike<%=postId%>" class="fa fa-thumbs-o-down" aria-hidden="true"></i>
													<small id="disLikeCount<%=postId%>"><%=dislike%><%-- ${val.post_dislike} --%></small>
												</span>
											
											<%}else if(userInterest.equals("Like")){ %>
											
												
												<span title="Like" onclick="showInterest('Like','<%=postId%>')">
												     <i id="like<%=postId%>" class="fa fa-thumbs-up" aria-hidden="true"></i>
												     <small id="likeCount<%=postId%>"><%=like%><%-- ${val.post_like} --%></small>
												</span>
												
												&nbsp;
												
												<span title="Dislike" onclick="showInterest('Dislike','<%=postId%>')">
													<i id="disLike<%=postId%>" class="fa fa-thumbs-o-down" aria-hidden="true"></i>
													<small id="disLikeCount<%=postId%>"><%=dislike%><%-- ${val.post_dislike} --%></small>
												</span>
											
											
											<%}else{ %>
											
												 <span title="Like" onclick="showInterest('Like','<%=postId%>')">
												     <i id="like<%=postId%>" class="fa fa-thumbs-o-up" aria-hidden="true"></i>
												     <small id="likeCount<%=postId%>"><%=like%><%-- ${val.post_like} --%></small>
												</span>
												
												&nbsp;
												
												<span title="Dislike" onclick="showInterest('Dislike','<%=postId%>')">
													<i id="disLike<%=postId%>" class="fa fa-thumbs-down" aria-hidden="true"></i>
													<small id="disLikeCount<%=postId%>"><%=dislike%><%-- ${val.post_dislike} --%></small>
												</span>
											
											<%} %>
											
											
											<!-- END OF LIKE DISLIKE SECTION -->
											
											
											
											&nbsp;
											<c:set var = "comment" value = "${val.comment}" />
											<span title="Comment"><i class="fa fa-comment" aria-hidden="true"></i><small id="cmtDiv${val.post_id}">${fn:length(comment)}</small></span>
										
										</div>
										
									</div>
								
									
									         <%if(fileType.equals("Image")) {%>
												
												<div class="entry-cover">
													<a class="sizeimg" data-toggle="modal" data-target="#postpopup"  title="Click to know more" href="#" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">
													    
													    <img class="center-block"  src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" />
													  
													</a>
													
													<c:if test = "${val.download_privilage == 'Yes'}">
														<a href="#" onclick="downloadPostFile('${val.post_id}','<%=userId%>')"><i class="fa fa-download downloadicn"></i></a>
													</c:if>
													
												</div>
												
												<%}else if(fileType.equals("Video")) { %>
													
													<div data-toggle="modal" data-target="#postpopup"  class="entry-cover" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">
														
														<span  class="thickbox play-button-link sizeimg">
															
															<span id="renderImage<%=postId%>" >
																	
																	<%
																		int pos = fileName.lastIndexOf(".");
																	    String renderfileName = fileName.substring(0, pos)+".png";
														             %>
																	<img class="center-block videoupsideimg"  alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>">
																	<img class="play-button" alt="" src="static/IndexResources/images/Play1Pressed.png" onclick="showVideo('NoPopUp','PostFile','<%=fileName%>','<%=userId%>','<%=postId%>')"/>
															
															</span>
															
															<span id="renderVideo<%=postId%>"> </span>
															
														</span>
														
														<c:if test = "${val.download_privilage == 'Yes'}">
														   
														   <a href="#" onclick="downloadPostFile('${val.post_id}','<%=userId%>')"><i class="fa fa-download downloadicn"></i></a>
													    
													    </c:if>
														
													</div>
												
												<%}else if(fileType.equals("Pdf")) { %>
												
													<div class="entry-cover">
														<a  data-toggle="modal" data-target="#postpopup"  title="Click to know more" href="#" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">
															
															<%
																int pos = fileName.lastIndexOf(".");
																String renderfileName = fileName.substring(0, pos)+".png";
														    %>
															<img class="center-block" alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>" /><!-- style="height:175px;"  --> 
															
															
														</a>
														
														<c:if test = "${val.download_privilage == 'Yes'}">
														   
														   <a href="#" onclick="downloadPostFile('${val.post_id}','<%=userId%>')"><i class="fa fa-download downloadicn"></i></a>
													    
													    </c:if>
													</div>
												
												<%}else if(fileType.equals("URL")) { %>
												
													<div class="entry-cover">
														<!--  class="viewmore" -->
														<a data-toggle="modal" data-target="#postpopup"  title="Click to know more" href="#" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">
															
															<iframe style="height:180px;width: 100%;" src="<%=fileName%>" frameborder="0" allowfullscreen></iframe>
															
														</a>
													</div>
												
												<%}else {%>
												
												<%}%>
												
													
												<c:if test="${empty val.postFile}">	
														
														<div class="entry-block" style="width:100%;">
															<div class="feed-showmore-height ">
															<div class="entry-title">
																<h3>${val.post_title}</h3>
															</div>							
															
															<div class="entry-content">
																<p>${val.post_desc}</p>
															</div>
															</div>
															<div class="show-more">
        <img src="static/IndexResources/images/log-in.png">&nbsp;<a href="#" data-toggle="modal" data-target="#postpopup" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">View Details</a>
    </div>
							
														</div>
												
												</c:if>
												
												<c:if test="${!empty val.postFile}">	
														
														<div class="entry-block">
															<div class="feed-showmore-height">
															<div class="entry-title">
																<h3>${val.post_title}</h3>
															</div>							
															
															<div class="entry-content">
																<p>${val.post_desc}</p>
															</div>
															</div>
															<div class="show-more">
        <img src="static/IndexResources/images/log-in.png">&nbsp;<a href="#" data-toggle="modal" data-target="#postpopup" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">View Details</a>
    </div>
															
														
														</div>
												
												</c:if>
												
												
									
								</article>
								
								
							</div>
						
							
						</c:forEach>		    
						
					</c:if>    
					    
					<c:if test="${empty myAllPost}">
					
							<div class="recent-content" style="color:red;text-align:center;font-size:18px;">
								As of now no one didn't post anything.
							</div>
							
					</c:if> 
					
						</div>
							</aside>
							</div>
					
				</div>
				
				
				
				
				
				
				
			</div>
		</div>
		<div class="section-padding"></div>
	</div>
	</div>
	
	
	
	
	
	
	<!-- Footer Main -->
	<%@include file="index_footer.jsp" %>
	<!-- END Footer Main -->
	
	<div id="postpopup" class="modal fade postpage" role="dialog">
  
  
    </div>



<!--end view details Popup  -->	
	
<%@include file="index_common_script.jsp" %>


<script src="static/adminResource/js/jquery.nicescroll.min.js"></script>


<script>

  //Hide Overflow of Body on DOM Ready //

     $(document).ready(
		function() {
		$(".popupdecs").niceScroll({cursorcolor:"#fff",autohidemode:false, zindex: 999});
		}
		);
		$(function() {  
		$('.reference-long').click(function(){
		    $(".popupdecs").getNiceScroll().resize();
		    $(".popupdecs").niceScroll({cursorcolor:"#fff",autohidemode:false, zindex: 999});
		  });
		});


</script>
	
</body>
</html>