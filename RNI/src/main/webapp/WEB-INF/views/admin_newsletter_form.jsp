<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


  <form:form  id="newsForm" modelAttribute="newsLetter" method="post" action="saveNewsRecord">
	                        
	  <div class="form-group">
	    <input type="text" placeholder="Newsletter heading" name="news_matter" path="news_matter" id="news_matter" class="form-control">
	    <span id="headErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group">
	    <select name="sent_user_type" path="sent_user_type" id="sent_user_type" class="form-control" onchange="showSTypeOpton(this.value)">
	    		<option value="To whom you want to send">To whom you want to send</option>
	    		<option value="Registred User">Registered User</option>
	    		<option value="Nonregistred User">Non-registered User</option>
	    </select>
	    <span id="uTypeErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group" style="display:none;" id="sTypeId">
	    <select name="sent_type" path="sent_type" id="sent_type" class="form-control" onchange="showUserCount(this.value)">
	    		<option value="Select which type subscriber">Select which type subscriber</option>
	    		<option value="Daily">Daily</option>
	    		<option value="Weekly">Weekly</option>
	    		<option value="Both">Both</option>
	    </select>
	    <span id="sTypeErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group">       
	    <label class="form-control-label">No.of Reciever</label>
	    <input type="text" placeholder="No.of reciever" readonly name="user_count" path="user_count" id="user_count" class="form-control">
	    <span id="countErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group">       
	    <label class="form-control-label">Attach File</label>
	    <input type="file" multiple name="files" id="files" class="form-control">
	    <span id="fileErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group row">
	  	<div class="offset-sm-10">       
	   	    <input type="button" value="Send" class="btn btn-primary" onclick="sendNewsLetter()">
	   	</div>
	  </div>
			  
   </form:form>