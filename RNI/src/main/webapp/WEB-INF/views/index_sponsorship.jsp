<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Sponsorship</title>

	
	<%@include file="index_common_style.jsp" %>
	
</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
 
<body data-offset="200" data-spy="scroll" data-target=".ow-navigation"  onload="activeMenu('menu3','index')">
	
	
	
	<!-- Loader And Header /- -->
	
	<%@include file="index_header.jsp" %>
	
	<!-- END of Loader and Header -->
	
	
	
	
	
	
	<!-- PageBanner -->
<div class="container-fluid page-banner about no-padding">
			<div class="container">
				<div class="banner-content-block">
					<div class="banner-content">
						<h3>Sponsorship</h3>
					</div>
				</div>
			</div><!-- 
		<div class="section-padding"></div> -->
	</div>
<!-- PageBanner /- --> 

<!-- Main Section -->
<div class="sponsorshipbg">
<div class="container-fluid eventsingle upcoming-event latest-blog our-history no-padding">
  <div class="container">
        <div class="entry-title text-left">
          <h3 class="hdrcnt text-left mrqwidth">
          <marquee scrolldelay="50" scrollamount="10" direction="left" onmouseover="this.stop();" onmouseout="this.start();">KMC will award 4 credit points to the delegates and 5 credit points to the faculty attending RETNET INDIA meet.</marquee></h3>
        </div>
        <div class="row">
        <div class="col-md-6 col-sm-6">
        <div class="entry-content">
          <p class="text-left pra">It is our great pleasure to invite the sponsorships for our upcoming RetnetIndia meet.</p>
        </div>
      
        <div class="entry-title text-left">
          <h3 class="sponsorhdr text-left">Retnetindia: Largest online discussion group of more than 900 practicing vitreo-retina specialists all over the globe</h3>
        </div>
        <div class="entry-content">
          <p class="text-left pra">This group was Co-founded by Dr. Mahesh P Shanmugam (Sankara Eye Hospitals, Bangalore) and Dr. Avinash Pathangey (LVPEI, Vishakhapatnam). There have been 5 successful conferences of this group in the past.</p>
        <p class="text-left pra">Conference dates this year are June 10th and 11th, 2017, and will be held in Clarks Exotica, Bangalore.This venue offers high quality conference facilities along with good quality food and accommodation and is conveniently located near Bangalore airport.</p>
        <p class="text-left pra">The event is for vitreoretina and uvea specialists and covers vast aspects of both medical and surgical management of various vitreo-retinal diseases. The event will start with the formal launch. This conference is unique as it comprises of challenging case presentations and great discussions and  is most awaited retina conference attended by 250-300 vitreoretina specialists.</p>
        </div>
        </div>
        <div class="col-md-6  col-sm-6">
				<div class="hurryup-block">
					<div id="timer-slider" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner aboutcarsl" role="listbox">
							<div class="item active">
								<div class="timer-box">
									<img src="static/IndexResources/images/sponsor1.jpg">	
								</div>
							</div>
							<div class="item">
								<div class="timer-box">
									<img src="static/IndexResources/images/sponsor2.jpg">
								</div>
							</div>
							<div class="item">
								<div class="timer-box">
									<img src="static/IndexResources/images/sponsor3.jpg">		
								</div>
							</div>
							
							
							
						</div>
						<!-- Controls -->
						<a class="left carousel-control" href="#timer-slider" role="button" data-slide="prev">
							<i class="fa fa-angle-left" aria-hidden="true"></i>
						</a>
						<a class="right carousel-control" href="#timer-slider" role="button" data-slide="next">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</a>
					</div>
				</div>
			</div>
      </div>
    <div class="row">
      <div class="col-md-12 col-sm-12"> 
        
        <!-- Schedule Section -->
        <div class="container-fluid no-padding schedule-section">
          <div class="section-header">
            <h3>Retnetindia Organising Committee</h3>
          </div>
             <%@include file="index_member_list.jsp" %>
        </div>
        <!-- Schedule Section /- --> 
        
      </div>
      
      <%-- <div class="col-md-3 col-sm-4 widget-area">
        	<%@include file="index_faculty_list.jsp" %>
      </div> --%>
      
    </div>
  </div>
  <div class="section-padding"></div>
</div>
	</div>
	
	
	
	
	<!-- Footer Main -->
	<%@include file="index_footer.jsp" %>
	<!-- END Footer Main -->
	
	
	<%@include file="index_common_script.jsp" %>

	
	
</body>
</html>