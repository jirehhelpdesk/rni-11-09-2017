<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<title>RetNet India</title>
<link rel="icon" type="image/x-icon" href="static/IndexResources/images/favicon.ico" />
<head>

<script src="static/pdf_viewer/pdfjs/build/pdf.js"></script>
<script src="static/pdf_viewer/pdfjs/build/pdf.worker.js"></script>

<script src="static/pdf_viewer/pdfjs/web/debugger.js"></script>
<script src="static/pdf_viewer/pdfjs/web/l10n.js"></script>
<script src="static/pdf_viewer/pdfjs/web/viewer.js"></script>


<link rel="stylesheet" type="text/css" href="static/pdf_viewer/pdfjs/web/viewer.css">
	
	
</head>

<body>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%          
      String fileCategory = (String)request.getAttribute("fileCategory");
	  String fileName = (String)request.getAttribute("fileName");
	  String userId = (String)request.getAttribute("userId");
	  String postId = (String)request.getAttribute("postId");
			
%>


<%if(fileCategory.equals("PostFile")){ %>

	<%-- <iframe  src="https://docs.google.com/viewer?url=http://www.medicineskaya.com/RNI/showPdf?fileCategory=PostFile&fileName=<%=fileName%>&userId=<%=userId%>&postId=<%=postId%>&embedded=true"
		style="width: 600px; height: 500px;" frameborder="0">
		
	</iframe> --%>
	
	<%-- <iframe src="http://mozilla.github.com/pdf.js/web/viewer.html?file=${pageContext.request.contextPath}<%="/showPdf?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" width="100%" height="100%"></iframe>
	
	<embed src="${pageContext.request.contextPath}<%="/showPdf?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" width="550" height="750"></embed>
	 --%>
	<%-- <object data="${pageContext.request.contextPath}<%="/showPdf?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>"  type="application/pdf" width="100%" height="100%">
	<p>Please add plug-in pdf viewer for this browser.</p>
	</object> --%>
	
	
	<canvas id="the-canvas" style="border:1px solid black;" ></canvas>
	
	
	
<%}%>

<%if(fileCategory.equals("CommentFile")){ %>	
	
	<!-- <div id="example1"></div> -->
	
	<object data="${pageContext.request.contextPath}<%="/showPdf?fileCategory=CommentFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>"  type="application/pdf" width="100%" height="100%"></object>
	
<%}%>


	<%-- <script src='static/pdf_viewer/pdfobject.js'></script>
	<script>
	   PDFObject.embed("${pageContext.request.contextPath}<%="/showPdf?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>",document.body);
	</script> --%>



<script type="text/javascript">

PDFJS.getDocument('${pageContext.request.contextPath}<%="/showPdf?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>').then(function(pdf) {
	  pdf.getPage(1).then(function(page) {
	    var scale = 1.5;
	    var viewport = page.getViewport(scale);

	    var canvas = document.getElementById('the-canvas');
	    var context = canvas.getContext('2d');
	    canvas.height = viewport.height;
	    canvas.width = viewport.width;

	    var renderContext = {
	      canvasContext: context,
	      viewport: viewport
	    };
	    page.render(renderContext);
	  });
	});
</script>



</body>

</html>
