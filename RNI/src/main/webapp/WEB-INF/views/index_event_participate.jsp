<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="description" content="">
<meta name="author" content="">
<title>Event Participate</title>
<%@include file="index_common_style.jsp"%>

<style>

.accommodation-header {
	font-size: 16px !important;
	color: #222 !important;
	font-weight: 400 !important;
	font-family: Open Sans, sans-serif;
	letter-spacing: 0.39px;
	text-align: left;
}

.table>thead:first-child>tr:first-child>th {
	border-top: 0;
	font-size: 16px !important;
	color: #222 !important;
	font-weight: 500 !important;
	font-family: Open Sans, sans-serif;
	letter-spacing: 0.39px;
	text-align: left;
	border: 1px solid #ddd;
}

.pleas-fill {
	border-top: 0;
	font-size: 16px !important;
	color: #222 !important;
	font-weight: 500 !important;
	font-family: Open Sans, sans-serif;
	letter-spacing: 0.39px;
	text-align: left;
}

.ulist {
	padding: 0 30px;
}

.no-margin-bm {
	margin-bottom: 0;
}

.table-bordered {
	border: 0px solid #ddd;
}

.eventlistimg {
	width: 100%;
	height: auto;
	margin-bottom: 11px;
}

.popupimg {
	padding: 5px;
	margin-bottom: 13px;
	text-align: center;
}

.popupimg img {
	max-height: 436px;
}
</style>

</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ page import = "java.io.*,java.util.*" %>
<%@page import="java.text.SimpleDateFormat"%>
					
<%
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	Date today = new Date();
	Date lastDate = sdf.parse("2018-06-01");
%>	
											
<body>
	
	<header class="header-main container-fluid no-padding"> <!-- Menu Block -->
	<div class="menu-block container-fluid no-padding affix-top"
		data-spy="affix" data-offset-top="100">
		<!--   -->
		<!-- Container -->
		<div class="container">
			<!-- User -->
			<div class="col-md-12">
				<a href="#"> <img src="static/IndexResources/images/logo.png"
					alt="logo" class="retnet center-block" width="170">
				</a>
			</div>
		</div>
		<!-- Container /- -->
	</div>
	
	<!-- Menu Block /- --> 
	</header>
	
	
	
	<div class="container-fluid page-banner about no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="banner-content-block">
				<div class="banner-content">
					<h3>Register for 10th RETNET Meeting</h3>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	
	<div class="eventlist">
		<div
			class="container-fluid eventlist upcoming-event latest-blog no-padding">
			<div class="container indexevent">
				<div class="row">
					<div class="col-md-12">
						<article class="type-post shadow-box">
						<div class="entry-block knowmore">
							<div class="col-md-12">
								<div class="entry-title">
									<h3 style="font-size: 22px ! important;font-weight: bold ! important;"> 10th RETNET MEETING </h3><br>
									<span style="color:#21a5b2;">31st May 2018 is the last date for online registration</span>
								</div>
								<div class="entry-meta">
									<div class="post-date eventdate">
										<p>
											Date<span>Location</span>
										</p>
									</div>
									<div class="post-metablock">
										<div class="post-time">
											<span>9/06/2018 - 10/06/2018</span>
										</div>
										<div class="post-location">
											<span>Bangalore, Venue- Clarks Exotica.</span>
										</div>
									</div>
								</div>
								
								
								<div class="entry-content">
									<h3 class="accommodation-header">Accommodation will be provided on first come first serve basis</h3>
									<h4 class="pleas-fill">Please read the following instructions carefully.</h4>
									<ul class="ulist">
										<li>All fields are mandatory in the registration from.</li>
										<li>Payment through online transfer only.</li>
									</ul>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Categories</th>
												<th>Online registration on or before 31/05/2018</th>
												<th>Onsite</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Registration only</td>
												<td>Rs.4500/- (Per Person)</td>
												<td>Rs.5000/- (Per Person)</td>
											</tr>
											<tr>
												<td colspan="3">(Registration includes 2 Lunch + 1 Dinner)</td>
											</tr>
										</tbody>
									</table>
								</div>
								
								<div class="entry-content">
									<h3 class="accommodation-header">Accommodation has been arranged at the venue a total of 75 rooms will be allotted on first come first serve basis.</h3>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>Categories</th>
												<th>Online registration on or before 31/05/2018</th>
												<th>from 1st June 2018 till onsite</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Single Occupancy + Registration</td>
												<td>Rs.7500 (Per Person)</td>
												<td>Rs.9500 (Per Person)</td>
											</tr>
											<tr>
												<td>Double Occupancy + Registration</td>
												<td>Rs.5800 (Per Person)</td>
												<td>Rs.7750 (Per Person)</td>
											</tr>
											<tr>
												<td>Triple Occupancy + Registration</td>
												<td>Rs.5250 (Per Person)</td>
												<td>Rs.7250 (Per Person)</td>
											</tr>
										</tbody>
									</table>
									<p>We will get back to you confirming the registration at the earliest</p>									
								</div>
								
								<div id="participateform10" class="participateform">
									<div class="contact-form-section knowmore">
										
										<div class="row">
											<div class="col-md-12">
												<h4 class="participatehdr text-center">Registration Form</h4>
											</div>
										</div>
										
										<div class="">
											
											<form:form  id="eventParticipateForm10" >
												
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<input type="text" class="form-control"
																	placeholder="First Name" value="" name="fName"
																	id="fName" required="">
																<div id="fNameErrId" class="errorStyle"></div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<input type="text" class="form-control"
																	placeholder="Last Name" value="" name="lName"
																	 id="lName" required="">
																<div id="lNameErrId" class="errorStyle"></div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-12">
													<div class="form-group">
														<textarea class="form-control" placeholder="Address"
															name="applicant_address" path="applicant_address"
															id="applicant_address" required></textarea>
														<div id="addressErrId" class="errorStyle"></div>
													</div>
												</div>
												
												<div class="col-md-4">
													<div class="form-group">
														<input type="text" class="form-control" placeholder="City"
															name="applicant_city" path="applicant_city"
															id="applicant_city" required="">
														<div id="cityErrId" class="errorStyle"></div>
													</div>
												</div>
												
												<div class="col-md-4">
													<div class="form-group">
														<input type="text" class="form-control"
															placeholder="State" name="applicant_state"
															path="applicant_state" id="applicant_state" required="">
														<div id="stateErrId" class="errorStyle"></div>
													</div>
												</div>
												
												<div class="col-md-4">
													<div class="form-group">
														<input type="text" class="form-control"
															placeholder="PIN Code" name="applicant_pincode"
															path="applicant_pincode" id="applicant_pincode"
															required="">
														<div id="pincodeErrId" class="errorStyle"></div>
													</div>
												</div>
												
												<div class="col-md-12">
													<div class="row">
														
														<div class="col-md-6">
															<div class="form-group">
																<input type="email" class="form-control"
																	placeholder="Email" value="" name="applicant_email"
																	path="applicant_email" id="applicant_email" required="">
																<div id="emailErrId" class="errorStyle"></div>
															</div>
														</div>
														
														<div class="col-md-6">
															<div class="form-group">
																<input type="text" class="form-control"
																	placeholder="Mobile" value="" name="applicant_mobile"
																	path="applicant_mobile" id="applicant_mobile">
																<div id="mobileErrId" class="errorStyle"></div>
															</div>
														</div>
														
													</div>
												</div>
												
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<input type="text" class="form-control"
																	placeholder="Affiliation / Current Practice Location"
																	name="applicant_affiliation"
																	path="applicant_affiliation" id="applicant_affiliation"
																	required="">
																<div id="affErrId" class="errorStyle"></div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<input type="text" class="form-control"
																	placeholder="Medical Registration Council No"
																	name="applicant_council_no" path="applicant_council_no"
																	id="applicant_council_no" required="">
																<div id="councilErrId" class="errorStyle"></div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-12">
													<div class="form-group">
														Food Preference : 
														&nbsp;&nbsp;
														<input type="radio" checked class="css-input css-radio css-radio-warning push-10-r" value="Veg" name="applicant_food_preference" path="applicant_food_preference" id="applicant_food_preference" > Veg
														&nbsp;&nbsp;
														&nbsp;&nbsp;
														<input type="radio" class="css-input css-radio css-radio-warning push-10-r" value="Non-Veg" name="applicant_food_preference" path="applicant_food_preference" id="applicant_food_preference" >Non Veg
														
														<div id="foodErrId" class="errorStyle"></div>
													</div>
												</div>
												
												<%  if (today.before(lastDate)) {
													System.out.println("today is before lastDate");%>        
											    	
											    	<div class="col-md-12">
														<div class="form-group no-margin-bm">
															<label class="css-input css-radio css-radio-warning push-10-r">
																<input type="radio" value="0.5" name="applicant_amount" path="applicant_amount"  checked onchange="changeRegCate('Registration only')"> <span></span>
																Rs. 4500 /- Registration only (per person)
															</label>
														</div>
													</div>
												
													<div class="col-md-12">
														<div class="form-group no-margin-bm">
															<label class="css-input css-radio css-radio-warning push-10-r">
																<input type="radio" value="1" name="applicant_amount" path="applicant_amount" onchange="changeRegCate('Single Occupancy + Registration')"> <span></span>
																Rs. 7500 /- Single Occupancy + Registration (per person)
															</label>
														</div>
													</div>
												
													<div class="col-md-12">
														<div class="form-group no-margin-bm">
															<label class="css-input css-radio css-radio-warning push-10-r">
																<input type="radio" value="1" name="applicant_amount" path="applicant_amount" onchange="changeRegCate('Double Occupancy + Registration')"> <span></span>
																Rs. 5800 /- Double Occupancy + Registration (per person)
															</label>
														</div>
													</div>
													
													<div class="col-md-12">
														<div class="form-group no-margin-bm">
															<label class="css-input css-radio css-radio-warning push-10-r">
																<input type="radio" value="1" name="applicant_amount" path="applicant_amount" onchange="changeRegCate('Triple Occupancy + Registration')"> <span></span>
																Rs. 5250 /- Triple Occupancy + Registration (per person)
															</label>
														</div>
													</div>
											    
												<%}else{%>
												
													<div class="col-md-12">
														<div class="form-group no-margin-bm">
															<label class="css-input css-radio css-radio-warning push-10-r">
																<input type="radio" value="1" name="applicant_amount" path="applicant_amount"  checked onchange="changeRegCate('Registration only')"> <span></span>
																Rs. 5000 /- Registration only (per person)
															</label>
														</div>
													</div>
												
													<div class="col-md-12">
														<div class="form-group no-margin-bm">
															<label class="css-input css-radio css-radio-warning push-10-r">
																<input type="radio" value="1" name="applicant_amount" path="applicant_amount" onclick="changeRegCate('Single Occupancy + Registration')"> <span></span>
																Rs. 9500 /- Single Occupancy + Registration (per person)
															</label>
														</div>
													</div>
												
													<div class="col-md-12">
														<div class="form-group no-margin-bm">
															<label class="css-input css-radio css-radio-warning push-10-r">
																<input type="radio" value="1" name="applicant_amount" path="applicant_amount" onclick="changeRegCate('Double Occupancy + Registration')"> <span></span>
																Rs. 7700 /- Double Occupancy + Registration (per person)
															</label>
														</div>
													</div>
													
													<div class="col-md-12">
														<div class="form-group no-margin-bm">
															<label class="css-input css-radio css-radio-warning push-10-r">
																<input type="radio" value="1" name="applicant_amount" path="applicant_amount" onchange="changeRegCate('Triple Occupancy + Registration')"> <span></span>
																Rs. 7250 /- Triple Occupancy + Registration (per person)
															</label>
														</div>
													</div>
													
												<%}%>
												
												<br>
												<br>
												<br>
												
												<div class="col-md-12" id="romSharingInfo" style="display:none;margin-top: 16px;">
													Mention with whom you want to share the room
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<input type="text" class="form-control"
																	placeholder="Full name"
																	name="room_partner_name"
																	path="room_partner_name" id="room_partner_name"
																	required="">
																<div id="partNameErrId" class="errorStyle"></div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<input type="text" class="form-control"
																	placeholder="Mobile no"
																	name="room_partner_mobile" path="room_partner_mobile"
																	id="room_partner_mobile" required="">
																<div id="partMobileErrId" class="errorStyle"></div>
															</div>
														</div>
													</div>
													
													
													<div class="row" id="2ndPartnerInfo" style="display:none;">
														<div class="col-md-6">
															<div class="form-group">
																<input type="text" class="form-control"
																	placeholder="Full name"
																	name="room_partner_name_2"
																	path="room_partner_name_2" id="room_partner_name_2"
																	required="">
																<div id="partName2ErrId" class="errorStyle"></div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<input type="text" class="form-control"
																	placeholder="Mobile no"
																	name="room_partner_mobile_2" path="room_partner_mobile_2"
																	id="room_partner_mobile_2" required="">
																<div id="partMobile2ErrId" class="errorStyle"></div>
															</div>
														</div>
													</div>
													
													
												</div>
												
												
												<input type="hidden" value="Registration only" name="reg_category" id="reg_category"/>
												
												<div class="row">
													<div class="col-md-12">
														<button type="button" class="buttoncs btn postbtn"value="" style="float: right; margin-top: 0;"
															onclick="saveApplication('participateform10','eventParticipateForm10','Before Reg')">Submit</button>
													</div>
												</div>
												
											</form:form>
											
											
										</div>
									</div>
								</div>
							</div>
						</div>
						
						
						<div class="entry-cover">
							<div class="portfolio_block columns3 pretty"
								data-animated="fadeIn">
								<div class="element  gall branding ">
									<a href="static/IndexResources/images/Flyer_1.jpg" class="viewmore" title="View Event images" target="_blank">
										<img class="eventlistimg" src="static/IndexResources/images/Flyer_1.jpg" alt="history" width="310" height="246">
									</a>
								</div>
								<div class="element  gall branding ">
									<a href="static/IndexResources/images/Flyer_2.jpg" class="viewmore" title="View Event images" target="_blank">
										<img class="eventlistimg" src="static/IndexResources/images/Flyer_2.jpg" alt="history" width="310" height="246">
									</a>
								</div>
								<div class="element  gall branding ">
									<a href="static/IndexResources/images/Flyer_3.jpg" target="_blank" class="viewmore" title="View Event images">
										<img class="eventlistimg" src="static/IndexResources/images/Flyer_3.jpg" alt="history" width="310" height="246">
									</a>
								</div>
								<div class="element  gall branding ">
									<a href="static/IndexResources/images/Flyer_4.jpg" target="_blank" class="viewmore" title="View Event images">
										<img class="eventlistimg" src="static/IndexResources/images/Flyer_4.jpg" alt="history" width="310" height="246">
									</a>
								</div>
								<div class="element  gall branding ">
									<a href="static/IndexResources/images/Flyer_5.jpg" target="_blank" class="viewmore" title="View Event images">
										<img class="eventlistimg" src="static/IndexResources/images/Flyer_5.jpg" alt="history" width="310" height="246">
									</a>
								</div>
							</div>
						</div>
						
						</article>
					</div>
				</div>
			</div>
			<div class="section-padding"></div>
		</div>
	</div>
	
	<footer class="footer-main container-fluid no-padding"> <!-- Container -->
	<div class="container">
		<div class="footer-menu">
			<!-- Copyrights -->
			<div class="copyrights ow-pull-left">
				<p>� 2018 | All rights reserved by Retnetindia</p>
			</div>
			<!-- Copyrights /- -->
			<!-- Navigation -->
			<nav class="ow-pull-right foltleft">
			<div class="">
				<ul class="nav navbar-nav">
					<li><span class="dgnpra">Design &amp; Developed by <a
							target="_" href="http://www.jirehsol.co.in/" class="dvby">Jireh</a></span></li>
				</ul>
			</div>
			</nav>
			<!-- Navigation /- -->
		</div>
		<!-- Footer Menu /- -->
	</div>
	<!-- Container /- --> </footer>
	
	<div id="commentPopUp" style="display:none;"></div>
	
	
	<div id="preloader" style="display: none;">
	
		<div class="cssload-container">
			<div class="cssload-whirlpool"></div>
		</div>
		
	</div>
	
	
	<%@include file="index_common_script.jsp"%>
</body>
</html>