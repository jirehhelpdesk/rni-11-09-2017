<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<%          
      String fileCategory = (String)request.getAttribute("fileCategory");
	  String fileName = (String)request.getAttribute("fileName");
	  String userId = (String)request.getAttribute("userId");
	  String postId = (String)request.getAttribute("postId");
	  String fileType = (String)request.getAttribute("fileType");		
%>


<div id="commentLight">

	<a href="#">
	
	   <%if(fileType.equals("Image")){ %>
	
	  		 <img src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>"  alt="" style="width:100%;height:100%;">
	  
	   <%}else{ %>
			
			<video controls style="width:100%;height:100%;" >
			  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/mp4" style="width:100%;height:100%;" >
			  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/ogg" style="width:100%;height:100%;">
			</video>
		
	   <%} %>
	   	
	</a>

</div>


<div id="commentFade" onClick="showPostFile('Close','PostFile','<%=fileName%>','<%=userId%>','<%=postId%>')"></div> 
