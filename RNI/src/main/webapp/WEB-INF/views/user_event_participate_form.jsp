
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
   
   
 <!-- <script>
	window.onload = function() {
		var d = new Date().getTime();
		document.getElementById("tid").value = d;
	};
</script> -->

<div class="contact-form-section knowmore">
<div class="row">
	<div class="col-md-12">
	<h4 class="participatehdr">Participate Form</h4>
	</div>
	</div>
	
	<c:forEach items="${eventDetails}" var="val">
								
			<c:set var="evntId" value="${val.event_id}"/>
			<% int evntId = (Integer)pageContext.getAttribute("evntId"); %>
			
			<c:set var="charge" value="${val.event_charge}"/>
			<% String charge = (String)pageContext.getAttribute("charge"); %>
			<% float amount = 0;%>
			
			<%if(charge.equals("Yes")) {%>
			
				<c:set var="amount" value="${val.event_charge_fee}"/>
				<% amount = (Float)pageContext.getAttribute("amount"); %>
			
			<%}%>
			
	
			<div class="">

				<form:form  id="eventParticipateForm${val.event_id}" modelAttribute="evntApplication" method="post" action="saveApplicaitionPassToPayment" >	          	
          
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Full Name" value="${userBean.first_name} ${userBean.last_name}" name="applicant_name" path="applicant_name" id="applicant_name" required />
							<div id="nameErrId" class="errorStyle"></div>
						</div>
					</div>
		
					<div class="col-md-12">
						<div class="form-group">
							<textarea  class="form-control" placeholder="Address" name="applicant_address" path="applicant_address" id="applicant_address" required ></textarea>
							<div id="addressErrId" class="errorStyle"></div>
						</div>
					</div>
		
					<div class="col-md-4">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="City" name="applicant_city" path="applicant_city" id="applicant_city" required />
							<div id="cityErrId" class="errorStyle"></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="State" name="applicant_state" path="applicant_state" id="applicant_state" required />
							<div id="stateErrId" class="errorStyle"></div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="PIN Code" name="applicant_pincode" path="applicant_pincode" id="applicant_pincode" required />
							<div id="pincodeErrId" class="errorStyle"></div>
						</div>
					</div>
		
					<div class="col-md-12">
						<div class="form-group">
							<input type="email" class="form-control" placeholder="Email" value="${userBean.email_id}" name="applicant_email" path="applicant_email" id="applicant_email" required />
							<div id="emailErrId" class="errorStyle"></div>
						</div>
					</div>
		
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Mobile" value="${userBean.mobile_no}" name="applicant_mobile" path="applicant_mobile" id="applicant_mobile" />
							<div id="mobileErrId" class="errorStyle"></div>
						</div>
					</div>
		
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Affiliation / Current Practice Location" name="applicant_affiliation" path="applicant_affiliation" id="applicant_affiliation" required />
							<div id="affErrId" class="errorStyle"></div>
						</div>
					</div>
		
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Medical Registration Council No" name="applicant_council_no" path="applicant_council_no" id="applicant_council_no" required />
							<div id="councilErrId" class="errorStyle"></div>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label class="css-input css-radio css-radio-warning push-10-r">
								<%if(charge.equals("Yes")) {%>
			
									<input type="radio" value="<%=amount%>" checked name="applicant_amount" path="applicant_amount" id="applicant_amount"><span></span>Rs. <%=amount%>/-Registration only
									
								<%}else{%>
								
									<input type="radio" value="0" checked name="applicant_amount"  path="applicant_amount" id="applicant_amount"><span></span>No Fee
									
								<%} %>
								
							</label>
						</div>
		
					</div>
		
						<div class="row">
							<div class="col-md-12">
								<button type="button" class="buttoncs btn postbtn" value="" style="float: left; margin-top: 0;" onclick="closeParticipateForm('participateform<%=evntId%>')">Close</button>
								
								<button type="button" class="buttoncs btn postbtn" value="" style="float: right; margin-top: 0;" onclick="saveApplication('participateform<%=evntId%>','eventParticipateForm<%=evntId%>','After Reg')">Submit</button>
								
							</div>
						</div>
					
				</form:form>
				
			</div>
	
	</c:forEach>
	
	
	
	

</div>