<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Events</title>

	
	<%@include file="index_common_style.jsp" %>
	
</head>


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<body data-offset="200" data-spy="scroll" data-target=".ow-navigation" onload="activeMenu('menu5','index')">
	
	
	
	<!-- Loader And Header /- -->
	
	<%@include file="index_header.jsp" %>
	
	<!-- END of Loader and Header -->
	
	
	
	
	
	
	<!-- PageBanner -->
<div class="container-fluid page-banner about no-padding">
		<div class="section-padding"></div>
			<div class="container">
				<div class="banner-content-block">
					<div class="banner-content">
						<h3>Events</h3>
					</div>
				</div>
			</div><!-- 
		<div class="section-padding"></div> -->
	</div>
<!-- PageBanner /- --> 




<!-- Main Section -->
<div class="eventlist">
<div class="container-fluid eventlist upcoming-event latest-blog no-padding">
		<div class="container indexevent">
			
			<div class="row">
				
				<div class="col-md-12">
						
					 <c:if test="${!empty eventList}">		
						
						<c:forEach items="${eventList}" var="val">
								
							<c:set var="eventId" value="${val.event_id}"/>
							<% int eventId = (Integer)pageContext.getAttribute("eventId"); %>
							
							<c:set var="eventCode" value="${val.event_code}"/>
							<% String eventCode = (String)pageContext.getAttribute("eventCode"); %>
													
													 
								<article class="type-post shadow-box">
							
										
										<div class="entry-cover">
											<div class="portfolio_block columns3 pretty" data-animated="fadeIn">
												
												<% int i = 0; %>
												<c:forEach items="${val.image}" var="image">
													
													<% i = i + 1; %>
													
													<c:set var="fileName" value="${image.file_name}"/>
													<% String fileName = (String)pageContext.getAttribute("fileName"); %>
													
													<%if(i==1){ %>
													
														<div class="element  gall branding ">
															
															<a href="#" class="viewmore" title="View Event images" data-toggle="modal" data-target="#postpopup" onclick="viewEventImage('${val.event_code}')">
															
															    <img class="eventlistimg" src="${pageContext.request.contextPath}<%="/previewEventMedia?fileCategory=EventFile&fileName="+fileName+"&eventId="+eventId+""%>" alt="history" width="310" height="246" />
															
															</a>
														</div>
													
													<%}%>
													
												</c:forEach>
												
											</div>
			
										</div>
										
										
										<div class="entry-block knowmore">
										<div class="eventshowmore-height">
											<div class="entry-title"><h3>${val.event_title}</h3>
											</div>
											<div class="entry-meta">
												<div class="post-date eventdate">
													<p>
														Date<span>Location</span>
													</p>
												</div>
												<div class="post-metablock">
													<div class="post-time">
														<span><fmt:formatDate pattern="dd/MM/yyyy" value="${val.event_start_date}" /> - <fmt:formatDate pattern="dd/MM/yyyy" value="${val.event_end_date}" /></span>
													</div>
													<div class="post-location">
														<span>${val.event_location}</span>
													</div>
												</div>
											</div>
											<div class="entry-content">
												<p>${val.event_desc}</p>
											</div>
											</div>
											<a href="eventOverview?event=<%=eventCode%>" class="viewmre buttoncs btn mrgnknowmore" title="Learn More" onclick="">Know More</a>
											
											<!-- <a href="#" class="learn-more" title="Learn More" >Participate</a> -->
											
										</div>
										
										
									</article>
						
						    </c:forEach>
						
						</c:if>
						
						<c:if test="${empty eventList}">	
						
								<div class="recent-content" style="color:red;text-align:center;font-size:18px;">
									As of now RetNet India didn't planed for any event.
								</div>
							
						</c:if>
						
						
				
				</div>
				
			</div>
		</div>
		<div class="section-padding"></div>
	</div>
	</div>
	
	
	
	<!-- Footer Main -->
	<%@include file="index_footer.jsp" %>
	<!-- END Footer Main -->
	
	
	<%@include file="index_common_script.jsp" %>

	
	
</body>
</html>