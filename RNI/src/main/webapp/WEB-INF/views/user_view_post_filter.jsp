<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
          
          <div class="widget-area mypostfeed" style="width:100%;">
					
						<aside class="widget widget_recent">
						
							<div class="widget-title">
								<span class="icon icon-Blog"></span>
								<h3>View Feed
								</h3>
								
							</div>
							
							<div class="recent-block">
							
<c:set var="userIdentityId" value="${userBean.user_id}"/>
<% int userIdentityId = (Integer)pageContext.getAttribute("userIdentityId"); %>
	
	
	<c:if test="${!empty myAllPost}">	
		
						
	   <c:forEach items="${myAllPost}" var="val">
			
			<% String fileType = "";%>
			<% String fileName = "";%>
		
			<% int n = 1;%>
			
			<c:forEach items="${val.postFile}" var="filVal">
				
				<%if(n==1){ %>
				
					<c:set var="fileType" value="${filVal.post_file_type}"/>
					<% fileType = (String)pageContext.getAttribute("fileType"); %>
					
					<c:set var="fileName" value="${filVal.post_file_name}"/>
					<% fileName = (String)pageContext.getAttribute("fileName"); %>
				    
			    <%} %>
				
				<%n = n + 1; %>
				
			</c:forEach>
			
			<c:set var="postId" value="${val.post_id}"/>
			<% int postId = (Integer)pageContext.getAttribute("postId"); %>
			  
			<c:set var="userId" value="${val.user.user_id}"/>
			<% int userId = (Integer)pageContext.getAttribute("userId"); %>
			
			
			
			
			<div class="col-md-12 col-sm-12 col-xs-12 mypostview viewpost">
			
			
				<article class="">
						
						
						<%long like = 0;long dislike = 0;String userInterest = ""; %>
						
						           <div class="entry-meta">
										
										<div class="post-date">
											<a href="#" title=""><i class="fa fa-calendar" aria-hidden="true"></i><span><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.post_cr_date}" /></span></a>
										</div>
										
										<div class="comment">
											
											<span title="View" ><i class="fa fa-eye" aria-hidden="true"></i><small id="viewPost<%=postId%>">${val.post_view}</small></span>
											&nbsp;
											
											
											<!-- START OF LIKE DISLIKE SECTION -->
											
											
											<c:if test="${!empty postInterest}">	
												
												<c:forEach items="${postInterest}" var="postInterest">
												
														<c:set var="interestPostId" value="${postInterest.post.post_id}"/>
														<%int interestPostId = (Integer)pageContext.getAttribute("interestPostId"); %>
														
														<%if(interestPostId==postId){%>  
															
															<c:set var="interest" value="${postInterest.interest}"/>
															<%String interest = (String)pageContext.getAttribute("interest"); %>
															
															<%if(interest.equals("Like")){ %>
																<%like = like + 1; %>
														    <%}else{ %>
														        <%dislike = dislike + 1; %>
														    <%} %>
															
															<c:set var="interestUserId" value="${postInterest.user.user_id}"/>
															<% int interestUserId = (Integer)pageContext.getAttribute("interestUserId"); %>
																
																<%if(userIdentityId==interestUserId){     
																	
																	userInterest = interest;
																}%>
														
														<%}%>
														
												</c:forEach>
												
											</c:if>
											
											
											
											
											<%if(userInterest.equals("")){ %>
											
											    <span title="Like" onclick="showInterest('Like','<%=postId%>')">
												     <i id="like<%=postId%>" class="fa fa-thumbs-o-up" aria-hidden="true"></i>
												     <small id="likeCount<%=postId%>"><%=like%><%-- ${val.post_like} --%></small>
												</span>
												
												&nbsp;
												
												<span title="Dislike" onclick="showInterest('Dislike','<%=postId%>')">
													<i id="disLike<%=postId%>" class="fa fa-thumbs-o-down" aria-hidden="true"></i>
													<small id="disLikeCount<%=postId%>"><%=dislike%><%-- ${val.post_dislike} --%></small>
												</span>
											
											<%}else if(userInterest.equals("Like")){ %>
											
												
												<span title="Like" onclick="showInterest('Like','<%=postId%>')">
												     <i id="like<%=postId%>" class="fa fa-thumbs-up" aria-hidden="true"></i>
												     <small id="likeCount<%=postId%>"><%=like%><%-- ${val.post_like} --%></small>
												</span>
												
												&nbsp;
												
												<span title="Dislike" onclick="showInterest('Dislike','<%=postId%>')">
													<i id="disLike<%=postId%>" class="fa fa-thumbs-o-down" aria-hidden="true"></i>
													<small id="disLikeCount<%=postId%>"><%=dislike%><%-- ${val.post_dislike} --%></small>
												</span>
											
											
											<%}else{ %>
											
												 <span title="Like" onclick="showInterest('Like','<%=postId%>')">
												     <i id="like<%=postId%>" class="fa fa-thumbs-o-up" aria-hidden="true"></i>
												     <small id="likeCount<%=postId%>"><%=like%><%-- ${val.post_like} --%></small>
												</span>
												
												&nbsp;
												
												<span title="Dislike" onclick="showInterest('Dislike','<%=postId%>')">
													<i id="disLike<%=postId%>" class="fa fa-thumbs-down" aria-hidden="true"></i>
													<small id="disLikeCount<%=postId%>"><%=dislike%><%-- ${val.post_dislike} --%></small>
												</span>
											
											<%} %>
											
											
											<!-- END OF LIKE DISLIKE SECTION -->
											
											
											
											&nbsp;
											<c:set var = "comment" value = "${val.comment}" />
											<span title="Comment"><i class="fa fa-comment" aria-hidden="true"></i><small id="cmtDiv${val.post_id}">${fn:length(comment)}</small></span>
										
										</div>
										
									</div>	
										
									
					         
					         
					         
					         
					         <%if(fileType.equals("Image")) {%>
								
								<div class="entry-cover">
									<a class="sizeimg" data-toggle="modal" data-target="#postpopup"  title="Click to know more" href="#" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">
									    
									    <img  class="center-block"  src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" />
									  
									</a>
									
									<c:if test = "${val.download_privilage == 'Yes'}">
										<a href="#" onclick="downloadPostFile('${val.post_id}','<%=userId%>')"><i class="fa fa-download downloadicn"></i></a>
									</c:if>
									
								</div>
								
								<%}else if(fileType.equals("Video")) { %>
									
									<div data-toggle="modal" data-target="#postpopup"  class="entry-cover" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">
										
										<span  class="thickbox play-button-link sizeimg">
											
											<span id="renderImage<%=postId%>" >
													
													<%
														int pos = fileName.lastIndexOf(".");
													    String renderfileName = fileName.substring(0, pos)+".png";
										             %>
													<img  alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>" style="height:175px;" >
													<img class="play-button" alt="" src="static/IndexResources/images/Play1Pressed.png" onclick="showVideo('NoPopUp','PostFile','<%=fileName%>','<%=userId%>','<%=postId%>')"/>
											
											</span>
											
											<span id="renderVideo<%=postId%>"> </span>
											
										</span>
										
										<c:if test = "${val.download_privilage == 'Yes'}">
										   
										   <a href="#" onclick="downloadPostFile('${val.post_id}','<%=userId%>')"><i class="fa fa-download downloadicn"></i></a>
									    
									    </c:if>
										
									</div>
								
								<%}else if(fileType.equals("Pdf")) { %>
								
									<div class="entry-cover">
										<a  data-toggle="modal" data-target="#postpopup"  title="Click to know more" href="#" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">
											
											<%
												int pos = fileName.lastIndexOf(".");
												String renderfileName = fileName.substring(0, pos)+".png";
										    %>
											<img class="center-block" alt="<%=fileName%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>" /><!-- style="height:175px;"  --> 
											
											
										</a>
										
										<c:if test = "${val.download_privilage == 'Yes'}">
										   
										   <a href="#" onclick="downloadPostFile('${val.post_id}','<%=userId%>')"><i class="fa fa-download downloadicn"></i></a>
									    
									    </c:if>
									</div>
								
								<%}else if(fileType.equals("URL")) { %>
								
									<div class="entry-cover">
										<a class="viewmore" data-toggle="modal" data-target="#postpopup"  title="Click to know more" href="#" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">
											
											<iframe style="height:180px;width: 200px;" src="<%=fileName%>" frameborder="0" allowfullscreen></iframe>
											
										</a>
									</div>
								
								<%}else {%>
								
								<%}%>
								
								
								
								<c:if test="${empty val.postFile}">	
														
														<div class="entry-block" style="width:100%;">
															<div class="feed-showmore-height ">
															<div class="entry-title">
																<h3>${val.post_title}</h3>
															</div>							
															
															<div class="entry-content">
																<p>${val.post_desc}</p>
															</div>
															</div>
															<div class="show-more">
        <img src="static/IndexResources/images/log-in.png">&nbsp;<a href="#" data-toggle="modal" data-target="#postpopup" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">View Details</a>
    </div>
							
														</div>
												
												</c:if>
												
												<c:if test="${!empty val.postFile}">	
														
														<div class="entry-block">
															<div class="feed-showmore-height">
															<div class="entry-title">
																<h3>${val.post_title}</h3>
															</div>							
															
															<div class="entry-content">
																<p>${val.post_desc}</p>
															</div>
															</div>
															<div class="show-more">
        <img src="static/IndexResources/images/log-in.png">&nbsp;<a href="#" data-toggle="modal" data-target="#postpopup" onclick="viewUserPost('${val.post_id}','${val.user.user_id}')">View Details</a>
    </div>
															
														
														</div>
												
												</c:if>
												
													
								
								
								
								
								
					
				</article>
				
				
			</div>
		
			
		</c:forEach>		    
		
	</c:if>    
	    
	<c:if test="${empty myAllPost}">
	
			<div class="recent-content" style="color:red;text-align:center;font-size:18px;">
				As of now no one didn't post anything.
			</div>
			
	</c:if> 
					
	</div>
							</aside>
							</div>			