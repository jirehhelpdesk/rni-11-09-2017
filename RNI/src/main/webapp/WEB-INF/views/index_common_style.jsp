

<!-- Standard Favicon -->
	
	
	<link rel="icon" type="image/x-icon" href="static/IndexResources/images/favicon.ico" />
	
	<!-- For iPhone 4 Retina display: -->
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="static/IndexResources/images/apple-icon-114x114.png">
	
	<!-- For iPad: -->
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="static/IndexResources/images/apple-icon-72x72.png">
	
	<!-- For iPhone: -->
	<link rel="apple-touch-icon-precomposed" href="static/IndexResources/images/apple-icon-57x57.png">
	
	<!-- Library - Bootstrap v3.3.5 -->
    <link rel="stylesheet" type="text/css" href="static/IndexResources/libraries/lib.css">
    <link rel="stylesheet" type="text/css" href="static/IndexResources/libraries/Stroke-Gap-Icon/stroke-gap-icon.css">
	
	<!-- Custom - Common CSS -->
	<link rel="stylesheet" type="text/css" href="static/IndexResources/css/plugins.css">
	<link rel="stylesheet" type="text/css" href="static/IndexResources/css/navigation-menu.css">
	<link rel="stylesheet" type="text/css" href="static/IndexResources/libraries/lightslider-master/lightslider.css">
	
	<!-- Custom - Theme CSS -->
	<link rel="stylesheet" type="text/css" href="static/IndexResources/css/style.css">
	<link rel="stylesheet" type="text/css" href="static/IndexResources/css/shortcode.css">
	<link rel="stylesheet" type="text/css" href="static/IndexResources/css/customestyle.css">
	<link rel="stylesheet" href="static/IndexResources/css/jquery.datetimepicker.css">
	<link href="static/IndexResources/css/prettyPhoto.css" rel="stylesheet">
	
	
	<link href="static/IndexResources/css/font.css" rel="stylesheet">
	
	
	<!--[if lt IE 9]>		
		<script src="js/html5/respond.min.js"></script>
    <![endif]-->
    
    
    
    <!-- SLIDER -->
    
    <link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/default.css">
	<link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/reset.css">
	<link rel="stylesheet" type="text/css" href="static/IndexResources/Imageslider/slideshow.css">
	<script src="static/IndexResources/js/jquery-3.2.1.min.js"></script>

	
	<link rel="stylesheet" href="static/loader/style.css"> 
	<link rel="stylesheet" href="static/IndexResources/css/customeresponsive.css">
	
	