<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
 
 <div class="container-fluid">
              
              <div class="row">
                	
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Content List</h3>
                    </div>
                    <div class="card-body">
                     
                     
                    <c:if test="${!empty contentList}">	
                      
                   		   <table class="table table-striped table-hover" data-toggle="table" data-pagination="true">
	                       
	                        <thead>
	                        
	                          <tr>
	                            <th>Sl.No</th>
	                            <th>Content For</th>
	                            <th>Heading</th>
	                            <th>Cr Date</th>
	                            <th>Manage</th>
	                          </tr>
	                          
	                        </thead>
	                        
	                        
	                        <tbody>
	                         
	                         <%int i = 1; %> 
	                         
	                         <c:forEach items="${contentList}" var="val">
									
									<tr>
			                            
			                            <td  scope="row"><%=i++%></td>
			                            <td>${val.content_for}</td>
			                            <td><span class="contentHide">${val.content_heading}</span></td>
			                            <td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.content_cr_date}" /></td>
			                            <td>
			                            	<a href="#" onclick="showEditFormContent('${val.content_id}')">Edit</a>
			                            		&nbsp;&nbsp;
			                            	<a href="#" onclick="deleteContent('${val.content_id}')">Delete</a>
			                            </td>
			                            
			                        </tr>
									 	  
			                  </c:forEach>
	                          
	                        </tbody>
	                        
	                      </table>
                   			
                     </c:if>
                     
                     <c:if test="${empty contentList}">	
                     
                     		<div style="color:red;text-align:center;font-size:18px;">
								As of now default content is displayed in the portal
							</div>
                     
                     </c:if> 
                      
                      
                    </div>
                    
                  </div>
                  
                </div>
                	
              </div>
              
 </div>