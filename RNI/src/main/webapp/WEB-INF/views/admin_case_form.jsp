<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="static/ckeditor/ckeditor.js"></script>
  

  <c:if test="${empty caseData}">	

       <div class="container-fluid">
              
              <div class="row">
                
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                         
                         <button type="button" id="closeCard" class="dropdown-toggle" onclick="showCaseForm('close')">Close Form</button>
                         
                      </div>
                    </div>
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Create Case</h3>
                    </div>
                    <div class="card-body">
                      
                      
                      
                      <form:form  class="form-horizontal" id="formBean" modelAttribute="formBean" method="post" action="saveCaseDetails" >
						
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Case Title</label>
                          <div class="col-sm-10">
                            <input type="text" placeholder="Event title or name" name="contentString1" path="contentString1" id="contentString1" class="form-control">
                            <span id="titleErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                                                
                        <div class="line"></div>
                        
                        
                        <div class="form-group row">
                          
                          <label class="col-sm-2 form-control-label">Add case related files</label>
                          <div class="col-sm-8">
                                
                                <input class="form-control"  type="file" name="files" id="files" multiple onchange="uploadBannerFile()">  
                                <span id="fileErrorId" class="help-block-none"></span>
                                
                          </div>
                          
                         
                        </div>
                        
                        <div class="form-group row">
		                          
	                          <label class="col-sm-2 form-control-label"><br><small class="text-primary"></small></label>
	                          
	                          <div class="col-sm-10">
	                            	
	                            	<ul id="selectedFileList" style="list-style: none;">

									
									</ul>
	
	                          </div>
		                          
		               </div>
                        
                        
                        <div class="line"></div>
                        <div class="form-group row">
                          <label class="col-sm-2 form-control-label">Case Description</label>
                          <div class="col-sm-10">
                            <textarea name="ckeditor" id="ckeditor" ></textarea>
                            <span id="descErrorId" class="help-block-none"></span>
                          </div>
                        </div>
                        
                       
                        <div class="line"></div>
                        <div class="form-group row">
                          <div class="col-sm-4 offset-sm-3">
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="saveCaseDetail('save')">Save</button>
                          </div>
                        </div>
                        
                        
                      </form:form>
                      
                    </div>
                  </div>
                </div>
                
                
              </div>
            </div>
    
 </c:if>





	<c:if test="${!empty caseData}">	
	
	<c:forEach items="${caseData}" var="val">
	
		<div class="container-fluid">
	              
	              <div class="row">
	                
	                <!-- Form Elements -->
	                <div class="col-lg-12">
	                  
	                  <div class="card">
	                    
	                    <div class="card-close">
	                      <div class="dropdown">
	                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
	                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
	                           <a href="#" class="dropdown-item edit" onclick="showEventForm('Close')"> <i class="fa fa-times"></i>Close Form</a>
	                           
	                         </div>
	                      </div>
	                    </div>
	                    
	                    <div class="card-header d-flex align-items-center">
	                      <h3 class="h4">Case Event</h3>
	                    </div>
	                    
	                    <div class="card-body">
	                      
	                      
	                      <form:form  class="form-horizontal" id="caseForm" modelAttribute="caseDetail" method="post" action="saveCaseDetails" >
							
	                        <div class="form-group row">
	                          <label class="col-sm-2 form-control-label">Case Title</label>
	                          <div class="col-sm-10">
	                            <input type="text" placeholder="Event title or name" name="event_title" path="event_title" id="event_title" class="form-control">
	                            <span id="titleErrorId" class="help-block-none"></span>
	                          </div>
	                        </div>
	                                                
	                        <div class="line"></div>
	                        
	                        
	                        <div class="form-group row">
	                          
	                          <label class="col-sm-2 form-control-label">Add case related files</label>
	                          <div class="col-sm-10">
	                                
	                                <input class="form-control" name='file' id="file" type="file" multiple onchange="uploadCaseFile()">  
	                                <span id="fileErrorId" class="help-block-none"></span>
	                          </div>
	                          
	                          
	                        </div>
	                        
	                        <div class="form-group row">
	                          <label class="col-sm-2 form-control-label"><br><small class="text-primary"></small></label>
	                          
	                          <div class="col-sm-10" id="imageList">
	                            
	                           
	                          </div>
	                          
	                        </div>
	                        
	                        
	                        <div class="line"></div>
	                        <div class="form-group row">
	                          <label class="col-sm-2 form-control-label">Case Description</label>
	                          <div class="col-sm-10">
	                            <textarea name="ckeditor" id="ckeditor" ></textarea>
	                            <span id="descErrorId" class="help-block-none"></span>
	                          </div>
	                        </div>
	                        
	                       
	                        <div class="line"></div>
	                        <div class="form-group row">
	                          <div class="col-sm-4 offset-sm-3">
	                            <button type="reset" class="btn btn-secondary">Cancel</button>
	                            <button type="button" class="btn btn-primary" onclick="saveCaseDetail('update')">Update</button>
	                          </div>
	                        </div>
	                        
	                        
	                      </form:form>
	                      
	                    </div>
	                  </div>
	                </div>
	                
	                
	              </div>
	            </div>
	
	
	</c:forEach>
		
	</c:if>        
   
   
   
            
	<script>
		CKEDITOR.replace('ckeditor');
	</script>
            