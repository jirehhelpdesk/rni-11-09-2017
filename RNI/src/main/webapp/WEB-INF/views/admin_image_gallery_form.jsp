<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


  <form:form  id="galleryForm" modelAttribute="galleryRecord" method="post" action="saveGalleryRecord">
	                        
	  <div class="form-group">
	    <input type="text" placeholder="Gallery title" name="gallery_text" path="gallery_text" id="gallery_text" class="form-control">
	    <span id="titleErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group">       
	    <label class="form-control-label">Attach File</label>
	    <input type="file" multiple name="files" id="files" class="form-control">
	    <span id="fileErrorId" class="help-block-none"></span>
	  </div>
	  
	  <div class="form-group row">
	  	<div class="offset-sm-10">       
	   	    <input type="button" value="Save" class="btn btn-primary" onclick="saveGalleryRecord()">
	   	</div>
	  </div>
			  
   </form:form>