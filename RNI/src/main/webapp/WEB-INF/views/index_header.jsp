	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
	
	
	<!-- Header -->
	
	<header class="header-main container-fluid no-padding">
	
	<div class="search-form">
				<div id="sb-search" class="sb-search">
				<form>
				<span class="archive-link pull-right header-search-span">
            		<span class="header-search">
                        <input type="text" class="form-control input-sm" id="searchValueId" placeholder="Search" onkeyup="searchUser(this.value)"/>
                        <a href="#link" role="button" class="sb-search-submit btn-srch" onclick="searchUserByClick()"><i class="fa fa-search"></i></a>
                    </span>
            	</span>
				</form>
				</div>
					
				</div>
	
		
		<!-- Menu Block -->
		<div class="menu-block container-fluid no-padding" data-spy="affix" data-offset-top="100"><!--   -->
			<!-- Container -->
			<div class="container">
				<!-- User -->
				
			
				
				
				<div class="col-md-2 no-padding">
				<a href="index">
				<img src="static/IndexResources/images/logo.png" alt="logo" class="retnet center-block" width="170">
				</a>
				</div>
				<div class="col-md-8 col-sm-12 col-xs-12">
					<!-- Navigation -->
					<nav class="navbar ow-navigation">
						<div class="navbar-header">
							<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						
						
						
						<div class="navbar-collapse collapse" id="navbar">
							
							<c:if test="${empty userBean.profileBean}">
								<ul class="nav navbar-nav menubar hdrmenu"><!--  style="padding-left: 88px;" -->
							</c:if>
							
							<c:if test="${!empty userBean.profileBean}">
							    <ul class="nav navbar-nav menubar hdrmenu"> 
							</c:if>
							
								<li id="menu1"><a title="Home" href="index">Home</a></li>
								
								<li id="menu2"><a title="About Us" href="aboutUs">About Us</a></li>
								
								<li id="menu3"><a title="Sponsorship" href="sponsorship">Sponsorship</a></li>
								
								<li id="menu4"><a title="Gallery" href="imageFolderGallery">Gallery</a></li>
								
								<!-- <li id="menu4" class="dropdown">
									<a aria-expanded="false" aria-haspopup="true" role="button" class="dropdown-toggle" title="Gallery" href="#">Gallery</a>
									<i class="ddl-switch fa fa-angle-down"></i>
									<ul class="dropdown-menu">
										<li><a title="Images" href="imageFolderGallery">Images</a></li>
										<li><a title="Videos" href="videosFolderGallery">Videos</a></li>
									</ul>
								</li> -->
								
								<li id="menu5" class="dropdown">
									<a aria-expanded="false" aria-haspopup="true" role="button" class="dropdown-toggle" title="Events" href="events">Events</a>
								</li>
								
								<li id="menu6"><a title="Contact" href="contactUs">Contact</a></li>
								
							</ul>
						</div>
						
						
						
					</nav><!-- Navigation /- -->
				</div>
				
				<!-- Register -->
					
					<c:if test="${!empty userBean}">
					
						<div class="col-md-2 col-sm-12 col-xs-12 register">
						<div class="col-md-12  col-xs-12 no-padding usernameright">
						<div class="dropdown usrprofle">
							
							<% int userIden = 0;%>
						   <c:if test="${!empty userBean.profileBean}">
				 		
							 	 <c:set var="userId" value="${userBean.user_id}"/>
								 <% userIden = (Integer)pageContext.getAttribute("userId"); %>
				 										
							 	 <c:set var="profilePhoto" value="${userBean.profileBean.profile_photo}"/>
								 <% String profilePhoto = (String)pageContext.getAttribute("profilePhoto"); %>
															    
							 	 <%if(profilePhoto.equals("")){ %>
									
									 <a href="#" class="user" title="User">
										<img src="static/IndexResources/images/user.png" alt="logo" width="46" height="41"/>
									 </a> <!-- User /- -->
								
								<%}else{ %>	 
								
									 <a href="#" class="user" title="User">
										<img src="${pageContext.request.contextPath}<%="/previewProfilePhoto?fileName="+profilePhoto+"&userId="+userIden+""%>" alt="Profile Picture" style="width:40px;height: 40px;border-radius: 50%;border: 3px solid #21a5b2;"/>
									 </a> <!-- User /- -->
								
								<%} %>
								
							</c:if>
				
				  			<a href="" title="Click to view dashboard"class="username dropdown-toggle"  data-toggle="dropdown">${userBean.first_name} <%-- ${userBean.last_name} --%> <span class="caret"></span></a>
								<ul class="dropdown-menu  dropdown-menu-right">
									<li><a href="userDashboard"> <i
											class="fa fa-newspaper-o pull-right"></i>View Feed
									</a></li>
									<li><a href="inViewUserProfile?userId=<%=userIden%>"> <i class="fa fa-user pull-right"></i>Profile
									</a></li>
									<li><a href="userSetting"> <i class="fa fa-cog pull-right"></i>Settings
									</a></li>
									<li class="divider"></li>
									<li><a href="userLogout"> <i
											class="fa fa-power-off pull-right"></i>Log out
									</a></li>
								</ul>
								
							</div>
						</div>
						
						
						<!-- <div class="col-md-2  col-xs-3">
							<a href="userLogout" title="Sign out" class="signin signout"><i class="fa fa-power-off"></i></a>
							</div> -->
							
						</div><!-- Register /- -->
						
					</c:if>
					
					<c:if test="${empty userBean}">
					
						<div class="col-md-2  col-sm-12 col-xs-12 register signbtn">
							<a href="userRegister" title="Register" class="signin buttoncs btn">Sign in</a>
						</div><!-- Register /- -->
						
					</c:if>
				
				<!-- Expanding Search -->
				<!--  <div class="">
				<div id="sb-search" class="">
				<form action="" class="search-form">
                <div class="form-group has-feedback">
            		<label for="search" class="sr-only">Search</label>
            		<input type="text" class="form-control" name="search" id="searchValueId" placeholder="Search" onkeyup="searchUser(this.value)"/>
              		<span class="glyphicon glyphicon-search form-control-feedback"  onclick="searchUserByClick()"></span>
            	</div>
            </form>
				</div>
				</div>   -->
				
				<!-- Expanding Search /- -->
				
				
				
			</div><!-- Container /- -->
		</div><!-- Menu Block /- -->
	</header>
	<!-- Header /- -->	