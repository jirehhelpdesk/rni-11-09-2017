<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
 
 
 <div class="container-fluid">
              
              <div class="row">
                	
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Newsletter List</h3>
                    </div>
                    <div class="card-body">
                     
                     
                    <c:if test="${!empty letterData}">	
                      
		                   
                   		   <table class="table table-striped table-hover" data-toggle="table" data-pagination="true">
	                       
	                        <thead>
	                        
	                          <tr>
	                            <th>Sl.No</th>
	                            <th>Sent Date</th>
	                            <th>Heading</th>
	                            <th>No.of User</th>
	                            <th>File attached</th>
	                            <!-- <th></th> -->
	                          </tr>
	                          
	                        </thead>
	                        
	                        
	                        <tbody>
	                         
	                         <%int i = 1; %> 
	                           <c:forEach items="${letterData}" var="val">
									
									<tr>
			                            
			                            <td scope="row"><%=i++%></td>
			                            <td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.news_sent_date}" /></td>
			                            <td><span class="contentHide">${val.news_matter}</span></td>
			                            <td>${val.user_count}</td>
			                            <td>
			                            	<c:set var = "attachFile" value = "${val.attachFile}" />
											${fn:length(attachFile)}
			                            </td>
			                            
			                            <!-- <td><a href="#" onclick=""></a></td> -->
			                            
			                        </tr>
									 	  
			                   </c:forEach>
	                          
	                          
	                        </tbody>
	                        
	                      </table>
                   			
                     </c:if>
                     
                     <c:if test="${empty letterData}">	
                     
                     		<div style="color:red;text-align:center;font-size:18px;">
								As per the search criteria no result found.
							</div>
                     
                     </c:if> 
                      
                      
                      
                      
                      
                      
                    </div>
                  </div>
                </div>
                	
                	
              </div>
              
 </div>
 <script src="static/adminResource/js/bootstrap-table.js"></script>