
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:forEach items="${showPost}" var="val">
										
	
	<c:set var="postId" value="${val.post_id}"/>
	<% int postId = (Integer)pageContext.getAttribute("postId"); %>
	  
	<c:set var="userId" value="${val.user.user_id}"/>
	<% int userId = (Integer)pageContext.getAttribute("userId"); %>
	 										
	 
	<div class="modal-dialog modal-lg">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">${val.post_title}</h4>
	      </div>
	      
	      <div class="modal-body">
	        
	        <div class="row">
	        <div class="col-md-12">
		        <div class="col-md-6 popupimg">
		        
		        
		        <!-- POST IMAGE SLIDER IF MULTIPLE IS AVAILABLE -->
		        
		        <div class="slideshow manageMulti">
												
					<input type="radio" name="ss1" id="ss1-item-1" class="slideshow--bullet" checked="checked" />
					
					<% int i = 0; %>
					<c:forEach items="${val.postFile}" var="fileVal">
							
							<% i = i + 1; %>
							
							<c:set var="fileType" value="${fileVal.post_file_type}"/>
							<% String fileType = (String)pageContext.getAttribute("fileType"); %>
							
							<c:set var="fileName" value="${fileVal.post_file_name}"/>
							<% String fileName = (String)pageContext.getAttribute("fileName"); %>
	
					
							<%if(i==1){ %>
							
								<div class="slideshow--item manageMulti">
									
									<%if(fileType.equals("Image")) {%>
		        
							        	<img class="img-responsive" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>">
							        
							        <%}else if(fileType.equals("Video")) { %>
							        
							        	<video class="img-responsive" controls="">
										  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/mp4">
										  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/ogg">
										</video>
							        
							        <%}else if(fileType.equals("Pdf")) { %>
							        
							        	<img class="img-responsive" alt="<%=fileName%>" src="static/IndexResources/common_images/PDF-Icon.png" /> 
											
							        <%}else{ %>		
							        
							        
							        <%} %>
									
									<label for="ss1-item-3" class="slideshow--nav slideshow--nav-previous">Go to slide 3</label>
									<label for="ss1-item-2" class="slideshow--nav slideshow--nav-next">Go to slide 2</label>
								</div>
							
							<%}else{ %>
							
								
								<input type="radio" name="ss1" id="ss1-item-<%=i%>" class="slideshow--bullet" />
								<div class="slideshow--item manageMulti">
									
									<%if(fileType.equals("Image")) {%>
		        
							        	<img class="img-responsive" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>">
							        
							        <%}else if(fileType.equals("Video")) { %>
							        
							        	<video class="img-responsive" controls="">
										  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/mp4">
										  	<source src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=PostFile&fileName="+fileName+"&userId="+userId+"&postId="+postId+""%>" type="video/ogg">
										</video>
							        
							        <%}else if(fileType.equals("Pdf")) { %>
							        
							        	<img class="img-responsive" alt="<%=fileName%>" src="static/IndexResources/common_images/PDF-Icon.png" /> 
											
							        <%}else{ %>		
							        
							        
							        <%} %>
									
									<label for="ss1-item-<%=i-1%>" class="slideshow--nav slideshow--nav-previous">Go to slide <%=i-1%></label>
									<label for="ss1-item-<%=i+1%>" class="slideshow--nav slideshow--nav-next">Go to slide <%=i+1%></label>
								
								</div>
					
							<%} %>
					
					
					</c:forEach>		
							
					
				</div>
		        
		        
		        </div>
	        
	        <div class="col-md-6 popupdecs boxscroll">
	        <div class="simplebar myElement">
		 
		 
		   <div class="parent">
	       
	        <div class="popst-desc">
	        	  <p>${val.post_desc}</p>
	        </div>
	        
	        
	        
	        <div class="post-comment">
	        
	        <c:if test="${!empty showComment}">	
	        
		        <section class="comment-list">
		        
		        <c:forEach items="${showComment}" var="cmt">
		          
			          <article class="row">
			            
			            <div class="col-md-12 col-sm-12">
			              
			              <div class="panel panel-default">
			                
			                <div class="panel-body">
				                  
				                  <header>
				                    
				                    
				                    <div class="comment-user">
			                    
					                      <c:set var="userId" value="${cmt.user.user_id}"/>
								 		  <% int userIden = (Integer)pageContext.getAttribute("userId"); %>
				 						  <% String profilePhoto = ""; %>
				 						  
					                      
					                      <c:if test="${!empty cmt.user.profile}">
					                      		<c:set var="profilePhoto" value="${cmt.user.profile.profile_photo}"/>
								 				<% profilePhoto = (String)pageContext.getAttribute("profilePhoto"); %>
								 				
								 				<%if(!profilePhoto.equals("")){ %>
					                      			<img src="${pageContext.request.contextPath}<%="/previewProfilePhoto?fileName="+profilePhoto+"&userId="+userIden+""%>" style="width:34px;height:34px;border-radius:5px;"/>
					                     		<%}else{ %>
					                     			<!-- <i class="fa fa-user"></i>  -->
					                     			<img src="static/IndexResources/images/user.png" style="width:34px;height:34px;border-radius:5px;background-color:#21a5b2;padding: 2px;"/>
					                     		<%} %>
					                      </c:if>
					                      
					                      <c:if test="${empty cmt.user.profile}">
					                      		<img src="static/IndexResources/images/user.png" style="width:34px;height:34px;border-radius:5px;background-color:#21a5b2;padding: 2px;"/>
					                      </c:if>
					                      
					                      ${cmt.user.first_name} ${cmt.user.last_name}
					                      
			                        </div>
				                    
				                    
				                    
				                    <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> <fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${cmt.comment_cr_date}" /></time>
				                  </header>
				                  
				                  <div class="col-md-12 no-padding">
				                  	
				                  	<c:set var="commentContain" value="${cmt.comment_contain}"/>
									<% String commentContain = (String)pageContext.getAttribute("commentContain"); %>
															
				                  		
									<%if(!commentContain.equals("No")) {%>
											
				                      		  
				                      		  <!--Start Comment Image  -->
				                      		  
						                      <div class="col-md-4 no-padding">
								                  
								                   <!-- Design for Comment File is available  -->
							                  
							                     
									                    <div class="commentFile commntimg">
									                  	 
									                  	<c:forEach items="${cmt.commentFile}" var="cmtFile"> 	
									                  		
									                  		<c:set var="commentFileType" value="${cmtFile.comment_file_type}"/>
															<% String commentFileType = (String)pageContext.getAttribute("commentFileType"); %>
															
															<c:set var="commentFile" value="${cmtFile.comment_file}"/>
															<% String commentFile = (String)pageContext.getAttribute("commentFile"); %>
															
									                  	    <%if(commentFileType.equals("Image")) {%>
								        
													        	<img  src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=CommentFile&fileName="+commentFile+"&userId="+userId+"&postId="+postId+""%>"  onclick="showCommentFile('Open','CommentFile','<%=commentFile%>','<%=userId%>','<%=postId%>','<%=commentFileType%>')">
													        
													        <%}else if(commentFileType.equals("Video")) { %>
													        
													        	<span class="thickbox play-button-link" onclick="showCommentFile('Open','CommentFile','<%=commentFile%>','<%=userId%>','<%=postId%>','<%=commentFileType%>')">
																		            
														            <span id="renderImageCmnt<%=postId%>" >
																			
																			<%
																				int pos = commentFile.lastIndexOf(".");
																			    String renderfileName = commentFile.substring(0, pos)+".png";
																            %>
																            
																			<img  alt="<%=commentFile%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=CommentFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>">
																			<img class="play-button alignPlayInPopUp" alt="" src="static/IndexResources/images/Play1Pressed.png" onclick="showVideo('Comment','CommentFile','<%=commentFile%>','<%=userId%>','<%=postId%>')"/>
																			
																	</span>
																	
																	<span id="renderVideoCmnt<%=postId%>">
																			
																	
																	</span>
															
																</span>
													        
													        <%}else if(commentFileType.equals("Pdf")) { %>
													        
													        	<a title="Load to click on the pdf" href="#"  onclick="showPdfFile('NoPopUp','CommentFile','<%=commentFile%>','<%=userId%>','<%=postId%>')"> 
																			
																	<%
																		int pos = commentFile.lastIndexOf(".");
																		String renderfileName = commentFile.substring(0, pos)+".png";
																    %>
																	<img alt="<%=commentFile%>" src="${pageContext.request.contextPath}<%="/previewUserMedia?fileCategory=CommentFile&fileName="+renderfileName+"&userId="+userId+"&postId="+postId+""%>" /> 
																	
																</a>
																	
													        <%}else{ %>		
													        
													        <%} %>
								        				
								        				    <br></br>
								        				
								        				</c:forEach>
								        				
								        				
									                   </div>
							                     
						                      </div>
						                      
						                      <!--End Comment Image  -->
						                      
						                      <!--Start Comment Post  -->
						                      
						                      <div class="col-md-8">
								                  
									                  <div class="comment-post">
									                    <p>
									                      ${cmt.comment_message}
									                    </p>
									                  </div>
								                  
						                     </div>
						                     
						                     <!--End Comment Post  -->
						                 
						                 
						                  
				                        <%}else{%>
				                  
						                  
						                  <div class="row">
				                      		  
						                      <!--Start Comment Post  -->
						                      
						                      <div class="col-md-12">
								                  
								                  <div class="comment-post">
								                    <p>
								                      ${cmt.comment_message}
								                    </p>
								                  </div>
								                  
						                     </div>
						                     
						                     <!--End Comment Post  -->
						                 
						                 </div>
				                  
				                  
				                      <%} %>
				                  
				                  
			                </div>
			                
			                
			                
			                
			              </div>
			            </div>
			            
			            </div>
			          </article>
		          
		          </c:forEach>
		          
		        </section>
		     
		    </c:if>
		    
		    <c:if test="${empty val.comment}">	
		    
		    		<div class="recent-content" style="color:red;text-align:center;font-size:18px;">
						As of now no one didn't comment on your post.
					</div>
		    		
		    </c:if> 
		        
	        </div>
	        
	        
	        
	        
	        </div>
	        </div>
	        </div>
	        
	        
	        </div>
	        
	        <%-- <div class="col-md-12">
	        
		        <form class="form-horizontal" id="commentForm">
			        <div class="form-group">
			        
				        <div class="input-group"> 
			                 <textarea class="form-control" placeholder="Add a comment" id="comment" name="comment"></textarea>
			                 <span class="input-group-addon" onclick="postComment('${val.post_id}')">
			                     <a href="#">Submit</a>  
			                 </span>
			             </div>
		             
			        </div>
		        </form>
	        
	        </div> --%>
	        
	        </div>
	      </div>
	      
	    </div>
	
	  </div>										
	 										
</c:forEach>


<script src="static/IndexResources/js/custome_scrollbar.js"></script>


