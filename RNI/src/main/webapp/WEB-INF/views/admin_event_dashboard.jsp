<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>


<div class="container-fluid">
              
              
              <div class="row">
                
                <!-- Form Elements -->
                <div class="col-lg-12">
                  
                  <div class="card">
                    
			              <div class="card-header d-flex align-items-center">
			                     <h3 class="h4">Last Events</h3>
			               </div>
			              
			              <div class="card-body">
			              
						              
						            <c:if test="${!empty eventList}">		
						
										<c:forEach items="${eventList}" var="val">
												
											<c:set var="eventId" value="${val.event_id}"/>
											<% int eventId = (Integer)pageContext.getAttribute("eventId"); %>
											
											<c:set var="eventCode" value="${val.event_code}"/>
											<% String eventCode = (String)pageContext.getAttribute("eventCode"); %>
											
											<c:set var="status" value="${val.event_status}"/>
											<% String eventStatus = (String)pageContext.getAttribute("status"); %>
											
											
											<div class="project">
								                
								                <div class="row bg-white has-shadow">
								                  
								                  <div class="left-col col-lg-6 d-flex align-items-center justify-content-between">
								                    
								                    <div class="project-title d-flex align-items-center">
								                      
								                         <div class="image has-shadow">
								                         
								                                <% String fileName = "";%>
								                                <% int i = 0; %>
																<c:forEach items="${val.image}" var="image">
																		
																	<%if(i==0){ %>
																		<c:set var="fileName" value="${image.file_name}"/>
																		<% fileName = (String)pageContext.getAttribute("fileName"); i=i+1;%>
															        <%} %>
																		
																</c:forEach>	
																
																<img src="${pageContext.request.contextPath}<%="/previewEventMedia?fileName="+fileName+"&eventId="+eventId+""%>" alt="..." class="img-fluid">
								                               
								                         </div>
								                      
									                     <div class="text">
									                        <p>${val.event_title}</p>
									                        <small>${val.event_location}</small>
									                     </div>
								                      
								                    </div>
								                    
								                    <div class="project-date"><span class="hidden-sm-down"> </span></div>
								                  
								                  </div>
								                  
								                  <div class="right-col col-lg-4 d-flex align-items-center">
								                    
								                    <div class="project-date"><span class="hidden-sm-down">Event date <strong class="postCount"><fmt:formatDate pattern="dd/MM/yyyy" value="${val.event_start_date}" /></strong></span></div>
								                    &nbsp; &nbsp;
								                    <div class="project-date"><span class="hidden-sm-down">Status <strong class="active">${val.event_status}</strong></span></div>
								                    
								                  </div>
								                  
								                 
								                  <div class="right-col col-lg-2 d-flex align-items-center">
								                    
								                    <div class="project-date">
								                        <span class="hidden-sm-down">Total Participant 
								                           
								                           <strong class="postCount">
								                               <c:set var = "evnApplication" value = "${val.evnApplication}" />
								                               ${fn:length(evnApplication)}
								                           </strong>
								                           
								                        </span>
								                    </div>
								                    &nbsp; &nbsp;
								                    
								                  </div>
								                  
								                </div>
								              </div>
											
											
										</c:forEach>
										
									</c:if>
									
									
									
									
									<c:if test="${empty eventList}">	
									 	
										<div style="color:red;text-align:center;">You have not added any event</div>
																	
								    </c:if>
						              
						             
			              </div>
              			
              		</div>	
              	</div>
              </div>
              
            </div>