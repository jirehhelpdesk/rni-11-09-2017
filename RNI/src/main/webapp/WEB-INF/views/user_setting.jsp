<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Settings</title>

	
	<%@include file="index_common_style.jsp" %>
	
	
</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<body data-offset="200" data-spy="scroll" data-target=".ow-navigation" onload="activeMenu('menu5','User')">
	
	
	
	<!-- Loader And Header /- -->
	
	<%@include file="index_header.jsp" %>
	
	<!-- END of Loader and Header -->
	
	<div class="container-fluid page-banner about no-padding">
			<div class="container">
				<div class="banner-content-block">
					<div class="banner-content">
						<h3>Setting</h3>
					</div>
				</div>
			</div><!-- 
		<div class="section-padding"></div> -->
	</div>
	<div class="settingbg">
		<div class="container-fluid eventlist upcoming-event latest-blog no-padding">
		
		<div class="manageSectionPadding section-padding"></div>
		
		<div class="container">
			
			
			
			<div class="row">
				
				<div class="col-md-3 col-sm-12 col-xs-12 widget-area">
					
					<%@include file="user_side_menu.jsp" %>
					
				</div>
				
				
				
				<div class="col-md-9 col-sm-12 col-xs-12 content-area ">
				<div class="widget-area mypostfeed" style="width:100%;">
					
						<aside class="widget widget_recent user-eventmrgn">
						
							<div class="widget-title">
								<span class="icon icon-Settings"></span>
								<h3>Settings
								</h3>
								
							</div>
							
							<div class="recent-block user-eventpadng">
				 <div class="fancy-collapse-panel">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingone">
                                <h4 class="panel-title">
                                
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseone" aria-expanded="false" aria-controls="collapseone"><span class="icon icon-File"></span>&nbsp;Account Details
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseone" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingone">
                                <div class="panel-body">
                                   
                                   
                                   <form:form class="booking-form-block" id="userRegForm" modelAttribute="userRegDetails" method="post" action="updateUserRegister" >	          				
									
											<div class="col-md-6 col-sm-6 col-xs-12">
												<div class="form-group">
													<input type="text" name="first_name" parh="first_name" id="first_name" class="form-control" placeholder="Enter your first name" required="" value="${userBean.first_name}"/>
												</div>
												<div id="fNameErrId" class="errorStyle"></div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<div class="form-group">
													<input type="text" name="last_name" parh="last_name" id="last_name" class="form-control" placeholder="Enter your last name" required="" value="${userBean.last_name}"/>
												</div>
												<div id="lNameErrId" class="errorStyle"></div>
											</div>
											
											
											<div class="col-md-6 col-sm-6 col-xs-12">
												<div class="form-group">
													<input type="email" name="email_id" path="email_id" id="email_id" class="form-control" placeholder="Your E-mail" required="" value="${userBean.email_id}"/>
												</div>
												<div id="emailErrId" class="errorStyle"></div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<div class="form-group">
													<input type="text" maxlength="10" name="mobile_no" path="mobile_no" id="mobile_no" class="form-control" placeholder="Your mobile no" required="" value="${userBean.mobile_no}"/>
												</div>
												<div id="mobileErrId" class="errorStyle"></div>
											</div>
											
																
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="form-group">
													<input type="text" name="profession" path="profession" id="profession" class="form-control" placeholder="Your profession" required="" value="${userBean.profession}"/>
												</div>
												<div id="professionErrId" class="errorStyle"></div>
											</div>
												
											
											
											<div class="col-md-12 col-sm-12 col-xs-12 knowmore">
													<button style="float:right;" class="btn buttoncs postbtn" type="button" value=""  title="Update Account Details" onclick="updateUserRegister()">Update</button>
												
											</div>
											
											<div id="alert-msg" class="alert-msg"></div>
											
								</form:form>
								
								
								
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="panel panel-default">
                            
                            <%@include file="user_profile_form.jsp" %>
                           
                        </div>
                        
                        
                        
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span class="icon icon-OpenedLock"></span>&nbsp;Change Password
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    
                                    <form id="passwordForm" class="">						
									
									<div class="col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<input type="password" name="curPassword" class="form-control" id="curPassword" placeholder="Your current password" required=""/>
										</div>
										<div id="curPwErrId" class="errorStyle"></div>
									</div>
									
									<div class="col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<input type="password" name="newPassword" class="form-control" id="newPassword" placeholder="your new password"/>
										</div>
										<div id="newPwErrId" class="errorStyle"></div>
									</div>
									
									<div class="col-md-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<input type="password" name="cnfPassword" class="form-control" id="cnfPassword" placeholder="Confirm your new password" required=""/>
										</div>
										<div id="cnfPwErrId" class="errorStyle"></div>
									</div>
										
									<div class="col-md-12 col-sm-12 col-xs-12 knowmore">
											<button style="float:right;" type="button" value="" id="btn_submit" class="btn buttoncs postbtn" title="Update password" onclick="changeUserPassword()">Update</button>
										</div>
									
									
									<div id="alert-msg" class="alert-msg"></div>
								</form>	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			
							</div>
							</aside>
							</div>
				
				</div>
				
				
				
			</div>
		</div>
		<div class="section-padding"></div>
	</div>
	</div>
	
	
	
	
	
	<!-- Footer Main -->
	<%@include file="index_footer.jsp" %>
	<!-- END Footer Main -->
	<%@include file="index_common_script.jsp" %>

  
	
	
	
</body>
</html>