<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
 
 <aside class="widget widget_similar_event">
   <div class="widget-title"> <span class="icon icon-Blog"></span>
     <h3>Faculty</h3>
   </div>
   
   <ul>
   
    <c:forEach items="${facultyList}" var="val">
    	
    	<li><a title="Faculty member of retnet india" href="#"><span class="icon icon-User"></span>
       <h4>${val.faculty_name}</h4>
       </a></li>
    
    </c:forEach>
    
   </ul>
 </aside>