
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="widget-area mypostfeed" style="width:100%;">
					
						<aside class="widget widget_recent">
						
							<div class="widget-title">
								<span class="icon icon-Notes"></span>
								<h3>Add Post
								</h3>
								
								</div>
								<div class="recent-block">
								<div class="addpost-bg">
								<form:form id="postForm" modelAttribute="postDetail" method="post"
	action="saveMyPost" enctype="multipart/form-data" class="knowmore">


	<div class="form-group">
		<input type="text" name="post_title" path="post_title" id="post_title"
			class="form-control" placeholder="Post Title" required="" />
		<div id="titleErrId" class="errorStyle"></div>
	</div>

	<div class="form-group">

		<label class="css-input css-radio css-radio-warning"> <input
			type="radio" name="attach_type" checked value="noAttach"
			title="Post will appear without attach."
			onclick="showAttachType(this.value)"><span></span>No
			attachment
		</label> <label class="css-input css-radio css-radio-warning push-10-r">
			<input type="radio" name="attach_type" value="file"
			title="Mutiple file can attach to the post"
			onclick="showAttachType(this.value)"><span></span> Attach
			file
		</label> <label class="css-input css-radio css-radio-warning"> <input
			type="radio" name="attach_type" value="url"
			title="Youtube video can attache to the post"
			onclick="showAttachType(this.value)"><span></span> Attach
			youtube url
		</label>

		<div id="attachTypeErrId" class="errorStyle"></div>
	</div>


	<div id="attachUrl" class="form-group" style="display: none;">
		<input type="text" name="url" id="url" class="form-control"
			placeholder="Enter video url" required="" />
		<div id="urlErrId" class="errorStyle"></div>
	</div>

	<div id="attachFile" class="form-group" style="display: none;">
		<input type="file" id="file" name="files"
			title="JPG,JPEG,GIF,PNG,MP4,WEBM,OGG,PDF files acceptable" multiple
			class="form-control" onchange="uploadSelectedFile('Post')"/> <label
			class="file-size">Max file size 40mb</label>
		<div id="fileErrId" class="errorStyle"></div>
	</div>

	<div class="form-group" id="fileListDiv"
		style="display: none; margin-bottom: 10px;">
		<ul id="selectedFileList" class="postFileNameList">

		</ul>
	</div>

	<div id="privilageDiv" class="form-group" style="display: none;">

		<label class="css-input css-radio css-radio-warning"> <input
			type="radio" name="download_privilage" value="Yes"
			title="Post file can be download"><span></span> Allow to
			download
		</label> <label class="css-input css-radio css-radio-warning push-10-r">
			<input type="radio" name="download_privilage" checked value="No"
			title="Post file can't be download"><span></span> Don't
			allow to download
		</label>

		<div id="privilageErrId" class="errorStyle"></div>

	</div>

	<div class="form-group">
		<textarea rows="6" name="post_desc" path="post_desc" id="post_desc"
			class="form-control" placeholder="Description"></textarea>

		<div id="descErrId" class="errorStyle"></div>
	</div>

	<!-- <div class="form-group">

		<label class="css-input css-radio css-radio-warning push-10-r">
			<input type="radio" name="publish_type" checked value="all"
			title="Post will visible to all those who not registered also"><span></span>
			For all
		</label> <label class="css-input css-radio css-radio-warning"> <input
			type="radio" name="publish_type" value="users"
			title="Post will only visible to the registered users"><span></span>
			For users
		</label>

		<div id="typeErrId" class="errorStyle"></div>

	</div> -->

		<div class="row">
			<div class="col-md-12">
				<button type="button" class="buttoncs btn postbtn"  onclick="saveMyPost()" style="float: right; margin-top: 0;">Post</button>
				<!-- <input type="button" value="Post" style="float: right; margin-top: 0;" class="btn buttoncs"> -->

		</div>
	</div>

</form:form>
</div>
</div>
							</aside>
							</div>



