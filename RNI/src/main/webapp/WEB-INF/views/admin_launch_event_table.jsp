<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
 
 
 <div class="container-fluid">
              
              <div class="row">
                	
                <div class="col-lg-12">
                     
                  <div class="card">
                    
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow">
                           <a href="downloadEventRegReport" class="dropdown-item"> <i class="fa fa-file-text"></i>Export for more details</a>
                        </div>
                      </div>
                    </div>
	                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Event Registration Record</h3>
                    </div>
                    
                    <div class="card-body">
                     
                     
	                    <c:if test="${!empty eventRegRecord}">	
	                      
			                   
	                   		   <table class="table table-striped table-hover" data-toggle="table" data-pagination="true">
		                       
			                        <thead>
			                        
			                          <tr>
			                            <th>Sl.No</th>
			                            <th>Name</th>
			                            <th>Email id</th>
			                            <th>Mobile</th>
			                            <th>Location</th>
			                            <th>Council no</th>
			                            <th>Registration type</th>
			                            <th>Amount</th>
			                            <th>Status</th>
			                            <th>Applied date</th>
			                          </tr>
			                          
			                        </thead>
		                        
		                        
			                        <tbody>
			                         
			                           <%int i = 1; %> 
			                           <c:forEach items="${eventRegRecord}" var="val">
														
											
											<tr>
					                            
					                            <td scope="row"><%=i++%></td>
					                            <td>${val.applicant_name}</td>
					                            <td>${val.applicant_email}</td>
					                            <td>${val.applicant_mobile}</td>
					                            <td>${val.applicant_city}</td>
					                            <td>${val.applicant_council_no}</td>
					                            <td>${val.registration_category}</td>
					                            <td>${val.applicant_amount}</td>
					                            <td>${val.applicant_status}</td>
					                            <td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.applied_date}" /></td>
					                        </tr>
											 	  
					                   </c:forEach>
			                          
			                          
			                        </tbody>
		                        
		                      </table>
	                   			
	                     </c:if>
	                     
	                     <c:if test="${empty eventRegRecord}">	
	                     
	                     		<div style="color:red;text-align:center;font-size:18px;">
									As of now no one is registered for the event.
								</div>
	                     
	                     </c:if> 
                      
                    </div>
                  </div>
                </div>
                	
                	
              </div>
              
 </div>