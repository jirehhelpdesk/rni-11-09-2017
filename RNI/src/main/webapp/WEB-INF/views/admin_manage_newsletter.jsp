<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
 
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Manage Newsletter</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    
    
    <%@include file="admin_common_style.jsp" %>
    
    
  </head>
  
  
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
  
  
  <body onload="activeMenu('menu7','Admin')">
    
    
    
    <div class="page form-page">
     
     
      <!-- Main Navbar-->
      <%@include file="admin_header.jsp" %>
      
      
      
      <div class="page-content d-flex align-items-stretch"> 
        
        
         <%@include file="admin_side_bar.jsp" %>
        
        
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Newsletter Management</h2>
            </div>
          </header>
          <div id="page-wrapper">
          <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              
              <div class="row">
               
               <div class="col-lg-3"></div>
               
                <!-- Basic Form-->
                <div class="col-lg-6">
                  <div class="card">
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Send Newsletter</h3>
                    </div>
                    <div class="card-body">
                      <p>Details will sent to user email id.</p>
                      
                         <div id="facultyFormDiv"><%@include file="admin_newsletter_form.jsp" %></div> 
                      
                    </div>
                  </div>
                </div>
                
                <div class="col-lg-3"></div>
                
                <!-- Horizontal Form-->
                <%-- <div class="col-lg-6">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Send feed to user member</h3>
                    </div>
                    <div class="card-body">
                      
                      <p>Details will sent to user email id.</p>
                      
                        <div id="memberFormDiv"><%@include file="admin_feed_form.jsp" %></div>
                      
                    </div>
                  </div>
                </div> --%>
                
                
               
              </div>
              
              
            </div>
          </section>
          
          <section id="newsDiv" class="tables" style="margin-top:-50px;">   
            
            	<%@include file="admin_newsletter_table.jsp" %>
            
          </section>
          
          
          
          <%-- 
          
          <section id="feedDiv" class="tables" style="margin-top:-100px;">   
            	
            	<%@include file="admin_feed_table.jsp" %>
            
          </section> --%>
          
          </div>
          
          <!-- Footer Main -->
			<%@include file="admin_footer.jsp" %>
		  <!-- END Footer Main -->
          
          
          
        </div>
        
      </div>
      
    </div>
    
    
    
    
    <%@include file="admin_common_script.jsp" %>
    
    
    
  </body>
</html>