<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
 
	
 
 <div class="container-fluid">
              
              <div class="row">
                	
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div class="dropdown">
                        <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                        <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                      </div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Post result</h3>
                    </div>
                    <div class="card-body">
                     
                     
                    <c:if test="${!empty postSearchResult}">	
                      
		                   
                   		   <table class="table table-striped table-hover" data-toggle="table" data-pagination="true">
	                       
	                        <thead>
	                          <tr>
	                            <th>Sl.No</th>
	                            <th>Name</th>
	                            <th>Title</th>
	                            <th>Post Type</th>
	                            <th>Post Date</th>
	                            <th>Post Status</th>
	                            <th>Manage</th>
	                          </tr>
	                        </thead>
	                        
	                        
	                        <tbody>
	                         
	                         <%int i = 1; %> 
	                           <c:forEach items="${postSearchResult}" var="val">
												
									<c:set var="postContain" value="${val.post_contain}"/>
									<% String postContain = (String)pageContext.getAttribute("postContain"); %>
									
									<c:set var="postId" value="${val.post_id}"/>
									<% int postId = (Integer)pageContext.getAttribute("postId"); %>
									  
									<c:set var="userId" value="${val.user.user_id}"/>
									<% int userId = (Integer)pageContext.getAttribute("userId"); %>
									
									<tr>
			                            
			                            <td scope="row"><%=i++%></td>
			                            <td>${val.user.first_name} ${val.user.last_name}</td>
			                            <td><span class="contentHide">${val.post_title}</span></td>
			                            
			                            <%if(postContain.equals("Yes")){ %>
			                            <td>Attached</td>
			                            <%}else{ %>
			                            <td>Not attached</td>
			                            <%} %>
			                            
			                            <td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.post_cr_date}" /></td>
			                            <td>${val.post_status}</td>
			                            
			                            <td><a href="#" onclick="showPostFromAdmin('${val.post_id}','${val.user.user_id}')">View</a></td>
			                            
			                        </tr>
									 	  
			                   </c:forEach>
	                          
	                          
	                        </tbody>
	                        
	                      </table>
                   			
                     </c:if>
                     
                     <c:if test="${empty postSearchResult}">	
                     
                     		<div style="color:red;text-align:center;font-size:18px;">
								As per the search criteria no result found.
							</div>
                     
                     </c:if> 
                      
                    </div>
                  </div>
                </div>
                	
                	
              </div>
              
 </div>
 <script src="static/adminResource/js/bootstrap-table.js"></script>