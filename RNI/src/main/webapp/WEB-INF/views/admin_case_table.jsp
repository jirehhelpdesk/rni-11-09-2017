<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
 
	
 
 <div class="container-fluid">
              
              <div class="row">
                	
                <div class="col-lg-12">
                  <div class="card">
                    
                    <div class="card-close">
                      <div class="dropdown">
                         
                         <button type="button" id="closeCard" class="dropdown-toggle" onclick="showCaseForm('show')">Add Case</button>
                         
                      </div>
                    </div>
                    
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Case record</h3>
                    </div>
                    <div class="card-body">
                     
                     
                    <c:if test="${!empty caseRecord}">	
                      
		                   
                   		   <table class="table table-striped table-hover" data-toggle="table" data-pagination="true">
	                       
	                        <thead>
	                          <tr>
	                            <th>Sl.No</th>
	                            <th>Title</th>
	                            <th>Added date</th>
	                            <th>Status</th>
	                            <th>Manage</th>
	                          </tr>
	                        </thead>
	                        
	                        
	                        <tbody>
	                         
	                         <%int i = 1; %> 
	                           <c:forEach items="${caseRecord}" var="val">
									
									<tr>
			                            
			                            <td scope="row"><%=i++%></td>
			                            <td><span class="contentHide">${val.case_title}</span></td>
			                            <td><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${val.case_cr_date}" /></td>
			                            <td>${val.case_status}</td>
			                            <td>
			                            	<a href="#" onclick="viewCaseDetails('${val.case_id}')">View</a>
			                            	&nbsp;&nbsp;
			                            	<a href="#" onclick="deleteCaseDetails('${val.case_id}')">Delete</a>
			                            </td>
			                            
			                        </tr>
			                        
			                   </c:forEach>
	                          
	                          
	                        </tbody>
	                        
	                      </table>
                   			
                     </c:if>
                     
                     <c:if test="${empty caseRecord}">	
                     
                     		<div style="color:red;text-align:center;font-size:18px;">
								As of now case is not added to the portal.
							</div>
                     
                     </c:if> 
                      
                    </div>
                  </div>
                </div>
                	
                	
              </div>
              
 </div>
 
 
<!--  <script src="static/adminResource/js/bootstrap-table.js"></script> -->