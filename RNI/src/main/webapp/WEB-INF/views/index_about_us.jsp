<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>About Us</title>

	
	<%@include file="index_common_style.jsp" %>
	
</head>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
 
<body data-offset="200" data-spy="scroll" data-target=".ow-navigation" onload="activeMenu('menu2','index')">
	
	
	
	<!-- Loader And Header /- -->
	
	<%@include file="index_header.jsp" %>
	
	<!-- END of Loader and Header -->
	
	
	
	
		<!-- PageBanner -->
<div class="container-fluid page-banner about no-padding">
			<div class="container">
				<div class="banner-content-block">
					<div class="banner-content">
						<h3>About Us</h3>
					</div>
				</div>
			</div><!-- 
		<div class="section-padding"></div> -->
	</div>
<!-- PageBanner /- --> 

<!-- Main Section -->
<div class="sponsorshipbg">
<div class="container-fluid eventsingle upcoming-event latest-blog our-history no-padding">
  <div class="container">
        <div class="entry-title text-left">
          <h3 class="hdrcnt text-left mrqwidth">
          <marquee  scrolldelay="50" scrollamount="10" direction="left" onmouseover="this.stop();" onmouseout="this.start();">KMC will award 4 credit points to the delegates and 5 credit points to the faculty attending RETNET INDIA meet.</marquee>
</h3>
        </div>
        <div class="row">
        <div class="col-md-6 col-sm-6">
        <div class="entry-content">

							<p class="text-left pra">RetNetIndia is a network of the retina specialists in
								india who help each other learn, hone their clinical and
								surgical skills for the greater benefit of patients and the
								field of vitreo retinal diseases and surgery. RetNetIndia
								strongly believes in the adage "Katradhu Kai Mann alavu;
								kalladadhu ulagaluvu", the prophetic words of Avvaiyar, the
								tamil poet of yore - in English " what you know is a mere
								handful; what you don't is as big as the universe". There is
								always more to learn!</p>

							<p class="text-left pra">Retnet meetings are unique in there are no didactic
								lectures, but only has case based discussions. This year we have
								enhanced the surgical session - "Fellows follies", a section
								that comprises of beginners mistakes and a more advanced
								"Consultant conundrums".</p>

							<p class="text-left pra">We have been together past 7 years, the meetings getting
								better with each year, thanks to the great cases all of you
								brought in. Hope we can join hands to make this the best retnet
								yet!</p>

		</div>
		</div>
		<div class="col-md-6  col-sm-6">
				<div class="hurryup-block">
					<div id="timer-slider" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner aboutcarsl" role="listbox">
							<div class="item active">
								<div class="timer-box">
									<img src="static/IndexResources/images/about01.jpg">	
								</div>
							</div>
							<div class="item">
								<div class="timer-box">
									<img src="static/IndexResources/images/about03.jpg">		
								</div>
							</div>
							<div class="item">
								<div class="timer-box">
									<img src="static/IndexResources/images/about04.jpg">		
								</div>
							</div>
							<div class="item">
								<div class="timer-box">
									<img src="static/IndexResources/images/about05.jpg">		
								</div>
							</div>
							
						</div>
						<!-- Controls -->
						<a class="left carousel-control" href="#timer-slider" role="button" data-slide="prev">
							<i class="fa fa-angle-left" aria-hidden="true"></i>
						</a>
						<a class="right carousel-control" href="#timer-slider" role="button" data-slide="next">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</a>
					</div>
				</div>
			</div>
			</div>
    <div class="row">
      <div class="col-md-12 col-sm-12"> 
        
        <!-- Schedule Section -->
        <div class="container-fluid no-padding schedule-section">
          <div class="section-header">
            <h3>Retnetindia Organising Committee</h3>
          </div>
          
          	<%@include file="index_member_list.jsp" %>
          
        </div>
        <!-- Schedule Section /- --> 
        
      </div>
      
      
      <%-- <div class="col-md-3 col-sm-4 widget-area">
      
        <%@include file="index_faculty_list.jsp" %>
        
      </div> --%>
      
    </div>
  </div>
  <div class="section-padding"></div>
</div>
	</div>
	
	
	
	
	
	
	<!-- Footer Main -->
	<%@include file="index_footer.jsp" %>
	<!-- END Footer Main -->
	
	
	<%@include file="index_common_script.jsp" %>

	
	
</body>
</html>