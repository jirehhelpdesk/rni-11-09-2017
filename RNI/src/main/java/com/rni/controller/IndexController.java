package com.rni.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.rni.bean.UserBean;
import com.rni.model.EventApplicationModel;
import com.rni.model.EventModel;
import com.rni.model.UserPostModel;
import com.rni.model.UserRegisterModel;
import com.rni.service.AdminService;
import com.rni.service.UserService;


@Controller
@RequestMapping("/")
public class IndexController {

	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private UserService userService;
	
	private static final Logger LOGGER = Logger.getLogger(IndexController.class);
	
	@RequestMapping(value = {"/"}, method = RequestMethod.GET)
	public ModelAndView index(HttpSession session) {
		
		if (session.getAttribute("userId") == null)
		{									
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("bannerRecord",adminService.getBannerFileList());
			
			return new ModelAndView("index",model);	
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
	    	
	    	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("bannerRecord",adminService.getBannerFileList());
			
			return new ModelAndView("index",model);	
		}
					
	}
	
	@RequestMapping(value = "/indexFrame", method = RequestMethod.GET)
	public ModelAndView indexFrame(HttpSession session) {
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		return new ModelAndView("index_frame",model);			
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{		
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("bannerRecord",adminService.getBannerFileList());
			
			return new ModelAndView("index",model);
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
	    	
	    	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("bannerRecord",adminService.getBannerFileList());
			
			return new ModelAndView("index",model);
		}
	
					
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView index(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("bannerRecord",adminService.getBannerFileList());
			
			return new ModelAndView("index",model);
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
	    	
	    	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("bannerRecord",adminService.getBannerFileList());
			
			return new ModelAndView("index",model);	
		}			
	}
	
	@RequestMapping(value = "/aboutUs", method = RequestMethod.GET)
	public ModelAndView aboutUs(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("userId") == null)
		{									
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("facultyList",adminService.getFacultyListInOrder());
			
			model.put("memberList",adminService.getMemberListInOrder());
			
			return new ModelAndView("index_about_us",model);
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
	    	
	    	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("facultyList",adminService.getFacultyListInOrder());
			
			model.put("memberList",adminService.getMemberListInOrder());
			
			return new ModelAndView("index_about_us",model);
		}		
		
					
	}
	
	@RequestMapping(value = "/sponsorship", method = RequestMethod.GET)
	public ModelAndView service(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("userId") == null)
		{									
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("facultyList",adminService.getFacultyListInOrder());
			
			model.put("memberList",adminService.getMemberListInOrder());
			
			return new ModelAndView("index_sponsorship",model);
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
	    	
	    	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("facultyList",adminService.getFacultyListInOrder());
			
			model.put("memberList",adminService.getMemberListInOrder());
			
			return new ModelAndView("index_sponsorship",model);
		}	
		
			
	}
	
	
	@RequestMapping(value = "/imageFolderGallery", method = RequestMethod.GET)
	public ModelAndView imageFolderGallery(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("userId") == null)
		{									
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("imagePost",userService.getPostsUnderCondition("Image"));
			
			//model.put("galleryFileRecord",adminService.getGalleryFileRecord("All"));
			
			model.put("galleryRecord",adminService.getGalleryRecord());
			
			return new ModelAndView("index_fld_img_gallery",model);
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
	    	
	    	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("imagePost",userService.getPostsUnderCondition("Image"));
			
			//model.put("galleryFileRecord",adminService.getGalleryFileRecord("All"));
			
			model.put("galleryRecord",adminService.getGalleryRecord());
			
			return new ModelAndView("index_fld_img_gallery",model);
		}			
	}
	
	@RequestMapping(value = "/imageGallery", method = RequestMethod.GET)
	public ModelAndView imageGallery(HttpServletRequest request,HttpSession session) {
		
		int galleryId = Integer.parseInt(request.getParameter("galId"));
		
		if (session.getAttribute("userId") == null)
		{									
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("imagePost",userService.getPostsUnderCondition("Image"));
			
			model.put("galleryFileRecord",adminService.getGalleryFileRecord("All",galleryId));
			
			return new ModelAndView("index_image_gallery",model);
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
	    	
	    	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("imagePost",userService.getPostsUnderCondition("Image"));
			
			model.put("galleryFileRecord",adminService.getGalleryFileRecord("All",galleryId));
			
			return new ModelAndView("index_image_gallery",model);
		}			
	}
	
	
	@RequestMapping(value = "/showGalleryFile", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showGalleryFile(HttpSession session,HttpServletRequest request) {
					
		request.setAttribute("fileCategory",request.getParameter("fileCategory"));
		request.setAttribute("fileName",request.getParameter("fileName"));
		request.setAttribute("fileType",request.getParameter("fileType"));
		
		return new ModelAndView("index_show_gallery_file");					
	}
	
	
	
	@RequestMapping(value = "/videosGallery", method = RequestMethod.GET)
	public ModelAndView videosGallery(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("userId") == null)
		{									
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("videoPost",userService.getPostsUnderCondition("Video"));
			
			return new ModelAndView("index_video_gallery",model);
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
	    	
	    	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("videoPost",userService.getPostsUnderCondition("Video"));
			
			return new ModelAndView("index_video_gallery",model);
		}	
	}
	
	@RequestMapping(value = "/events", method = RequestMethod.GET)
	public ModelAndView events(HttpServletRequest request,HttpSession session) {
						
		if (session.getAttribute("userId") == null)
		{									
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("eventList",userService.getEventList());
			
			return new ModelAndView("index_event",model);			
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
	    	
	    	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("eventList",userService.getEventList());
			
			return new ModelAndView("index_event",model);			
		}		
	}
	
	@RequestMapping(value = "/contactUs", method = RequestMethod.GET)
	public ModelAndView contactUs(HttpServletRequest request,HttpSession session) {
						
		if (session.getAttribute("userId") == null)
		{									
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("facultyList",adminService.getFacultyListInOrder());
			
			model.put("memberList",adminService.getMemberListInOrder());
			
			return new ModelAndView("index_contact",model);					
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
	    	
	    	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("facultyList",adminService.getFacultyListInOrder());
			
			model.put("memberList",adminService.getMemberListInOrder());
			
			
			return new ModelAndView("index_contact",model);					
		}	
	}
	
	
	
	// USER HANDLLER URL  ////////////////////////////////////////////////////////////////
	
	
	@RequestMapping(value = "/userRegister", method = RequestMethod.GET)
	public ModelAndView userRegister(HttpServletRequest request,HttpSession session,HttpServletResponse res) {
		
		if (session.getAttribute("userId") == null)
		{									
			String user = request.getParameter("user");
			
			request.setAttribute("user",user);
			
			request.setAttribute("actionMessage","No Message");
			
			/*Cookie cookie = null;
		    Cookie[] cookies = null;

		    // Get an array of Cookies associated with this domain
		    cookies = request.getCookies();
		    
		    if(cookies!=null)
		    {
		    	for (int i = 0; i < cookies.length; i++) {
		            
			    	cookie = cookies[i];
			    	
			    	res.addCookie(cookie);
			    	
			    	if(cookie.getName().equals("CheckRemember"))
			    	{
			    		LOGGER.info("Remember="+cookie.getValue());
			    	}
			    	
			    	if(cookie.getName().equals("loginEmail"))
			    	{
			    		LOGGER.info("User Name="+cookie.getValue());
			    	}
			    	
			    	if(cookie.getName().equals("loginpassword"))
			    	{
			    		LOGGER.info("Password ="+cookie.getValue());
			    	}		    	
		        }
		    }*/
		    
			request.setAttribute("layOutMessage","SignIn");
		      
			return new ModelAndView("index_user_registration");	
        }
		else
		{
			return new ModelAndView("redirect:/userDashboard");
		}
						
	}
	
	
	@RequestMapping(value = "/userSignUp", method = RequestMethod.GET)
	public ModelAndView userSignUp(HttpServletRequest request,HttpSession session,HttpServletResponse res) {
		
		if (session.getAttribute("userId") == null)
		{									
			String user = request.getParameter("user");
			
			request.setAttribute("user",user);
			
			request.setAttribute("actionMessage","No Message");
			
			request.setAttribute("layOutMessage","SignUp");
			
			return new ModelAndView("index_user_registration");	
        }
		else
		{
			return new ModelAndView("redirect:/userDashboard");
		}
						
	}
	

	@RequestMapping(value = "/checkUserEmailId", method = RequestMethod.POST)
	@ResponseBody public String checkUserEmailId(@ModelAttribute("userRegDetails") UserRegisterModel userRegDetails,HttpSession session,HttpServletRequest request) {
			
		String emailId = userRegDetails.getEmail_id();
		
		String status = userService.checkUserExist(emailId,"emailId");
		
		return status;			
	}
	
	@RequestMapping(value = "/checkUserPhno", method = RequestMethod.POST)
	@ResponseBody public String checkUserPhno(@ModelAttribute("userRegDetails") UserRegisterModel userRegDetails,HttpSession session,HttpServletRequest request) {
			
		String mobile = userRegDetails.getMobile_no();
		
		String status = userService.checkUserExist(mobile,"mobile");
		
		return status;						
	}
	
	@RequestMapping(value = "/saveUserRegister", method = RequestMethod.POST)
	@ResponseBody public String saveUserRegister(@ModelAttribute("userRegDetails") UserRegisterModel userRegDetails,HttpSession session,MultipartHttpServletRequest request) {
		
		String status = userService.saveUserRegister(userRegDetails,request);
		
		return status;						
	}
	
	
	@RequestMapping(value = "/saveSubscriptionRequest", method = RequestMethod.POST)
	@ResponseBody public String saveSubscriptionRequest(HttpSession session,HttpServletRequest request) {
		
		String emailId = request.getParameter("emailId");
		
		String status = userService.saveSubscriptionRequest(emailId);
		
		return status;						
	}
	
	
	@RequestMapping(value = "/searchUserProfile", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchUserProfile(HttpSession session,HttpServletRequest request) {
					
		String searchValue = request.getParameter("searchValue");
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("userSearchList",userService.userSearchList(searchValue));
		
		return new ModelAndView("index_user_search_list",model);					
	}
	
	@RequestMapping(value = "/inViewUserProfile", method = RequestMethod.GET)
	@ResponseBody public ModelAndView inViewUserProfile(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{												
			return new ModelAndView("redirect:/userRegister");							
        }
		else
		{
			int userId = Integer.parseInt(request.getParameter("userId"));
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("searchedUserDetails",userService.getUserDetails(userId));
			
			int currentUserId = (Integer) session.getAttribute("userId");
			UserBean userBean = userService.getUserDetails(currentUserId);
			model.put("userBean",userBean);
			
			if(userId==currentUserId)
			{
				request.setAttribute("userType","currentUser");
			}
			else
			{
				request.setAttribute("userType","diffUser");
			}
			
			return new ModelAndView("index_user_profile_dashboard",model);		
		}	
							
	}
	
	
	// FORGET PASSWORD 
	
	@RequestMapping(value = "/sendLinkForForgetPw", method = RequestMethod.POST)
	public @ResponseBody String sendLinkForForgetPw(HttpSession session,HttpServletRequest request) {
		
		String status = "";
		
		String emailId = (String)request.getParameter("forgetPwEmailId");
		
		status = userService.sendLinkForgetPassword(emailId);
		
		return status;
	}
	
	@RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
	public String resetPassword(HttpSession session,HttpServletRequest request) {
	
		String uniqueId = request.getParameter("requnIkedij");
		
		String requestedEmailID = userService.getEmailIdFromForgetPassword(uniqueId);
		
		if(requestedEmailID.equals("Not Exist"))
		{
			String user = request.getParameter("user");
			
			request.setAttribute("user",user);
			request.setAttribute("actionMessage", "Link Expired");
			
			request.setAttribute("layOutMessage","SignIn");
			
			return "index_user_registration";
		}
		else
		{
			request.setAttribute("requestedEmailID", requestedEmailID);
			request.setAttribute("uniqueId", uniqueId);	
			
			return "index_user_reset_password";
		}
	}
	
	
	@RequestMapping(value = "/saveUserResetPassword", method = RequestMethod.POST)
	public @ResponseBody String saveUserResetPassword(HttpSession session,HttpServletRequest request) {
	
		String status = "";
		
		String emailId = request.getParameter("requestedEmailID");
		String password = request.getParameter("password");
		
		status = userService.getChangePassword(emailId,password);
		
		return status;
	}
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/abstractSubmission", method = RequestMethod.GET)
	public @ResponseBody ModelAndView abstractSubmission(HttpSession session,HttpServletRequest request) {
	
		
	   return new ModelAndView("index_event_abstract_doc_form");
	}
	
	
	@RequestMapping(value = "/saveAbstractSubmission", method = RequestMethod.POST)
	@ResponseBody public String saveAbstractSubmission(HttpSession session,MultipartHttpServletRequest request) {
				
		return userService.saveAbstractSubmission(session,request);						
	}
	
	
	// PARTICIPATE IN EVENT REGISTRATION
	
	
	@RequestMapping(value = "/partcipateOnEvent", method = RequestMethod.GET)
	public @ResponseBody ModelAndView partcipateOnEvent(HttpSession session,HttpServletRequest request) {
	
		
	   return new ModelAndView("index_event_participate");
	}
	
	@RequestMapping(value = "/indexSaveApplicaitionPassToPayment", method = RequestMethod.POST)
	@ResponseBody public String indexSaveApplicaitionPassToPayment(@ModelAttribute("evntApplication") EventApplicationModel evntApplication,HttpSession session,HttpServletRequest request) {
		
		String status = "";
		
		status = userService.indexSaveApplicaitionPassToPayment(evntApplication,session,request);
		
		if(evntApplication.getApplicant_amount()>0)
		{
			return status;
		}
		else
		{
			return "No gateway";
		}						
	}
	
	@RequestMapping(value = "/indexPaymentOverview", method = RequestMethod.GET)
	public ModelAndView indexPaymentOverview(HttpSession session,HttpServletRequest request) {
		
		int applicationId = (Integer) session.getAttribute("application_id");
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("applicantDetails",userService.getApplicationDetails(applicationId));
		
		return new ModelAndView("index_event_payment_overview",model);	
	}
	
	
	@RequestMapping(value = "/indexPaymentStatus", method = RequestMethod.POST)
	public ModelAndView indexPaymentStatus(HttpSession session,HttpServletRequest request) {
		
		int applicationId = (Integer) session.getAttribute("application_id");
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		EventApplicationModel evntApplication = new EventApplicationModel();
		evntApplication.setApplication_id(applicationId);
		
		userService.indexUpdateEventApplication(evntApplication,request);
		
		model.put("applicantDetails",userService.getApplicationDetails(applicationId));
		
		return new ModelAndView("index_event_payment_status",model);	
	}	
	
	
	
	// PARTICIPATE IN PRE CONFERENCE WORKSHOP
	

	@RequestMapping(value = "/preConferenceWorkShop", method = RequestMethod.GET)
	public @ResponseBody ModelAndView preConferenceWorkShop(HttpSession session,HttpServletRequest request) {
	
		
	   return new ModelAndView("index_pre_conference");
	}
	
	
	@RequestMapping(value = "/indexSaveConfWorkShopPassToPayment", method = RequestMethod.POST)
	@ResponseBody public String indexSaveConfWorkShopPassToPayment(@ModelAttribute("evntApplication") EventApplicationModel evntApplication,HttpSession session,HttpServletRequest request) {
		
		String status = "";
		
		status = userService.indexSaveConfWorkShopPassToPayment(evntApplication,session,request);
		
		if(evntApplication.getApplicant_amount()>0)
		{
			return status;
		}
		else
		{
			return "No gateway";
		}						
	}
		
	
	@RequestMapping(value = "/indexPreConferenceOverview", method = RequestMethod.GET)
	public ModelAndView indexPreConferenceOverview(HttpSession session,HttpServletRequest request) {
		
		int applicationId = (Integer) session.getAttribute("application_id");
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("applicantDetails",userService.getApplicationDetails(applicationId));
		
		return new ModelAndView("index_workshop_payment_overview",model);	
	}
	
	@RequestMapping(value = "/indexWorkPaymentStatus", method = RequestMethod.POST)
	public ModelAndView indexWorkPaymentStatus(HttpSession session,HttpServletRequest request) {
		
		int applicationId = (Integer) session.getAttribute("application_id");
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		EventApplicationModel evntApplication = new EventApplicationModel();
		evntApplication.setApplication_id(applicationId);
		
		userService.indexUpdateWorkshopApplication(evntApplication,request);
		
		model.put("applicantDetails",userService.getApplicationDetails(applicationId));
		
		return new ModelAndView("index_event_payment_status",model);	
	}	
	
	
	
}
