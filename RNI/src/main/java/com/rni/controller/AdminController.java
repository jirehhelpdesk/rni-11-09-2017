package com.rni.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.rni.bean.FormBean;
import com.rni.bean.NewsLetterBean;
import com.rni.bean.RedNetGalleryBean;
import com.rni.bean.UserBean;
import com.rni.bean.UserPostBean;
import com.rni.model.AdminDetails;
import com.rni.model.CommitteeMemberModel;
import com.rni.model.EventApplicationModel;
import com.rni.model.EventModel;
import com.rni.model.FacultyModel;
import com.rni.model.IndexBannerModel;
import com.rni.model.NewsLetterModel;
import com.rni.model.PortalContentModel;
import com.rni.model.UserPostModel;
import com.rni.model.UserRegisterModel;
import com.rni.service.AdminService;
import com.rni.service.AdminServiceImpl;
import com.rni.service.UserService;
import com.rni.util.ReportingExcel;

@Controller
public class AdminController {

	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private UserService userService;
	
	ReportingExcel reportExcel = new ReportingExcel();
	
	private static final Logger LOGGER = Logger.getLogger(AdminServiceImpl.class);
	
	
	@RequestMapping(value = {"/serviceNotfound"}, method = RequestMethod.GET)
	public ModelAndView serviceNotfound(HttpSession session) {
		
		return new ModelAndView("error_page");			
	}
	
	@RequestMapping(value = "/adminLogout", method = RequestMethod.GET)
	public String adminLogout(HttpSession session,HttpServletRequest request) {
		
		session.removeAttribute("adminId");			 
		
		adminService.setAdminAccess("offline");
		
		session.setAttribute("actionMessage", "offline");
		
		return "redirect:/adminLogin";							
	}
	
	@RequestMapping(value = "/adminLogin", method = RequestMethod.GET)
	public String adminLogin(@ModelAttribute("adminDetails") AdminDetails adminDetails,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{							
			adminService.setAdminAccess("offline");
			
			session.setAttribute("actionMessage","offline");
			
			return "admin_login";		
        }
		else
		{
			return "admin_login";			
		}		
	}
	
	
	@RequestMapping(value = "/authAdminCredential", method = RequestMethod.GET)
	public String getValidateAdminWithGet(HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{							
			adminService.setAdminAccess("offline");
			
        	return "redirect:/adminLogin";
        }
		else
		{
			return "redirect:/adminLogin";		
		}				
	}
	
	
	@RequestMapping(value = "/authAdminCredential", method = RequestMethod.POST)
	public String getValidateAdmin(@ModelAttribute("adminDetails") AdminDetails adminDetails,HttpSession session,HttpServletRequest request) {
		
		String admin_user_id = adminDetails.getAdmin_user_id();
        String admin_password = adminDetails.getAdmin_password();
        
        String authenticateStatus = adminService.getValidateAdmin(admin_user_id,admin_password);
        
        if(authenticateStatus.equals("correct"))
        {
        	session.setAttribute("adminId", admin_user_id);
        	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
        	
        	return "redirect:/adminHome";	
        }
        else if(authenticateStatus.equals("online"))
        {
        	session.setAttribute("actionMessage", "online");
        	
        	return "admin_login";	
        }
        else
        {
        	session.setAttribute("actionMessage", "incorrect");
        	
        	return "admin_login";
        }	
        
	}
	
	
	@RequestMapping(value = "/adminHome", method = RequestMethod.GET)
	public ModelAndView adminDash(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			model.put("portalStatus",adminService.getUpdateOnPortalStatus());
			
			model.put("eventList",adminService.getLastFiveEventList());
			
			
			return new ModelAndView("admin_dashboard",model);				
		}							
	}
	
	
	@RequestMapping(value = "/manUser", method = RequestMethod.GET)
	public ModelAndView manUser(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_manage_user",model);				
		}							
	}
	
	
	/// ******************************************  STARTS OF EVENT MANAGE CONTROLLER  ******************************************************//
	
	
	@RequestMapping(value = "/manEvent", method = RequestMethod.GET)
	public ModelAndView manEvent(@ModelAttribute("eventDetail") EventModel eventDetail,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			model.put("eventList",adminService.getLastFiveEventList());
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_manage_event",model);				
		}							
	}
	
	
	@RequestMapping(value = "/saveEventFile", method = RequestMethod.POST)
	@ResponseBody public String saveEventFile(HttpSession session,MultipartHttpServletRequest request) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return "redirect:/adminLogin";
        }
		else
		{			
			String status = adminService.saveEventFile(request,session);
			
			String fileList = "";
			
			if(status.equals("success"))
			{
				fileList = adminService.getCurrentUploadedFiles(session);
			}
			else
			{
				fileList = "No file";
			}
			
			return fileList;
		}							
	}
	
	
	@RequestMapping(value = "/deleteTempFile", method = RequestMethod.POST)
	@ResponseBody public String deleteTempFile(HttpSession session,HttpServletRequest request) {
		
		    String status = adminService.deleteTempFile(request,session);
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			String fileList = "";
			
			if(status.equals("success"))
			{
				fileList = adminService.getCurrentUploadedFiles(session);
			}
			else
			{
				fileList = "No file";
			}
			
			return fileList;						
	}
	
	
	@RequestMapping(value = "/saveEventDetail", method = RequestMethod.POST)
	@ResponseBody public String saveEventDetail(@ModelAttribute("eventDetail") EventModel eventDetail,MultipartHttpServletRequest request,HttpSession session) {
		
		System.out.println("Reached");
		
		String status= "";
		
		status = adminService.saveEventDetails(eventDetail,request,session);
		
		return status;
	}
	
	@RequestMapping(value = "/updateEventDetail", method = RequestMethod.POST)
	@ResponseBody public String updateEventDetail(@ModelAttribute("eventDetail") EventModel eventDetail,MultipartHttpServletRequest request,HttpSession session) {
		
		String status= "";
		
		status = adminService.updateEventDetail(eventDetail,request,session);
		
		return status;
	}
	
	@RequestMapping(value = "/searchEvent", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchEvent(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("eventList",adminService.getSearchEvent(request));
			
			return new ModelAndView("admin_event_table",model);
		}
	}
	
	@RequestMapping(value = "/showEventForm", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showEventForm(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{						
			return new ModelAndView("admin_event_form");
		}
	}
	
	@RequestMapping(value = "/detailsOnEvent", method = RequestMethod.POST)
	@ResponseBody public ModelAndView detailsOnEvent(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			int eventId = Integer.parseInt(request.getParameter("eventId"));
			
			model.put("eventData",adminService.detailsOnEvent(eventId));
			
			return new ModelAndView("admin_event_form",model);
		}
	}
	
	
	@RequestMapping(value = "/showEventReport", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showEventReport(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			int eventId = Integer.parseInt(request.getParameter("eventId"));
			
			model.put("eventReportData",adminService.eventReportData(eventId));
			
			request.setAttribute("eventId", eventId);
			
			return new ModelAndView("admin_event_report_table",model);
		}
	}
	
	@RequestMapping(value = "/downloadEventReport", method = RequestMethod.GET)
	@ResponseBody public ModelAndView downloadEventReport(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{						
			int eventId = Integer.parseInt(request.getParameter("eventId"));
			
			List<EventApplicationModel> eModel = adminService.eventReportData(eventId);
			
			String status = reportExcel.generateEventReportInExcel(eModel);
			
			String fileName = status;
			
			String reportDirectory = "";
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
			
			reportDirectory = fileResource.getString("eventDirectory");
			
			reportDirectory = reportDirectory+"/"+eventId;
			
			request.setAttribute("rootDirectory",reportDirectory);
			request.setAttribute("fileName",fileName);
			
			return new ModelAndView("commonDownload");
			
		}
	}
	
	
	
	// *************************************  END OF MANAGE EVENT CONTROLLER  ************************************************//
	
	
	
	
	// *************************************  START OF MANAGE EVENT CONTROLLER  ************************************************//
	
	
	
	@RequestMapping(value = "/manPost", method = RequestMethod.GET)
	public ModelAndView manPost(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_manage_post",model);				
		}							
	}
	
	
	@RequestMapping(value = "/searchPost", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchPost(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{
			String status= "";
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("postSearchResult",adminService.getSearchPost(request));
			
			return new ModelAndView("admin_post_table",model);
		}
	}
	
	
	@RequestMapping(value = "/showPostFromAdmin", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showPostFromAdmin(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{
			int postId = Integer.parseInt(request.getParameter("postId"));
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("showPost",userService.getAllPost(postId,"Other","Single"));
			model.put("showComment",userService.getAllPostComment(postId));
			
			return new ModelAndView("admin_show_post",model);		
		}	
	}
	
	
	@RequestMapping(value = "/blockUserPost", method = RequestMethod.POST)
	@ResponseBody public String blockUserPost(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return "session_failed";
        }
		else
		{
			String status = "";
			
			int postId = Integer.parseInt(request.getParameter("postId"));
			String postStatus = request.getParameter("status");
			
			status = adminService.getBlockUserPost(postId,postStatus);
			
			return status;		
		}	
	}
	
	
	@RequestMapping(value = "/manageCaseOfDiscussion", method = RequestMethod.POST)
	@ResponseBody public String manageCaseOfDiscussion(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return "session_failed";
        }
		else
		{
			String status = "";
			
			int postId = Integer.parseInt(request.getParameter("postId"));
			String action = request.getParameter("action");
			
			status = adminService.manageCaseOfDiscussion(postId,action);
			
			return status;		
		}	
	}
	
	
	// *************************************  END OF MANAGE POST CONTROLLER  ************************************************//
	
	
	///  ****************************************  START OF MANAGE USER CONTROLLER  ***************************************///
	
	
	@RequestMapping(value = "/searchUser", method = RequestMethod.POST)
	@ResponseBody public ModelAndView searchUser(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("userSearchResult",adminService.getSearchUser(request));
			
			return new ModelAndView("admin_user_table",model);
		}
	}
	
	@RequestMapping(value = "/showUserFromAdmin", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showUserFromAdmin(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			int userId = Integer.parseInt(request.getParameter("userId"));
			
			UserBean userBean = userService.getUserDetails(userId);
			
			model.put("userBean",userBean);
			
			return new ModelAndView("admin_view_user",model);
		}
	}
	
	@RequestMapping(value = "/changeUserStatus", method = RequestMethod.POST)
	@ResponseBody public String changeUserStatus(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return "session_failed";
        }
		else
		{			
			String status = "";
			
			int userId = Integer.parseInt(request.getParameter("userId"));
			String userStatus = request.getParameter("status");
			
			status = adminService.updateUserStatus(userStatus,userId);
			
			return status;
		}
	}
	
	///  ****************************************  END OF MANAGE USER CONTROLLER  **************************************** ///
	
	
    //////////// **********************  START OF ADMIN SETTING CONTROLLER  ************************************ ////////////////////////////////
	
	
	@RequestMapping(value = "/manSetting", method = RequestMethod.GET)
	public ModelAndView manSetting(@ModelAttribute("admin") AdminDetails admin,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_setting",model);				
		}							
	}
	
	@RequestMapping(value = "/saveAdminDetails", method = RequestMethod.POST)
	@ResponseBody public String saveAdminDetails(@ModelAttribute("admin") AdminDetails admin,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return "session_failed";
        }
		else
		{			
			admin.setAdmin_updated_date(new Date());
			
			return adminService.updateAdminDetails(admin);				
		}							
	}
	
	
	@RequestMapping(value = "/updateAdminPassword", method = RequestMethod.POST)
	@ResponseBody public String updateAdminPassword(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return "session_failed";
        }
		else
		{						
			return adminService.updateAdminPassword(request,session);				
		}							
	}
	
////////////END OF ADMIN SETTING CONTROLLER  ////////////////////////////////
	
	
	
//////////// *************************** START OF ADMIN PORTAL MANAGE CONTROLLER  *************************** ////////////////////////////////
	
	@RequestMapping(value = "/manPortal", method = RequestMethod.GET)
	public ModelAndView manPortal(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("facultyList",adminService.getFacultyList());
			
			model.put("memberList",adminService.getMemberList());
			
			model.put("contentList",adminService.getContentList());
			
			model.put("adminData",adminService.getAdminDetails());
			
			return new ModelAndView("admin_manage_portal",model);				
		}							
	}
	
	
	@RequestMapping(value = "/saveFaculty", method = RequestMethod.POST)
	@ResponseBody public ModelAndView saveFaculty(@ModelAttribute("faculty") FacultyModel faculty,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			if(adminService.saveFacultyDetails(faculty).equals("success"))
			{
				model.put("facultyList",adminService.getFacultyList());
			}
			
			return new ModelAndView("admin_faculty_table",model);
		}
	}
	
	
	@RequestMapping(value = "/showEditFormFaculty", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showEditFormFaculty(@ModelAttribute("faculty") FacultyModel faculty,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			int facultyId = Integer.parseInt(request.getParameter("facultyId"));
			
			model.put("facultyData",adminService.getFacultyData(facultyId));
			
			return new ModelAndView("admin_faculty_form",model);
		}
	}
	
	
	@RequestMapping(value = "/deleteFaculty", method = RequestMethod.POST)
	@ResponseBody public ModelAndView deleteFaculty(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			int facultyId = Integer.parseInt(request.getParameter("facultyId"));
			
			if(adminService.deleteFacultyData(facultyId).equals("success"))
			{
				model.put("facultyList",adminService.getFacultyList());
			}
			
			return new ModelAndView("admin_faculty_table",model);
		}
	}
	
	
	
	@RequestMapping(value = "/saveComMember", method = RequestMethod.POST)
	@ResponseBody public ModelAndView saveComMember(@ModelAttribute("member") CommitteeMemberModel member,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			if(adminService.saveMemberDetails(member).equals("success"))
			{
				model.put("memberList",adminService.getMemberList());
			}
			
			return new ModelAndView("admin_member_table",model);
		}
	}
	
	
	@RequestMapping(value = "/showEditFormMember", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showEditFormMember(@ModelAttribute("member") CommitteeMemberModel member,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			int memberId = Integer.parseInt(request.getParameter("memberId"));
			
			model.put("memberData",adminService.getMemberData(memberId));
			
			return new ModelAndView("admin_member_form",model);
		}
	}
	
	
	
	@RequestMapping(value = "/deleteMember", method = RequestMethod.POST)
	@ResponseBody public ModelAndView deleteMember(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			int memberId = Integer.parseInt(request.getParameter("memberId"));
			
			if(adminService.deleteMemberData(memberId).equals("success"))
			{
				model.put("memberList",adminService.getMemberList());
			}
			
			return new ModelAndView("admin_member_table",model);
		}
	}
	
	
	@RequestMapping(value = "/showPortalContentForm", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showPortalContentForm(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{						
			return new ModelAndView("admin_portal_content_form");
		}
	}
	
	
	@RequestMapping(value = "/savePortalContent", method = RequestMethod.POST)
	@ResponseBody public ModelAndView savePortalContent(@ModelAttribute("prtlContent") PortalContentModel prtlContent,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			String actionRequest = request.getParameter("actionRequest");
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			if(actionRequest.equals("Save"))
			{
				if(adminService.savePortalContent(prtlContent).equals("success"))
				{
					model.put("contentList",adminService.getContentList());
				}
			}
			else
			{
				prtlContent.setContent_id(Integer.parseInt(request.getParameter("contentId")));
				
				if(adminService.updatePortalContent(prtlContent).equals("success"))
				{
					model.put("contentList",adminService.getContentList());
				}
			}
			
			
			return new ModelAndView("admin_portal_content_table",model);
		}
	}
	
	@RequestMapping(value = "/showEditFormContent", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showEditFormContent(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			int contentId = Integer.parseInt(request.getParameter("contentId"));
			
			model.put("contentData",adminService.getContentDetails("contentId",""+contentId,""));
			
			return new ModelAndView("admin_content_form",model);
		}
	}
	
	
	@RequestMapping(value = "/deleteContent", method = RequestMethod.POST)
	@ResponseBody public ModelAndView deleteContent(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			int contentId = Integer.parseInt(request.getParameter("contentId"));
			
			if(adminService.deleteContent(contentId).equals("success"))
			{
				model.put("contentList",adminService.getContentList());
			}
			else
			{
				model.put("contentList",adminService.getContentList());
			}
			
			return new ModelAndView("admin_portal_content_table",model);
		}
	}
	
	
	
///////////////////////////////   END OF ADMIN PORTAL MANAGE CONTROLLER  //////////////////////////////////////////////////////////////
	
	
	
	
	
////////////----------------      START OF ADMIN PORTAL MANAGE NEWSLETTER  ---------------------------////////////////////////////////
	
	
	@RequestMapping(value = "/manNewsLetter", method = RequestMethod.GET)
	public ModelAndView manNewsLetter(@ModelAttribute("newsLetter") NewsLetterBean newsLetter,HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			model.put("letterData",adminService.getNewsData());
			
			return new ModelAndView("admin_manage_newsletter",model);				
		}							
	}
	
	
	@RequestMapping(value = "/showUserCount", method = RequestMethod.POST)
	@ResponseBody public String showUserCount(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return "session_failed";
        }
		else
		{			
			String value = request.getParameter("value");
			
			return adminService.getUserCount(value);
		}
	}    
	
	@RequestMapping(value = "/sendNewsLetter", method = RequestMethod.POST)
	@ResponseBody public String sendNewsLetter(@ModelAttribute("newsLetter") NewsLetterBean newsLetter,HttpSession session,MultipartHttpServletRequest request) {
		
		if (session.getAttribute("adminId") == null)
		{									
			return "session_failed";
        }
		else
		{
			String status = adminService.sendNewsLetter(newsLetter,request,session);
			
			LOGGER.info("reached controller");
			
			return status;		
		}							
	}
	
	
////////////----------------      END OF ADMIN PORTAL MANAGE NEWSLETTER  ----------------------------////////////////////////////////
	
	
	
	
	
	
	
////////////---------------------- START OF ADMIN PORTAL MANAGE GALLERY  ----------------------------////////////////////////////////
	
	
	@RequestMapping(value = "/manGallary", method = RequestMethod.GET)
	public ModelAndView manGallary(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			model.put("galleryRecord",adminService.getGalleryRecord());
			
			return new ModelAndView("admin_manage_gallery",model);				
		}							
	}
	
	//
	
	@RequestMapping(value = "/saveGalleryRecord", method = RequestMethod.POST)
	@ResponseBody public String saveGalleryRecord(@ModelAttribute("galleryRecord") RedNetGalleryBean galleryRecord,HttpSession session,MultipartHttpServletRequest request) {
		
		if (session.getAttribute("adminId") == null)
		{									
			return "session_failed";
        }
		else
		{
			String status = adminService.saveGalleryRecord(galleryRecord,request,session);
			
			LOGGER.info("reached controller");
			
			return status;		
		}							
	}
	
	
	
	@RequestMapping(value = "/deleteGallery", method = RequestMethod.POST)
	@ResponseBody public String deleteGallery(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("adminId") == null)
		{									
			return "session_failed";
        }
		else
		{
			String status = "";
			
			int galleryId = Integer.parseInt(request.getParameter("galleryId"));
			
			status = adminService.deleteGallery(galleryId);
			
			return status;		
		}	
		
						
	}
	
	
////////////---------------------- END OF ADMIN PORTAL MANAGE GALLERY  --------------------------------////////////////////////////////

	
////////////---------------------- START OF ADMIN PORTAL MANAGE CASE  --------------------------------////////////////////////////////

	
	@RequestMapping(value = "/manCase", method = RequestMethod.GET)
	public ModelAndView manCase(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
        	return new ModelAndView("redirect:/adminLogin");
        }
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			model.put("caseRecord",adminService.getCaseRecord("All"));
			
			return new ModelAndView("admin_manage_case",model);				
		}							
	}
	
	
	@RequestMapping(value = "/saveCaseDetail", method = RequestMethod.POST)
	@ResponseBody public String saveCaseDetail(@ModelAttribute("formBean") FormBean formBean,HttpSession session,MultipartHttpServletRequest request) {
	
		String status = adminService.saveCaseDetail(formBean,request,session);
		
		return status;
	}
	
	@RequestMapping(value = "/viewCaseDetails", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewCaseDetails(HttpSession session,HttpServletRequest request) {
	
		Map<String, Object> model = new HashMap<String, Object>();
		
		int caseId = Integer.parseInt(request.getParameter("caseId"));
		
		model.put("caseDetails",adminService.getCaseRecord(caseId+""));
		
		return new ModelAndView("admin_case_details",model);			
	}
	
	
	@RequestMapping(value = "/changeCaseStatus", method = RequestMethod.POST)
	@ResponseBody public String changeCaseStatus(HttpSession session,HttpServletRequest request) {
	
		String status = adminService.changeCaseStatus(request);
		
		return status;
	}
	
	
	@RequestMapping(value = "/deleteCaseDetails", method = RequestMethod.POST)
	@ResponseBody public String deleteCaseDetails(HttpSession session,HttpServletRequest request) {
	
		String status = adminService.deleteCaseDetails(request);
		
		return status;
	}
	
	
////////////---------------------- END OF ADMIN PORTAL MANAGE CASE  --------------------------------////////////////////////////////
	
	
	

////////////---------------------- START OF ADMIN PORTAL MANAGE BANNER  --------------------------------////////////////////////////////

	
	
	@RequestMapping(value = "/manBanner", method = RequestMethod.GET)
	public ModelAndView manBanner(HttpServletRequest request,HttpSession session) {
	
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			model.put("bannerRecord",adminService.getAllBannerFileList());
			
			return new ModelAndView("admin_manage_banner",model);				
		}							
	}

	@RequestMapping(value = "/captureBrowseFiles", method = RequestMethod.POST)
	@ResponseBody public String captureBrowseFiles(@ModelAttribute("formBean") FormBean formBean,HttpSession session,MultipartHttpServletRequest request) {
	
		String listFileName = adminService.captureBrowseFiles(formBean,request,session);
		
		return listFileName;
	}
	
	
	@RequestMapping(value = "/deleteBrowseFile", method = RequestMethod.POST)
	@ResponseBody public String deleteBrowseFile(HttpSession session,HttpServletRequest request) {
	
		
		String listFileName = adminService.deleteBrowseFile(request,session);
		
		return listFileName;
	}
	
	
	@RequestMapping(value = "/saveBannerFiles", method = RequestMethod.POST)
	@ResponseBody public String saveBannerFiles(@ModelAttribute("formBean") FormBean formBean,HttpSession session,MultipartHttpServletRequest request) {
	
		String status = adminService.saveBannerFiles(formBean,request,session);
		
		return status;
	}
	
	
	
	
	
	@RequestMapping(value = "/viewBanner", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewBanner(HttpSession session,HttpServletRequest request) {
	
		Map<String, Object> model = new HashMap<String, Object>();
		
		int bannerId = Integer.parseInt(request.getParameter("bannerId"));
		
		model.put("bannerDetail",adminService.getBannerRecord(bannerId));
		
		return new ModelAndView("admin_banner_details",model);			
	}
	
	
	@RequestMapping(value = "/changeBannerStatus", method = RequestMethod.POST)
	@ResponseBody public String changeBannerStatus(HttpSession session,HttpServletRequest request) {
	
		String status = adminService.changeBannerStatus(request);
		
		return status;
	}
	
	
	@RequestMapping(value = "/deleteBanner", method = RequestMethod.POST)
	@ResponseBody public String deleteBanner(HttpSession session,HttpServletRequest request) {
	
		String status = adminService.deleteBanner(request);
		
		return status;
	}
	
	
	
	
	
	
	
	@RequestMapping(value = "/masterLaunch", method = RequestMethod.GET)
	public ModelAndView masterLaunch(HttpServletRequest request,HttpSession session) {
	
		if (session.getAttribute("adminId") == null)
		{					
			adminService.setAdminAccess("offline");
			
			return new ModelAndView("redirect:/adminLogin");
		}
		else
		{			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("adminData",adminService.getAdminDetails());
			
			model.put("eventRegRecord",adminService.getEventRegRecord());
			
			model.put("preConRegRecord",adminService.getPreConRegRecord());
			
			model.put("abstractDocRecord",adminService.getAbstractDocRecord());
			
			return new ModelAndView("admin_master_launch",model);				
		}							
	}
	
	
	@RequestMapping(value = "/downloadAbstractDoc", method = RequestMethod.GET)
	@ResponseBody public ModelAndView downloadAbstractDoc(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{												
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
			
			String reportDirectory = fileResource.getString("eventAbstractSubmission");
			
			request.setAttribute("rootDirectory",reportDirectory);
			request.setAttribute("fileName",request.getParameter("fileName"));
			
			return new ModelAndView("commonDownload");			
		}
	}
	
	
	@RequestMapping(value = "/downloadEventRegReport", method = RequestMethod.GET)
	@ResponseBody public ModelAndView downloadEventRegReport(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{									
			List<EventApplicationModel> eModel = adminService.getEventRegRecord();
			
			String status = reportExcel.generateEventBeforeLaunchRpt(eModel);
			
			String fileName = status;
			
			String reportDirectory = "";
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
			
			reportDirectory = fileResource.getString("eventDirectory");
			
			reportDirectory = reportDirectory+"/EventReportBeforeLaunch";
			
			request.setAttribute("rootDirectory",reportDirectory);
			request.setAttribute("fileName",fileName);
			
			return new ModelAndView("commonDownload");
			
		}
	}
	
	
	@RequestMapping(value = "/downloadPreConfRegReport", method = RequestMethod.GET)
	@ResponseBody public ModelAndView downloadPreConfRegReport(HttpServletRequest request,HttpSession session) {
		
		if (session.getAttribute("adminId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{									
			List<EventApplicationModel> eModel = adminService.getEventRegRecord();
			
			String status = reportExcel.generateEventBeforeLaunchRpt(eModel);
			
			String fileName = status;
			
			String reportDirectory = "";
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
			
			reportDirectory = fileResource.getString("eventDirectory");
			
			reportDirectory = reportDirectory+"/EventReportBeforeLaunch";
			
			request.setAttribute("rootDirectory",reportDirectory);
			request.setAttribute("fileName",fileName);
			
			return new ModelAndView("commonDownload");
			
		}
	}
	
	
////////////---------------------- END OF ADMIN PORTAL MANAGE BANNER  --------------------------------////////////////////////////////

	
	
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewGallaryFile", method = RequestMethod.GET)
	@ResponseBody public byte[] previewGallaryFile(HttpServletRequest request,HttpSession session) throws IOException {
		
		String fileName = request.getParameter("fileName");
		
		// Required file Config for entire Controller 
		 
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
		
		String fileDir = fileResource.getString("galleryDirectory");
		
		// End of Required file Config for entire Controller 
		 
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewFile", method = RequestMethod.GET)
	@ResponseBody public byte[] previewFile(HttpServletRequest request,HttpSession session) throws IOException {
		
		String fileName = request.getParameter("fileName");
		String fileCategory = request.getParameter("fileCategory");
		String fileDir = "";
		
		// Required file Config for entire Controller 
		 
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
		
		if(fileCategory.equals("BannerFile"))
		{
			fileDir = fileResource.getString("bannerDirectory");
		}
		else if(fileCategory.equals("GalleryFile"))
		{
			fileDir = fileResource.getString("galleryDirectory");
		}
		else if(fileCategory.equals("CaseFile"))
		{
			fileDir = fileResource.getString("caseDirectory");
			
			fileDir = fileDir + "/" + request.getParameter("caseId");
		}
		else if(fileCategory.equals("CaseCommentFile"))
		{
			fileDir = fileResource.getString("caseDirectory");
			
			fileDir = fileDir + "/" + request.getParameter("caseId") + "/" + request.getParameter("userId");
		}
		else
		{
			// MAKE A DEFAULT FILE URL FOR LAST ALTERNATIVE
		}
		
		// End of Required file Config for entire Controller 
		 
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(value = "/sendBulkMail", method = RequestMethod.GET)
	@ResponseBody public ModelAndView sendBulkMail(HttpServletRequest request,HttpSession session) throws IOException 
	{
		String status = adminService.bulkMailSendStatus();
		
		request.setAttribute("status",status);
		
		return new ModelAndView("admin_bulk_mail_status");
	}
	
	
	
}
