package com.rni.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.ccavenue.security.AesCryptUtil;
import com.rni.bean.FormBean;
import com.rni.bean.UserBean;
import com.rni.bean.UserPostBean;
import com.rni.bean.UserPostCommentBean;
import com.rni.model.EventApplicationModel;
import com.rni.model.EventModel;
import com.rni.model.UserPostFile;
import com.rni.model.UserPostModel;
import com.rni.model.UserProfileModel;
import com.rni.model.UserRegisterModel;
import com.rni.service.AdminService;
import com.rni.service.UserService;
import com.rni.service.UserServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import java.io.OutputStream;
 
 

@Controller
public class UserController {
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private UserService userService;
	
	private static final Logger LOGGER = Logger.getLogger(UserController.class);
	
	private static final long serialVersionUID = -7767828383799037391L;
	
	@RequestMapping(value = "/authUserCredential", method = RequestMethod.GET)
	public String authBuyerCredentialWithGet(HttpSession session) {
		
		if (session.getAttribute("userId") == null)
		{									
        	return "redirect:/userRegister";
        }
		else
		{
			return "redirect:/userDashboard";
		}		
	}
	
	@RequestMapping(value = "/authUserCredential", method = RequestMethod.POST)
	public String authBuyerCredential(@ModelAttribute("userDetails") UserRegisterModel userDetails,HttpSession session,HttpServletRequest request,HttpServletResponse res) {
		
		String userId = request.getParameter("loginEmail");
        String password = request.getParameter("loginpassword");
        String CheckRemember = request.getParameter("CheckRemember");
        
        String authenticateStatus = userService.getValidateUserAuthentication(userId,password);
        
        if(authenticateStatus.equals("Active"))
        {	
        	if(CheckRemember.equals("Yes"))
        	{
        		Cookie ckRemember = new Cookie("CheckRemember", CheckRemember);
        		Cookie ckUser = new Cookie("loginEmail", userId);
            	Cookie ckPassword = new Cookie("loginpassword", password);
            	/* Add the cookie to the client browser. */
            	res.addCookie(ckUser);
            	res.addCookie(ckPassword);
            	res.addCookie(ckRemember);
            	
            	// (60*60*24) Set expiry date after 24 Hrs for both the cookies.
            	ckRemember.setMaxAge(60*60*24);
            	ckUser.setMaxAge(60*60*24);
            	ckPassword.setMaxAge(60*60*24);
        	}
        	else
        	{
        		Cookie ckRemember = new Cookie("CheckRemember", CheckRemember);
        		res.addCookie(ckRemember);
        		ckRemember.setMaxAge(60*60*24);
        	}
        	
        	int user_Id = userService.getUserIdViaEmailId(userId);
        	
        	session.setAttribute("userId", user_Id);
        	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
        	
        	return "redirect:/userDashboard";
        }
        else if(authenticateStatus.equals("Deactive"))
        {
        	request.setAttribute("actionMessage", "Deactive");
        	
        	request.setAttribute("layOutMessage","SignIn");
		      
			return "index_user_registration";	
        }
        else if(authenticateStatus.equals("User Exist"))
        {
        	request.setAttribute("actionMessage", "Failed");
        	
        	request.setAttribute("layOutMessage","SignIn");
		      
			return "index_user_registration";	
        }
        else
        {
        	request.setAttribute("actionMessage", "Not a User");
        	
        	request.setAttribute("layOutMessage","SignIn");
		      
			return "index_user_registration";	
        }	            
	}
	
	@RequestMapping(value = "/activeAccount", method = RequestMethod.GET)
	public String activeAccount(@ModelAttribute("userDetails") UserRegisterModel userDetails,HttpSession session,HttpServletRequest request) {
	
		String activeAccountId = request.getParameter("account");
		
		String emailId = userService.getEmailIdViaUnqid(activeAccountId);
		
		if(emailId.equals("Not Exist"))
		{
			request.setAttribute("actionMessage", "Not Exist");
			
			request.setAttribute("layOutMessage","SignIn");
			
        	return "index_user_registration";    	
		}
		else if(emailId.equals("Over"))
		{			
			request.setAttribute("actionMessage", "Over");
			
			request.setAttribute("layOutMessage","SignIn");
			
			return "index_user_registration";	     	
		}
		else
		{				
        	String accountStatus = userService.getActiveAcount(emailId,activeAccountId);
			
        	if(accountStatus.equals("success"))
        	{
        		request.setAttribute("actionMessage", "Active");
        	}
        	else
        	{
        		request.setAttribute("actionMessage", "Failed");
        	}
        	
        	request.setAttribute("layOutMessage","SignIn");
        	
			return "index_user_registration";	  
		}		
	}
	

	@RequestMapping(value = "/userLogout", method = RequestMethod.GET)
	public String buyerLogout(HttpSession session,HttpServletRequest request) {
					
		session.removeAttribute("userId");	
		session.removeAttribute("selectedFiles");			 
		request.setAttribute("actionMessage", "success");
        
		return "redirect:/userRegister";				
	}
	
	
	// ********************************   START OPERATION OF VIEW FEEDS  *******************************************************  //
	
	
	@RequestMapping(value = "/userDashboard", method = RequestMethod.GET)
	public ModelAndView userDashboard(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("redirect:/userRegister");
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("myAllPost",userService.getAllPost(userId,"All","All"));
			
			/*model.put("feedTree",userService.getFeedMenus());*/
			
			model.put("postInterest",userService.getAllPostInterest());
			
			/*Map<String, List<String>> objMap = new HashMap<String, List<String>>();*/
			
			return new ModelAndView("user_view_post",model);	
		}					
	}
	
	
	@RequestMapping(value = "/showFilterFeedOption", method = RequestMethod.POST)
	@ResponseBody public String showFilterFeedOption(HttpSession session,HttpServletRequest request) {
	
		return userService.getYearListForPost();
	}
	
	
	@RequestMapping(value = "/showMonth", method = RequestMethod.POST)
	@ResponseBody public String showMonth(HttpSession session,HttpServletRequest request) {
	
		return userService.getMontListForPost(request.getParameter("year"));
	}
	
	
	@RequestMapping(value = "/showFeedViaMonth", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showFeedViaMonth(HttpSession session,HttpServletRequest request) {
	
		int userId = (Integer) session.getAttribute("userId");
    	
		Map<String, Object> model = new HashMap<String, Object>();	
		
		UserBean userBean = userService.getUserDetails(userId);
		model.put("userBean",userBean);
		
		model.put("myAllPost",userService.getFilterFeedsViaDuration(userId,request));
		
		model.put("postInterest",userService.getAllPostInterest());
		
		return new ModelAndView("user_view_post_filter",model);
	}
	
	
	@RequestMapping(value = "/showFilterFeed", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showFilterFeed(HttpSession session,HttpServletRequest request) {
	
		int userId = (Integer) session.getAttribute("userId");
    	
		Map<String, Object> model = new HashMap<String, Object>();	
		
		String selectValue = request.getParameter("selectValue");
		
		UserBean userBean = userService.getUserDetails(userId);
		
		model.put("userBean",userBean);
		
		model.put("myAllPost",userService.getFilterFeeds(userId,selectValue));
		
		model.put("postInterest",userService.getAllPostInterest());
		
		return new ModelAndView("user_view_post_filter",model);
	}
	
	
	// ********************************   END OF OPERATION OF VIEW FEEDS  *******************************************************  //
	
	
	
	// ********************************   START OPERATION OF POST FEEDS  *******************************************************  //
	
	
	@RequestMapping(value = "/postFeed", method = RequestMethod.GET)
	public ModelAndView postFeed(@ModelAttribute("postDetail") UserPostBean postDetail,HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("redirect:/userRegister");
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("myAllPost",userService.getAllPost(userId,"My","All"));
			
			model.put("postInterest",userService.getAllPostInterest());
			
			session.removeAttribute("selectedFiles");	
			
			return new ModelAndView("user_post_feed",model);	
		}					
	}
	
	
	@RequestMapping(value = "/captureTempPostFiles", method = RequestMethod.POST)
	@ResponseBody public String captureTempPostFiles(@ModelAttribute("postDetail") UserPostBean postDetail,HttpSession session,MultipartHttpServletRequest request) {
	
		String listFileName = userService.captureTempPostFiles(postDetail,request,session);
		
		return listFileName;
	}
	
	@RequestMapping(value = "/captureTempCommentFiles", method = RequestMethod.POST)
	@ResponseBody public String captureTempCommentFiles(@ModelAttribute("commentBean") UserPostCommentBean commentBean,HttpSession session,MultipartHttpServletRequest request) {
	
		String listFileName = userService.captureTempCommentFiles(commentBean,request,session);
		
		return listFileName;
	}
	
	
	@RequestMapping(value = "/deletePostFile", method = RequestMethod.POST)
	@ResponseBody public String deletePostFile(HttpSession session,HttpServletRequest request) {
	
		
		String listFileName = userService.removeTempPostFiles(request,session);
		
		return listFileName;
	}
	
	@RequestMapping(value = "/saveMyPost", method = RequestMethod.POST)
	@ResponseBody public String saveMyPost(@ModelAttribute("postDetail") UserPostBean postDetail,HttpSession session,MultipartHttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
        	return "redirect:/userRegister";
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
        	
			postDetail.setUser_id(userId);
			String status = userService.saveUserPostDetails(postDetail,request,session);
			
			session.removeAttribute("selectedFiles");	
			
			return status;		
		}							
	}
	
	@RequestMapping(value = "/showUserPost", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showUserPost(@ModelAttribute("postComment") UserPostCommentBean postComment,HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("redirect:/userRegister");
        }
		else
		{
			int postId = Integer.parseInt(request.getParameter("postId"));
			int userId = Integer.parseInt(request.getParameter("userId"));
        	
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("showPost",userService.getAllPost(postId,"Other","Single"));
			model.put("showComment",userService.getAllPostComment(postId));
			
			session.setAttribute("viewStartTime", new Date());
			
			return  new ModelAndView("user_show_post",model);		
		}							
	}
	
	
	@RequestMapping(value = "/calculateView", method = RequestMethod.POST)
	@ResponseBody public String calculateView(HttpSession session,HttpServletRequest request) {
	
		session.setAttribute("viewEndTime", new Date());
		
		long countView = userService.calculateViewPost(session,request);  
		
		return countView+"";
	}
	
	
	@RequestMapping(value = "/viewIndexUserPost", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewIndexUserPost(HttpSession session,HttpServletRequest request) {
		
		int postId = Integer.parseInt(request.getParameter("postId"));
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("showPost",userService.getAllPost(postId,"Other","Single"));
		
		model.put("showComment",userService.getAllPostComment(postId));
		
		return new ModelAndView("user_show_index_post",model);							
	}
	
	
	@RequestMapping(value = "/postComment", method = RequestMethod.POST)
	@ResponseBody public String postComment(@ModelAttribute("postComment") UserPostCommentBean postComment,HttpSession session,MultipartHttpServletRequest request) {
		
		String status = "";
		
		int postId = Integer.parseInt(request.getParameter("postId"));
		int userId = (Integer) session.getAttribute("userId");
		
		status = userService.savePostComment(postComment,postId,userId,request,session);
		
		return  status;								
	}
	
	
	@RequestMapping(value = "/updateCommentCount", method = RequestMethod.POST)
	@ResponseBody public String updateCommentCount(HttpSession session,HttpServletRequest request) {
		
		int postId = Integer.parseInt(request.getParameter("postId"));
		
		long count = userService.getCountPostComment(postId);
		
		return count+"";							
	}
	
	
	@RequestMapping(value = "/showVideo", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showVideo(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{			
			request.setAttribute("fileCategory",request.getParameter("fileCategory"));
			request.setAttribute("fileName",request.getParameter("fileName"));
			request.setAttribute("userId",request.getParameter("userId"));
			request.setAttribute("postId",request.getParameter("postId"));
			
			return new ModelAndView("user_show_video");	
		}					
	}
	
	@RequestMapping(value = "/showPdfFile", method = RequestMethod.GET)
	public void showPdfFile(HttpServletRequest request, HttpSession session,HttpServletResponse response) throws ServletException, IOException 
	{
		String fileName = request.getParameter("fileName");
		String fileCategory = request.getParameter("fileCategory");
		
		
		// Required file Config for entire Controller 
		 
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
		
		String fileDir = "";
		
		if(fileCategory.equals("PostFile"))
		{
			int userId = Integer.parseInt(request.getParameter("userId"));
			int postId = Integer.parseInt(request.getParameter("postId"));
			
			fileDir = fileResource.getString("userPostDirectory");
			
			fileDir = fileDir+"/"+userId+"/"+postId;
		}
		if(fileCategory.equals("CommentFile"))
		{
			int userId = Integer.parseInt(request.getParameter("userId"));
			int postId = Integer.parseInt(request.getParameter("postId"));
			
			fileDir = fileResource.getString("userPostDirectory");
			
			fileDir = fileDir+"/"+userId+"/"+postId+"/CommentFile";			 
		}
		
		if(fileCategory.equals("CaseFile"))
		{
			fileDir = fileResource.getString("caseDirectory");
			
			fileDir = fileDir + "/" + request.getParameter("caseId"); 	 
		}
		
		if(fileCategory.equals("CaseCommentFile"))
		{
			fileDir = fileResource.getString("caseDirectory");
			
			fileDir = fileDir + "/" + request.getParameter("caseId") + "/" + request.getParameter("userId"); 
		}
		
		LOGGER.info("File Location - "+fileDir+"/"+fileName);
		
		File pdfFile = new File(fileDir+"/"+fileName);
		
		response.setContentType("application/pdf");
		response.addHeader("Content-Disposition", "inline; filename=" + fileName);
		response.setContentLength((int) pdfFile.length());
		
		FileInputStream fileInputStream = new FileInputStream(pdfFile);
		OutputStream responseOutputStream = response.getOutputStream();
		int bytes;
		while ((bytes = fileInputStream.read()) != -1) {
			responseOutputStream.write(bytes);
	    }
	}
	
	
	
	@RequestMapping(value = "/showCommentFile", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showCommentFile(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{
			request.setAttribute("fileCategory",request.getParameter("fileCategory"));
			request.setAttribute("fileName",request.getParameter("fileName"));
			request.setAttribute("userId",request.getParameter("userId"));
			request.setAttribute("postId",request.getParameter("postId"));
			request.setAttribute("fileType",request.getParameter("fileType"));
			
			return new ModelAndView("user_show_comment_file");	
		}					
	}
	
	@RequestMapping(value = "/showInterest", method = RequestMethod.POST)
	@ResponseBody public String showInterest(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return "session_failed";
        }
		else
		{
			String status = "";
			
			String interest = request.getParameter("interest");
			int postId = Integer.parseInt(request.getParameter("postId"));
			int userId = (Integer) session.getAttribute("userId");
			
			status = userService.showInterest(interest,postId,userId);
			
			return status;	
		}					
	}
	
	
	@RequestMapping(value = "/countInterest", method = RequestMethod.POST)
	@ResponseBody public String countInterest(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return "session_failed";
        }
		else
		{
			String status = "";
			
			String interest = request.getParameter("interest");
			int postId = Integer.parseInt(request.getParameter("postId"));
			
			status = userService.countInterest(interest,postId)+"";
			
			return status;	
		}					
	}
	
	
	@RequestMapping(value = "/downloadPostFile", method = RequestMethod.GET)
	@ResponseBody public void downloadPostFile(HttpSession session,HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
					
			int userId = Integer.parseInt(request.getParameter("userId"));
			int postId = Integer.parseInt(request.getParameter("postId"));
			
			String reportDirectory = "";
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
			
			reportDirectory = fileResource.getString("userPostDirectory");
			
			reportDirectory = reportDirectory+"/"+userId+"/"+postId;
			
			
			// Set the content type based to zip
			response.setContentType("Content-type: text/zip");
			response.setHeader("Content-Disposition","attachment; filename=postFile.zip");

			// List of files to be downloaded
			
			List<String> fileList = userService.makeZipPostFile(postId,userId);
			
			ServletOutputStream out = response.getOutputStream();
			ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(out));

			for (int i=0;i<fileList.size();i++) {
				
				File srcFile = new File(reportDirectory+"/"+fileList.get(i));
				FileInputStream fis = new FileInputStream(srcFile);
				
				zos.putNextEntry(new ZipEntry(srcFile.getName()));

				
				try {
					fis = new FileInputStream(srcFile);

				} catch (FileNotFoundException fnfe) {
					// If the file does not exists, write an error entry instead of
					// file
					// contents
					zos.write(("ERRORld not find file " + srcFile.getName())
							.getBytes());
					zos.closeEntry();
					System.out.println("Couldfind file "
							+ srcFile.getAbsolutePath());
					continue;
				}

				BufferedInputStream fif = new BufferedInputStream(fis);

				// Write the contents of the file
				int data = 0;
				while ((data = fif.read()) != -1) {
					zos.write(data);
				}
				fif.close();

				zos.closeEntry();
				System.out.println("Finishedng file " + srcFile.getName());
			}

			zos.close();
									
	}
	
	
	/*@RequestMapping(value = "/downloadPostFile", method = RequestMethod.GET)
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// Set the content type based to zip
		response.setContentType("Content-type: text/zip");
		response.setHeader("Content-Disposition",
				"attachment; filename=mytest.zip");

		// List of files to be downloaded
		List files = new ArrayList();
		
		files.add(new File("E:/first.txt"));
		files.add(new File("E:/second.txt"));
		files.add(new File("E:/third.txt"));

		ServletOutputStream out = response.getOutputStream();
		ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(out));

		for (int i=0;i<files.size();i++) {
			
			File file = (File) files.get(i);
			
			System.out.println("Adding " + file.getName());
			zos.putNextEntry(new ZipEntry(file.getName()));

			// Get the file
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(file);

			} catch (FileNotFoundException fnfe) {
				// If the file does not exists, write an error entry instead of
				// file
				// contents
				zos.write(("ERRORld not find file " + file.getName())
						.getBytes());
				zos.closeEntry();
				System.out.println("Couldfind file "
						+ file.getAbsolutePath());
				continue;
			}

			BufferedInputStream fif = new BufferedInputStream(fis);

			// Write the contents of the file
			
			int data = 0;
			while ((data = fif.read()) != -1) {
				zos.write(data);
			}
			
			fif.close();

			zos.closeEntry();
			System.out.println("Finishedng file " + file.getName());
		}

		zos.close();
	}*/
	
	
	
	// *************************************** END OF OPERATION OF POST FEEDS *********************************************** //
	
	
	
	
	// *********************************** START OPERATION OF USER EVENTS  *******************************************************  //
	
	
	@RequestMapping(value = "/userEvent", method = RequestMethod.GET)
	public ModelAndView userEvent(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("redirect:/userRegister");
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("eventList",userService.getEventList());
			
			request.setAttribute("myParticipatedEvent",userService.myParticipatedEventList(userId));
			
			return new ModelAndView("user_event",model);	
		}					
	}
	
	
	@RequestMapping(value = "/viewEventImage", method = RequestMethod.POST)
	@ResponseBody public ModelAndView viewEventImage(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("redirect:/userRegister");
        }
		else
		{
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			String eventCode = request.getParameter("eventCode");
			
			model.put("eventDetails",userService.getEventRecordViaCode(eventCode));
			
			return  new ModelAndView("user_event_image",model);		
		}							
	}
	
	@RequestMapping(value = "/eventOverview", method = RequestMethod.GET)
	public ModelAndView eventOverview(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("redirect:/userRegister");
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			String eventCode = request.getParameter("event");
			
			model.put("eventDetails",userService.getEventRecordViaCode(eventCode));
			
			return new ModelAndView("user_event_overview",model);	
		}					
	}
	
	
	@RequestMapping(value = "/showParticipateForm", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showParticipateForm(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
			int eventId = Integer.parseInt(request.getParameter("eventId"));
			
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("eventDetails",userService.getEventRecordViaId(eventId));
			
			session.setAttribute("applicationEventId", eventId);
			
			return new ModelAndView("user_event_participate_form",model);	
		}					
	}
	
	
	@RequestMapping(value = "/showAppliedForm", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showAppliedForm(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
			int eventId = Integer.parseInt(request.getParameter("eventId"));
			
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("eventDetails",userService.getEventAppliedForm(userId,eventId));
			
			return new ModelAndView("user_event_applied_form",model);	
		}					
	}
	
	@RequestMapping(value = "/saveApplicaitionPassToPayment", method = RequestMethod.POST)
	@ResponseBody public String saveApplicaitionPassToPayment(@ModelAttribute("evntApplication") EventApplicationModel evntApplication,HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
        	return "session_failed";
        }
		else 
		{
			String status = "";
			
			int userId = (Integer) session.getAttribute("userId");
			int eventId = (Integer) session.getAttribute("applicationEventId");
			
			UserRegisterModel userObj = new UserRegisterModel();
			EventModel eventObj = new EventModel();
			
			userObj.setUser_id(userId);
			eventObj.setEvent_id(eventId);
			
			evntApplication.setUser(userObj);
			evntApplication.setEvent(eventObj);
			
			status = userService.saveApplicaitionPassToPayment(evntApplication,session,request);
			
			if(evntApplication.getApplicant_amount()>0)
			{
				return status;
			}
			else
			{
				return "No gateway";
			}					
		}							
	}
	

	@RequestMapping(value = "/paymentOverview", method = RequestMethod.GET)
	public ModelAndView paymentOverview(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
			return new ModelAndView("redirect:/userRegister");
        }
		else 
		{
			int applicationId = (Integer) session.getAttribute("application_id");
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("applicantDetails",userService.getApplicationDetails(applicationId));
			
			return new ModelAndView("user_event_payment_overview",model);
		}		
	}
	
	@RequestMapping(value = "/testCCavenueRequest", method = RequestMethod.POST)
	public String testCCavenueRequest(HttpSession session,HttpServletRequest request) {
		
		return "ccavRequestHandler";
	}
	
	@RequestMapping(value = "/paymentStatus", method = RequestMethod.POST)
	public ModelAndView paymentStatus(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
			return new ModelAndView("redirect:/userRegister");
        }
		else 
		{
			int applicationId = (Integer) session.getAttribute("application_id");
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			int userId = (Integer) session.getAttribute("userId");
	    	UserBean userBean = userService.getUserDetails(userId);
	    	model.put("userBean",userBean);
			
			EventApplicationModel evntApplication = new EventApplicationModel();
			evntApplication.setApplication_id(applicationId);
			
			userService.getUpdateEventApplication(evntApplication,request);
			
			model.put("applicantDetails",userService.getApplicationDetails(applicationId));
			
			return new ModelAndView("user_event_payment_status",model);
		}		
	}	
	
	
	// CCAVENUE KIT
	
	@RequestMapping(value = "/saveApplicaitionPassToPayment", method = RequestMethod.GET)
	@ResponseBody public ModelAndView saveApplicaitionPassToPayment(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("session_failed");
        }
		else 
		{
			int applicationId = (Integer) session.getAttribute("application_id");
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			int userId = (Integer) session.getAttribute("userId");
	    	UserBean userBean = userService.getUserDetails(userId);
	    	model.put("userBean",userBean);
			
			EventApplicationModel evntApplication = new EventApplicationModel();
			evntApplication.setApplication_id(applicationId);
			
			userService.getUpdateEventApplication(evntApplication,request);
			
			model.put("applicantDetails",userService.getApplicationDetails(applicationId));
			
			return new ModelAndView("user_event_payment_status",model);				
		}							
	}
	
	
	@RequestMapping(value = "/testCCavenue", method = RequestMethod.GET)
	public String testCCavenue(HttpSession session,HttpServletRequest request) {
		
		return "ccavdataFrom";
	}
	
	@RequestMapping(value = "/testCCavenueResponse", method = RequestMethod.POST)
	public String testCCavenueResponse(HttpSession session,HttpServletRequest request) {
		
		return "ccavResponseHandler";
	}
	
	 
	// ********************************   END OF OPERATION OF USER EVENTS  *******************************************************  //
	
	
	
	// ********************************   START OPERATION OF USER CASE OF DICUSSION  *******************************************************  //
	
	
	@RequestMapping(value = "/userCaseMonth", method = RequestMethod.GET)
	public ModelAndView userCaseMonth(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("redirect:/userRegister");
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			model.put("caseList",adminService.getCaseRecord("Active"));
			
			return new ModelAndView("user_case_list",model);	
		}					
	}
	
	
	@RequestMapping(value = "/caseDetails", method = RequestMethod.GET)
	public ModelAndView caseDetails(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("redirect:/userRegister");
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			int caseId = Integer.parseInt(request.getParameter("caseId"));
			
			model.put("caseDetails",adminService.getCaseRecord(caseId+""));
			
			model.put("caseComment",adminService.getUserCaseComment(caseId));
			
			return new ModelAndView("user_case_readmore",model);	
		}					
	}
	
	
	@RequestMapping(value = "/getUserCaseComment", method = RequestMethod.POST)
	@ResponseBody public ModelAndView getUserCaseComment(HttpSession session,HttpServletRequest request) {
			
		int caseId = Integer.parseInt(request.getParameter("caseId"));
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("caseComment",adminService.getUserCaseComment(caseId));
		
		return new ModelAndView("user_case_comment",model);				
	}
	
	
	@RequestMapping(value = "/submitCaseComment", method = RequestMethod.POST)
	@ResponseBody public String submitCaseComment(@ModelAttribute("formBean") FormBean formBean,HttpSession session,MultipartHttpServletRequest request) {
	
		String status = userService.submitCaseComment(formBean,request,session);
		
		return status;
	}
	
	
	@RequestMapping(value = "/showCaseFile", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showCaseFile(HttpSession session,HttpServletRequest request) {
			
		int caseId = Integer.parseInt(request.getParameter("caseId"));
		
		request.setAttribute("fileCategory","CaseFile");
		request.setAttribute("fileName",request.getParameter("fileName"));
		request.setAttribute("fileType",request.getParameter("fileType"));
		request.setAttribute("caseId",caseId);
		
		return new ModelAndView("index_show_gallery_file");		
	}
	
	@RequestMapping(value = "/showCaseCommentFile", method = RequestMethod.POST)
	@ResponseBody public ModelAndView showCaseCommentFile(HttpSession session,HttpServletRequest request) {
			
		int caseId = Integer.parseInt(request.getParameter("caseId"));
		int commentUserid = Integer.parseInt(request.getParameter("userId"));
		
		request.setAttribute("fileCategory",request.getParameter("fileCategory"));
		request.setAttribute("fileName",request.getParameter("fileName"));
		request.setAttribute("fileType",request.getParameter("fileType"));
		request.setAttribute("caseId",caseId);
		request.setAttribute("commentUserid", commentUserid);
		
		return new ModelAndView("index_show_gallery_file");		
	}
	
	
	////// ********************************   END OF OPERATION OF USER USER CASE OF DICUSSION  *******************************************************  //
	
	
	
	////// ********************************   START OPERATION OF USER SETTING  *******************************************************  //
	
	
	@RequestMapping(value = "/calculateUserAge", method = RequestMethod.POST)
	@ResponseBody public String calculateUserAge(HttpSession session,HttpServletRequest request) {
					
		String ageString = "";
		int age = 0;
		
		String dob = request.getParameter("dob");
		Date today = new Date();
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		
        try {
        	
            Date birthDate = formatter.parse(dob);
            
            Calendar cal = Calendar.getInstance();
            cal.setTime(birthDate);
            
            int birthdayYear = cal.get(Calendar.YEAR);
            
            cal.setTime(today);
            
            int todayYear = cal.get(Calendar.YEAR);
            
            age = todayYear - birthdayYear;
            
            ageString = age+"";

        } catch (ParseException e) {
            e.printStackTrace();
        }
		 
		return ageString;
	}
	
	@RequestMapping(value = "/userSetting", method = RequestMethod.GET)
	public ModelAndView userSetting(HttpSession session,HttpServletRequest request) {
					
		if (session.getAttribute("userId") == null)
		{									
        	return new ModelAndView("redirect:/userRegister");
        }
		else
		{
			int userId = (Integer) session.getAttribute("userId");
        	
        	UserBean userBean = userService.getUserDetails(userId);
			
			Map<String, Object> model = new HashMap<String, Object>();	
			
			model.put("userBean",userBean);
			
			return new ModelAndView("user_setting",model);	
		}					
	}
	
	@RequestMapping(value = "/checkUserEmailIdOnUpdate", method = RequestMethod.POST)
	@ResponseBody public String checkUserEmailId(@ModelAttribute("userRegDetails") UserRegisterModel userRegDetails,HttpSession session,HttpServletRequest request) {
			
		String emailId = userRegDetails.getEmail_id();
		
		int userId = (Integer) session.getAttribute("userId");
		
		String status = userService.checkUserExistOnUpdate(userId,emailId,"emailId");
		
		return status;			
	}
	
	@RequestMapping(value = "/checkUserPhnoOnUpdate", method = RequestMethod.POST)
	@ResponseBody public String checkUserPhno(@ModelAttribute("userRegDetails") UserRegisterModel userRegDetails,HttpSession session,HttpServletRequest request) {
			
		String mobile = userRegDetails.getMobile_no();
		
		int userId = (Integer) session.getAttribute("userId");
		
		String status = userService.checkUserExistOnUpdate(userId,mobile,"mobile");
		
		return status;						
	}
	
	@RequestMapping(value = "/updateUserRegister", method = RequestMethod.POST)
	@ResponseBody public String updateUserRegister(@ModelAttribute("userRegDetails") UserRegisterModel userRegDetails,HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
        	return "expired";
        }
		else
		{
			String status = "";
			
			int userId = (Integer) session.getAttribute("userId");
			
			userRegDetails.setUser_id(userId);
			
			status = userService.updateUserRegister(userRegDetails);
			
			return  status;		
		}							
	}
	
	
	@RequestMapping(value = "/changeUserPassword", method = RequestMethod.POST)
	@ResponseBody public String changeUserPassword(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
        	return "expired";
        }
		else
		{
			String status = "";
			
			int userId = (Integer) session.getAttribute("userId");
			
			status = userService.changeUserPassword(userId,request);
			
			return  status;		
		}							
	}
	
	
	@RequestMapping(value = "/saveUserProfileData", method = RequestMethod.POST)
	@ResponseBody public String saveUserProfileData(@ModelAttribute("userProfile") UserProfileModel userProfile,HttpSession session,MultipartHttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
        	return "expired";
        }
		else
		{
			String status = "";
			
			int userId = (Integer) session.getAttribute("userId");
			UserRegisterModel userRegDetails = new UserRegisterModel();
			userRegDetails.setUser_id(userId);
			
			userProfile.setUser(userRegDetails);
			
			status = userService.saveUserProfileData(userProfile,request);
					
			return  status;		
		}							
	}
	
	
	@RequestMapping(value = "/updateUserProfileData", method = RequestMethod.POST)
	@ResponseBody public String updateUserProfileData(@ModelAttribute("userProfile") UserProfileModel userProfile,HttpSession session,MultipartHttpServletRequest request) {
		
		if (session.getAttribute("userId") == null)
		{									
        	return "expired";
        }
		else
		{
			String status = "";
			
			int userId = (Integer) session.getAttribute("userId");
			UserRegisterModel userRegDetails = new UserRegisterModel();
			userRegDetails.setUser_id(userId);
			
			userProfile.setUser(userRegDetails);
			userProfile.setProfile_id(Integer.parseInt(request.getParameter("profileId")));
			
			status = userService.updateUserProfileData(userProfile,request);
					
			return  status;		
		}							
	}
	
	// ********************************   END OF  OPERATION OF USER SETTING  *******************************************************  //
	
	
	
	
	
	// **************************************  COMMON CONTROLLER FOR THE PORTAL   *************************************************** ///
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewUserMedia", method = RequestMethod.GET)
	@ResponseBody public byte[] previewUserMedia(HttpServletRequest request,HttpSession session) throws IOException {
		
		String fileCategory = request.getParameter("fileCategory");
		String fileName = request.getParameter("fileName");
		
		int userId = Integer.parseInt(request.getParameter("userId"));
		int postId = Integer.parseInt(request.getParameter("postId"));
		
		// Required file Config for entire Controller 
		 
		String fileDir = "";
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
		
		if(fileCategory.equals("PostFile"))
		{
			fileDir = fileResource.getString("userPostDirectory");
			
			fileDir = fileDir+"/"+userId+"/"+postId;			 
		}
		else if(fileCategory.equals("CommentFile"))
		{
			fileDir = fileResource.getString("userPostDirectory");
			
			fileDir = fileDir+"/"+userId+"/"+postId+"/CommentFile";			 
		}
		else
		{
			
		}
		
		// End of Required file Config for entire Controller 
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewEventMedia", method = RequestMethod.GET)
	@ResponseBody public byte[] previewEventMedia(HttpServletRequest request,HttpSession session) throws IOException {
		
		
		String fileName = request.getParameter("fileName");
		
		int eventId = Integer.parseInt(request.getParameter("eventId"));
		
		// Required file Config for entire Controller 
		 
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
		
		String fileDir = fileResource.getString("eventDirectory");
		
		fileDir = fileDir+"/"+eventId;
		 
		// End of Required file Config for entire Controller 
		 
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewProfilePhoto", method = RequestMethod.GET)
	@ResponseBody public byte[] previewProfilePhoto(HttpServletRequest request,HttpSession session) throws IOException {
		
		
		String fileName = request.getParameter("fileName");
		
		int userId = Integer.parseInt(request.getParameter("userId"));
		
		// Required file Config for entire Controller 
		 
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
		
		String fileDir = fileResource.getString("userProfileDirectory");
		
		fileDir = fileDir+"/"+userId;
		 
		// End of Required file Config for entire Controller 
		 
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	
	@RequestMapping(value = "/downloadFile", method = RequestMethod.GET)
	public ModelAndView downloadFile(HttpServletRequest request,HttpSession session) {
		
		String fileName = request.getParameter("fileName");
		
		int userId = Integer.parseInt(request.getParameter("userId"));
		int postId = Integer.parseInt(request.getParameter("postId"));
		
		String reportDirectory = "";
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
		
		reportDirectory = fileResource.getString("userPostDirectory");
		
		reportDirectory = reportDirectory+"/"+userId+"/"+postId;
		
		request.setAttribute("rootDirectory",reportDirectory);
		request.setAttribute("fileName",fileName);
		
		return new ModelAndView("commonDownload");
	}
	
	
	
	@RequestMapping(value = "/showPdf", method = RequestMethod.GET)
	public void showPdf(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String fileName = request.getParameter("fileName");
		String fileCategory = request.getParameter("fileCategory");
		
		int userId = Integer.parseInt(request.getParameter("userId"));
		int postId = Integer.parseInt(request.getParameter("postId"));
		
		// Required file Config for entire Controller 
		 
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
		
		String fileDir = "";
		
		if(fileCategory.equals("PostFile"))
		{
			fileDir = fileResource.getString("userPostDirectory");
			
			fileDir = fileDir+"/"+userId+"/"+postId;
		}
		if(fileCategory.equals("CommentFile"))
		{
			fileDir = fileResource.getString("userPostDirectory");
			
			fileDir = fileDir+"/"+userId+"/"+postId+"/CommentFile";			 
		}
		
		LOGGER.info("File Location - "+fileDir+"/"+fileName);
		
		File file = new File(fileDir+"/"+fileName);
		response.setHeader("Content-Type", "pdf");
		response.setHeader("Content-Length", String.valueOf(file.length()));
		response.setHeader("Content-Disposition", "inline; filename="+fileName+"");
		
		Files.copy(file.toPath(), response.getOutputStream());
		
	}
	
	/*@RequestMapping(value = "/renderPdf", method = RequestMethod.GET)
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		performTask(request, response);
	}

	@RequestMapping(value = "/renderPdf", method = RequestMethod.POST)
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		performTask(request, response);
	}
	
	private void performTask(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	IOException {

		String pdfFileName = "Empower-July-2017.pdf";
		String contextPath = "C:/Users/server/Downloads/";
		File pdfFile = new File(contextPath + pdfFileName);
		
		response.setContentType("application/pdf");
		response.addHeader("Content-Disposition", "inline; filename=" + pdfFileName);
		response.setContentLength((int) pdfFile.length());
		
		FileInputStream fileInputStream = new FileInputStream(pdfFile);
		OutputStream responseOutputStream = response.getOutputStream();
		int bytes;
		while ((bytes = fileInputStream.read()) != -1) {
			responseOutputStream.write(bytes);
	}
	
   }*/
	
		
}
