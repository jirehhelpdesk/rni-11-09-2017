package com.rni.bean;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class RedNetGalleryBean {

	private String gallery_text;
	private List<MultipartFile> files;
	
	public String getGallery_text() {
		return gallery_text;
	}
	public void setGallery_text(String gallery_text) {
		this.gallery_text = gallery_text;
	}
	public List<MultipartFile> getFiles() {
		return files;
	}
	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}
	
	
}
