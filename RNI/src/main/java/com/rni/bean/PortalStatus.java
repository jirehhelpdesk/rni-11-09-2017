package com.rni.bean;

public class PortalStatus {

	private long noOfUser;
	private long noOfEvent;
	private long noOfFaculty;
	private long noOfComMember;
	
	private long noOfPost;

	public long getNoOfUser() {
		return noOfUser;
	}

	public void setNoOfUser(long noOfUser) {
		this.noOfUser = noOfUser;
	}

	public long getNoOfEvent() {
		return noOfEvent;
	}

	public void setNoOfEvent(long noOfEvent) {
		this.noOfEvent = noOfEvent;
	}

	public long getNoOfFaculty() {
		return noOfFaculty;
	}

	public void setNoOfFaculty(long noOfFaculty) {
		this.noOfFaculty = noOfFaculty;
	}

	public long getNoOfComMember() {
		return noOfComMember;
	}

	public void setNoOfComMember(long noOfComMember) {
		this.noOfComMember = noOfComMember;
	}

	public long getNoOfPost() {
		return noOfPost;
	}

	public void setNoOfPost(long noOfPost) {
		this.noOfPost = noOfPost;
	}
	
	
}
