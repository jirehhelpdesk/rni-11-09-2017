package com.rni.bean;

import java.util.Date;

public class UserBean {

	
    private int user_id;
	private String first_name;
	private String last_name;
	private String email_id;
	private String mobile_no;
	private String password;
	private Date cr_date;
    private String status;
	private String profession;
	private String feeds_update;
	private String subscription_type;
	
	// PROFILE DETAILS 
	
	private UserProfileBean profileBean;

	
	
	
	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCr_date() {
		return cr_date;
	}
	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getProfession() {
		return profession;
	}
	public void setProfession(String profession) {
		this.profession = profession;
	}
	public String getFeeds_update() {
		return feeds_update;
	}
	public void setFeeds_update(String feeds_update) {
		this.feeds_update = feeds_update;
	}
	public String getSubscription_type() {
		return subscription_type;
	}
	public void setSubscription_type(String subscription_type) {
		this.subscription_type = subscription_type;
	}
	
	
	// PROFILE DETAILS GETTER SETTER 
	
	
	public UserProfileBean getProfileBean() {
		return profileBean;
	}
	public void setProfileBean(UserProfileBean profileBean) {
		this.profileBean = profileBean;
	}
	
	
}
