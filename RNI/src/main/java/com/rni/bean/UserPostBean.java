package com.rni.bean;

import java.io.Serializable;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class UserPostBean implements Serializable {

	private static final long serialVersionUID = 74458L;
	 
	private String post_title;
    private String post_desc;
    private String post_publish_type;
    private String download_privilage;
    private int user_id;
    
    private List<MultipartFile> files;

    
    
    
	public String getPost_title() {
		return post_title;
	}

	public void setPost_title(String post_title) {
		this.post_title = post_title;
	}

	public String getPost_desc() {
		return post_desc;
	}

	public void setPost_desc(String post_desc) {
		this.post_desc = post_desc;
	}

	public String getPost_publish_type() {
		return post_publish_type;
	}

	public void setPost_publish_type(String post_publish_type) {
		this.post_publish_type = post_publish_type;
	}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getDownload_privilage() {
		return download_privilage;
	}

	public void setDownload_privilage(String download_privilage) {
		this.download_privilage = download_privilage;
	}
    
	
}
