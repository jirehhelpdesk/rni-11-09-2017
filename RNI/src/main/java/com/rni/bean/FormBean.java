package com.rni.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class FormBean implements Serializable {

	private static final long serialVersionUID = 74458L;
	 
	private List<MultipartFile> files;
	
    private String contentString1;
    private String contentString2;
    private String contentString3;
    private String contentString4;
    private String contentString5;
    
    private Date contentDate1;

    
    
	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	public String getContentString1() {
		return contentString1;
	}

	public void setContentString1(String contentString1) {
		this.contentString1 = contentString1;
	}

	public String getContentString2() {
		return contentString2;
	}

	public void setContentString2(String contentString2) {
		this.contentString2 = contentString2;
	}

	public String getContentString3() {
		return contentString3;
	}

	public void setContentString3(String contentString3) {
		this.contentString3 = contentString3;
	}

	public String getContentString4() {
		return contentString4;
	}

	public void setContentString4(String contentString4) {
		this.contentString4 = contentString4;
	}

	public String getContentString5() {
		return contentString5;
	}

	public void setContentString5(String contentString5) {
		this.contentString5 = contentString5;
	}

	public Date getContentDate1() {
		return contentDate1;
	}

	public void setContentDate1(Date contentDate1) {
		this.contentDate1 = contentDate1;
	}
    
}
