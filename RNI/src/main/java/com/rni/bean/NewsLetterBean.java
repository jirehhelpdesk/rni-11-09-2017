package com.rni.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class NewsLetterBean implements Serializable {

	private static final long serialVersionUID = 74458L;
	
	private long user_count;
	private Date news_sent_date;
	private String sent_user_type;
	private String sent_type;
	private String news_matter;

	private List<MultipartFile> files;

	
	public long getUser_count() {
		return user_count;
	}

	public void setUser_count(long user_count) {
		this.user_count = user_count;
	}

	public Date getNews_sent_date() {
		return news_sent_date;
	}

	public void setNews_sent_date(Date news_sent_date) {
		this.news_sent_date = news_sent_date;
	}

	public String getSent_user_type() {
		return sent_user_type;
	}

	public void setSent_user_type(String sent_user_type) {
		this.sent_user_type = sent_user_type;
	}

	public String getSent_type() {
		return sent_type;
	}

	public void setSent_type(String sent_type) {
		this.sent_type = sent_type;
	}

	public String getNews_matter() {
		return news_matter;
	}

	public void setNews_matter(String news_matter) {
		this.news_matter = news_matter;
	}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}
	
	
	
}
