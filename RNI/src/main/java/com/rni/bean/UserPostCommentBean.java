package com.rni.bean;

import java.io.Serializable;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class UserPostCommentBean implements Serializable {

	private static final long serialVersionUID = 74458L;
	 
	private String post_comment;
    private List<MultipartFile> commentFiles;
	
    
    public String getPost_comment() {
		return post_comment;
	}
	public void setPost_comment(String post_comment) {
		this.post_comment = post_comment;
	}
	public List<MultipartFile> getCommentFiles() {
		return commentFiles;
	}
	public void setCommentFiles(List<MultipartFile> commentFiles) {
		this.commentFiles = commentFiles;
	}

}
