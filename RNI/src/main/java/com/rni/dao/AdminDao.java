package com.rni.dao;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.rni.model.AbstractSubmissionModel;
import com.rni.model.AdminDetails;
import com.rni.model.BulkMailEmailModel;
import com.rni.model.CaseFileModel;
import com.rni.model.CaseModel;
import com.rni.model.CaseUserCommentModel;
import com.rni.model.CommitteeMemberModel;
import com.rni.model.EmailModel;
import com.rni.model.EventApplicationModel;
import com.rni.model.EventImageModel;
import com.rni.model.EventModel;
import com.rni.model.FacultyModel;
import com.rni.model.IndexBannerModel;
import com.rni.model.LinkValidationModel;
import com.rni.model.NewsLetterAttachedFile;
import com.rni.model.NewsLetterModel;
import com.rni.model.RedNetGalleryRecord;
import com.rni.model.RedNetGalleryRecordFile;
import com.rni.model.TempFileUploadModel;
import com.rni.model.UserPostModel;
import com.rni.model.UserRegisterModel;
import com.rni.model.PortalContentModel;


public interface AdminDao {

	
	public long getPortalNoOfUser();
	public long getPortalNoOfEvent();
	public long getPortalNoOfFaculty();
	public long getPortalNoOfComMember();
	public long getPortalNoOfPost();
	
	
	
	public String getValidateAdmin(String user_id,String password);
	
	public String setAdminAccess(String access);
	
	public String getSaveEmailDetails(EmailModel emailDetail);
	
	public String saveLinkDetails(LinkValidationModel linkModel);
	
	public String saveEventFile(TempFileUploadModel tempFile);
	
	public List<TempFileUploadModel> getCurrentUploadedFiles(HttpSession session);
	
	public String deleteTempFile(HttpServletRequest request,HttpSession session);
	
	public String saveEventDetails(EventModel eventDetail);
	
	public String saveEventImgeDetails(EventImageModel eventImgDetail);
	
	public int getEventIdViaCode(String eventCode);
	
	public int getLastEventId();
	
	public String deleteAllTempFile(HttpSession session);
	
	public List<AdminDetails> getAdminDetails();
	
	public List<UserPostModel> getSearchPost(HttpServletRequest request);
	
	public String getBlockUserPost(int postId,String postStatus);
	
	public List<UserRegisterModel> getSearchUser(HttpServletRequest request);
	
	public String saveFacultyDetails(FacultyModel faculty);
	
	public List<FacultyModel> getFacultyList();
	
	public List<CommitteeMemberModel> getMemberList();
	
	public String saveMemberDetails(CommitteeMemberModel member);
	
	public List<CommitteeMemberModel> getMemberListInOrder();
	
	public List<FacultyModel> getFacultyListInOrder();
	
	public List<FacultyModel> getFacultyData(int facultyId);
	
	public List<CommitteeMemberModel> getMemberData(int memberId);
	
	public String deleteMemberData(int memberId);
	
	public String deleteFacultyData(int facultyId);
	
	public String updateAdminDetails(AdminDetails admin);
	
	public String updateAdminPassword(HttpServletRequest request,HttpSession session);
	
	public String updateUserStatus(String userStatus,int userId);
	
	public List<EventModel> getLastFiveEventList();
	
	public List<EventModel> getSearchEvent(HttpServletRequest request);
	
	public List<EventModel> detailsOnEvent(int eventId);
	
	public List<EventApplicationModel> eventReportData(int eventId);
	
	public long getUserCount(String value);
	
	public String saveNewsLetterRecord(NewsLetterModel nwsModel);
	
	public String saveNewsLetterFile(NewsLetterAttachedFile newsFile);
	
	public int getNewsId(NewsLetterModel nwsModel);
	
	public List<String> getEmailIdList(NewsLetterModel nwsModel);
	
	public List<RedNetGalleryRecord> getGalleryRecord();
	
	public String saveGalleryRecord(RedNetGalleryRecord galModel);
	
	public int getGalleryId(RedNetGalleryRecord galModel);
	
	public String saveGalleryFile(RedNetGalleryRecordFile galFileModel);
	
	public List<RedNetGalleryRecordFile> getGalleryFileRecord(String fileType,int galleryId);
	
	public List<RedNetGalleryRecord> getGalleryRecord(int galleryId);
	
	public String deleteGalleryFileRecord(int galleryId);
	
	public String deleteGalleryInfoRecord(int galleryId);
	
	public List<NewsLetterModel> getNewsData();
	
	
	public String manageCaseOfDiscussion(int postId,String action);
	
	public List<IndexBannerModel> getBannerFileList();
	
	public String saveTemporaryBrowseFiles(TempFileUploadModel tempFile);
	
	public String saveBannerFileRecord(IndexBannerModel bannerModel);
	
	public List<IndexBannerModel> getAllBannerFileList();
	
	public String saveCaseDetail(CaseModel caseModel);
	
	public int getCaseId(CaseModel caseModel);
	
	public String getSaveCaseFileModel(CaseFileModel caseFileModel);
	
	public List<CaseModel> getCaseRecord(String condition);
	
	public List<CaseUserCommentModel> getUserCaseComment(int caseId);
	
	public String changeCaseStatus(HttpServletRequest request);
	
	public String deleteCaseDetails(HttpServletRequest request);
	
	public List<IndexBannerModel> getBannerRecord(int bannerId);
	
	public String changeBannerStatus(HttpServletRequest request);
	
	public String deleteBanner(HttpServletRequest request);
	
	public String savePortalContent(PortalContentModel prtlContent);
	
	public List<PortalContentModel> getContentList();
	
	public List<PortalContentModel> getContentDetails(String condition,String contentFor,String contentType);
	
	public String updatePortalContent(PortalContentModel prtlContent);
	
	public String  deleteContent(int contentId);
	
	public String saveAbstractSubmission(AbstractSubmissionModel absDoc);
	
	public List<EventApplicationModel> getEventRegRecord();
	
	public List<AbstractSubmissionModel> getAbstractDocRecord();
	
	public List<EventApplicationModel> getPreConRegRecord();
	
	
	public List<BulkMailEmailModel> getBulkMailEmailId();
	
	
	
}
