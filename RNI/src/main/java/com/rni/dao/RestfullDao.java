package com.rni.dao;

import java.util.List;

import com.rni.model.UserPostModel;

public interface RestfullDao {

	
	public List<UserPostModel> getAllPost();
	
	public UserPostModel getPostInformation(int post_id);
	
	
	
}
