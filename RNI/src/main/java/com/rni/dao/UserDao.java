package com.rni.dao;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.rni.model.CaseCommentFileModel;
import com.rni.model.CaseUserCommentModel;
import com.rni.model.EventApplicationModel;
import com.rni.model.EventModel;
import com.rni.model.LinkValidationModel;
import com.rni.model.SubscriptionRequestModel;
import com.rni.model.TempFileUploadModel;
import com.rni.model.UserPostCommentFileModel;
import com.rni.model.UserPostCommentModel;
import com.rni.model.UserPostFile;
import com.rni.model.UserPostInterestModel;
import com.rni.model.UserPostModel;
import com.rni.model.UserProfileModel;
import com.rni.model.UserRegisterModel;


public interface UserDao {

	
	public String checkUserExist(String value,String condition);
	
	public String saveUserRegister(UserRegisterModel userRegDetails);
	
	public String getValidateUserAuthentication(String userId,String password);
	
	public int getUserIdViaEmailId(String userId);
	
	public List<UserRegisterModel> getUserDetails(int userId);
	
	public String getEmailIdViaUnqid(String activeAccountId);
	
	public String getActiveAcount(String emailId,String activeAccountId);
	
	public String saveUserPostDetails(UserPostModel postDetail);
	
	public int getPostIdViaPostDetails(UserPostModel postDetail);
	
	public List<UserPostModel> getAllPost(int userId,String forWhom,String postType);
	
	public List<UserPostModel> getFilterFeeds(int userId,String selectValue);
	
	public String savePostComment(UserPostCommentModel comment);
	
	public List<UserPostModel> getPostsUnderCondition(String postType);
	
	public List<EventModel> getEventList();
	
	public List<EventModel> getEventRecordViaCode(String eventCode);
	
	public String changeUserPassword(int userId,String curPassword,String newPassword);
	
	public List<Date> getFeedMenus();
	
	public String checkUserExistOnUpdate(int userId,String checkValue,String checkType);
	
	public List<EventModel> getEventRecordViaId(int eventId);
	
	public String updateUserRegister(UserRegisterModel userRegDetails);
	
	public String saveApplicaitionPassToPayment(EventApplicationModel evntApplication);
	
	public int getUserIdViaPostId(int postId);
	
	public String updateEventStatus(EventModel eventModel);
	
	public List<EventApplicationModel> myParticipatedEventList(int userId);
	
	public List<EventApplicationModel> getEventAppliedForm(int userId,int eventId);
	
	public String savePostFileDetails(UserPostFile fileModel);
	
	public String saveCommentFileDetails(UserPostCommentFileModel cmtFileModel);
	
	public int getLastCommentId(int userId);
	
	public String saveUserProfileData(UserProfileModel userProfile);
	
	public String getUserProfilePhoto(UserProfileModel userProfile);
	
	public List<UserPostCommentModel> getAllPostComment(int postId);
	
	
	public List<UserPostInterestModel> checkInterest(UserPostInterestModel interestModel);
	
	public String saveInterest(UserPostInterestModel interestModel);
	
	public String updateInterest(UserPostInterestModel interestModel);
	
	public String removeInterest(UserPostInterestModel interestModel);
	
	public long countInterest(String interest,int postId);
	
	public String increaseCountViewPost(int postId);
	
	public long getCountViewPost(int postId);
	
	public String updatePostInterest(String interest,int postId,long totalCount);
	
	public List<UserPostInterestModel> getAllPostInterest();
	
	public String saveTemporaryFileDetails(TempFileUploadModel tempFile);
	
	public String checkEmailIdForNewsLetter(String emailId);
	
	public String saveSubscriptionRequest(SubscriptionRequestModel subObj);
	
	public long getCountPostComment(int postId);
	
	public List<UserRegisterModel> userSearchList(String searchValue);
	
	public List<UserPostModel> getPostFileName(int postId);
	
	public List<Long> getYearListForPost();
	
	public List<Integer> getMontListForPost(String year);
	
	public List<UserPostModel> getFilterFeedsViaDuration(int userId,HttpServletRequest request);
	
	public String submitCaseComment(CaseUserCommentModel caseComment);
	
	public int getCaseCmntId(CaseUserCommentModel caseComment);
	
	public String saveCasComtFile(CaseCommentFileModel casComtFile);
	
	public String getEmailIdFromForgetPassword(String uniqueId);
	
	public String getChangePassword(String emailId,String encPassword);
	
	public String saveLinkValidatedetails(LinkValidationModel linkValidate);
	
	public int getApplicationId(EventApplicationModel evntApplication);
	
	public List<EventApplicationModel> getApplicationDetails(int applicationId);
	
	public String getUpdateEventApplication(EventApplicationModel evntApplication);
	
	public List getEventDetails(int applicationId);
	
	public int getApplicationIdBeforeReg(EventApplicationModel evntApplication);
	
	public EventApplicationModel getApplicantInfo(int application_id);
	
	
	
	
	
	
	
	
	
	
}
