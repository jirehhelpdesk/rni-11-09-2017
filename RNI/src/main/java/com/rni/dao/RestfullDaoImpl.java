package com.rni.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.rni.model.UserPostModel;
import com.rni.model.UserRegisterModel;

@SuppressWarnings("unused")
@Repository("restDao")
public class RestfullDaoImpl implements RestfullDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings("unchecked")
	public List<UserPostModel> getAllPost()
	{
		/*
		
		List<UserPostModel> postList = new ArrayList<UserPostModel>(); 
		
		Session session = sessionFactory.openSession();
		
		Criteria c = session.createCriteria(UserPostModel.class, "post");
		
		//c.createAlias("user.profile", "profile");
		
		postList = c.list();
		
		return postList;
		
		*/
		
		Session session = sessionFactory.openSession();
		
		List<UserPostModel> postList = session.createCriteria(UserPostModel.class).list();
		
		session.close();
		return postList;
	}
	
	
	public UserPostModel getPostInformation(int post_id)
	{
		Session session = sessionFactory.openSession();
		
		UserPostModel postDetail = (UserPostModel) session.load(UserPostModel.class,post_id);
		
		session.beginTransaction();
		
		return postDetail;
	}
	
}
