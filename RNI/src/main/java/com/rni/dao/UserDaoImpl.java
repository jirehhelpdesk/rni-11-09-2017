package com.rni.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.ModelAndView;

import com.rni.model.CaseCommentFileModel;
import com.rni.model.CaseUserCommentModel;
import com.rni.model.EventApplicationModel;
import com.rni.model.EventModel;
import com.rni.model.LinkValidationModel;
import com.rni.model.SubscriptionRequestModel;
import com.rni.model.TempFileUploadModel;
import com.rni.model.UserPostCommentFileModel;
import com.rni.model.UserPostCommentModel;
import com.rni.model.UserPostFile;
import com.rni.model.UserPostInterestModel;
import com.rni.model.UserPostModel;
import com.rni.model.UserProfileModel;
import com.rni.model.UserRegisterModel;


@SuppressWarnings("unused")
@Repository("userDao")
public class UserDaoImpl implements UserDao  {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	private static final Logger LOGGER = Logger.getLogger(UserDaoImpl.class);
	
	@SuppressWarnings("unchecked")
	public String checkUserExist(String value,String condition)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		 
		if(condition.equals("emailId"))
		{
			String hql = "select email_id from UserRegisterModel where email_id = '"+value+"'";
			List<String> lsdata = session.createQuery(hql).list();	
			
			tx.commit();
			session.close();

			if(lsdata.size()>0)
			{
				return "Exist";
			}
			else
			{
				return "Not Exist";
			}
		}
		else
		{
			String hql = "select mobile_no from UserRegisterModel where mobile_no = '"+value+"'";
			List<String> lsdata = session.createQuery(hql).list();	
			
			tx.commit();
			session.close();

			if(lsdata.size()>0)
			{
				return "Exist";
			}
			else
			{
				return "Not Exist";
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public String checkUserExistOnUpdate(int userId,String checkValue,String checkType)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		 
		if(checkType.equals("emailId"))
		{ 
			Criteria criteria = session.createCriteria(UserRegisterModel.class);
			criteria.add(Restrictions.eq("email_id", checkValue));
			criteria.add(Restrictions.ne("user_id", userId));
			List<UserRegisterModel> result = criteria.list();
			
			session.close();

			if(result.size()>0)
			{
				return "Exist";
			}
			else
			{
				return "Not Exist";
			}		
		}
		else
		{
			Criteria criteria = session.createCriteria(UserRegisterModel.class);
			criteria.add(Restrictions.eq("mobile_no", checkValue));
			criteria.add(Restrictions.ne("user_id", userId));
			List<UserRegisterModel> result = criteria.list();
			
			session.close();

			if(result.size()>0)
			{
				return "Exist";
			}
			else
			{
				return "Not Exist";
			}	
		}
	}
	
	public String saveUserRegister(UserRegisterModel userRegDetails)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(userRegDetails);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
			}
			else 
			{
				transactionStatus = "failure";
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of user registration exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getValidateUserAuthentication(String userId,String password)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select status from UserRegisterModel where email_id = '"+userId+"' and password='"+password+"'";
		List<String> status = session.createQuery(hql).list();	
		
		if(status.size()>0)
		{
			session.close();
			String loginStatus = status.get(0);
			
			if(loginStatus.equals("Active"))
			{
				return status.get(0);
			}
			else if(loginStatus.equals("Deactive"))
			{
				return status.get(0);
			}
			else if(loginStatus.equals("Blocked"))
			{
				return "Blocked";
			}
			else
			{
				return "Failed";
			}
		}
		else
		{
			String checkEmailHql = "select status from UserRegisterModel where email_id = '"+userId+"'";
			List<String> checkEmail = session.createQuery(checkEmailHql).list();	
			
			if(checkEmail.size()>0)
			{
				return "User Exist";
			}
			else
			{
				return "No User";
			}			
		}
	}
	
	@SuppressWarnings("unchecked")
	public int getUserIdViaEmailId(String userId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select user_id from UserRegisterModel where email_id = '"+userId+"'";
		List<Integer> user_Id = session.createQuery(hql).list();	
		
		if(user_Id.size()>0)
		{
			session.close();
			
			return user_Id.get(0);
		}
		else
		{
			return 0;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserRegisterModel> getUserDetails(int userId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from UserRegisterModel where user_id = "+userId+"";
		List<UserRegisterModel> userData = session.createQuery(hql).list();	
		
		session.close();
		
		return userData;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getEmailIdViaUnqid(String activeAccountId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from LinkValidationModel where validate_id = '"+activeAccountId+"' and validate_status='Pending' and validate_for='Account Activation'";
		List<LinkValidationModel> userData = session.createQuery(hql).list();	
		
		session.close();
		
		if(userData.size()>0)
		{
			if(userData.get(0).getValidate_status().equals("Pending"))
			{
				return userData.get(0).getValidate_email_id();
			}
			else
			{
				return "Over";
			}
		}
		else
		{
			return "Not Exist";
		}
	}
	
	public String getActiveAcount(String emailId,String activeAccountId)
	{
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery("update UserRegisterModel set status = :status where email_id = :emailId");
		updateHql.setParameter("emailId", emailId);
		
		updateHql.setParameter("status","Active");
		
	    int result  = updateHql.executeUpdate();
	    
	    if(result==1)
		{			
	    	org.hibernate.Query updateHqlLink = updateSession.createQuery("update LinkValidationModel set validate_status = :status where validate_id = :activeAccountId");
	    	updateHqlLink.setParameter("activeAccountId", activeAccountId);
			
	    	updateHqlLink.setParameter("status","Over");
			
			int linkResult  = updateHqlLink.executeUpdate();
			
			if(result==1)
			{
				tx.commit();
		 	    updateSession.close();
		 	    
				return "success";	
			}
			else
			{
				return "failed";
			}	    	    
		}
		else
		{			
			return "failed";
		}
	}
	
	
	public String saveUserPostDetails(UserPostModel postDetail)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(postDetail);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
			}
			else 
			{
				transactionStatus = "failure";
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of user postDetail exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public int getPostIdViaPostDetails(UserPostModel postDetail)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select post_id from UserPostModel where user_id = "+postDetail.getUser().getUser_id()+" and post_cr_date=(select MAX(post_cr_date) from UserPostModel where user_id = "+postDetail.getUser().getUser_id()+")";
		List<Integer> userData = session.createQuery(hql).list();	
		
		session.close();
		
		if(userData.size()>0)
		{
			return userData.get(0);			
		}
		else
		{
			return 0;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<UserPostModel> getAllPost(int id,String forWhom,String postType)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		if(forWhom.equals("My"))
		{
			hql = "from UserPostModel where user_id = "+id+" and post_status='Active' ORDER BY post_cr_date DESC";			
		}
		else if(forWhom.equals("Other"))
		{
			hql = "from UserPostModel where post_id = "+id+"";			
		}
		else if(forWhom.equals("All"))
		{
			if(postType.equals("Image"))
			{
				hql = "from UserPostModel where post_file_type = '"+postType+"' and post_status='Active' ORDER BY post_cr_date DESC";			
			}
			else if(postType.equals("Video"))
			{
				hql = "from UserPostModel where post_file_type = '"+postType+"' and post_status='Active' ORDER BY post_cr_date DESC";			
			}
			else
			{
				// In this condition all the post will retrive with all users	
				hql = "from UserPostModel where post_status='Active' ORDER BY post_cr_date DESC";	
			}			
		}
		else
		{
			
		}
		
		List<UserPostModel> postData = session.createQuery(hql).list();	
		
	    session.close();
	    
		return postData;					
	}
	
	@SuppressWarnings("unchecked")
	public List<UserPostModel> getFilterFeeds(int userId,String selectValue)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		List<UserPostModel> postData = new ArrayList<UserPostModel>();
		
		if(selectValue.equals("Most viewed feeds"))
		{
			hql = "from UserPostModel ORDER BY post_view DESC";	
			
			postData = session.createQuery(hql).list();	
			session.close();
		}
		else if(selectValue.equals("Most liked feeds"))
		{
			/*Criteria c = session.createCriteria(UserPostModel.class, "post");
			c.createAlias("post.postInterest", "postInterest");
			c.add(Restrictions.eq("postInterest.interest", "Like"));
			c.addOrder(Order.desc("postInterest.post_cr_date"));
			
			LOGGER.info("Join Query="+c.toString());
			
			postData = c.list();
			
			session.close();*/
		}
		else if(selectValue.equals("Feed of the discussion"))
		{
			hql = "from UserPostModel where feed_of_discussion='Yes' ORDER BY post_view DESC";	
			
			postData = session.createQuery(hql).list();	
			session.close();
		}
		else
		{
			
		}
		
		return postData;
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public int getUserIdViaPostId(int postId)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		hql = "from UserPostModel where post_id='"+postId+"'";	
		
		List<UserPostModel> postData = session.createQuery(hql).list();	
		
	    session.close();
	    
	    if(postData.size()>0)
	    {
	    	return postData.get(0).getUser().getUser_id();
	    }
	    else
	    {
	    	return 0;
	    }	
	}
	
	@SuppressWarnings("unchecked")
	public List<UserPostModel> getPostsUnderCondition(String postType)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		List<UserPostModel> postData = new ArrayList<UserPostModel>();
		
		if(postType.equals("Image"))
		{
			Criteria c = session.createCriteria(UserPostModel.class, "post");
			c.createAlias("post.postFile", "postFile"); // inner join by default	
			
			c.add(Restrictions.eq("post.post_publish_type", "all"));
			c.add(Restrictions.eq("post.post_status", "Active"));
			c.addOrder(Order.desc("post.post_cr_date"));
			
			c.add(Restrictions.eq("postFile.post_file_type", postType));
			
			postData = c.list();
			
			//hql = "from UserPostModel where post_file_type = '"+postType+"' and post_publish_type='all' and post_status='Active' ORDER BY post_cr_date DESC";			
		}
		else if(postType.equals("Video"))
		{
			Criteria c = session.createCriteria(UserPostModel.class, "post");
			c.createAlias("post.postFile", "postFile"); // inner join by default	
			
			c.add(Restrictions.eq("post.post_publish_type", "all"));
			c.add(Restrictions.eq("post.post_status", "Active"));
			c.addOrder(Order.desc("post.post_cr_date"));
			
			c.add(Restrictions.eq("postFile.post_file_type", postType));
			
			postData = c.list();
			
			//hql = "from UserPostModel where post_file_type = '"+postType+"' and post_publish_type='all' and post_status='Active' ORDER BY post_cr_date DESC";				
		}
		else
		{
			// In this condition all the post will retrive with all users	
			hql = "from UserPostModel where post_status='Active' ORDER BY post_cr_date DESC";	
			postData = session.createQuery(hql).list();	
		}	
		
	    session.close();
	    
		return postData;					
	}
	
	
	public String savePostComment(UserPostCommentModel comment)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(comment);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
			}
			else 
			{
				transactionStatus = "failure";
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of user comment exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EventModel> getEventList()
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		hql = "from EventModel ORDER BY event_cr_date DESC";			
		
		List<EventModel> eventData = session.createQuery(hql).list();	
		
	    session.close();
	    
		return eventData;	
	}
	
	@SuppressWarnings("unchecked")
	public List<EventModel> getEventRecordViaCode(String eventCode)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		hql = "from EventModel where event_code='"+eventCode+"'";			
		
		List<EventModel> eventData = session.createQuery(hql).list();	
		
	    session.close();
	    
		return eventData;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventModel> getEventRecordViaId(int eventId)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		hql = "from EventModel where event_id="+eventId+"";			
		
		List<EventModel> eventData = session.createQuery(hql).list();	
		
	    session.close();
	    
		return eventData;
	}
	
	@SuppressWarnings("unchecked")
	public String changeUserPassword(int userId,String curPassword,String newPassword)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		 
		String hql = "from UserRegisterModel where user_id = "+userId+" and password = '"+curPassword+"'";
		List<String> lsdata = session.createQuery(hql).list();	
		
		if(lsdata.size()>0)
		{
			org.hibernate.Query updateHql = session.createQuery("update UserRegisterModel set password = :newPassword where user_id = :userId");
			updateHql.setParameter("userId", userId);
			
			updateHql.setParameter("newPassword",newPassword);
			
		    int result  = updateHql.executeUpdate();
		    
		    tx.commit();
			session.close();
			
		    if(result==1)
			{
		    	return "success";
			}
		    else
		    {
		    	return "failed";
		    }
		}
		else
		{
			tx.commit();
			session.close();
			
			return "incorrect";
		}
		
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Date> getFeedMenus()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select post_cr_date from UserPostModel ORDER BY post_cr_date DESC";
		List<Date> dateData = session.createQuery(hql).list();	
		
		session.close();
		
		return dateData;
	}
	
	
	public String updateUserRegister(UserRegisterModel userRegDetails)
	{
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery(
				"update UserRegisterModel set first_name = :firstName, "
				+ "last_name = :lastName, email_id = :emailId, "
				+ "mobile_no = :mobile, profession = :profession  "
				+ "where user_id = :userId");
		
		updateHql.setParameter("firstName", userRegDetails.getFirst_name());
		updateHql.setParameter("lastName", userRegDetails.getLast_name());
		updateHql.setParameter("emailId", userRegDetails.getEmail_id());
		updateHql.setParameter("mobile", userRegDetails.getMobile_no());
		updateHql.setParameter("profession", userRegDetails.getProfession());
		
		updateHql.setParameter("userId", userRegDetails.getUser_id());
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
 	    updateSession.close();
 	    
	    if(result==1)
		{			
			return "success";	
		}
		else
		{
			return "failed";
		}	
	}
	
	
	public String saveApplicaitionPassToPayment(EventApplicationModel evntApplication)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(evntApplication);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
			}
			else 
			{
				transactionStatus = "failure";
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of user comment exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	@SuppressWarnings("unchecked")
	public int getApplicationId(EventApplicationModel evntApplication)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select application_id from EventApplicationModel where user_id = "+evntApplication.getUser().getUser_id()+" and event_id = "+evntApplication.getEvent().getEvent_id()+" and applied_date=(select MAX(applied_date) from EventApplicationModel)";
		List<Integer> data = session.createQuery(hql).list();	
		
		session.close();
		
		if(data.size()>0)
		{
			return data.get(0);			
		}
		else
		{
			return 0;
		}
	}
	
	@SuppressWarnings("unchecked")
	public int getApplicationIdBeforeReg(EventApplicationModel evntApplication)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select application_id from EventApplicationModel where applicant_email = '"+evntApplication.getApplicant_email()+"' and applied_date=(select MAX(applied_date) from EventApplicationModel)";
		List<Integer> data = session.createQuery(hql).list();	
		
		session.close();
		
		if(data.size()>0)
		{
			return data.get(0);			
		}
		else
		{
			return 0;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EventApplicationModel> getApplicationDetails(int applicationId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from EventApplicationModel where application_id="+applicationId+"";
		List<EventApplicationModel> applicationData = session.createQuery(hql).list();	
		
		session.close();
		
		return applicationData;
	}
	
	public String getUpdateEventApplication(EventApplicationModel evntApplication)
	{
		int result  = 0;
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery("update EventApplicationModel set applicant_status = :status ,applicant_transaction_id = :transId, applicant_transaction_reason =:reason, applicant_transaction_error_code = :errorcode where application_id = :applicationId");
		
		updateHql.setParameter("status", evntApplication.getApplicant_status());
		updateHql.setParameter("transId", evntApplication.getApplicant_transaction_id());
		updateHql.setParameter("reason", evntApplication.getApplicant_transaction_reason());
		updateHql.setParameter("errorcode", evntApplication.getApplicant_transaction_error_code());
		
		updateHql.setParameter("applicationId", evntApplication.getApplication_id());
		
		LOGGER.info("Update Query=="+updateHql.toString());
		
		try
		{
			result  = updateHql.executeUpdate();
		}
	    catch(Exception e)
		{
	    	LOGGER.info("While update transaction status error got="+e.getMessage());
		}
	    
	    tx.commit();
 	    updateSession.close();
 	    
	    if(result==1)
		{			
			return "success";	
		}
		else
		{
			return "failed";
		}
	}
	
	public String updateEventStatus(EventModel eventModel)
	{		
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery("update EventModel set event_status = :status where event_id = :eventId");
		
		updateHql.setParameter("status", eventModel.getEvent_status());
		
		updateHql.setParameter("eventId", eventModel.getEvent_id());
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
 	    updateSession.close();
 	    
	    if(result==1)
		{			
			return "success";	
		}
		else
		{
			return "failed";
		}		    
	}
	
	@SuppressWarnings("unchecked")
	public List<EventApplicationModel> myParticipatedEventList(int userId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from EventApplicationModel where user_id="+userId+"";
		List<EventApplicationModel> applicationData = session.createQuery(hql).list();	
		
		session.close();
		
		return applicationData;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventApplicationModel> getEventAppliedForm(int userId,int eventId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from EventApplicationModel where user_id="+userId+" and event_id="+eventId+"";
		List<EventApplicationModel> applicationData = session.createQuery(hql).list();	
		
		session.close();
		
		return applicationData;
	}
	
	public String savePostFileDetails(UserPostFile fileModel)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(fileModel);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
			}
			else 
			{
				transactionStatus = "failure";
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of post file records exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	public String saveCommentFileDetails(UserPostCommentFileModel cmtFileModel)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(cmtFileModel);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
			}
			else 
			{
				transactionStatus = "failure";
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of post file records exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	@SuppressWarnings("unchecked")
	public int getLastCommentId(int userId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select comment_id from UserPostCommentModel where user_id = "+userId+" and comment_cr_date=(select MAX(comment_cr_date) from UserPostCommentModel where user_id = "+userId+")";
		List<Integer> userData = session.createQuery(hql).list();	
		
		session.close();
		
		if(userData.size()>0)
		{
			return userData.get(0);			
		}
		else
		{
			return 0;
		}
	}
	
	public String saveUserProfileData(UserProfileModel userProfile)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(userProfile);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
				LOGGER.info("User profile data saved successfully.");
			}
			else 
			{
				transactionStatus = "failure";
				LOGGER.error("While saving User profile data failure happens.");
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of user profile details exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	@SuppressWarnings("unchecked")
	public String getUserProfilePhoto(UserProfileModel userProfile)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select profile_photo from UserProfileModel where profile_id = "+userProfile.getProfile_id()+"";
		List<String> userData = session.createQuery(hql).list();	
		
		session.close();
		
		if(userData.size()>0)
		{
			return userData.get(0);			
		}
		else
		{
			return "";
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<UserPostCommentModel> getAllPostComment(int postId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from UserPostCommentModel where post_id = "+postId+" ORDER BY comment_cr_date DESC";
		List<UserPostCommentModel> commentData = session.createQuery(hql).list();	
		
		session.close();
		
		return commentData;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserPostInterestModel> checkInterest(UserPostInterestModel interestModel)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from UserPostInterestModel where post_id = "+interestModel.getPost().getPost_id()+" and user_id = "+interestModel.getUser().getUser_id()+"";
		List<UserPostInterestModel> interestData = session.createQuery(hql).list();	
		
		session.close();
		
		return interestData;
	}
	
	public String saveInterest(UserPostInterestModel interestModel)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(interestModel);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
				LOGGER.info("User profile data saved successfully.");
			}
			else 
			{
				transactionStatus = "failure";
				LOGGER.error("While saving User profile data failure happens.");
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of user profile details exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	
	public String updateInterest(UserPostInterestModel interestModel)
	{
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery("update UserPostInterestModel set interest = :interest,interest_date = :date  where post_id = :postId and user_id = :userId");
		
		updateHql.setParameter("interest", interestModel.getInterest());
		updateHql.setParameter("date", new Date());
		
		updateHql.setParameter("postId", interestModel.getPost().getPost_id());
		updateHql.setParameter("userId", interestModel.getUser().getUser_id());
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
 	    updateSession.close();
 	    
	    if(result==1)
		{			
			return "success";	
		}
		else
		{
			return "failed";
		}	
	}
	
	public String removeInterest(UserPostInterestModel interestModel)
	{
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery("Delete from UserPostInterestModel where post_id = :postId and user_id = :userId");
		
		updateHql.setParameter("postId", interestModel.getPost().getPost_id());
		updateHql.setParameter("userId", interestModel.getUser().getUser_id());
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
 	    updateSession.close();
 	    
	    if(result==1)
		{			
			return "success";	
		}
		else
		{
			return "failed";
		}	
	}
	
	public long countInterest(String interest,int postId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from UserPostInterestModel where interest='"+interest+"' and post_id="+postId+"").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	@SuppressWarnings("unchecked")
	public String increaseCountViewPost(int postId)
	{
		Session session = sessionFactory.openSession();
		String status = "";
		
		String hql = "Select post_view from UserPostModel where post_id = "+postId+"";
		List<Long> viewData = session.createQuery(hql).list();	
		
		LOGGER.info("Last Total view="+viewData.get(0));
		
		long newTotalView = viewData.get(0) + 1;
		
		
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("update UserPostModel set post_view = :postView where post_id = :postId");
		
		updateHql.setParameter("postView", newTotalView);
		updateHql.setParameter("postId", postId);
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
 	    
	    if(result==1)
		{			    	
	    	status = "success";	
		}
		else
		{
			status = "failed";
		}
	    
	    
		session.close();
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public long getCountViewPost(int postId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "Select post_view from UserPostModel where post_id = "+postId+"";
		List<Long> viewData = session.createQuery(hql).list();	
		
		LOGGER.info("Last Total view="+viewData.get(0));
		
		session.close();
		
		return viewData.get(0);
		
	}
	
	
	public String updatePostInterest(String interest,int postId,long totalCount)
	{
		Session session = sessionFactory.openSession();
		String status = "";
		
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("update UserPostModel set post_like = :totalCount where post_id = :postId");
		
		updateHql.setParameter("totalCount", totalCount);
		updateHql.setParameter("postId", postId);
		
		LOGGER.info("Update Post Like Query="+updateHql.toString());
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
 	    
	    if(result==1)
		{			    	
	    	status = "success";	
		}
		else
		{
			status = "failed";
		}
	    
		session.close();
		
		return status;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<UserPostInterestModel> getAllPostInterest()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from UserPostInterestModel";
		List<UserPostInterestModel> viewData = session.createQuery(hql).list();	
		
		session.close();
		
		return viewData;
	}
	
	public String saveTemporaryFileDetails(TempFileUploadModel tempFile)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(tempFile);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
				LOGGER.info("Temporary file saved successfully.");
			}
			else 
			{
				transactionStatus = "failure";
				LOGGER.error("While saving Temporary file data failure happens.");
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving Temporary file details exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public String checkEmailIdForNewsLetter(String emailId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from SubscriptionRequestModel where email_id='"+emailId+"'";
		
		List<SubscriptionRequestModel> data = session.createQuery(hql).list();	
		
		session.close();
		
		if(data.size()>0)
		{
			return "Exist";
		}
		else
		{
			return "No Exist";
		}
		
	}
	
	
	public String saveSubscriptionRequest(SubscriptionRequestModel subObj)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(subObj);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
				LOGGER.info("Visitor email saved successfully for newsletter.");
			}
			else 
			{
				transactionStatus = "failure";
				LOGGER.error("While saving Visitor email failed to save for newsletter.");
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of Visitor email exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	public long getCountPostComment(int postId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from UserPostCommentModel where post_id="+postId+"").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserRegisterModel> userSearchList(String searchValue)
	{
		List<UserRegisterModel> userModel = new ArrayList<UserRegisterModel>(); 
		Session session = sessionFactory.openSession();
		
		userModel = session.createCriteria(UserRegisterModel.class)
	       .add(Restrictions.disjunction()
	       .add(Restrictions.like("first_name", "%"+searchValue+"%"))
	       .add(Restrictions.like("last_name", "%"+searchValue+"%")) 
	       .add(Restrictions.like("mobile_no", "%"+searchValue+"%")) 
	       .add(Restrictions.like("email_id", "%"+searchValue+"%")))
	       .list();
		
		if(userModel.size()<=0)
		{   		
			Criteria c = session.createCriteria(UserRegisterModel.class, "user");
			c.createAlias("user.profile", "profile");
			c.add(Restrictions.disjunction()
			  .add(Restrictions.like("profile.area_of_interest", "%"+searchValue+"%"))		
			  .add(Restrictions.like("profile.pincode", "%"+searchValue+"%"))			
			  .add(Restrictions.like("profile.practice_as", "%"+searchValue+"%"))		
			  .add(Restrictions.like("profile.practice_at", "%"+searchValue+"%")));
			
			LOGGER.info("Join Query="+c.toString());
			
			userModel = c.list();
			
			//c.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		}
		 
		session.close();
		
		return userModel;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserPostModel> getPostFileName(int postId)
	{
		List<UserPostModel> fileList = new ArrayList<UserPostModel>();
		Session session = sessionFactory.openSession();
		
		String hql = "from UserPostModel where post_id = "+postId+"";
		
		fileList = session.createQuery(hql).list();	
		
		session.close();
		return fileList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Long> getYearListForPost()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select distinct year(post.post_cr_date) from UserPostModel post";
		List<Long> yearList = session.createQuery(hql).list();	
		
		session.close();
		return yearList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Integer> getMontListForPost(String year)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select distinct month(post.post_cr_date) from UserPostModel post where year(post.post_cr_date) ="+year+"";
		List<Integer> yearList = session.createQuery(hql).list();	
		
		session.close();
		return yearList;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserPostModel> getFilterFeedsViaDuration(int userId,HttpServletRequest request)
	{
		List<UserPostModel> postList = new ArrayList<UserPostModel>();
		Session session = sessionFactory.openSession();
		
		if(request.getParameter("filterFeed").equals("Find feeds with duration"))
		{
			String hql = "from UserPostModel post where year(post.post_cr_date) ="+request.getParameter("year")+" and month(post.post_cr_date) = "+request.getAttribute("monthSequence")+" ORDER BY post_cr_date DESC";
			postList = session.createQuery(hql).list();				
		}
		else if(request.getParameter("filterFeed").equals("Most viewed feeds"))
		{
			
		}
		else
		{
			
		}
		
		session.close();
		return postList;
	}
	
	public String submitCaseComment(CaseUserCommentModel caseComment)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(caseComment);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of CaseUserCommentModel details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;

	}
	
	@SuppressWarnings("unchecked")
	public int getCaseCmntId(CaseUserCommentModel caseComment)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		hql = "select case_comment_id from CaseUserCommentModel where user_id = "+caseComment.getUser().getUser_id()+" and case_comment_cr_date = (select MAX(case_comment_cr_date) from CaseUserCommentModel)";			
		
		List<Integer> caseData = session.createQuery(hql).list();	
		
	    session.close();
	    
	    if(caseData.size()>0)
	    {
	    	return caseData.get(0);
	    }
	    else
	    {
	    	return 0;
	    }
	}
	
	public String saveCasComtFile(CaseCommentFileModel casComtFile)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(casComtFile);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of CaseCommentFileModel details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	
	public String saveLinkValidatedetails(LinkValidationModel linkValidate)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(linkValidate);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;	
	}
	
	
	@SuppressWarnings("unchecked")
	public String getEmailIdFromForgetPassword(String uniqueId)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		String hql = "select validate_email_id from LinkValidationModel where validate_id = '"+uniqueId+"' and validate_init_date=(select MAX(validate_init_date) from LinkValidationModel where validate_id = '"+uniqueId+"' and validate_status='Pending')";
		List<String> lsdata = session.createQuery(hql).list();	
		
		tx.commit();
		session.close();

		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "Not Exist";
		}	
	}
	
	
	@SuppressWarnings("unchecked")
	public String getChangePassword(String emailId,String encPassword)
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query hql = session.createQuery("update UserRegisterModel set password = :encPassword where email_id = :emailId");
		hql.setParameter("emailId", emailId);
		
		hql.setParameter("encPassword",encPassword);
		
	    int result  = hql.executeUpdate();
	    
		if(result==1)
		{			
			String uniqueId = "select validate_id from LinkValidationModel where validate_email_id = '"+emailId+"' and validate_init_date=(select MAX(validate_init_date) from LinkValidationModel where validate_email_id = '"+emailId+"' and validate_status='Pending')";
			List<String> lsdata = session.createQuery(uniqueId).list();	
			
			if(lsdata.size()>0)
			{
				org.hibernate.Query updateHql = session.createQuery("update LinkValidationModel set validate_status = :setStatus where validate_id = :uniqueId");
				updateHql.setParameter("uniqueId", lsdata.get(0));			
				updateHql.setParameter("setStatus", "Over");
				
			    int update  = updateHql.executeUpdate();
				
			    tx.commit();
			    session.close();

			    if(update==1)
				{
			    	return "success";
				}
			    else
			    {
			    	return "failed";
			    }	
			}
			else
			{
				return "failed";
			}			    
		}
		else
		{			
			return "failed";
		}		
	}
	
	
	@SuppressWarnings("unchecked")
	public List getEventDetails(int applicationId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "select applicant_email,applicant_mobile,applicant_address,applicant_city,applicant_state,applicant_pincode from EventApplicationModel where application_id="+applicationId+"";
 
		List<EventApplicationModel> l = session.createQuery(hql).list();
		
		System.out.println("Selected value list=="+l.toString());
		
		return l;
	}
	
	
	@SuppressWarnings("unchecked")
	public EventApplicationModel getApplicantInfo(int application_id)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "from EventApplicationModel where application_id="+application_id+"";
 
		List<EventApplicationModel> info = session.createQuery(hql).list();
		
		if(info.size()>0)
		{
			return info.get(0);
		}
		else
		{
			return null;
		}
	}
	
}
