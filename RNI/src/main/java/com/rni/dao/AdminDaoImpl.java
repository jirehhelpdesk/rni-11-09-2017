package com.rni.dao;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.ModelAndView;

import com.rni.model.AbstractSubmissionModel;
import com.rni.model.AdminDetails;
import com.rni.model.BulkMailEmailModel;
import com.rni.model.CaseFileModel;
import com.rni.model.CaseModel;
import com.rni.model.CaseUserCommentModel;
import com.rni.model.CommitteeMemberModel;
import com.rni.model.EmailModel;
import com.rni.model.EventApplicationModel;
import com.rni.model.EventImageModel;
import com.rni.model.EventModel;
import com.rni.model.FacultyModel;
import com.rni.model.IndexBannerModel;
import com.rni.model.LinkValidationModel;
import com.rni.model.NewsLetterAttachedFile;
import com.rni.model.NewsLetterModel;
import com.rni.model.PortalContentModel;
import com.rni.model.RedNetGalleryRecord;
import com.rni.model.RedNetGalleryRecordFile;
import com.rni.model.TempFileUploadModel;
import com.rni.model.UserPostModel;
import com.rni.model.UserRegisterModel;
import com.rni.util.PasswordGenerator;


@SuppressWarnings("unused")
@Repository("adminDao")
public class AdminDaoImpl implements AdminDao  {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	private static final Logger LOGGER = Logger.getLogger(AdminDaoImpl.class);
	
	
	public long getPortalNoOfUser()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from UserRegisterModel").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long getPortalNoOfEvent()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from EventModel").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long getPortalNoOfFaculty()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from FacultyModel").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long getPortalNoOfComMember()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from CommitteeMemberModel").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	public long getPortalNoOfPost()
	{
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		long count = 0;
		
		count = ((Long)session.createQuery("select count(*) from UserPostModel").uniqueResult()).longValue();
		
		tx.commit();
		session.close();
		 
		return count;
	}
	
	
	
	public String setAdminAccess(String access)
	{
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery("update AdminDetails set admin_access = :adminAccess where admin_id = :adminId");
		updateHql.setParameter("adminId", 1);
		
		updateHql.setParameter("adminAccess",access);
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
	    updateSession.close();
	    
	    if(result==1)
		{			
			return "success";	    
		}
		else
		{			
			return "failed";
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public String getValidateAdmin(String user_id,String password)
	{	
		Session session = sessionFactory.openSession();
		
		String hql = "from AdminDetails where admin_id=1";
		List<AdminDetails> lsdata = session.createQuery(hql).list();
		
		session.close();
		
		if(lsdata.get(0).getAdmin_access().equals("online"))
		{
			return "online";
		}
		else
		{
			if(lsdata.get(0).getAdmin_user_id().equals(user_id) && lsdata.get(0).getAdmin_password().equals(password))
			{				
				Session updateSession = sessionFactory.openSession();
				Transaction tx = updateSession.beginTransaction();

				org.hibernate.Query updateHql = updateSession.createQuery("update AdminDetails set admin_access = :adminAccess , current_access_time = :currentAccessTime , last_access_time = :lastAccessTime where admin_id = :adminId");
				updateHql.setParameter("adminId", 1);
				
				updateHql.setParameter("adminAccess","online");
				updateHql.setParameter("currentAccessTime",new Date());
				updateHql.setParameter("lastAccessTime",lsdata.get(0).getCurrent_access_time());
				
			    int result  = updateHql.executeUpdate();
			    
			    tx.commit();
			    updateSession.close();
			    
				return "correct";
			}
			else
			{
				return "incorrect";
			}
		}		
	}
	
	
	public String getSaveEmailDetails(EmailModel emailDetail)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(emailDetail);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
			}
			else 
			{
				transactionStatus = "failure";
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of Email exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}



	
	public String saveLinkDetails(LinkValidationModel linkModel) {
		
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(linkModel);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
			}
			else 
			{
				transactionStatus = "failure";
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of link exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
		
	}
	
	public String saveEventFile(TempFileUploadModel tempFile)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(tempFile);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
			}
			else 
			{
				transactionStatus = "failure";
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of event temp file exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<TempFileUploadModel> getCurrentUploadedFiles(HttpSession sessionObj)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		hql = "from TempFileUploadModel where session_id = '"+sessionObj.getId()+"'";			
		
		List<TempFileUploadModel> tempData = session.createQuery(hql).list();	
		
	    session.close();
	    
		return tempData;		
	}
	
	public String deleteTempFile(HttpServletRequest request,HttpSession sessionObj)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("delete from TempFileUploadModel where session_id = :sessionId and file_name = :fileName");
		updateHql.setParameter("sessionId", sessionObj.getId());
		updateHql.setParameter("fileName", request.getParameter("fileName"));
		
	    int result  = updateHql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{
			status = "success";
		}
		else
		{
			LOGGER.error("During delete of temp file details problem arise.");
			
			status = "failed";
		}
		
		return status;
	}
	
	public String saveEventDetails(EventModel eventDetail)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(eventDetail);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
			}
			else 
			{
				transactionStatus = "failure";
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of event details exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventModel> getLastFiveEventList()
	{
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from EventModel ORDER BY event_cr_date DESC");
		query.setFirstResult(0);
		query.setMaxResults(5); 
		List<EventModel> result = query.list();
		
		session.close();
		
		return result;
	}
	
	
	public String saveEventImgeDetails(EventImageModel eventImgDetail)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try 
		{
			transaction = session.beginTransaction();
			session.saveOrUpdate(eventImgDetail);
			transaction.commit();
			
			if (transaction.wasCommitted()) 
			{
				transactionStatus = "success";
			}
			else 
			{
				transactionStatus = "failure";
			}
		} 
		catch (Exception he) 
		{
			LOGGER.error("During saving the data of event file exception arise.");
			
			transactionStatus = "failure";
		} 
		finally
		{
			session.close();
		}

		return transactionStatus;
	}
	
	@SuppressWarnings("unchecked")
	public int getLastEventId()
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		hql = "from EventModel where event_cr_date = (select MAX(event_cr_date) from EventModel)";			
		
		List<EventModel> eventData = session.createQuery(hql).list();	
		
	    session.close();
	    
	    if(eventData.size()>0)
	    {
	    	return eventData.get(0).getEvent_id();
	    }
	    else
	    {
	    	return 0;
	    }
	}
	
	@SuppressWarnings("unchecked")
	public int getEventIdViaCode(String eventCode)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		hql = "from EventModel where event_code = '"+eventCode+"'";			
		
		List<EventModel> eventData = session.createQuery(hql).list();	
		
	    session.close();
	    
	    if(eventData.size()>0)
	    {
	    	return eventData.get(0).getEvent_id();
	    }
	    else
	    {
	    	return 0;
	    }
		
	}
	
	public String deleteAllTempFile(HttpSession sessionObj)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("delete from TempFileUploadModel where session_id = :sessionId");
		updateHql.setParameter("sessionId", sessionObj.getId());
		
	    int result  = updateHql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{
			status = "success";
		}
		else
		{
			LOGGER.error("During delete of temp file details problem arise.");
			
			status = "failed";
		}
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public List<AdminDetails> getAdminDetails()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "";
		
		hql = "from AdminDetails where admin_id=1";			
		
		List<AdminDetails> admin = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return admin;	    
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserPostModel> getSearchPost(HttpServletRequest request)
	{
		List<UserPostModel> postList = new ArrayList<UserPostModel>();
		String searchType = request.getParameter("searchType");
		
		Session session = sessionFactory.openSession();
		
		if(searchType.equals("Title"))
		{		
			String hql = "from UserPostModel where post_title LIKE '%"+request.getParameter("post_title")+"%' ORDER BY post_cr_date DESC";		
			postList = session.createQuery(hql).list();					
		}
		else
		{
			DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(request.getParameter("start_date"));
		        endingDate = (Date) formatter.parse(request.getParameter("end_date"));
		        
		    } catch (ParseException e) {
		    	
		    	LOGGER.error("Exception Raise :: While converting String date to date.");
		        e.printStackTrace();
		    }	
		    
		    Criteria criteria = session.createCriteria(UserPostModel.class,"post");	
		    
		    criteria.add(Restrictions.ge("post.post_cr_date",startingDate));
			criteria.add(Restrictions.le("post.post_cr_date",endingDate));		
			criteria.addOrder(Order.desc("post.post_cr_date"));
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			
			LOGGER.info("Search Query="+criteria.toString());
			
			postList = criteria.list();				
		}
		
		session.close();
		
		return postList;
	}
	
	public String getBlockUserPost(int postId,String postStatus)
	{
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();
		int result = 0;
		
		try{
		org.hibernate.Query updateHql = updateSession.createQuery("update UserPostModel set post_status = :status where post_id = :postId");
		updateHql.setParameter("postId", postId);
		
		updateHql.setParameter("status",postStatus);
		
	    result  = updateHql.executeUpdate();
	    
	    tx.commit();
	    updateSession.close();
	    
		}
		catch(Exception e)
		{
			LOGGER.error("While updating poststatus some error arise.");
		}
		
	    if(result>0)
	    {
	    	LOGGER.info("Post status got successfully updated.");
	    	return "success";
	    }
	    else
	    {
	    	LOGGER.info("While updating poststatus some error arise.");
	    	return "error";
	    }	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserRegisterModel> getSearchUser(HttpServletRequest request)
	{
		List<UserRegisterModel> userList = new ArrayList<UserRegisterModel>();
		String searchType = request.getParameter("searchType");
		
		Session session = sessionFactory.openSession();
		
		if(searchType.equals("Name"))
		{		
			String hql = "from UserRegisterModel where first_name LIKE '%"+request.getParameter("searchValue")+"%' ORDER BY cr_date DESC";		
			userList = session.createQuery(hql).list();			
			
		}
		else if(searchType.equals("Email"))
		{		
			String hql = "from UserRegisterModel where email_id = '"+request.getParameter("searchValue")+"'";		
			userList = session.createQuery(hql).list();					
		}
		else
		{
			DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(request.getParameter("start_date"));
		        endingDate = (Date) formatter.parse(request.getParameter("end_date"));
		        
		    } catch (ParseException e) {
		    	
		    	LOGGER.error("Exception Raise :: While converting String date to date.");
		        e.printStackTrace();
		    }	
		    
		    Criteria criteria = session.createCriteria(UserRegisterModel.class,"user");	
		    
		    criteria.add(Restrictions.ge("user.cr_date",startingDate));
			criteria.add(Restrictions.le("user.cr_date",endingDate));		
			criteria.addOrder(Order.desc("user.cr_date"));
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			
			LOGGER.info("Search Query="+criteria.toString());
			
			userList = criteria.list();				
		}
		
		session.close();
		
		return userList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventModel> getSearchEvent(HttpServletRequest request)
	{
		List<EventModel> userList = new ArrayList<EventModel>();
		String searchType = request.getParameter("searchType");
		
		Session session = sessionFactory.openSession();
		
		if(searchType.equals("Title"))
		{		
			String hql = "from EventModel where event_title LIKE '%"+request.getParameter("searchValue")+"%' ORDER BY event_cr_date DESC";		
			userList = session.createQuery(hql).list();			
			
		}
		else
		{
			DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(request.getParameter("start_date"));
		        endingDate = (Date) formatter.parse(request.getParameter("end_date"));
		        
		    } catch (ParseException e) {
		    	
		    	LOGGER.error("Exception Raise :: While converting String date to date.");
		        e.printStackTrace();
		    }	
		    
		    Criteria criteria = session.createCriteria(EventModel.class,"event");	
		    
		    criteria.add(Restrictions.ge("event.event_start_date",startingDate));
			criteria.add(Restrictions.le("event.event_end_date",endingDate));		
			criteria.addOrder(Order.desc("event.event_cr_date"));
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
			
			//LOGGER.info("Search Query="+criteria.toString());
			
			userList = criteria.list();				
		}
		
		session.close();
		
		return userList;
	}
	
	public String saveFacultyDetails(FacultyModel faculty)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(faculty);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of faculty details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<FacultyModel> getFacultyList()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "";
		
		hql = "from FacultyModel ORDER BY faculty_cr_date";			
		
		List<FacultyModel> faculty = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return faculty;
	}
	
	public String saveMemberDetails(CommitteeMemberModel member)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(member);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of member details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CommitteeMemberModel> getMemberList()
	{
		Session session = sessionFactory.openSession();

		String hql = "";

		hql = "from CommitteeMemberModel ORDER BY member_cr_date";

		List<CommitteeMemberModel> member = session.createQuery(hql).list();

		session.close();

		return member;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CommitteeMemberModel> getMemberListInOrder()
	{
		Session session = sessionFactory.openSession();

		String hql = "";

		hql = "from CommitteeMemberModel ORDER BY member_name";

		List<CommitteeMemberModel> member = session.createQuery(hql).list();

		session.close();

		return member;
	}
	
	@SuppressWarnings("unchecked")
	public List<FacultyModel> getFacultyListInOrder()
	{
		Session session = sessionFactory.openSession();
		
		String hql = "";
		
		hql = "from FacultyModel ORDER BY faculty_name";			
		
		List<FacultyModel> faculty = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return faculty;
	}
	
	@SuppressWarnings("unchecked")
	public List<FacultyModel> getFacultyData(int facultyId)
	{
		Session session = sessionFactory.openSession();
		
		String hql = "";
		
		hql = "from FacultyModel where faculty_id="+facultyId+"";			
		
		List<FacultyModel> faculty = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return faculty;
	}
	
	@SuppressWarnings("unchecked")
	public List<CommitteeMemberModel> getMemberData(int memberId)
	{
		Session session = sessionFactory.openSession();

		String hql = "";

		hql = "from CommitteeMemberModel where committee_member_id="+memberId+"";

		List<CommitteeMemberModel> member = session.createQuery(hql).list();

		session.close();

		return member;
	}
	
	
	public String deleteMemberData(int memberId)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("delete from CommitteeMemberModel where committee_member_id = :memberId");
		updateHql.setParameter("memberId", memberId);
		
	    int result  = updateHql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{
			status = "success";
		}
		else
		{
			LOGGER.error("During delete of CommitteeMemberModel details problem arise.");
			
			status = "failed";
		}
		
		return status;
	}
	
	public String deleteFacultyData(int facultyId)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("delete from FacultyModel where faculty_id = :facultyId");
		updateHql.setParameter("facultyId", facultyId);
		
	    int result  = updateHql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{
			status = "success";
		}
		else
		{
			LOGGER.error("During delete of FacultyModel details problem arise.");
			
			status = "failed";
		}
		
		return status;
	}
	
	public String updateAdminDetails(AdminDetails admin)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("Update from AdminDetails set "
				+ "admin_name = :fullName, admin_user_id = :userName, "
				+ "admin_designation =:designation, admin_updated_date = :updateDate "
				+ "where admin_id = :adminId");
		
		updateHql.setParameter("adminId", admin.getAdmin_id());
		
		updateHql.setParameter("fullName", admin.getAdmin_name());
		updateHql.setParameter("userName", admin.getAdmin_user_id());
		updateHql.setParameter("designation", admin.getAdmin_designation());
		updateHql.setParameter("updateDate", admin.getAdmin_updated_date());
		
	    int result  = updateHql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{
			status = "success";
		}
		else
		{
			LOGGER.error("During update of AdminDetails details problem arise.");
			
			status = "failed";
		}
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public String updateAdminPassword(HttpServletRequest request,HttpSession sessionObj)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		PasswordGenerator pwGenerator = new PasswordGenerator();
		
		int adminId = 1;
		
		String curPassword = request.getParameter("curPassword");
		String newPassword = request.getParameter("newPassword");
		
		try {
			
			curPassword = pwGenerator.encrypt(curPassword);
			newPassword = pwGenerator.encrypt(newPassword);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.error("While encrypting the admin password some exception arise.");
			e.printStackTrace();
		} 
		
		
		if(session.createQuery("from AdminDetails where admin_id="+adminId+" and admin_password='"+curPassword+"'").list().size()>0)
		{
			org.hibernate.Query updateHql = session.createQuery("Update from AdminDetails set "
					+ "admin_password = :password "
					+ "where admin_id = :adminId");
			
			updateHql.setParameter("adminId", adminId);
			
			updateHql.setParameter("password", newPassword);
			
		    int result  = updateHql.executeUpdate();
			
		    tx.commit();
		    session.close();
		    
			if(result==1)
			{
				status = "success";
			}
			else
			{
				LOGGER.error("During update of AdminDetails details problem arise.");
				
				status = "failed";
			}
		}
		else
		{
			status = "No Match";
		}
		
		return status;
	}
	
	public String updateUserStatus(String userStatus,int userId)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("Update from UserRegisterModel set "
				+ "status = :userStatus "
				+ "where user_id = :userId");
		
		updateHql.setParameter("userId", userId);
		
		updateHql.setParameter("userStatus", userStatus);
		
	    int result  = updateHql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{
			status = "success";
		}
		else
		{
			LOGGER.error("During update of AdminDetails details problem arise.");
			
			status = "failed";
		}
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventModel> detailsOnEvent(int eventId)
	{
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from EventModel where event_id = "+eventId+"");
		List<EventModel> result = query.list();
		
		session.close();
		
		return result;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<EventApplicationModel> eventReportData(int eventId)
	{
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from EventApplicationModel where event_id = "+eventId+" ORDER BY applied_date");
		List<EventApplicationModel> result = query.list();
		
		session.close();
		
		return result;
	}
	
	public long getUserCount(String value)
	{
		Session session = sessionFactory.openSession();
		long count = 0;
		
		if(value.equals("Daily"))
		{
			count = ((Long)session.createQuery("select count(*) from UserRegisterModel where subscription_type='"+value+"'").uniqueResult()).longValue();			
		}
		else if(value.equals("Weekly"))
		{
			count = ((Long)session.createQuery("select count(*) from UserRegisterModel where subscription_type='"+value+"'").uniqueResult()).longValue();			
		}
		else if(value.equals("Both"))
		{
			count = ((Long)session.createQuery("select count(*) from UserRegisterModel").uniqueResult()).longValue();			
		}
		else
		{
			count = ((Long)session.createQuery("select count(*) from SubscriptionRequestModel").uniqueResult()).longValue();			
		}
		
		
		session.close();
		 
		return count;
	}
	
	
	public String saveNewsLetterRecord(NewsLetterModel nwsModel)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(nwsModel);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of news letter details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	public String saveNewsLetterFile(NewsLetterAttachedFile newsFile)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(newsFile);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of news letter attached file details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	@SuppressWarnings("unchecked")
	public int getNewsId(NewsLetterModel nwsModel)
	{
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("select news_id from NewsLetterModel where news_matter = '"+nwsModel.getNews_matter()+"' and news_sent_date = (select MAX(news_sent_date) from NewsLetterModel)");
		List<Integer> result = query.list();
		
		session.close();
		
		return result.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getEmailIdList(NewsLetterModel nwsModel)
	{
		Session session = sessionFactory.openSession();
		long count = 0;
		List<String> emailList = new ArrayList<String>();
		
		if(nwsModel.getSent_user_type().equals("Registred User"))
		{
			if(nwsModel.getSent_type().equals("Daily"))
			{
				Query query = session.createQuery("select email_id from UserRegisterModel where subscription_type='"+nwsModel.getSent_type()+"'");
				emailList = query.list();
			}
			else if(nwsModel.getSent_type().equals("Weekly"))
			{
				Query query = session.createQuery("select email_id from UserRegisterModel where subscription_type='"+nwsModel.getSent_type()+"'");
				emailList = query.list();
			}
			else 
			{
				Query query = session.createQuery("select email_id from UserRegisterModel");
				emailList = query.list();
			}
		}
		else
		{
			Query query = session.createQuery("select email_id from SubscriptionRequestModel where status ='Active'");
			emailList = query.list();
		}
		
		session.close();
		 
		return emailList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<NewsLetterModel> getNewsData()
	{
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from NewsLetterModel");
		List<NewsLetterModel> result = query.list();
		
		session.close();
		
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<RedNetGalleryRecord> getGalleryRecord()
	{
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from RedNetGalleryRecord ORDER BY cr_date DESC");
		List<RedNetGalleryRecord> result = query.list();
		
		session.close();
		
		return result;
	}
	
	public String saveGalleryRecord(RedNetGalleryRecord galModel)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(galModel);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of galModel details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	public int getGalleryId(RedNetGalleryRecord galModel)
	{
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("select gallery_id from RedNetGalleryRecord where gallery_text = '"+galModel.getGallery_text()+"' and cr_date = (select MAX(cr_date) from RedNetGalleryRecord)");
		List<Integer> result = query.list();
		
		session.close();
		
		return result.get(0);
	}
	
	public String saveGalleryFile(RedNetGalleryRecordFile galFileModel)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(galFileModel);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of galFileModel details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<RedNetGalleryRecordFile> getGalleryFileRecord(String fileType,int galleryId)
	{
		Session session = sessionFactory.openSession();
		
		if(fileType.equals("All"))
		{
			Query query = session.createQuery("from RedNetGalleryRecordFile where gallery_id = "+galleryId+" ORDER BY cr_date DESC");
			List<RedNetGalleryRecordFile> result = query.list();
			
			session.close();
			
			return result;
		}
		else 
		{
			Query query = session.createQuery("from RedNetGalleryRecordFile where file_type='"+fileType+"' and gallery_id = "+galleryId+" ORDER BY cr_date DESC");
			List<RedNetGalleryRecordFile> result = query.list();
			
			session.close();
			
			return result;
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<RedNetGalleryRecord> getGalleryRecord(int galleryId)
	{
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from RedNetGalleryRecord where gallery_id="+galleryId+"");
		List<RedNetGalleryRecord> result = query.list();
		
		session.close();
		
		return result;
	}
	
	public String deleteGalleryFileRecord(int galleryId)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("delete from RedNetGalleryRecordFile where gallery_id = :galleryId");
		updateHql.setParameter("galleryId", galleryId);
		
	    int result  = updateHql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{
			status = "success";
		}
		else
		{
			LOGGER.error("During delete of gallery file details problem arise.");
			
			status = "failed";
		}
		
		return status;
	}
	
	public String deleteGalleryInfoRecord(int galleryId)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("delete from RedNetGalleryRecord where gallery_id = :galleryId");
		updateHql.setParameter("galleryId", galleryId);
		
	    int result  = updateHql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{
			status = "success";
		}
		else
		{
			LOGGER.error("During delete of gallery file info details problem arise.");
			
			status = "failed";
		}
		
		return status;
	}
	
	public String manageCaseOfDiscussion(int postId,String action)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("Update from UserPostModel set "
				+ "feed_of_discussion = :action "
				+ "where post_id = :postId");
		
		updateHql.setParameter("postId", postId);
		
		updateHql.setParameter("action", action);
		
	    int result  = updateHql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{
			status = "success";
		}
		else
		{
			LOGGER.error("During update of feed_of_discussion column of UserPostModel details problem arise.");
			
			status = "failed";
		}
		
		return status;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<IndexBannerModel> getBannerFileList()
	{
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from IndexBannerModel where banner_status='Active'");
		List<IndexBannerModel> result = query.list();
		
		session.close();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<IndexBannerModel> getAllBannerFileList()
	{
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from IndexBannerModel ORDER BY banner_cr_date DESC");
		List<IndexBannerModel> result = query.list();
		
		session.close();
		
		return result;
	}
	
	
	public String saveTemporaryBrowseFiles(TempFileUploadModel tempFile)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(tempFile);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of TempFileUploadModel details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	
	
	public String saveBannerFileRecord(IndexBannerModel bannerModel)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(bannerModel);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of IndexBannerModel details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	public String saveCaseDetail(CaseModel caseModel)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(caseModel);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of CaseModel details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	@SuppressWarnings("unchecked")
	public int getCaseId(CaseModel caseModel)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		hql = "select case_id from CaseModel where case_title = '"+caseModel.getCase_title()+"' and case_cr_date = (select MAX(case_cr_date) from CaseModel)";			
		
		List<Integer> caseData = session.createQuery(hql).list();	
		
	    session.close();
	    
	    if(caseData.size()>0)
	    {
	    	return caseData.get(0);
	    }
	    else
	    {
	    	return 0;
	    }
	}
	
	@SuppressWarnings("unchecked")
	public List<CaseModel> getCaseRecord(String condition)
	{
		Session session = sessionFactory.openSession();
		String hql = "";
		
		if(condition.equals("All"))
		{
			hql = "from CaseModel ORDER BY case_cr_date DESC";			
			
			List<CaseModel> caseData = session.createQuery(hql).list();	
			
		    session.close();
		    
		    return caseData;
		}
		else if(condition.equals("Active"))
		{
			hql = "from CaseModel where case_status = '"+condition+"' ORDER BY case_cr_date DESC";			
			
			List<CaseModel> caseData = session.createQuery(hql).list();	
			
		    session.close();
		    
		    return caseData;
		}
		else
		{
			int caseId = Integer.parseInt(condition);
			
			hql = "from CaseModel where case_id = "+caseId+"";			
			
			List<CaseModel> caseData = session.createQuery(hql).list();	
			
		    session.close();
		    
		    return caseData;
		}
	}
	
	public String getSaveCaseFileModel(CaseFileModel caseFileModel)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(caseFileModel);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of CaseFileModel details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<CaseUserCommentModel> getUserCaseComment(int caseId)
	{
		Session session = sessionFactory.openSession();
		String hql = "from CaseUserCommentModel where case_id = "+caseId+"";			
		
		List<CaseUserCommentModel> caseComment = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return caseComment;
	}
	
	
	public String changeCaseStatus(HttpServletRequest request)
	{
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery("update CaseModel set case_status = :caseStatus where case_id = :caseId");
		updateHql.setParameter("caseId", Integer.parseInt(request.getParameter("caseId")));
		
		updateHql.setParameter("caseStatus",request.getParameter("caseStatus"));
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
	    updateSession.close();
	    
	    if(result==1)
		{			
			return "success";	    
		}
		else
		{			
			return "failed";
		}
	}
	
	public String deleteCaseDetails(HttpServletRequest request)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("delete from CaseModel where case_id = :caseId");
		
		updateHql.setParameter("caseId", Integer.parseInt(request.getParameter("caseId")));
		
	    int result  = updateHql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{
			status = "success";
		}
		else
		{
			LOGGER.error("During delete of CaseModel details problem arise.");
			
			status = "failed";
		}
		
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public List<IndexBannerModel> getBannerRecord(int bannerId)
	{
		Session session = sessionFactory.openSession();
		String hql = "from IndexBannerModel where banner_id = "+bannerId+"";			
		
		List<IndexBannerModel> banner = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return banner;
	}
	
	public String changeBannerStatus(HttpServletRequest request)
	{
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery("update IndexBannerModel set banner_status = :bannerStatus where banner_id = :bannerId");
		updateHql.setParameter("bannerId", Integer.parseInt(request.getParameter("bannerId")));
		
		updateHql.setParameter("bannerStatus",request.getParameter("bannerStatus"));
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
	    updateSession.close();
	    
	    if(result==1)
		{			
			return "success";	    
		}
		else
		{			
			return "failed";
		}
	}
	
	public String deleteBanner(HttpServletRequest request)
	{
		String status = "";
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		org.hibernate.Query updateHql = session.createQuery("delete from IndexBannerModel where banner_id = :bannerId");
		
		updateHql.setParameter("bannerId", Integer.parseInt(request.getParameter("bannerId")));
		
	    int result  = updateHql.executeUpdate();
		
	    tx.commit();
	    session.close();
	    
		if(result==1)
		{
			status = "success";
		}
		else
		{
			LOGGER.error("During delete of IndexBannerModel details problem arise.");
			
			status = "failed";
		}
		
		return status;
	}
	
	public String savePortalContent(PortalContentModel prtlContent)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(prtlContent);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of PortalContentModel details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<PortalContentModel> getContentList()
	{
		Session session = sessionFactory.openSession();
		String hql = "from PortalContentModel ORDER BY content_cr_date";			
		
		List<PortalContentModel> dataList = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return dataList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PortalContentModel> getContentDetails(String condition,String contentFor,String contentType)
	{
		Session session = sessionFactory.openSession();
		String hql = "";			
		
		if(condition.equals("contentId"))
		{
			// PASS 'content_id' WITH 'contentFor' STRING PARAMETER
			
			hql = "from PortalContentModel where content_id = "+contentFor+"";			
		}
		else
		{
			if(contentType.equals("Heading"))
			{
				hql = "select content_heading from PortalContentModel where content_for='"+condition+"'";	
			}
			else if(contentType.equals("Description"))
			{
				hql = "select content_desc from PortalContentModel where content_for='"+condition+"'";
			}
			else
			{
				hql = "from PortalContentModel where content_for='"+condition+"'";	
			}
		}
		
		List<PortalContentModel> dataList = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return dataList;
	}
	
	public String updatePortalContent(PortalContentModel prtlContent)
	{
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery("update PortalContentModel set content_heading = :contentHeading , content_for = :contentFor where content_id = :contentId");
		updateHql.setParameter("contentId", prtlContent.getContent_id());
		
		updateHql.setParameter("contentHeading",prtlContent.getContent_heading());
		updateHql.setParameter("contentFor",prtlContent.getContent_for());
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
	    updateSession.close();
	    
	    if(result==1)
		{			
			return "success";	    
		}
		else
		{			
			return "failed";
		}
	}
	
	public String  deleteContent(int contentId)
	{
		Session updateSession = sessionFactory.openSession();
		Transaction tx = updateSession.beginTransaction();

		org.hibernate.Query updateHql = updateSession.createQuery("delete from PortalContentModel where content_id = :contentId");
		updateHql.setParameter("contentId", contentId);
		
	    int result  = updateHql.executeUpdate();
	    
	    tx.commit();
	    updateSession.close();
	    
	    if(result==1)
		{			
			return "success";	    
		}
		else
		{			
			return "failed";
		}
	}
	
	public String saveAbstractSubmission(AbstractSubmissionModel absDoc)
	{
		String transactionStatus = "";

		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(absDoc);
			transaction.commit();

			if (transaction.wasCommitted()) {
				transactionStatus = "success";
			} else {
				transactionStatus = "failure";
			}
		} catch (Exception he) {
			LOGGER.error("During saving the data of AbstractSubmissionModel details exception arise.");

			transactionStatus = "failure";
		} finally {
			session.close();
		}

		return transactionStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EventApplicationModel> getEventRegRecord()
	{
		Session session = sessionFactory.openSession();
		String hql = "from EventApplicationModel where event_type='Event before reg' ORDER BY applied_date";			
		
		List<EventApplicationModel> dataList = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return dataList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EventApplicationModel> getPreConRegRecord()
	{
		Session session = sessionFactory.openSession();
		String hql = "from EventApplicationModel where event_type='Pre Conference Workshop' ORDER BY applied_date";			
		
		List<EventApplicationModel> dataList = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return dataList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AbstractSubmissionModel> getAbstractDocRecord()
	{
		Session session = sessionFactory.openSession();
		String hql = "from AbstractSubmissionModel ORDER BY uploaded_date";			
		
		List<AbstractSubmissionModel> dataList = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return dataList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BulkMailEmailModel> getBulkMailEmailId()
	{
		Session session = sessionFactory.openSession();
		String hql = "from BulkMailEmailModel where email_id != '' ORDER BY id DESC";			
		
		List<BulkMailEmailModel> dataList = session.createQuery(hql).list();	
		
	    session.close();
	    
	    return dataList;
	}
	
	
}
