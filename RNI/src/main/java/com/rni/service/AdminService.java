package com.rni.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.rni.bean.FormBean;
import com.rni.bean.NewsLetterBean;
import com.rni.bean.PortalStatus;
import com.rni.bean.RedNetGalleryBean;
import com.rni.model.AbstractSubmissionModel;
import com.rni.model.AdminDetails;
import com.rni.model.BulkMailEmailModel;
import com.rni.model.CaseModel;
import com.rni.model.CaseUserCommentModel;
import com.rni.model.CommitteeMemberModel;
import com.rni.model.EmailModel;
import com.rni.model.EventApplicationModel;
import com.rni.model.EventModel;
import com.rni.model.FacultyModel;
import com.rni.model.IndexBannerModel;
import com.rni.model.NewsLetterModel;
import com.rni.model.PortalContentModel;
import com.rni.model.RedNetGalleryRecord;
import com.rni.model.RedNetGalleryRecordFile;
import com.rni.model.UserPostModel;
import com.rni.model.UserRegisterModel;


public interface AdminService {

	
	public PortalStatus getUpdateOnPortalStatus();
	
	
	public String getValidateAdmin(String user_id,String password);
	
	public String setAdminAccess(String access);
	
	public String saveEventFile(MultipartHttpServletRequest request,HttpSession session);
	
	public String getCurrentUploadedFiles(HttpSession session);
	
	public String deleteTempFile(HttpServletRequest request,HttpSession session);
	
	public String saveEventDetails(EventModel eventDetail,MultipartHttpServletRequest request,HttpSession session);
	
	public String updateEventDetail(EventModel eventDetail,MultipartHttpServletRequest request,HttpSession session);
	
	public AdminDetails getAdminDetails();
	
	public List<UserPostModel> getSearchPost(HttpServletRequest request);
	
	public String getBlockUserPost(int postId,String postStatus);
	
	public List<UserRegisterModel> getSearchUser(HttpServletRequest request);
	
	public String saveFacultyDetails(FacultyModel faculty);
	
	public List<FacultyModel> getFacultyList();
	
	public String saveMemberDetails(CommitteeMemberModel member);
	
	public List<CommitteeMemberModel> getMemberList();
	
	public List<FacultyModel> getFacultyListInOrder();
	
	public List<CommitteeMemberModel> getMemberListInOrder();
	
	public List<FacultyModel> getFacultyData(int facultyId);
	
	public List<CommitteeMemberModel> getMemberData(int memberId);
	
	public String deleteMemberData(int memberId);
	
	public String deleteFacultyData(int facultyId);
	
	public String updateAdminDetails(AdminDetails admin);
	
	public String updateAdminPassword(HttpServletRequest request,HttpSession session);	
	
	public String updateUserStatus(String userStatus,int userId);
	
	public List<EventModel> getLastFiveEventList();
	
	public List<EventModel> getSearchEvent(HttpServletRequest request);
	
	public List<EventModel> detailsOnEvent(int eventId);
	
	public List<EventApplicationModel> eventReportData(int eventId);
	
	public String getUserCount(String value);
	
	public String sendNewsLetter(NewsLetterBean newsLetter,MultipartHttpServletRequest request,HttpSession session);
	
	public List<NewsLetterModel> getNewsData();
	
	public List<RedNetGalleryRecord> getGalleryRecord();
	
	public String saveGalleryRecord(RedNetGalleryBean galleryRecord,MultipartHttpServletRequest request,HttpSession session);
	
	public List<RedNetGalleryRecordFile> getGalleryFileRecord(String fileType,int galleryId);
	
	public String deleteGallery(int galleryId);
	
	public String manageCaseOfDiscussion(int postId,String action);
	
	public List<IndexBannerModel> getBannerFileList();
	
	public String captureBrowseFiles(FormBean formBean,MultipartHttpServletRequest request,HttpSession session);
	
	public String saveBannerFiles(FormBean formBean,MultipartHttpServletRequest request,HttpSession session);
	
	public String deleteBrowseFile(HttpServletRequest request,HttpSession session);
	
	public List<IndexBannerModel> getAllBannerFileList();
	
	public String saveCaseDetail(FormBean formBean,MultipartHttpServletRequest request,HttpSession session);
	
	public List<CaseModel> getCaseRecord(String condition);
	
	public List<CaseUserCommentModel> getUserCaseComment(int caseId);
	
	public String changeCaseStatus(HttpServletRequest request);
	
	public String deleteCaseDetails(HttpServletRequest request);
	
	public List<IndexBannerModel> getBannerRecord(int bannerId);
	
	public String changeBannerStatus(HttpServletRequest request);
	
	public String deleteBanner(HttpServletRequest request);
	
	public String savePortalContent(PortalContentModel prtlContent);
	
	public List<PortalContentModel> getContentList();
	
	public List<PortalContentModel> getContentDetails(String condition,String contentFor,String contentType);
	
	public String updatePortalContent(PortalContentModel prtlContent);
	
	public String deleteContent(int contentId);
	
	public List<EventApplicationModel> getEventRegRecord();
	
	public List<EventApplicationModel> getPreConRegRecord();
	
	public List<AbstractSubmissionModel> getAbstractDocRecord();
	
	public List<BulkMailEmailModel> getBulkMailEmailId();
	
	public String bulkMailSendStatus() throws IOException;
	
	
}
