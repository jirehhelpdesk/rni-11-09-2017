package com.rni.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.rni.bean.FormBean;
import com.rni.bean.UserBean;
import com.rni.bean.UserPostBean;
import com.rni.bean.UserPostCommentBean;
import com.rni.model.EventApplicationModel;
import com.rni.model.EventModel;
import com.rni.model.NewsLetterModel;
import com.rni.model.UserPostCommentModel;
import com.rni.model.UserPostInterestModel;
import com.rni.model.UserPostModel;
import com.rni.model.UserProfileModel;
import com.rni.model.UserRegisterModel;


public interface UserService {

	
	
	public String saveUserRegister(UserRegisterModel userRegDetails,MultipartHttpServletRequest request);
	
	public String checkUserExist(String value,String condition);
	
	public String getValidateUserAuthentication(String userId,String password);
	
	public int getUserIdViaEmailId(String userId);
	
	public UserBean getUserDetails(int userId);
	
	public String getEmailIdViaUnqid(String activeAccountId);
	
	public String getActiveAcount(String emailId,String activeAccountId);
	
	public String sendLinkForgetPassword(String emailId);
	
	public String getEmailIdFromForgetPassword(String uniqueId);
	
	public String getChangePassword(String emailId,String password);
	
	public String saveUserPostDetails(UserPostBean postDetail,MultipartHttpServletRequest request,HttpSession session);
	
	public List<UserPostModel> getAllPost(int userId,String forWhom,String postType);
	
	public List<UserPostModel> getFilterFeeds(int userId,String selectValue);
	
	public List<UserPostModel> getFilterFeedsViaDuration(int userId,HttpServletRequest request);
	
	public String savePostComment(UserPostCommentBean postComment,int postId,int userId,MultipartHttpServletRequest request,HttpSession session);
	
	public List<UserPostModel> getPostsUnderCondition(String postType);
	
	public List<EventModel> getEventList();
	
	public List<EventModel> getEventRecordViaCode(String eventCode);
	
	public String updateUserRegister(UserRegisterModel userRegDetails);
	
	public String changeUserPassword(int userId,HttpServletRequest request);
	
	public Map<String, List<String>> getFeedMenus();
	
	public String checkUserExistOnUpdate(int userId,String checkValue,String checkType);
	
	public List<EventModel> getEventRecordViaId(int eventId);
	
	public String saveApplicaitionPassToPayment(EventApplicationModel evntApplication,HttpSession session,HttpServletRequest request);

	public String myParticipatedEventList(int userId);

	public List<EventApplicationModel> getEventAppliedForm(int userId,int eventId);
	
	public String saveUserProfileData(UserProfileModel userProfile,MultipartHttpServletRequest request);
	
	public String updateUserProfileData(UserProfileModel userProfile,MultipartHttpServletRequest request);
	
	public List<UserPostCommentModel> getAllPostComment(int postId);

	public String showInterest(String interest,int postId,int userId);
	
	public long countInterest(String interest,int postId);
	
	public long calculateViewPost(HttpSession session,HttpServletRequest request); 
	
	public List<UserPostInterestModel> getAllPostInterest();
	
	public String captureTempPostFiles(UserPostBean postDetail,MultipartHttpServletRequest request,HttpSession session);
	
	public String captureTempCommentFiles(UserPostCommentBean commentBean,MultipartHttpServletRequest request,HttpSession session);
	
	public String removeTempPostFiles(HttpServletRequest request,HttpSession session);
	
	public String saveSubscriptionRequest(String emailId);
	
	public long getCountPostComment(int postId);
	
	public List<UserRegisterModel> userSearchList(String searchValue);
	
	public List<String> makeZipPostFile(int postId,int userId);
	
	public String getYearListForPost();
	
	public String getMontListForPost(String year);
	
	public String submitCaseComment(FormBean formBean,MultipartHttpServletRequest request,HttpSession session);
	
	public List<EventApplicationModel> getApplicationDetails(int applicationId);
	
	public String getUpdateEventApplication(EventApplicationModel evntApplication,HttpServletRequest request);
	
	
	
	
	public String indexSaveApplicaitionPassToPayment(EventApplicationModel evntApplication,HttpSession session,HttpServletRequest request);
	
	public String indexUpdateEventApplication(EventApplicationModel evntApplication,HttpServletRequest request);
	
	public String saveAbstractSubmission(HttpSession session,MultipartHttpServletRequest request);
	
	
	public String indexSaveConfWorkShopPassToPayment(EventApplicationModel evntApplication,HttpSession session,HttpServletRequest request);
	
	public String indexUpdateWorkshopApplication(EventApplicationModel evntApplication,HttpServletRequest request);
	
	
}
