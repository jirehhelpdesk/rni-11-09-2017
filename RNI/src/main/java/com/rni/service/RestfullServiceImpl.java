package com.rni.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rni.dao.AdminDao;
import com.rni.dao.RestfullDao;
import com.rni.dao.UserDao;
import com.rni.model.UserPostModel;

@Service("restService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class RestfullServiceImpl implements RestfullService {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private AdminDao adminDao;
	
	@Autowired
	private RestfullDao restDao;
	
	
	public List<UserPostModel> getAllPost()
	{
		return restDao.getAllPost();
	}
	
	public UserPostModel getPostInformation(int post_id)
	{
		return restDao.getPostInformation(post_id);
	}
	
	
}
