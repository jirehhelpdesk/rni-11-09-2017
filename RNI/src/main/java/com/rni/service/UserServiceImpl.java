package com.rni.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ccavenue.security.AesCryptUtil;
import com.rni.bean.FormBean;
import com.rni.bean.UserBean;
import com.rni.bean.UserPostBean;
import com.rni.bean.UserPostCommentBean;
import com.rni.bean.UserProfileBean;
import com.rni.dao.AdminDao;
import com.rni.dao.UserDao;
import com.rni.model.AbstractSubmissionModel;
import com.rni.model.CaseCommentFileModel;
import com.rni.model.CaseModel;
import com.rni.model.CaseUserCommentModel;
import com.rni.model.EmailModel;
import com.rni.model.EventApplicationModel;
import com.rni.model.EventImageModel;
import com.rni.model.EventModel;
import com.rni.model.LinkValidationModel;
import com.rni.model.NewsLetterModel;
import com.rni.model.SubscriptionRequestModel;
import com.rni.model.TempFileUploadModel;
import com.rni.model.UserPostCommentFileModel;
import com.rni.model.UserPostCommentModel;
import com.rni.model.UserPostFile;
import com.rni.model.UserPostInterestModel;
import com.rni.model.UserPostModel;
import com.rni.model.UserProfileModel;
import com.rni.model.UserRegisterModel;
import com.rni.scheduler.VideoRenderScheduler;
import com.rni.synchronsJob.AdminSentMail;
import com.rni.util.CaptureIPandMACaddressUtil;
import com.rni.util.CaptureVideoIdFromUrl;
import com.rni.util.ConvertStringToDate;
import com.rni.util.EmailNotificationUtil;
import com.rni.util.EmailSentUtil;
import com.rni.util.ImageRenderUtil;
import com.rni.util.PasswordGenerator;
import com.rni.util.RandomNoGenerator;
import com.rni.util.UniquePatternGeneration;


@Service("userService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserServiceImpl implements UserService  {

	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private AdminDao adminDao;
	
	@Autowired
	private AdminSentMail mailJob;
	
	PasswordGenerator pwGenerator = new PasswordGenerator();
	
	EmailNotificationUtil emailUtil = new EmailNotificationUtil();
	
	CaptureIPandMACaddressUtil ipMacAddress = new CaptureIPandMACaddressUtil();
	
	ImageRenderUtil videoRender = new ImageRenderUtil();
	
	CaptureVideoIdFromUrl embedVideoId = new CaptureVideoIdFromUrl();
	
	UniquePatternGeneration generatorObject = new UniquePatternGeneration();
	
	ConvertStringToDate cnvrtDate = new ConvertStringToDate();
	
	private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);
	
	
	
	public String checkUserExist(String value,String condition)
	{
		return userDao.checkUserExist(value,condition);
	}
	
	public String saveUserRegister(UserRegisterModel userRegDetails,MultipartHttpServletRequest request)
	{
		String status = "";
		
		userRegDetails.setCr_date(new Date());
		userRegDetails.setStatus("Deactive");
		
		
		try {
			
			userRegDetails.setPassword(pwGenerator.encrypt(userRegDetails.getPassword()));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			LOGGER.error("Can cause password encryption");
			
			e.printStackTrace();
		}

		status = userDao.saveUserRegister(userRegDetails);
		
		if(status.equals("success"))
		{
			UserProfileModel userProfile = new UserProfileModel();
			
			int userId = userDao.getUserIdViaEmailId(userRegDetails.getEmail_id());
			
			userRegDetails.setUser_id(userId);
			
			userProfile.setUnder_graduation(request.getParameter("under_graduation"));
			userProfile.setUnder_graduation_institute(request.getParameter("under_graduation_institute"));
			userProfile.setUnder_graduation_university(request.getParameter("under_graduation_university"));
			userProfile.setUnder_graduation_place(request.getParameter("under_graduation_place"));
			userProfile.setUnder_graduation_state(request.getParameter("under_graduation_state"));
			
			userProfile.setPost_graduation(request.getParameter("post_graduation"));
			userProfile.setPost_graduation_institute(request.getParameter("post_graduation_institute"));
			userProfile.setPost_graduation_university(request.getParameter("post_graduation_university"));
			userProfile.setPost_graduation_place(request.getParameter("post_graduation_place"));
			userProfile.setPost_graduation_state(request.getParameter("post_graduation_state"));
			
			userProfile.setAdditional_qualification(request.getParameter("additional_qualification"));
			userProfile.setPractice_as(request.getParameter("practice_as"));
			userProfile.setPractice_at(request.getParameter("practice_at"));
			
			userProfile.setFellowship(request.getParameter("fellowship"));
			userProfile.setFellowship_sub_speciality(request.getParameter("fellowship_sub_speciality"));
			userProfile.setFellowship_institute(request.getParameter("fellowship_institute"));
			userProfile.setFellowship_university(request.getParameter("fellowship_university"));
			userProfile.setFellowship_place(request.getParameter("fellowship_place"));
			userProfile.setFellowship_state(request.getParameter("fellowship_state"));
			
			userProfile.setGender(request.getParameter("gender"));
			userProfile.setPhone_number(request.getParameter("phone_number"));
			userProfile.setWebsite_url(request.getParameter("website_url"));
			userProfile.setYear_of_experience(Integer.parseInt(request.getParameter("year_of_experience")));
			userProfile.setUser(userRegDetails);
			userProfile.setPincode(request.getParameter("pincode"));
			
			String dob = request.getParameter("dob");	
			
	        try
	        {	            
	            userProfile.setDob(cnvrtDate.convertStringIntoDate(dob));	            
	        }
	        catch (Exception ex)
	        {
	            LOGGER.info("while convert string to date Exception "+ex);
	        }
			
	        // SAVING PROFILE DETAILS IN A DIFFERNET TABLE .....
			
			saveUserProfileData(userProfile,request);
			
			/// ...........................
			
			
			String validateId = "";
			String repositoryFile = "resources/wellcomeRegistration";

			ResourceBundle resource = ResourceBundle.getBundle(repositoryFile);
		 	 
			String mailSubject = resource.getString("mailSubject");
			String defaultUrl = resource.getString("defaultUrl");
			
			validateId += generatorObject.generateUniqueCode(userRegDetails.getEmail_id());
			
			String link = defaultUrl+"activeAccount?account="+validateId;
			
			String reasonFor = "NotRequired";
			
			String extraInfo = "NotRequired";
						
			String emailBody = emailUtil.emailNotification(userRegDetails.getFirst_name(),userRegDetails.getEmail_id(),repositoryFile,link,"Activate Account",reasonFor,extraInfo);
			
			String mailStatus = "";
			
			/* SENDING MAIL */
		    
				    EmailSentUtil emailStatus = new EmailSentUtil();
		 	        
					try {
						mailStatus = emailStatus.getEmailSent(userRegDetails.getEmail_id(),mailSubject,emailBody);
					} 
					catch (Exception e) {
						
						// TODO Auto-generated catch block
						mailStatus = "failed";
						LOGGER.error("Some exception arise while sending mail");
						e.printStackTrace();
					}
					
		  
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(userRegDetails.getEmail_id());
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				status = adminDao.getSaveEmailDetails(emailDetail);
			}
			
			
			LinkValidationModel linkModel = new LinkValidationModel();
			
			linkModel.setUser_type("User");
			linkModel.setValidate_email_id(userRegDetails.getEmail_id());
			linkModel.setValidate_for("Account Activation");
			linkModel.setValidate_init_date(new Date());
			linkModel.setValidate_status("Pending");
			linkModel.setValidate_id(validateId);
			
			status = adminDao.saveLinkDetails(linkModel);
			
		}
		
		
		return status;
	}
	
	
	public String getValidateUserAuthentication(String userId,String password)
	{
		String status = "";
		
		try {
					
			String encryptPassword = pwGenerator.encrypt(password);
			
			status = userDao.getValidateUserAuthentication(userId,encryptPassword);		
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 			
		return status;
	}
	
	public int getUserIdViaEmailId(String userId)
	{
		return userDao.getUserIdViaEmailId(userId);
	}
	
	public UserBean getUserDetails(int userId)
	{
		UserBean eBean = new UserBean();
		
		List<UserRegisterModel> eModel = userDao.getUserDetails(userId);
		
		for(int i=0;i<eModel.size();i++)
		{
			eBean.setUser_id(eModel.get(i).getUser_id());
			eBean.setEmail_id(eModel.get(i).getEmail_id());
			eBean.setFirst_name(eModel.get(i).getFirst_name());
			eBean.setLast_name(eModel.get(i).getLast_name());
			eBean.setMobile_no(eModel.get(i).getMobile_no());
			eBean.setCr_date(eModel.get(i).getCr_date());
			eBean.setProfession(eModel.get(i).getProfession());
			eBean.setStatus(eModel.get(i).getStatus());
			eBean.setSubscription_type(eModel.get(i).getSubscription_type());
			eBean.setFeeds_update(eModel.get(i).getFeeds_update());
			
			// PROFILE DETAILS
			
			if(eModel.get(i).getProfile()!=null)
			{				
				UserProfileBean profileBean = new UserProfileBean();
				
				LOGGER.info("How about graduation info="+eModel.get(i).getProfile().getUnder_graduation());
				
				profileBean.setProfile_id(eModel.get(i).getProfile().getProfile_id());
				profileBean.setDob(eModel.get(i).getProfile().getDob());
				profileBean.setAge(eModel.get(i).getProfile().getAge());
				profileBean.setGender(eModel.get(i).getProfile().getGender());
				
				profileBean.setUnder_graduation(eModel.get(i).getProfile().getUnder_graduation());
				profileBean.setUnder_graduation_institute(eModel.get(i).getProfile().getUnder_graduation_institute());
				profileBean.setUnder_graduation_university(eModel.get(i).getProfile().getUnder_graduation_university());
				profileBean.setUnder_graduation_place(eModel.get(i).getProfile().getUnder_graduation_place());
				profileBean.setUnder_graduation_state(eModel.get(i).getProfile().getUnder_graduation_state());
				
				profileBean.setPost_graduation(eModel.get(i).getProfile().getPost_graduation());
				profileBean.setPost_graduation_institute(eModel.get(i).getProfile().getPost_graduation_institute());
				profileBean.setPost_graduation_university(eModel.get(i).getProfile().getPost_graduation_university());
				profileBean.setPost_graduation_place(eModel.get(i).getProfile().getPost_graduation_place());
				profileBean.setPost_graduation_state(eModel.get(i).getProfile().getPost_graduation_state());
				
				profileBean.setFellowship(eModel.get(i).getProfile().getFellowship());
				profileBean.setFellowship_sub_speciality(eModel.get(i).getProfile().getFellowship_sub_speciality());
				profileBean.setFellowship_institute(eModel.get(i).getProfile().getFellowship_institute());
				profileBean.setFellowship_university(eModel.get(i).getProfile().getFellowship_university());
				profileBean.setFellowship_place(eModel.get(i).getProfile().getFellowship_place());
				profileBean.setFellowship_state(eModel.get(i).getProfile().getFellowship_state());
				
				profileBean.setAdditional_qualification(eModel.get(i).getProfile().getAdditional_qualification());
				profileBean.setYear_of_experience(eModel.get(i).getProfile().getYear_of_experience());
				profileBean.setPhone_number(eModel.get(i).getProfile().getPhone_number());
				profileBean.setPincode(eModel.get(i).getProfile().getPincode());
				profileBean.setPractice_at(eModel.get(i).getProfile().getPractice_at());
				profileBean.setPractice_as(eModel.get(i).getProfile().getPractice_as());
				profileBean.setProfile_cr_date(eModel.get(i).getProfile().getCr_date());
				profileBean.setWebsite_url(eModel.get(i).getProfile().getWebsite_url());
				profileBean.setProfile_photo(eModel.get(i).getProfile().getProfile_photo());
				profileBean.setArea_of_interest(eModel.get(i).getProfile().getArea_of_interest());
				profileBean.setOther_area_of_interest(eModel.get(i).getProfile().getOther_area_of_interest());
				
				eBean.setProfileBean(profileBean);
			}
			
		}
		
		return eBean;
	}
	
	
	
	
	public String getEmailIdViaUnqid(String activeAccountId)
	{
		String emailId = "";
		
		emailId = userDao.getEmailIdViaUnqid(activeAccountId);
		
		return emailId;
	}
	
	public String getActiveAcount(String emailId,String activeAccountId)
	{
		String status = "";
		
		status = userDao.getActiveAcount(emailId,activeAccountId);
		
		return status;
	}
	
	
	public String sendLinkForgetPassword(String emailId)
	{
		String validateId = "";
		
		if(userDao.checkUserExist(emailId, "emailId").equals("Not Exist"))
		{
			return "Not Exist";
		}
		else
		{
			LinkValidationModel linkValidate = new LinkValidationModel();
			
			linkValidate.setValidate_email_id(emailId);
			
			try {
				
				UniquePatternGeneration generatorObject = new UniquePatternGeneration();
				
				validateId += generatorObject.generateUniqueCode(emailId);
				
				linkValidate.setValidate_id(validateId);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String repositoryFile = "resources/forgetPassword";
			ResourceBundle resource = ResourceBundle.getBundle(repositoryFile);
			String mailSubject = resource.getString("mailSubject");
			String defaultUrl = resource.getString("defaultUrl");
			
			String link = defaultUrl+"resetPassword?requnIkedij="+validateId+"";
			
			String reasonFor = "ResetPassword";
			
			String extraInfo = "NotRequired";
			
			String emailBody = emailUtil.emailNotification("User",emailId,"resources/forgetPassword",link,"Reset Passwod",reasonFor,extraInfo);
			
			
			String mailStatus = "";
			
			/* SENDING MAIL */
		    
			
				    EmailSentUtil emailStatus = new EmailSentUtil();
		 	        
					try {
						mailStatus = emailStatus.getEmailSent(emailId,mailSubject,emailBody);
					} 
					catch (Exception e) {
						
						// TODO Auto-generated catch block
						mailStatus = "failed";
						e.printStackTrace();
					}
					
		  
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(emailId);
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				adminDao.getSaveEmailDetails(emailDetail);
			}
			
		    if(mailStatus.equals("success"))
		    {
		    	// Save email sent details
		    	
				linkValidate.setValidate_status("Pending");
				linkValidate.setValidate_init_date(new Date());
				linkValidate.setValidate_for("Forget Password");			
				
				userDao.saveLinkValidatedetails(linkValidate);
		    }
		    else
		    {
		    	
		    }	    
		    
		    return mailStatus;
		}
		
		
	}
	
	public String getEmailIdFromForgetPassword(String uniqueId)
	{
		return userDao.getEmailIdFromForgetPassword(uniqueId);
	}
	
	public String getChangePassword(String emailId,String password)
	{
		String encPassword = "";
		
		try {
					
			System.out.println("password="+password);
			encPassword += pwGenerator.encrypt(password);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return userDao.getChangePassword(emailId,encPassword);
	}
	
	public String captureTempPostFiles(UserPostBean postDetail,MultipartHttpServletRequest request,HttpSession session)
	{
		String fileNameList = "";
		String imageFormat = "JPG,jpg,jpeg,JPEG,gif,GIF,png,PNG";
		String videoFormat = "mp4,MP4,3gp,3GP,flv,FLV,avi,AVI,mpeg,MPEG";
		String docFormat = "PDF,pdf";
		
		List<MultipartFile> selectedFiles = postDetail.getFiles();
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
 	    
 	    String filePath = fileResource.getString("tempPostFileDirectory");
 	    	
		for(int i=0;i<selectedFiles.size();i++)
		{
			TempFileUploadModel tempFile = new TempFileUploadModel();
			String fileName = "";
			
			MultipartFile multipartFile = selectedFiles.get(i);
			
			fileName = multipartFile.getOriginalFilename();
        	String extension=fileName.substring(fileName.lastIndexOf('.')+1);
        	fileName = fileName.replaceAll(" ","_");  
            
            LOGGER.info("File Transfering in the system.");
            
            if(imageFormat.contains(extension))
            {
            	tempFile.setFile_type("Image");
            }
            else if(videoFormat.contains(extension))
            {
            	tempFile.setFile_type("Video");
            }
            else if(docFormat.contains(extension))
            {
            	tempFile.setFile_type("Pdf");
            }
            else{ }
            
            tempFile.setFile_name(fileName);
            tempFile.setCr_date(new Date());
            tempFile.setSession_id(session.getId());
            tempFile.setUpload_for("Post");
            tempFile.setUpload_from("User");
            
            File transferFile = new File(filePath, fileName);
            
            try
            {
            	File fld = new File(filePath+"/");
		    	
		        if(!fld.exists())
		    	{
				    fld.mkdirs();
		    	}
		        
		        LOGGER.info("each File transfer link:"+transferFile.toString());
		        
                multipartFile.transferTo(transferFile);
                
                String status = userDao.saveTemporaryFileDetails(tempFile);
                
                
            } catch (Exception e)
            {
            	LOGGER.error("While Transfering the file from user system to server exception rais.");
                e.printStackTrace();
            }
			
			tempFile = null;
		}
		
		// GET ALL THE LIST OF EXISTING FILE NAMES
		
		List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
		
		for(int i =0;i<fileCollection.size();i++)
		{
			fileNameList += fileCollection.get(i).getFile_name() + "/";
		}
		
		if(!fileNameList.equals(""))
		{
			fileNameList = fileNameList.substring(0,fileNameList.length()-1);
		}
		
		return fileNameList;
	}
	
	
	public String captureTempCommentFiles(UserPostCommentBean commentBean,MultipartHttpServletRequest request,HttpSession session)
	{
		String fileNameList = "";
		
		String imageFormat = "JPG,jpg,jpeg,JPEG,gif,GIF,png,PNG";
		String videoFormat = "mp4,MP4,3gp,3GP,flv,FLV,avi,AVI,mpeg,MPEG";
		String docFormat = "PDF,pdf";
		
		List<MultipartFile> selectedFiles = commentBean.getCommentFiles();
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
 	    
 	    String filePath = fileResource.getString("tempPostFileDirectory");
 	    	
		for(int i=0;i<selectedFiles.size();i++)
		{
			TempFileUploadModel tempFile = new TempFileUploadModel();
			String fileName = "";
			
			MultipartFile multipartFile = selectedFiles.get(i);
			
			fileName = multipartFile.getOriginalFilename();
        	String extension=fileName.substring(fileName.lastIndexOf('.')+1);
        	fileName = fileName.replaceAll(" ","_");  
            
            LOGGER.info("File Transfering in the system.");
            
            if(imageFormat.contains(extension))
            {
            	tempFile.setFile_type("Image");
            }
            else if(videoFormat.contains(extension))
            {
            	tempFile.setFile_type("Video");
            }
            else if(docFormat.contains(extension))
            {
            	tempFile.setFile_type("Pdf");
            }
            else{ }
            
            tempFile.setFile_name(fileName);
            tempFile.setCr_date(new Date());
            tempFile.setSession_id(session.getId());
            tempFile.setUpload_for("Comment");
            tempFile.setUpload_from("User");
            
            File transferFile = new File(filePath, fileName);
            
            try
            {
            	File fld = new File(filePath+"/");
		    	
		        if(!fld.exists())
		    	{
				    fld.mkdirs();
		    	}
		        
		        LOGGER.info("each File transfer link:"+transferFile.toString());
		        
                multipartFile.transferTo(transferFile);
                
                String status = userDao.saveTemporaryFileDetails(tempFile);
                
                
            } catch (Exception e)
            {
            	LOGGER.error("While Transfering the file from user system to server exception rais.");
                e.printStackTrace();
            }
			
			tempFile = null;
		}
		
		// GET ALL THE LIST OF EXISTING FILE NAMES
		
		List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
		
		for(int i =0;i<fileCollection.size();i++)
		{
			fileNameList += fileCollection.get(i).getFile_name() + "/";
		}
		
		if(!fileNameList.equals(""))
		{
			fileNameList = fileNameList.substring(0,fileNameList.length()-1);
		}
		
		return fileNameList;
	}
	
	
	public String removeTempPostFiles(HttpServletRequest request,HttpSession session)
	{
		String fileNameList = "";
		
		String status = adminDao.deleteTempFile(request,session);
		
		if(status.equals("success"))
		{			
			// DELETE FILE FROM THE SERVER LOCATION 
			
			String fileName = request.getParameter("fileName");
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
			String fileDirectory = fileResource.getString("tempPostFileDirectory");
				
			fileDirectory = fileDirectory+"/"+fileName;
				
			try{

	    		File file = new File(fileDirectory);

	    		if(file.delete())
	    		{
	    			LOGGER.info(file.getName() + " is deleted!");
	    		}else{
	    			LOGGER.error("Delete operation is failed.");
	    		}
	    	}
			catch(Exception e)
			{
				LOGGER.error("While deleting temporary file from post "+fileName + " exception rais!");
				
	    		e.printStackTrace();
	    	}
		}
		
		
		// GET ALL THE LIST OF EXISTING FILE NAMES
		
		List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
		
		for(int i =0;i<fileCollection.size();i++)
		{
			fileNameList += fileCollection.get(i).getFile_name() + "/";
		}
		
		if(!fileNameList.equals(""))
		{
			fileNameList = fileNameList.substring(0,fileNameList.length()-1);
		}
		
		return fileNameList;
	}
	
	
	/* (non-Javadoc)
	 * @see com.rni.service.UserService#saveUserPostDetails(com.rni.bean.UserPostBean, org.springframework.web.multipart.MultipartHttpServletRequest, javax.servlet.http.HttpSession)
	 */
	public String saveUserPostDetails(UserPostBean postDetail,MultipartHttpServletRequest request,HttpSession session)
	{
		String status = "";
		
		UserPostModel postModel = new UserPostModel();
		
		UserRegisterModel userModel = new UserRegisterModel();
		userModel.setUser_id(postDetail.getUser_id());
		
		List<MultipartFile> files = postDetail.getFiles();
		
		String imageFormat = "JPG,jpg,jpeg,JPEG,gif,GIF,png,PNG";
		String videoFormat = "mp4,MP4,3gp,3GP,flv,FLV,avi,AVI,mpeg,MPEG";
		String docFormat = "PDF,pdf";
		
		String fileName = "";
		String extension = "";
		String fileReName = "";
		String fileAvailable = "";
		String attachUrl = "";
		
		String attachType = request.getParameter("attachType");
		
		postModel.setPost_view(0);
		postModel.setPost_like(0);
		postModel.setPost_dislike(0);
		
		postModel.setPost_title(postDetail.getPost_title());
		postModel.setPost_desc(postDetail.getPost_desc());
		postModel.setUser(userModel);
		postModel.setPost_publish_type(request.getParameter("publish"));
		
		postModel.setPost_cr_date(new Date());
		postModel.setPost_status("Active");
		postModel.setPost_publish_type("all");
		
		List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
		
		if(attachType.equals("file"))
		{
			if(fileCollection.size()>0)
			{
				postModel.setPost_contain("Yes");
				postModel.setDownload_privilage(postDetail.getDownload_privilage());
			}
			else
			{
				postModel.setPost_contain("No");
				postModel.setDownload_privilage("No");
			}
		}
		else if(attachType.equals("url"))
		{
			attachUrl = request.getParameter("url");
			postModel.setPost_contain("Yes");
			postModel.setDownload_privilage("No");
		}
		else
		{
			postModel.setPost_contain("No");
			postModel.setDownload_privilage("No");
		}
		
		// SAVING THE POST RECORD DATA
		
		status = userDao.saveUserPostDetails(postModel);
		
		if(status.equals("success"))
		{
			int postId = userDao.getPostIdViaPostDetails(postModel);
			
			postModel.setPost_id(postId);
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
	 	    String filePath = fileResource.getString("userPostDirectory");
	 	    		
	 	    File ourPath = new File(filePath);
	 	    
	 	    String storePath = ourPath.getPath()+"/"+postModel.getUser().getUser_id()+"/"+postId;	 	    
	 	    
	 	    // IF FILE EXIST WITH THE POST THEN CAPTURING IN THE STORAGE
	 	    
	 	    
	 	         // MOVING THE FILE FROM TEMP DIRECTORY TO EVENT DIRECTORY
			     
			 	   if(attachType.equals("file"))
					{
					 	   if (postModel.getPost_contain().equals("Yes"))
					       {
					 				for(int i=0;i<fileCollection.size();i++)
					 				{
					 					UserPostFile fileModel = new UserPostFile();
					 					
					 					fileName = fileCollection.get(i).getFile_name();
					 					fileReName = fileName.replaceAll(" ","_");  
						                
						                fileModel.setPost_file_type(fileCollection.get(i).getFile_type()); 
						                fileModel.setPost_file_name(fileReName);
						                fileModel.setFile_cr_date(new Date());
						                fileModel.setPost(postModel);
						                
						                // TRANSFER THE FILE TO THE ACTUAL POST LOCATION
						                
					 					String tempDirectory = fileResource.getString("tempPostFileDirectory");
					 					tempDirectory = tempDirectory+"/"+fileName;
					 					
					 					String changeDirectory = storePath;
					 					File changeFolder = new File(changeDirectory);
					 					changeFolder.mkdirs();
					 					
					 					File currentfile = new File(tempDirectory);
					 			         
					 			        // renaming the file and moving it to a new location
					 			        if(currentfile.renameTo(new File(changeDirectory+"/"+fileName)))
					 			        {
					 			            // if file copied successfully then delete the original file
					 			        	currentfile.delete();
					 			            LOGGER.info("File moved successfully");
					 			        }
					 			        else
					 			        {
					 			        	LOGGER.info("Failed to move the file");
					 			        }
					 			        
					 			        // SAVE THE POST FILE RECORD 
					 			        
					 			        userDao.savePostFileDetails(fileModel);
					 			        
					 			        if(fileModel.getPost_file_type().equals("Video"))
					                    {
					                    	videoRender.convertImageFromVideo(storePath,fileReName);
					                    }
					                    else if(fileModel.getPost_file_type().equals("Pdf"))
					                    {
					                    	videoRender.saveFirstPageThumbnail(storePath,fileReName);
					                    }
					 			       
					 			       fileModel = null;
					 				}
					       }		
			 				
					}	
			 	    else if(attachType.equals("url"))
					{
						UserPostFile fileModel = new UserPostFile();
						
						attachUrl = embedVideoId.getYoutubeEmbedIdFromUrl(attachUrl);
						
			 	    	fileModel.setPost_file_type("URL"); 
		                fileModel.setPost_file_name(attachUrl);
		                fileModel.setFile_cr_date(new Date());
		                fileModel.setPost(postModel);
			 	    	
		                userDao.savePostFileDetails(fileModel);
					}
					else
					{
						// No need to do operation save post file operation.
					}
	 				
	 	   
	 	   adminDao.deleteAllTempFile(session);
	 	    
		}
		       
		return status;
	}
	
	
	public List<UserPostModel> getAllPost(int userId,String forWhom,String postType)
	{
		return userDao.getAllPost(userId,forWhom,postType);
	}
	
	public List<UserPostModel> getFilterFeeds(int userId,String selectValue)
	{
		return userDao.getFilterFeeds(userId,selectValue);
	}
	
	public List<UserPostModel> getPostsUnderCondition(String postType)
	{
		return userDao.getPostsUnderCondition(postType);
	}
	
	public List<UserPostInterestModel> getAllPostInterest()
	{
		return userDao.getAllPostInterest();
	}
	
	public String savePostComment(UserPostCommentBean postComment,int postId,int userId,MultipartHttpServletRequest request,HttpSession session)
	{
		String status = "";
		String imageFormat = "JPG,jpg,jpeg,JPEG,gif,GIF,png,PNG";
		String videoFormat = "mp4,MP4,3gp,3GP,flv,FLV,avi,AVI,mpeg,MPEG";
		String docFormat = "PDF,pdf";
		String fileAvailable = "";
		
		UserRegisterModel user = new UserRegisterModel();
		user.setUser_id(userId);
		UserPostModel post = new UserPostModel();
		post.setPost_id(postId);
		
		UserPostCommentModel comment = new UserPostCommentModel();
		
		comment.setComment_cr_date(new Date());
		comment.setComment_message(postComment.getPost_comment());
		comment.setUser(user);
		comment.setPost(post);
		comment.setCommentt_status("Active");
		
		List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
		
		if(fileCollection.size()>0)
		{
			comment.setComment_contain("Yes");
		}
		else
		{
			comment.setComment_contain("No");
		}
		
		
		// SAVE COMMENT DETAILS
		
		status = userDao.savePostComment(comment);
		
		if(status.equals("success"))
		{
			// CAPTURE COMMENT FILE DETAILS
			
			String fileName = "";
			String extension = "";
			String fileReName = "";
			int commentId = userDao.getLastCommentId(userId);
			
			int postedUserId = userDao.getUserIdViaPostId(postId);
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
	 	    String filePath = fileResource.getString("userPostDirectory");
	 	    		
	 	    File ourPath = new File(filePath);
	 	    
	 	    String storePath = ourPath.getPath()+"/"+postedUserId+"/"+postId+"/CommentFile";	 	    
	 	    
	 	    // IF FILE EXIST IN THE TEMP FILE MOVED TO THE ACTUAL FILE DIRECTORY
	 	    
	 	    if (comment.getComment_contain().equals("Yes"))
	        {	 	    		 	    	
	 				for(int i=0;i<fileCollection.size();i++)
	 				{
	 					UserPostCommentFileModel cmtFileModel = new UserPostCommentFileModel();
	 					
	 					fileName = fileCollection.get(i).getFile_name();
		            	extension=fileName.substring(fileName.lastIndexOf('.')+1);
		                fileReName = fileName.replaceAll(" ","_");  
		                
		                LOGGER.info("File Transfering in the system.");
		                
		                if(imageFormat.contains(extension))
		                {
		                	cmtFileModel.setComment_file_type("Image");
		                }
		                else if(videoFormat.contains(extension))
		                {
		                	cmtFileModel.setComment_file_type("Video");
		                	cmtFileModel.setComment_file_render("");
		                }
		                else if(docFormat.contains(extension))
		                {
		                	cmtFileModel.setComment_file_type("Pdf");
		                }
		                else{ }
		                
		                cmtFileModel.setComment_file(fileReName);
		                cmtFileModel.setComment_file_cr_date(new Date());
		                comment.setComment_id(commentId);
		                cmtFileModel.setComment(comment);
		                
		                
		                // TRANSFER THE FILE TO THE ACTUAL COMMENT LOCATION
		                
	 					String tempDirectory = fileResource.getString("tempPostFileDirectory");
	 					tempDirectory = tempDirectory+"/"+fileName;
	 					
	 					String changeDirectory = storePath;
	 					File changeFolder = new File(changeDirectory);
	 					changeFolder.mkdirs();
	 					
	 					File currentfile = new File(tempDirectory);
	 			         
	 			        // renaming the file and moving it to a new location
	 			        if(currentfile.renameTo(new File(changeDirectory+"/"+fileName)))
	 			        {
	 			            // if file copied successfully then delete the original file
	 			        	currentfile.delete();
	 			            LOGGER.info("File moved successfully for comment file");
	 			        }
	 			        else
	 			        {
	 			        	LOGGER.info("Failed to move the file for comment file");
	 			        }
	 			        
	 			        // SAVE THE POST FILE RECORD 
	 			        
	 			        userDao.saveCommentFileDetails(cmtFileModel);
	                    
	                    if(cmtFileModel.getComment_file_type().equals("Video"))
	                    {
	                    	videoRender.convertImageFromVideo(storePath,fileReName);
	                    }
	                    else if(cmtFileModel.getComment_file_type().equals("Pdf"))
	                    {
	                    	videoRender.saveFirstPageThumbnail(storePath,fileReName);
	                    }
	 			        
	 				}
	        }
	 	    
	 	    
	 	    // END OF MOVING FILE FROM TEMP DIRECTORY TO ACTUAL COMMENT DIRECTORY
	 	    
	 	   adminDao.deleteAllTempFile(session);
	 	    
		}
		
		
		return status;
	}
	
	
	public List<EventModel> getEventList()
	{
		return userDao.getEventList();
	}
	
	public List<EventModel> getEventRecordViaCode(String eventCode)
	{
		return userDao.getEventRecordViaCode(eventCode);
	}
	
	public String checkUserExistOnUpdate(int userId,String checkValue,String checkType)
	{
		return userDao.checkUserExistOnUpdate(userId,checkValue,checkType);
	}
	
	public String updateUserRegister(UserRegisterModel userRegDetails)
	{		
		
		return userDao.updateUserRegister(userRegDetails);
	}
	
	public String changeUserPassword(int userId,HttpServletRequest request)
	{
		String status = "";
		
		String curPassword = request.getParameter("curPassword");
		String newPassword = request.getParameter("newPassword");
		
		try {
			
			curPassword = pwGenerator.encrypt(curPassword);
			newPassword = pwGenerator.encrypt(newPassword);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			LOGGER.error("Can cause password encryption");
			
			e.printStackTrace();
		}
		
		status = userDao.changeUserPassword(userId,curPassword,newPassword);
		
		return status;
	}
	
	public Map<String, List<String>> getFeedMenus()
	{
		Map<String, List<String>> objMap = new HashMap<String, List<String>>();
		
		List<Date> dateList = userDao.getFeedMenus();
		
		for(int i=0;i<dateList.size();i++)
		{
			int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(dateList.get(i)));
			String textYear = year+"";
		}
		
		return objMap;
	}
	
	public List<EventModel> getEventRecordViaId(int eventId)
	{
		return userDao.getEventRecordViaId(eventId);
	}
	
	public String saveApplicaitionPassToPayment(EventApplicationModel evntApplication,HttpSession session,HttpServletRequest request)
	{
		String status = "";
		
		RandomNoGenerator randomNumber = new RandomNoGenerator();
		
		evntApplication.setApplied_date(new Date());
		evntApplication.setApplicant_session_id(session.getId());
		
		try {
			
			evntApplication.setApplicant_order_id("RNI"+randomNumber.generatePin());
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(evntApplication.getApplicant_amount()>0)
		{
			evntApplication.setApplicant_status("Pending");		
		}
		else
		{
			evntApplication.setApplicant_status("Success");		
		}
			evntApplication.setApplicant_amount(Float.parseFloat(request.getParameter("applicant_amount")));
		
		try {
			
			evntApplication.setApplicant_ip_address(ipMacAddress.getIPddress(request));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*generatorObject
		evntApplication.saveApplicaitionPassToPayment(evntApplication, session, request)
		*/
		status = userDao.saveApplicaitionPassToPayment(evntApplication);
		
		if(status.equals("success"))
		{
			session.setAttribute("application_id", userDao.getApplicationId(evntApplication));
		}
		
		return status;
	}
	
	public String getUpdateEventApplication(EventApplicationModel evntApplication,HttpServletRequest request)
	{
		/*String workingKey = "FE9A3B3E41741770D640C1324BB22053";		// FOR TEST
		
		//String workingKey = "D3C69242B88468C94B1452361AB1331F";   // FOR LIVE
		
		String encResp= request.getParameter("encResp");
		AesCryptUtil aesUtil=new AesCryptUtil(workingKey);
		String decResp = aesUtil.decrypt(encResp);
		StringTokenizer tokenizer = new StringTokenizer(decResp, "&");
		Hashtable hs=new Hashtable();
		String pair=null, pname=null, pvalue=null;
		
		while (tokenizer.hasMoreTokens()) {
			pair = (String)tokenizer.nextToken();
			if(pair!=null) {
				StringTokenizer strTok=new StringTokenizer(pair, "=");
				pname=""; pvalue="";
				if(strTok.hasMoreTokens()) {
					
					pname=(String)strTok.nextToken();
					
					// status reference_no amount reason error_code success_count
					
					if(strTok.hasMoreTokens())
					{	pvalue=(String)strTok.nextToken();
						hs.put(pname, pvalue);
					}
					
					if(pname.equals("order_status"))
					{							
						evntApplication.setApplicant_status(pvalue);
					}
					if(pname.equals("tracking_id"))
					{
						evntApplication.setApplicant_transaction_id(pvalue);
					}
					
					if(pname.equals("status_message"))
					{
						evntApplication.setApplicant_transaction_reason(pvalue);
					}
					
					if(pname.equals("status_code"))
					{
						evntApplication.setApplicant_transaction_error_code(pvalue);
					}
					
					LOGGER.info("Param=="+pname+"=="+pvalue);
					
				}
			}
		}
		
		String status = userDao.getUpdateEventApplication(evntApplication);
		*/
		/*if(status.equals("success"))
		{
			if(evntApplication.getApplicant_status().equals("Success"))
			{
				String repositoryFile = "resources/eventNotification";

				ResourceBundle resource = ResourceBundle.getBundle(repositoryFile);
			 	 
				String mailSubject = resource.getString("mailSubject");
				String defaultUrl = resource.getString("defaultUrl");
				
				String reasonFor = "Order";
				
				String extraInfo = evntApplication.getApplicant_address()+"/"+evntApplication.getApplicant_city()+","+evntApplication.getApplicant_state()+","+evntApplication.getApplicant_pincode()+"/"+evntApplication.getApplicant_amount()+"/"+evntApplication.getApplicant_status()+"/"+evntApplication.getApplicant_transaction_id()+"/"+evntApplication.getApplicant_order_id();
						
				String link = defaultUrl;
				
				String emailBody = emailUtil.emailNotification(evntApplication.getApplicant_name(),evntApplication.getApplicant_email(),repositoryFile,link,"Home",reasonFor,extraInfo);
				
				String mailStatus = "";
				
				  SENDING MAIL 
			    
				    EmailSentUtil emailStatus = new EmailSentUtil();
		 	        
					try 
					{
						mailStatus = emailStatus.getEmailSent(evntApplication.getApplicant_email(),mailSubject,emailBody);
					} 
					catch (Exception e) {
						
						// TODO Auto-generated catch block
						
						mailStatus = "failed";
						LOGGER.error("Some exception arise while sending mail");
						e.printStackTrace();
					}
				
			      END OF SENDING MAIL 
				
				if(!mailStatus.equals("success"))
				{
					EmailModel emailDetail = new EmailModel();
					
					emailDetail.setEmail_id(evntApplication.getApplicant_email());
					emailDetail.setEmail_subject(mailSubject);
					emailDetail.setEmail_content(emailBody);
					emailDetail.setEmail_status("failed");
					emailDetail.setCr_date(new Date());
					
					status = adminDao.getSaveEmailDetails(emailDetail);
				}			
			}
		}
		
		return status;
		
		*/
		
		List l = userDao.getEventDetails(evntApplication.getApplication_id());
		Iterator it  = l.iterator();
		
		while(it.hasNext())
		{
			System.out.println("element==="+it.next());
		}
		
		String repositoryFile = "resources/eventNotification";

		ResourceBundle resource = ResourceBundle.getBundle(repositoryFile);
	 	 
		String mailSubject = resource.getString("mailSubject");
		String defaultUrl = resource.getString("defaultUrl");
		
		String reasonFor = "Order";
		
		
		String extraInfo = evntApplication.getApplicant_address()+"/"+evntApplication.getApplicant_city()+","+evntApplication.getApplicant_state()+","+evntApplication.getApplicant_pincode()+"/"+evntApplication.getApplicant_amount()+"/"+evntApplication.getApplicant_status()+"/"+evntApplication.getApplicant_transaction_id()+"/"+evntApplication.getApplicant_order_id();
				
		String link = defaultUrl;
		
		String emailBody = emailUtil.emailNotification(evntApplication.getApplicant_name(),evntApplication.getApplicant_email(),repositoryFile,link,"Home",reasonFor,extraInfo);
		
		String mailStatus = "";
		
		/* SENDING MAIL */
	     
		    EmailSentUtil emailStatus = new EmailSentUtil();
 	        
			try 
			{
				System.out.println("Mail sart to send=="+evntApplication.getApplicant_email());
				mailStatus = emailStatus.getEmailSent(evntApplication.getApplicant_email(),mailSubject,emailBody);
				System.out.println("Mail end for send");
			} 
			catch (Exception e) {
				
				// TODO Auto-generated catch block
				mailStatus = "failed";
				System.out.println("Some exception arise while sending mail==="+e.getMessage());
				e.printStackTrace();
			}
		 
	     /* END OF SENDING MAIL */
		
		
			LOGGER.info("Email send successfully");
			
		return "success";
	}
	
	
	public List<EventApplicationModel> getApplicationDetails(int applicationId)
	{
		return userDao.getApplicationDetails(applicationId);
	}
	
	public String myParticipatedEventList(int userId)
	{
		String eventList = "";
		
		try
		{
			List<EventApplicationModel> applicationList = userDao.myParticipatedEventList(userId);
			
			if(applicationList.size()>0)
			{
				for(int i =0;i<applicationList.size();i++)
				{
					eventList += applicationList.get(i).getEvent().getEvent_id() + ",";
				}
				
				if(!eventList.equals(""))
				{
					eventList = eventList.substring(0, eventList.length()-1);
				}
				
				LOGGER.info("Got the result checking the user participated event.");
			}
			else
			{
				eventList = "0";
			}			
		}
		catch(Exception e)
		{
			LOGGER.error("Exception Rise checking the user participated event.");
		}
		
		return eventList;
	}
	
	public List<EventApplicationModel> getEventAppliedForm(int userId,int eventId)
	{
		return userDao.getEventAppliedForm(userId,eventId);
	}
	
	public String saveUserProfileData(UserProfileModel userProfile,MultipartHttpServletRequest request)
	{
		String status = "";
		String fileName = "";
		
		MultipartFile multipartFile = request.getFile("file");
	    fileName = multipartFile.getOriginalFilename();
	    
	    if(!fileName.equals(""))
	    {
	    	fileName = fileName.replaceAll(" ","_");  
	    }
	     
	    userProfile.setCr_date(new Date());
	    
	    String areaOfInterest = request.getParameter("areaOfInterest");
	    if(areaOfInterest.contains("/"))
	    {
	    	areaOfInterest = areaOfInterest.substring(0, areaOfInterest.length()-1);
	    	userProfile.setArea_of_interest(areaOfInterest);
	    	
	    	if(areaOfInterest.contains("Other"))
		    {
		    	userProfile.setOther_area_of_interest(request.getParameter("otherInterest"));
		    }
		    else
		    {
		    	userProfile.setOther_area_of_interest("");
		    }
	    }
	    else
	    {
	    	userProfile.setArea_of_interest(areaOfInterest);
	    	userProfile.setOther_area_of_interest("");
	    }
	    
	    
		userProfile.setProfile_photo(fileName);
		
		String dob = request.getParameter("dob");
		
        try
        {
        	userProfile.setDob(cnvrtDate.convertStringIntoDate(dob));	                      
        }
        catch (Exception ex)
        {
            LOGGER.info("Exception "+ex);
        }
		
        
		status = userDao.saveUserProfileData(userProfile);
		
		if(status.equals("success"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
	 	    String filePath = fileResource.getString("userProfileDirectory");
	 	    		
	 	    File ourPath = new File(filePath);
	 	    
	 	    String storePath = ourPath.getPath()+"/"+userProfile.getUser().getUser_id()+"/";	 	    
	 	    
	        if(!fileName.equals(""))
			{	    				
		        File fld = new File(storePath);
		    	
		        if(!fld.exists())
		    	{
				    fld.mkdirs();
		    	}
		        try 
		        {
	            	multipartFile.transferTo(new File(storePath+fileName));
				} 
		        catch (Exception e) 
		        {
					// TODO Auto-generated catch block
					e.printStackTrace();					                    
	            }		        
	 	        // End of saving File
			}	        
		}
		
		return status;
	}
	
	
	public String updateUserProfileData(UserProfileModel userProfile,MultipartHttpServletRequest request)
	{
		
		String status = "";
		String fileName = "";
		
		MultipartFile multipartFile = request.getFile("file");
	    fileName = multipartFile.getOriginalFilename();
	    
	    if(!fileName.equals(""))
	    {
	    	fileName = fileName.replaceAll(" ","_");  
	    }
	    else
	    {
	    	/*fileName = userDao.getUserProfilePhoto(userProfile);*/
	    	userProfile.setProfile_photo(userDao.getUserProfilePhoto(userProfile));
	    }
	    
	    userProfile.setCr_date(new Date());
	    userProfile.setUpdate_date(new Date());
	    
	    String areaOfInterest = request.getParameter("areaOfInterest");
	    if(areaOfInterest.contains("/"))
	    {
	    	areaOfInterest = areaOfInterest.substring(0, areaOfInterest.length()-1);
	    	userProfile.setArea_of_interest(areaOfInterest);
	    	
	    	if(areaOfInterest.contains("Other"))
		    {
		    	userProfile.setOther_area_of_interest(request.getParameter("otherInterest"));
		    }
		    else
		    {
		    	userProfile.setOther_area_of_interest("");
		    }
	    }
	    else
	    {
	    	userProfile.setArea_of_interest(areaOfInterest);
	    	userProfile.setOther_area_of_interest("");
	    }
	    
		
	    String dob = request.getParameter("dob");
		
        try
        {
        	userProfile.setDob(cnvrtDate.convertStringIntoDate(dob));	                      
        }
        catch (Exception ex)
        {
            LOGGER.info("Exception "+ex);
        }
		
		status = userDao.saveUserProfileData(userProfile);
		
		if(status.equals("success"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
	 	    String filePath = fileResource.getString("userProfileDirectory");
	 	    		
	 	    File ourPath = new File(filePath);
	 	    
	 	    String storePath = ourPath.getPath()+"/"+userProfile.getUser().getUser_id()+"/";	 	    
	 	    
	        if(!fileName.equals(""))
			{	    				
		        File fld = new File(storePath);
		    	
		        if(!fld.exists())
		    	{
				    fld.mkdirs();
		    	}
		        try 
		        {
	            	multipartFile.transferTo(new File(storePath+fileName));
				} 
		        catch (Exception e) 
		        {
					// TODO Auto-generated catch block
					e.printStackTrace();					                    
	            }		        
	 	        // End of saving File
			}
	        
		}
		
		return status;
	}
	
	
	public List<UserPostCommentModel> getAllPostComment(int postId)
	{
		return userDao.getAllPostComment(postId);
	}
	
	public String showInterest(String interest,int postId,int userId)
	{
		String status = "";
		
		UserPostInterestModel interestModel = new UserPostInterestModel();
		
		UserRegisterModel user = new UserRegisterModel();
		user.setUser_id(userId);
		UserPostModel post = new UserPostModel();
		post.setPost_id(postId);
		
		interestModel.setInterest(interest);
		interestModel.setInterest_date(new Date());
		interestModel.setUser(user);
		interestModel.setPost(post);
		
		List<UserPostInterestModel> data = userDao.checkInterest(interestModel);
		
		if(data.size()>0)
		{
			if(data.get(0).getInterest().equals(interestModel.getInterest()))
			{				
				status =  userDao.removeInterest(interestModel);		
				if(status.equals("success"))
				{
					LOGGER.info("UPdate on post interest count.");
					long totalCount = userDao.countInterest(interest,postId);
					LOGGER.info("Total Count of this postid="+postId+"--Total Count--"+totalCount);
					
					String updateInterest = userDao.updatePostInterest(interest,userId,totalCount);					
				}
			}
			else
			{
				status =  userDao.updateInterest(interestModel);
				if(status.equals("success"))
				{
					LOGGER.info("UPdate on post interest count.");
					long totalCount = userDao.countInterest(interest,postId);
					LOGGER.info("Total Count of this postid="+postId+"--Total Count--"+totalCount);
					
					String updateInterest = userDao.updatePostInterest(interest,userId,totalCount);					
				}
			}
		}
		else
		{
			status =  userDao.saveInterest(interestModel);		
			if(status.equals("success"))
			{
				LOGGER.info("UPdate on post interest count.");
				long totalCount = userDao.countInterest(interest,postId);
				LOGGER.info("Total Count of this postid="+postId+"--Total Count--"+totalCount);
				String updateInterest = userDao.updatePostInterest(interest,userId,totalCount);				
			}
		}
		
		return status;
	}
	
	public long countInterest(String interest,int postId)
	{
		return userDao.countInterest(interest,postId);
	}
	
	public long calculateViewPost(HttpSession session,HttpServletRequest request)
	{
		long countView = 0;
		String status =  "";
		
		Date startTime = (Date)session.getAttribute("viewStartTime");
		Date endTime = (Date)session.getAttribute("viewEndTime");
		int postId = Integer.parseInt(request.getParameter("postId"));
		
		long diffSeconds = (endTime.getTime()-startTime.getTime())/1000;
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
 	    
 	    int duration = Integer.parseInt(fileResource.getString("viewDuration"));
 	    
 	   LOGGER.info("duration in seconds="+diffSeconds);
 	   
		if(diffSeconds>=duration)
		{			
			LOGGER.info("increase the ciew Count");
			status =  userDao.increaseCountViewPost(postId);
		}
		
		if(status.equals("success"))
		{
			countView = userDao.getCountViewPost(postId);
		}
		
		return countView;
	}
	
	
	public String saveSubscriptionRequest(String emailId)
	{
		SubscriptionRequestModel subObj = new SubscriptionRequestModel();
		
		String checkEmailId = userDao.checkEmailIdForNewsLetter(emailId);
		
		if(checkEmailId.equals("No Exist"))
		{
			subObj.setCr_date(new Date());
			subObj.setStatus("Active");
			subObj.setEmail_id(emailId);
			
			return userDao.saveSubscriptionRequest(subObj);
		}
		else
		{
			return "Exist";
		}
	}
	
	public long getCountPostComment(int postId)
	{
		return userDao.getCountPostComment(postId);
	}
	
	public List<UserRegisterModel> userSearchList(String searchValue)
	{
		List<UserRegisterModel> modelList = new ArrayList<UserRegisterModel>();
		
		modelList = userDao.userSearchList(searchValue);
		
		for(int i=0;i<modelList.size();i++)
		{
			LOGGER.info("User Result="+modelList.get(i).getFirst_name()+"---"+modelList.get(i).getEmail_id()+"---"+modelList.get(i).getMobile_no());
			if(modelList.get(i).getProfile()!=null)
			{
				LOGGER.info("Profile Result="+modelList.get(i).getProfile().getPractice_at());
			}
			else
			{
				LOGGER.info("Profile Result=Not exist");
			}
			
			LOGGER.info("-----");
		}
		
		return modelList;
	}
	
	
	public List<String> makeZipPostFile(int postId,int userId)
	{
		String fileDir = "";
		List<String> fileList = new ArrayList<String>();
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
		
		fileDir = fileResource.getString("userPostDirectory");
		fileDir = fileDir+"/"+userId+"/"+postId;		
		
		List<UserPostModel> postData = userDao.getPostFileName(postId);
		
		if(postData.size()>0)
		{
			Set<UserPostFile> postFile = postData.get(0).getPostFile();
			
			for (Iterator<UserPostFile> it = postFile.iterator(); it.hasNext();) 
			{				  
				 UserPostFile quModel= (UserPostFile)it.next();
				 
				 fileList.add(quModel.getPost_file_name());
			}
		}
		
		return fileList;
	}
	
	public String getYearListForPost()
	{
		String yearString = "";
		
		List<Long> yearList = userDao.getYearListForPost();
		
		if(yearList.size()>0)
		{
			for(int i=0;i<yearList.size();i++)
			{
				yearString += yearList.get(i) + "/";
			}
			
			if(yearString.length()>0)
			{
				yearString = yearString.substring(0, yearString.length()-1);
			}
		}
		else
		{
			yearString = "No Record";
		}

		return yearString;
	}
	
	public String getMontListForPost(String year)
	{
		String monthString = "";
		
		List<Integer> monthList = userDao.getMontListForPost(year);
		
		Map<Integer, String> map = new HashMap<Integer, String>();
	   
		map.put(1, "January");
	    map.put(2, "February");
	    map.put(3, "March");
	    map.put(4, "April");
	    map.put(5, "May");
	    map.put(6, "June");
	    map.put(7, "July");
	    map.put(8, "August");
	    map.put(9, "September");
	    map.put(10, "October");
	    map.put(11, "November");
	    map.put(12, "December");
	    
		if(monthList.size()>0)
		{
			for(int i=0;i<monthList.size();i++)
			{
				monthString += map.get(monthList.get(i)) + "/";
			}
			
			if(monthString.length()>0)
			{
				monthString = monthString.substring(0, monthString.length()-1);
			}
		}
		else
		{
			monthString = "No Record";
		}
		
		LOGGER.info("Month List="+monthString);
		
		return monthString;
	}
	
	
	public List<UserPostModel> getFilterFeedsViaDuration(int userId,HttpServletRequest request)
	{
		List<UserPostModel> postModel = new ArrayList<UserPostModel>();
		
	    Map<Integer, String> map = new HashMap<Integer, String>();
		   
		map.put(1, "January");
	    map.put(2, "February");
	    map.put(3, "March");
	    map.put(4, "April");
	    map.put(5, "May");
	    map.put(6, "June");
	    map.put(7, "July");
	    map.put(8, "August");
	    map.put(9, "September");
	    map.put(10, "October");
	    map.put(11, "November");
	    map.put(12, "December");
	    
	    
	    for(int i=1;i<13;i++)
		{
	    	if(map.get(i).equals(request.getParameter("month")))
	    	{
	    		request.setAttribute("monthSequence",i);
	    	}	    	
		}
	    
		postModel = userDao.getFilterFeedsViaDuration(userId,request);
		 
		return postModel; 
	}
	
	
	
	
	public String submitCaseComment(FormBean formBean,MultipartHttpServletRequest request,HttpSession session)
	{
		String status = "";
		int userId = (Integer) session.getAttribute("userId");
		int caseId = Integer.parseInt(formBean.getContentString2());
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
 	    String storePath = fileResource.getString("caseDirectory");
 	    
 	    List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
 	   
 	    UserRegisterModel user = new UserRegisterModel();
		user.setUser_id(userId);
		
		CaseModel caseModel = new CaseModel();
		caseModel.setCase_id(Integer.parseInt(formBean.getContentString2()));
		
 	    CaseUserCommentModel caseComment = new CaseUserCommentModel();
 	   
 	    caseComment.setCase_comment_message(formBean.getContentString1());
 	    caseComment.setCase_comment_cr_date(new Date());
 	    caseComment.setCase_commentt_status("Active");
 	    caseComment.setCaseModel(caseModel);
 	    caseComment.setUser(user);
 	    
 	    if(fileCollection.size()>0)
 	    {
 	    	caseComment.setCase_comment_contain("Yes");
 	    }
 	    else
 	    {
 	    	caseComment.setCase_comment_contain("No");
 	    }
		
 	    status = userDao.submitCaseComment(caseComment);
 	    
 	    if(status.equals("success"))
 	    {
 	    	int caseCmntId = userDao.getCaseCmntId(caseComment);
 	    	caseComment.setCase_comment_id(caseCmntId);
 	    	
 	    	storePath = storePath + "/" + caseId + "/" + userId;
 	    	
 	    	for(int i=0;i<fileCollection.size();i++)
			{
 	    		CaseCommentFileModel casComtFile = new CaseCommentFileModel();
 	    		
 	    		casComtFile.setCase_comment_file(fileCollection.get(i).getFile_name());
 	    		casComtFile.setCase_comment_file_type(fileCollection.get(i).getFile_type());
 	    		casComtFile.setCase_comment_file_cr_date(new Date());
 	    		casComtFile.setCaseComment(caseComment);
 	    		
 	    		status = userDao.saveCasComtFile(casComtFile);
 	    		
 	    		if(status.equals("success"))
 	    		{	    			
 	    			// TRANSFER THE FILE TO THE ACTUAL COMENT RESPONSE LOCATION
	                
					String tempDirectory = fileResource.getString("tempPortalFileDirectory");
					tempDirectory = tempDirectory+"/"+fileCollection.get(i).getFile_name();
					
					String changeDirectory = storePath;
					File changeFolder = new File(changeDirectory);
					changeFolder.mkdirs();
					
					File currentfile = new File(tempDirectory);
			         
			        // renaming the file and moving it to a new location
			        if(currentfile.renameTo(new File(changeDirectory+"/"+fileCollection.get(i).getFile_name())))
			        {
			            // if file copied successfully then delete the original file
			        	currentfile.delete();
			            LOGGER.info("File moved successfully");
			        }
			        else
			        {
			        	LOGGER.info("Failed to move the file");
			        }
			        
			        if(casComtFile.getCase_comment_file_type().equals("Video"))
                    {
                    	videoRender.convertImageFromVideo(storePath,fileCollection.get(i).getFile_name());
                    }
                    else if(casComtFile.getCase_comment_file_type().equals("Pdf"))
                    {
                    	videoRender.saveFirstPageThumbnail(changeDirectory,fileCollection.get(i).getFile_name());
                    }
 			        
			        // SAVE THE COMMENT RESPONSE FILE RECORD 
 	    		}
			}
 	    }
 	    
 	   adminDao.deleteAllTempFile(session);
 	   
 	    return status;
	}
	
	
	
	
	
	
	
	
	
	
	// EVENT PARTICIPATION BEFORE REGISTRATION 
	
	public String indexSaveApplicaitionPassToPayment(EventApplicationModel evntApplication,HttpSession session,HttpServletRequest request)
	{
		String status = "";
		
		RandomNoGenerator randomNumber = new RandomNoGenerator();
		
		evntApplication.setApplicant_name(request.getParameter("fName")+" "+request.getParameter("lName"));
		evntApplication.setApplied_date(new Date());
		evntApplication.setApplicant_session_id(session.getId());
		evntApplication.setEvent_type("Event before reg");
		
		try {
			
			evntApplication.setApplicant_order_id("RNI"+randomNumber.generatePin());
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(evntApplication.getApplicant_amount()>0)
		{
			evntApplication.setApplicant_status("Pending");		
		}
		else
		{
			evntApplication.setApplicant_status("Success");		
		}
		
		System.out.println("Application amount-"+request.getParameter("applicant_amount"));
		
		evntApplication.setApplicant_amount(Float.parseFloat(request.getParameter("applicant_amount")));
		evntApplication.setRegistration_category(request.getParameter("reg_category"));
		
		try {
			
			evntApplication.setApplicant_ip_address(ipMacAddress.getIPddress(request));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*generatorObject
		evntApplication.saveApplicaitionPassToPayment(evntApplication, session, request)
		*/
		
		status = userDao.saveApplicaitionPassToPayment(evntApplication);
		
		if(status.equals("success"))
		{
			int application_id = userDao.getApplicationIdBeforeReg(evntApplication);
			session.setAttribute("application_id", application_id);				       
		}
		
		return status;
	}
	
	
	public String indexUpdateEventApplication(EventApplicationModel evntApplication,HttpServletRequest request)
	{
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/paymentGateway");
	    String workingKey = fileResource.getString("workingKey");
	    
		
		String encResp= request.getParameter("encResp");
		AesCryptUtil aesUtil=new AesCryptUtil(workingKey);
		String decResp = aesUtil.decrypt(encResp);
		StringTokenizer tokenizer = new StringTokenizer(decResp, "&");
		Hashtable hs=new Hashtable();
		String pair=null, pname=null, pvalue=null;
		
		while (tokenizer.hasMoreTokens()) {
			pair = (String)tokenizer.nextToken();
			if(pair!=null) {
				StringTokenizer strTok=new StringTokenizer(pair, "=");
				pname=""; pvalue="";
				if(strTok.hasMoreTokens()) {
					
					pname=(String)strTok.nextToken();
					
					// status reference_no amount reason error_code success_count
					
					if(strTok.hasMoreTokens())
					{	pvalue=(String)strTok.nextToken();
						hs.put(pname, pvalue);
					}
					
					if(pname.equals("order_status"))
					{							
						evntApplication.setApplicant_status(pvalue);
					}
					if(pname.equals("tracking_id"))
					{
						evntApplication.setApplicant_transaction_id(pvalue);
					}
					
					if(pname.equals("status_message"))
					{
						evntApplication.setApplicant_transaction_reason(pvalue);
					}
					
					if(pname.equals("status_code"))
					{
						evntApplication.setApplicant_transaction_error_code(pvalue);
					}
					
					LOGGER.info("Param=="+pname+"=="+pvalue);
					
				}
			}
		}
		
		LOGGER.info("Stage=="+evntApplication.toString());
		
		String status = userDao.getUpdateEventApplication(evntApplication);
		
		LOGGER.info("Status=="+status);
		
		if(status.equals("success"))
		{
			if(evntApplication.getApplicant_status().equals("Success"))
			{
				LOGGER.info("Email set up started");
				
				EventApplicationModel applicantInfo = userDao.getApplicantInfo(evntApplication.getApplication_id());
				
				String repositoryFile = "resources/eventNotification";

				ResourceBundle resource = ResourceBundle.getBundle(repositoryFile);
			 	 
				String mailSubject = resource.getString("mailSubject");
				String defaultUrl = resource.getString("defaultUrl");
				
				String reasonFor = "Order";
				
				String extraInfo = applicantInfo.getApplicant_city()+","+applicantInfo.getApplicant_state()+","+applicantInfo.getApplicant_pincode()+"/"+applicantInfo.getApplicant_amount()+"/"+applicantInfo.getApplicant_status()+"/"+applicantInfo.getApplicant_transaction_id()+"/"+applicantInfo.getRegistration_category();
						
				System.out.println("Array Value-"+extraInfo);
				
				
				String link = defaultUrl;
				
				String emailBody = emailUtil.emailNotification(applicantInfo.getApplicant_name(),applicantInfo.getApplicant_email(),repositoryFile,link,"Home",reasonFor,extraInfo);
				
				String mailStatus = "";
				
				 /* SENDING MAIL */
			    
				    EmailSentUtil emailStatus = new EmailSentUtil();
		 	        
					try 
					{
						mailStatus = emailStatus.getEmailSent(applicantInfo.getApplicant_email(),mailSubject,emailBody);
					} 
					catch (Exception e) {
						
						// TODO Auto-generated catch block
						
						mailStatus = "failed";
						LOGGER.error("Some exception arise while sending mail");
						e.printStackTrace();
					}
				
			     /* END OF SENDING MAIL */
				
				if(!mailStatus.equals("success"))
				{
					EmailModel emailDetail = new EmailModel();
					
					emailDetail.setEmail_id(applicantInfo.getApplicant_email());
					emailDetail.setEmail_subject(mailSubject);
					emailDetail.setEmail_content(emailBody);
					emailDetail.setEmail_status("failed");
					emailDetail.setCr_date(new Date());
					
					status = adminDao.getSaveEmailDetails(emailDetail);
				}			
			}
		}
		
		return status;
		
	}
	
	
	public String indexSaveConfWorkShopPassToPayment(EventApplicationModel evntApplication,HttpSession session,HttpServletRequest request)
	{
		String status = "";
		
		RandomNoGenerator randomNumber = new RandomNoGenerator();
		
		evntApplication.setApplicant_name(request.getParameter("fName")+" "+request.getParameter("lName"));
		evntApplication.setApplied_date(new Date());
		evntApplication.setApplicant_session_id(session.getId());
		evntApplication.setEvent_type("Pre Conference Workshop");
		
		try {
			
			evntApplication.setApplicant_order_id("RNI"+randomNumber.generatePin());
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(evntApplication.getApplicant_amount()>0)
		{
			evntApplication.setApplicant_status("Pending");		
		}
		else
		{
			evntApplication.setApplicant_status("Success");		
		}
		
		System.out.println("Application amount-"+request.getParameter("applicant_amount"));
		
		evntApplication.setApplicant_amount(Float.parseFloat(request.getParameter("applicant_amount")));
		evntApplication.setRegistration_category(request.getParameter("reg_category"));
		
		try {
			
			evntApplication.setApplicant_ip_address(ipMacAddress.getIPddress(request));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*generatorObject
		evntApplication.saveApplicaitionPassToPayment(evntApplication, session, request)
		*/
		
		status = userDao.saveApplicaitionPassToPayment(evntApplication);
		
		if(status.equals("success"))
		{
			int application_id = userDao.getApplicationIdBeforeReg(evntApplication);
			session.setAttribute("application_id", application_id);				       
		}
		
		return status;
	}
	
	public String indexUpdateWorkshopApplication(EventApplicationModel evntApplication,HttpServletRequest request)
	{
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/paymentGateway");
	    String workingKey = fileResource.getString("workingKey");
	    
		String encResp= request.getParameter("encResp");
		AesCryptUtil aesUtil=new AesCryptUtil(workingKey);
		String decResp = aesUtil.decrypt(encResp);
		StringTokenizer tokenizer = new StringTokenizer(decResp, "&");
		Hashtable hs=new Hashtable();
		String pair=null, pname=null, pvalue=null;
		
		while (tokenizer.hasMoreTokens()) {
			pair = (String)tokenizer.nextToken();
			if(pair!=null) {
				StringTokenizer strTok=new StringTokenizer(pair, "=");
				pname=""; pvalue="";
				if(strTok.hasMoreTokens()) {
					
					pname=(String)strTok.nextToken();
					
					// status reference_no amount reason error_code success_count
					
					if(strTok.hasMoreTokens())
					{	pvalue=(String)strTok.nextToken();
						hs.put(pname, pvalue);
					}
					
					if(pname.equals("order_status"))
					{							
						evntApplication.setApplicant_status(pvalue);
					}
					if(pname.equals("tracking_id"))
					{
						evntApplication.setApplicant_transaction_id(pvalue);
					}
					
					if(pname.equals("status_message"))
					{
						evntApplication.setApplicant_transaction_reason(pvalue);
					}
					
					if(pname.equals("status_code"))
					{
						evntApplication.setApplicant_transaction_error_code(pvalue);
					}
					
					LOGGER.info("Param=="+pname+"=="+pvalue);
					
				}
			}
		}
		
		LOGGER.info("Stage=="+evntApplication.toString());
		
		String status = userDao.getUpdateEventApplication(evntApplication);
		
		LOGGER.info("Status=="+status);
		
		if(status.equals("success"))
		{
			if(evntApplication.getApplicant_status().equals("Success"))
			{
				LOGGER.info("Email set up started");
				
				EventApplicationModel applicantInfo = userDao.getApplicantInfo(evntApplication.getApplication_id());
				
				String repositoryFile = "resources/preWorkShopNotification";

				ResourceBundle resource = ResourceBundle.getBundle(repositoryFile);
			 	 
				String mailSubject = resource.getString("mailSubject");
				String defaultUrl = resource.getString("defaultUrl");
				
				String reasonFor = "Order";
				
				String extraInfo = applicantInfo.getApplicant_city()+","+applicantInfo.getApplicant_state()+","+applicantInfo.getApplicant_pincode()+"/"+applicantInfo.getApplicant_amount()+"/"+applicantInfo.getApplicant_status()+"/"+applicantInfo.getApplicant_transaction_id()+"/"+applicantInfo.getRegistration_category();
						
				System.out.println("Array Value-"+extraInfo);
				
				
				String link = defaultUrl;
				
				String emailBody = emailUtil.emailNotification(applicantInfo.getApplicant_name(),applicantInfo.getApplicant_email(),repositoryFile,link,"Home",reasonFor,extraInfo);
				
				String mailStatus = "";
				
				 /* SENDING MAIL */
			    
				    EmailSentUtil emailStatus = new EmailSentUtil();
		 	        
					try 
					{
						mailStatus = emailStatus.getEmailSent(applicantInfo.getApplicant_email(),mailSubject,emailBody);
					} 
					catch (Exception e) {
						
						// TODO Auto-generated catch block
						
						mailStatus = "failed";
						LOGGER.error("Some exception arise while sending mail");
						e.printStackTrace();
					}
				
			     /* END OF SENDING MAIL */
				
				if(!mailStatus.equals("success"))
				{
					EmailModel emailDetail = new EmailModel();
					
					emailDetail.setEmail_id(applicantInfo.getApplicant_email());
					emailDetail.setEmail_subject(mailSubject);
					emailDetail.setEmail_content(emailBody);
					emailDetail.setEmail_status("failed");
					emailDetail.setCr_date(new Date());
					
					status = adminDao.getSaveEmailDetails(emailDetail);
				}			
			}
		}
		
		return status;
		
	}
	
	
	
	
	public String saveAbstractSubmission(HttpSession session,MultipartHttpServletRequest request)
	{
		String status = "";
		
		AbstractSubmissionModel absDoc = new AbstractSubmissionModel();
		
		absDoc.setAbstract_title(request.getParameter("abstract_title"));
		absDoc.setSender_name(request.getParameter("first_name")+" "+request.getParameter("last_name"));
		absDoc.setSender_email(request.getParameter("email"));
		absDoc.setSender_mobile(request.getParameter("mobile"));
		
		String fileName = "";
		
		MultipartFile multipartFile = request.getFile("file");
	    fileName = multipartFile.getOriginalFilename();
	    
	    if(!fileName.equals(""))
	    {
	    	fileName = fileName.replaceAll(" ","_");  
	    }
	     
	    absDoc.setSender_file_name(fileName);
	    absDoc.setUploaded_date(new Date());
	    
	    status = adminDao.saveAbstractSubmission(absDoc);
	    
	    if(status.equals("success"))
	    {
	    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
	 	    String filePath = fileResource.getString("eventAbstractSubmission");
	 	    		
	 	    File ourPath = new File(filePath);
	 	    
	 	    String storePath = ourPath.getPath()+"/";	 	    
	 	    
	        if(!fileName.equals(""))
			{	    				
		        File fld = new File(storePath);
		    	
		        if(!fld.exists())
		    	{
				    fld.mkdirs();
		    	}
		        try 
		        {
	            	multipartFile.transferTo(new File(storePath+fileName));
				} 
		        catch (Exception e) 
		        {
					// TODO Auto-generated catch block
					e.printStackTrace();					                    
	            }		        
	 	        // End of saving File
			}	
	        
	        
	        // SEND MAIL FOR ABSTRACT SUBMISSION UPLOADED 
	        
	        LOGGER.info("Email set up started");
			
			String repositoryFile = "resources/abstractionSubNotify";

			ResourceBundle resource = ResourceBundle.getBundle(repositoryFile);
		 	 
			String mailSubject = resource.getString("mailSubject");
			String defaultUrl = resource.getString("defaultUrl");
			
			String reasonFor = "Abstract";
			
			String extraInfo = absDoc.getAbstract_title();
					
			System.out.println("Array Value-"+extraInfo);
			
			
			String link = defaultUrl+"partcipateOnEvent";
			
			String emailBody = emailUtil.emailNotification(absDoc.getSender_name(),absDoc.getSender_email(),repositoryFile,link,"Register for Event",reasonFor,extraInfo);
			
			String mailStatus = "";
			
			 /* SENDING MAIL */
		    
			    EmailSentUtil emailStatus = new EmailSentUtil();
	 	        
				try 
				{
					mailStatus = emailStatus.getEmailSent(absDoc.getSender_email(),mailSubject,emailBody);
				} 
				catch (Exception e) {
					
					// TODO Auto-generated catch block
					
					mailStatus = "failed";
					LOGGER.error("Some exception arise while sending mail");
					e.printStackTrace();
				}
			
		     /* END OF SENDING MAIL */
			
			if(!mailStatus.equals("success"))
			{
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(absDoc.getSender_email());
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				status = adminDao.getSaveEmailDetails(emailDetail);
			}
	    }
	    
		return status;
	}
	
	
	
	
	
	
	
	
	
}
