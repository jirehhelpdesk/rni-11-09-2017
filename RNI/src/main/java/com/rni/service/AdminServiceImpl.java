package com.rni.service;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.rni.bean.FormBean;
import com.rni.bean.NewsLetterBean;
import com.rni.bean.PortalStatus;
import com.rni.bean.RedNetGalleryBean;
import com.rni.bean.UserPostBean;
import com.rni.dao.AdminDao;
import com.rni.dao.UserDaoImpl;
import com.rni.model.AbstractSubmissionModel;
import com.rni.model.AdminDetails;
import com.rni.model.BulkMailEmailModel;
import com.rni.model.CaseFileModel;
import com.rni.model.CaseModel;
import com.rni.model.CaseUserCommentModel;
import com.rni.model.CommitteeMemberModel;
import com.rni.model.EmailModel;
import com.rni.model.EventApplicationModel;
import com.rni.model.EventImageModel;
import com.rni.model.EventModel;
import com.rni.model.FacultyModel;
import com.rni.model.IndexBannerModel;
import com.rni.model.NewsLetterAttachedFile;
import com.rni.model.NewsLetterModel;
import com.rni.model.PortalContentModel;
import com.rni.model.RedNetGalleryRecord;
import com.rni.model.RedNetGalleryRecordFile;
import com.rni.model.TempFileUploadModel;
import com.rni.model.UserPostModel;
import com.rni.model.UserRegisterModel;
import com.rni.synchronsJob.AdminSentMail;
import com.rni.util.ConvertStringToDate;
import com.rni.util.EmailNotificationUtil;
import com.rni.util.EmailSentUtil;
import com.rni.util.ImageRenderUtil;
import com.rni.util.PasswordGenerator;
import com.rni.util.SendBulkMail;
import com.rni.util.UniquePatternGeneration;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

@Service("adminService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AdminServiceImpl implements AdminService  {

	
	@Autowired
	private AdminDao adminDao;
	
	@Autowired
	private AdminSentMail mailJob;
	
	PasswordGenerator pwGenerator = new PasswordGenerator();
	
	ImageRenderUtil videoRender = new ImageRenderUtil();
	
	EmailNotificationUtil emailUtil = new EmailNotificationUtil();
	
	private static final Logger LOGGER = Logger.getLogger(AdminServiceImpl.class);
	
	public PortalStatus getUpdateOnPortalStatus()
	{
		PortalStatus statusBean = new PortalStatus();
		
		statusBean.setNoOfUser(adminDao.getPortalNoOfUser());
		statusBean.setNoOfEvent(adminDao.getPortalNoOfEvent());
		statusBean.setNoOfFaculty(adminDao.getPortalNoOfFaculty());
		statusBean.setNoOfPost(adminDao.getPortalNoOfPost());
		statusBean.setNoOfComMember(adminDao.getPortalNoOfComMember());
		
		return statusBean;
	}
	
	
	public String setAdminAccess(String access)
	{
		return adminDao.setAdminAccess(access);
	}
	
	@Transactional(readOnly = false)
	public String getValidateAdmin(String user_id,String password)
	{
		String encPassword = "";
		
		try {
			encPassword = pwGenerator.encrypt(password);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return adminDao.getValidateAdmin(user_id,encPassword);
	}
	

	public String saveEventFile(MultipartHttpServletRequest request,HttpSession session)
	{
		String status = "";
		String imageFormat = "JPG,jpg,jpeg,JPEG,gif,GIF,png,PNG";
		String videoFormat = "mp4,MP4,3gp,3GP,flv,FLV,avi,AVI,mpeg,MPEG";
		
		TempFileUploadModel tempFile = new TempFileUploadModel();
		
		String fileName = "";
		String extension = "";
		String fileReName = "";
		
		Iterator<String> itr =  request.getFileNames(); 
		
		
        MultipartFile multipartFile = request.getFile("file");
        fileName = multipartFile.getOriginalFilename();
        
        if(!fileName.equals(""))
		{
        	extension=fileName.substring(fileName.lastIndexOf('.')+1);
            fileReName = fileName.replaceAll(" ","_");  
            
            if(imageFormat.contains(extension))
            {
            	tempFile.setFile_type("Image");
            }
            else 
            {
            	tempFile.setFile_type("Video");
            }  
            
            tempFile.setFile_name(fileReName);
		}
        
        tempFile.setCr_date(new Date());
        tempFile.setUpload_for("Event");
        tempFile.setUpload_from("Admin");
        tempFile.setSession_id(session.getId());
        
        status = adminDao.saveEventFile(tempFile);
		
		if(status.equals("success"))
		{					
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
	 	    String filePath = fileResource.getString("tempFileDirectory");
	 	    		
	 	    File ourPath = new File(filePath);
	 	    
	 	    String storePath = ourPath.getPath()+"/";	 	    
	 	    
	        if(!fileName.equals(""))
			{	    				
		        File fld = new File(storePath);
		    	
		        if(!fld.exists())
		    	{
				    fld.mkdirs();
		    	}
		       
		        try 
		        {
	            	multipartFile.transferTo(new File(storePath+fileReName));
	            	
	            	if(tempFile.getFile_type().equals("Video"))
                    {
                    	videoRender.convertImageFromVideo(storePath,fileReName);
                    }
                    else if(tempFile.getFile_type().equals("Pdf"))
                    {
                    	videoRender.saveFirstPageThumbnail(storePath,fileReName);
                    }	            	
				} 
		        catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();					                    
	            }		        
	 	        // End of saving File
			}
		}
		
		return status;
	}
	
	public String getCurrentUploadedFiles(HttpSession session)
	{
		String fileList = "";
		
		List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
		
		if(fileCollection.size()>0)
		{
			for(int i=0;i<fileCollection.size();i++)
			{
				fileList += fileCollection.get(i).getFile_name() + "/";
			}
			
			if(fileList.length()>0)
			{
				fileList = fileList.substring(0, fileList.length()-1);
			}
		}
		else
		{
			fileList = "No file";
		}
		
		return fileList;
	}
	
	public String deleteTempFile(HttpServletRequest request,HttpSession session)
	{
		String status = "";
		
		status = adminDao.deleteTempFile(request,session);
		
		if(status.equals("success"))
		{			
			String fileName = request.getParameter("fileName");
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
			String fileDirectory = fileResource.getString("tempFileDirectory");
				
			fileDirectory = fileDirectory+"/"+fileName;
				
			try{

	    		File file = new File(fileDirectory);

	    		if(file.delete())
	    		{
	    			System.out.println(file.getName() + " is deleted!");
	    		}else{
	    			System.out.println("Delete operation is failed.");
	    		}
	    	}
			catch(Exception e)
			{
	    		e.printStackTrace();
	    	}
		}
		
		return status;
	}
	
	
	public String saveEventDetails(EventModel eventDetail,MultipartHttpServletRequest request,HttpSession session)
	{
		String status = "";
		
		int lastEventId = adminDao.getLastEventId();
		
		String eventCode = "EVNT"+session.getId().substring(6)+lastEventId;
		
		String eventCharge = request.getParameter("publish");
		
		eventDetail.setEvent_code(eventCode);
		eventDetail.setEvent_cr_date(new Date());
		eventDetail.setEvent_charge(eventCharge);
		eventDetail.setEvent_status("Open");
		
		
		eventDetail.setEvent_desc(request.getParameter("textAreaContent"));
		
		if(eventCharge.equals("Yes"))
		{
			eventDetail.setEvent_charge_fee(Float.parseFloat(request.getParameter("fee")));
		}
		
        try
        {
        	ConvertStringToDate dateConvert = new ConvertStringToDate();
        	
            eventDetail.setEvent_start_date(dateConvert.convertStringIntoDate(request.getParameter("event_start_date")));
            eventDetail.setEvent_end_date(dateConvert.convertStringIntoDate(request.getParameter("event_end_date")));
        }
        catch (Exception ex)
        {
            LOGGER.error("Exception "+ex.getMessage());
        }
        
		status = adminDao.saveEventDetails(eventDetail);
		
		if(status.equals("success"))
		{
			int eventId = adminDao.getEventIdViaCode(eventCode);
			
			eventDetail .setEvent_id(eventId);
			
			List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
			
			// MOVING THE FILE FROM TEMP DIRECTORY TO EVENT DIRECTORY
			
			for(int i=0;i<fileCollection.size();i++)
			{
				EventImageModel eveImageObj = new EventImageModel();
				
				String fileName = fileCollection.get(i).getFile_name();
				
				ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
		 	    
				String tempDirectory = fileResource.getString("tempFileDirectory");
				tempDirectory = tempDirectory+"/"+fileName;
				
				String changeDirectory = fileResource.getString("eventDirectory");
				changeDirectory = changeDirectory+"/"+eventId;
				File changeFolder = new File(changeDirectory);
				changeFolder.mkdirs();
				
				File currentfile = new File(tempDirectory);
		         
		        // renaming the file and moving it to a new location
		        if(currentfile.renameTo(new File(changeDirectory+"/"+fileName)))
		        {
		            // if file copied successfully then delete the original file
		        	currentfile.delete();
		            System.out.println("File moved successfully");
		        }
		        else
		        {
		            System.out.println("Failed to move the file");
		        }
		        
				eveImageObj.setFile_name(fileCollection.get(i).getFile_name());
				eveImageObj.setFile_type(fileCollection.get(i).getFile_type());
				eveImageObj.setCr_date(new Date());
				eveImageObj.setEvent(eventDetail);
				
				adminDao.saveEventImgeDetails(eveImageObj);
				
				eveImageObj = null;
			}
			
			adminDao.deleteAllTempFile(session);
			
		}
		
		return status;
	}
	
	
	public String updateEventDetail(EventModel eventDetail,MultipartHttpServletRequest request,HttpSession session)
	{
		String status = "";
		
		
		
		
		return status;
	}
	
	public List<EventModel> getLastFiveEventList()
	{
		return adminDao.getLastFiveEventList();
	}
	
	public List<EventModel> getSearchEvent(HttpServletRequest request)
	{
		return adminDao.getSearchEvent(request);
	}
	
	public List<EventModel> detailsOnEvent(int eventId)
	{
		return adminDao.detailsOnEvent(eventId);
	}
	
	public AdminDetails getAdminDetails()
	{
		AdminDetails adminObj = new AdminDetails();
		
		List<AdminDetails> listObj = adminDao.getAdminDetails();
		
		if(listObj.size()>0)
		{
			return listObj.get(0);
		}
		else
		{
			return adminObj;
		}
	}
	
	
	public List<UserPostModel> getSearchPost(HttpServletRequest request)
	{
		List<UserPostModel> postList = new ArrayList<UserPostModel>();
		
		postList = adminDao.getSearchPost(request);
		
		return postList;
	}
	
	public String getBlockUserPost(int postId,String postStatus)
	{
		return adminDao.getBlockUserPost(postId,postStatus);
	}
	
	
	public List<UserRegisterModel> getSearchUser(HttpServletRequest request)
	{
		return adminDao.getSearchUser(request);
	}
	
	public String saveFacultyDetails(FacultyModel faculty)
	{
		faculty.setFaculty_cr_date(new Date());
		
		return adminDao.saveFacultyDetails(faculty);
	}
	
	public List<FacultyModel> getFacultyList()
	{
		return adminDao.getFacultyList();
	}
	
	public String saveMemberDetails(CommitteeMemberModel member)
	{
		member.setMember_cr_date(new Date());
		
		return adminDao.saveMemberDetails(member);
	}
	
	public List<CommitteeMemberModel> getMemberList()
	{
		return adminDao.getMemberList();
	}
	
	public List<FacultyModel> getFacultyListInOrder()
	{
		return adminDao.getFacultyListInOrder();
	}
	
	public List<CommitteeMemberModel> getMemberListInOrder()
	{
		return adminDao.getMemberListInOrder();
	}
	
	public List<FacultyModel> getFacultyData(int facultyId)
	{
		return adminDao.getFacultyData(facultyId);
	}
	
	public List<CommitteeMemberModel> getMemberData(int memberId)
	{
		return adminDao.getMemberData(memberId);
	}
	
	public String deleteMemberData(int memberId)
	{
		return adminDao.deleteMemberData(memberId);
	}
	
	public String deleteFacultyData(int facultyId)
	{
		return adminDao.deleteFacultyData(facultyId);
	}
	
	
	public String updateAdminDetails(AdminDetails admin)
	{
		return adminDao.updateAdminDetails(admin);
	}
	
	public String updateAdminPassword(HttpServletRequest request,HttpSession session)
	{
		return adminDao.updateAdminPassword(request,session);
	}
	
	public String updateUserStatus(String userStatus,int userId)
	{
		return adminDao.updateUserStatus(userStatus,userId);
	}
	
	public List<EventApplicationModel> eventReportData(int eventId)
	{
		return adminDao.eventReportData(eventId);
	}
	
	public String getUserCount(String value)
	{
		String countInString = "";
		
		long count = adminDao.getUserCount(value);
		
		countInString = count+"";
		
		return countInString;
	}
	
	public String sendNewsLetter(NewsLetterBean newsLetter,MultipartHttpServletRequest request,HttpSession session)
	{
		String status = "";
		
		NewsLetterModel nwsModel = new NewsLetterModel();
		String fileNameList = "";
		
		nwsModel.setNews_sent_date(new Date());
		nwsModel.setUser_count(newsLetter.getUser_count());
		nwsModel.setSent_type(newsLetter.getSent_type());
		nwsModel.setSent_user_type(newsLetter.getSent_user_type());
		nwsModel.setNews_matter(newsLetter.getNews_matter());
		
		LOGGER.info("reached here");
		
		if(adminDao.saveNewsLetterRecord(nwsModel).equals("success"))
		{		
			int newsId = adminDao.getNewsId(nwsModel);
				
			nwsModel.setNews_id(newsId);
			
			// UPLOAD THE ATTACHED FILE THEN SEND MAIL TO USER 
			
			
			List<MultipartFile> selectedFiles = newsLetter.getFiles();
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
	 	    String filePath = fileResource.getString("newsDirectory");
	 	    	
			for(int i=0;i<selectedFiles.size();i++)
			{
				NewsLetterAttachedFile newsFile = new NewsLetterAttachedFile();
				
				String fileName = "";
				
				MultipartFile multipartFile = selectedFiles.get(i);
				
				fileName = multipartFile.getOriginalFilename();
	        	fileName = fileName.replaceAll(" ","_");  
	            
	            LOGGER.info("File Transfering in the system.");
	            
	            newsFile.setFile_name(fileName);
	            newsFile.setCr_date(new Date());
	            newsFile.setNews(nwsModel);
	            
	            File transferFile = new File(filePath, fileName);
	            
	            fileNameList += fileName + "/";
	             
	            try
	            {
	            	File fld = new File(filePath+"/");
			    	
			        if(!fld.exists())
			    	{
					    fld.mkdirs();
			    	}
			        
			        LOGGER.info("each File transfer link:"+transferFile.toString());
			        
	                multipartFile.transferTo(transferFile);
	                
	                status = adminDao.saveNewsLetterFile(newsFile);
	                
	            }catch (Exception e)
	            {
	            	LOGGER.error("While Transfering the file from user system to server exception rais.");
	                e.printStackTrace();
	            }				
			}
			
			if(status.equals("success"))
			{	
				// ALL MAIL SENT TO THE USER BACK END AUTOMATICALLY
				
				LOGGER.info("News matter="+nwsModel.getNews_matter());
				
				List<String> emailIdList = adminDao.getEmailIdList(nwsModel);
				
				mailJob.sendNewsLetterMail(emailIdList,nwsModel.getNews_matter(),fileNameList);
			}			
		}
		else
		{
			status = "error";
		}
		
		return status;
	}
	
	public List<NewsLetterModel> getNewsData()
	{
		return adminDao.getNewsData();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public List<RedNetGalleryRecord> getGalleryRecord()
	{
		return adminDao.getGalleryRecord();
	}
	
	public List<RedNetGalleryRecordFile> getGalleryFileRecord(String fileType,int galleryId)
	{
		return adminDao.getGalleryFileRecord(fileType,galleryId);
	}
	
	
	public String saveGalleryRecord(RedNetGalleryBean galleryRecord,MultipartHttpServletRequest request,HttpSession session)
	{
		String status = "";
		
		String folderName = galleryRecord.getGallery_text().replaceAll(" ","_");
		
		RedNetGalleryRecord galModel = new RedNetGalleryRecord();
		
		galModel.setGallery_text(galleryRecord.getGallery_text());
		galModel.setCr_date(new Date());
		
		List<MultipartFile> selectedFiles = galleryRecord.getFiles();
		
		if(adminDao.saveGalleryRecord(galModel).equals("success"))
		{	
			int galleryId = adminDao.getGalleryId(galModel);
			galModel.setGallery_id(galleryId);
			
			String imageFormat = "JPG,jpg,jpeg,JPEG,gif,GIF,png,PNG";
			String videoFormat = "mp4,MP4,3gp,3GP,flv,FLV,avi,AVI,mpeg,MPEG";
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
	 	    String filePath = fileResource.getString("galleryDirectory");
	 	    
	 	    //filePath = filePath + "/" + folderName;
	 	   
			for(int i=0;i<selectedFiles.size();i++)
			{
				RedNetGalleryRecordFile galFileModel = new RedNetGalleryRecordFile();
				
				String fileName = "";
				
				MultipartFile multipartFile = selectedFiles.get(i);
				
				fileName = multipartFile.getOriginalFilename();
	        	String extension=fileName.substring(fileName.lastIndexOf('.')+1);
	        	fileName = fileName.replaceAll(" ","_");  
	            
	            LOGGER.info("File Transfering in the system.");
	            
	            if(imageFormat.contains(extension))
	            {
	            	galFileModel.setFile_type("Image");
	            }
	            else if(videoFormat.contains(extension))
	            {
	            	galFileModel.setFile_type("Video");
	            }
	            else{ }
	            
	            galFileModel.setFile_name(fileName);
	            galFileModel.setCr_date(new Date());
	            galFileModel.setGallery(galModel);
	            
	            File transferFile = new File(filePath, fileName);
	            
	            try
	            {
	            	File fld = new File(filePath+"/");
			    	
			        if(!fld.exists())
			    	{
					    fld.mkdirs();
			    	}
			        
			        LOGGER.info("each File transfer link:"+transferFile.toString());
			        
	                multipartFile.transferTo(transferFile);
	                
	                status = adminDao.saveGalleryFile(galFileModel);	                	                
	            } 
	            catch (Exception e)
	            {
	            	LOGGER.error("While Transfering the file from user system to server exception rais.");
	                e.printStackTrace();
	            }	         
			}    
	            
			status = "success";
						
		}
		else
		{
			status = "error";
		}
		
		return status;
	}
	
	public String deleteGallery(int galleryId)
	{
        String status = "";
		
		List<RedNetGalleryRecord> galRecord = adminDao.getGalleryRecord(galleryId);
		
		List<RedNetGalleryRecordFile> fileList = galRecord.get(0).getGalleryFile();
		
		for(int i=0;i<fileList.size();i++)
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
			String fileDirectory = fileResource.getString("galleryDirectory");
				
			fileDirectory = fileDirectory+"/"+fileList.get(i).getFile_name();
				
			try{

	    		File file = new File(fileDirectory);

	    		if(file.delete())
	    		{
	    			status = "success";
	    			
	    		}else{
	    			LOGGER.error("Delete operation is failed.");
	    		}
	    	}
			catch(Exception e)
			{
				LOGGER.error("Delete operation is exception=."+e.getMessage());
	    		e.printStackTrace();
	    	}
		}
		
		if(status.equals("success"))
		{
			adminDao.deleteGalleryFileRecord(galleryId);
			adminDao.deleteGalleryInfoRecord(galleryId);
		}
			
		
		return status;
	}
	
	
	public String manageCaseOfDiscussion(int postId,String action)
	{
		return adminDao.manageCaseOfDiscussion(postId,action);
	}
	
	public List<IndexBannerModel> getBannerFileList()
	{
		return adminDao.getBannerFileList();
	}
	
	public String captureBrowseFiles(FormBean formBean,MultipartHttpServletRequest request,HttpSession session)
	{
		String fileNameList = "";
		String imageFormat = "JPG,jpg,jpeg,JPEG,gif,GIF,png,PNG";
		String videoFormat = "mp4,MP4";
		String docFormat = "PDF,pdf";
		
		List<MultipartFile> selectedFiles = formBean.getFiles();
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
 	    
 	    String filePath = fileResource.getString("tempPortalFileDirectory");
 	    	
		for(int i=0;i<selectedFiles.size();i++)
		{
			TempFileUploadModel tempFile = new TempFileUploadModel();
			String fileName = "";
			
			MultipartFile multipartFile = selectedFiles.get(i);
			
			fileName = multipartFile.getOriginalFilename();
        	String extension=fileName.substring(fileName.lastIndexOf('.')+1);
        	fileName = fileName.replaceAll(" ","_");  
            
            LOGGER.info("File Transfering in the system.");
            
            if(imageFormat.contains(extension))
            {
            	tempFile.setFile_type("Image");
            }
            else if(videoFormat.contains(extension))
            {
            	tempFile.setFile_type("Video");
            }
            else if(docFormat.contains(extension))
            {
            	tempFile.setFile_type("Pdf");
            }
            else{ }
            
            tempFile.setFile_name(fileName);
            tempFile.setCr_date(new Date());
            tempFile.setSession_id(session.getId());
            tempFile.setUpload_for("Banner");
            tempFile.setUpload_from("Admin");
            
            File transferFile = new File(filePath, fileName);
            
            try
            {
            	File fld = new File(filePath+"/");
		    	
		        if(!fld.exists())
		    	{
				    fld.mkdirs();
		    	}
		        
		        LOGGER.info("each File transfer link:"+transferFile.toString());
		        
                multipartFile.transferTo(transferFile);
                
                String status = adminDao.saveTemporaryBrowseFiles(tempFile);
                
                
            } catch (Exception e)
            {
            	LOGGER.error("While Transfering the file from user system to server exception rais.");
                e.printStackTrace();
            }
			
			tempFile = null;
		}
		
		// GET ALL THE LIST OF EXISTING FILE NAMES
		
		List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
		
		for(int i =0;i<fileCollection.size();i++)
		{
			fileNameList += fileCollection.get(i).getFile_name() + "/";
		}
		
		if(!fileNameList.equals(""))
		{
			fileNameList = fileNameList.substring(0,fileNameList.length()-1);
		}
		
		return fileNameList;
	}
	
	
	
	public String deleteBrowseFile(HttpServletRequest request,HttpSession session)
	{
		String fileNameList = "";
		
		String status = adminDao.deleteTempFile(request,session);
		
		if(status.equals("success"))
		{			
			// DELETE FILE FROM THE SERVER LOCATION 
			
			String fileName = request.getParameter("fileName");
			
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	    
			String fileDirectory = fileResource.getString("tempPortalFileDirectory");
				
			fileDirectory = fileDirectory+"/"+fileName;
				
			try{

	    		File file = new File(fileDirectory);

	    		if(file.delete())
	    		{
	    			LOGGER.info(file.getName() + " is deleted!");
	    		}else{
	    			LOGGER.error("Delete operation is failed.");
	    		}
	    	}
			catch(Exception e)
			{
				LOGGER.error("While deleting temporary file from post "+fileName + " exception rais!");
				
	    		e.printStackTrace();
	    	}
		}
		
		
		// GET ALL THE LIST OF EXISTING FILE NAMES
		
		List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
		
		for(int i =0;i<fileCollection.size();i++)
		{
			fileNameList += fileCollection.get(i).getFile_name() + "/";
		}
		
		if(!fileNameList.equals(""))
		{
			fileNameList = fileNameList.substring(0,fileNameList.length()-1);
		}
		
		return fileNameList;
	}
	
	
	@SuppressWarnings("unchecked")
	public String saveBannerFiles(FormBean formBean,MultipartHttpServletRequest request,HttpSession session)
	{
		String status = "";
		
		List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
 	    String storePath = fileResource.getString("bannerDirectory");
 	    
 	    
		for(int i=0;i<fileCollection.size();i++)
		{
			IndexBannerModel bannerModel = new IndexBannerModel();
			
			bannerModel.setBanner_file_name(fileCollection.get(i).getFile_name());
			bannerModel.setBanner_file_type(fileCollection.get(i).getFile_type());
			bannerModel.setBanner_cr_date(new Date());
			bannerModel.setBanner_status("Deactive");
			
			status = adminDao.saveBannerFileRecord(bannerModel);
			
			if(status.equals("success"))
			{
				   File ourPath = new File(storePath);
				
				 // TRANSFER THE FILE TO THE ACTUAL BANNER LOCATION
                
					String tempDirectory = fileResource.getString("tempPortalFileDirectory");
					tempDirectory = tempDirectory+"/"+fileCollection.get(i).getFile_name();
					
					String changeDirectory = storePath;
					File changeFolder = new File(changeDirectory);
					changeFolder.mkdirs();
					
					File currentfile = new File(tempDirectory);
			         
			        // renaming the file and moving it to a new location
			        if(currentfile.renameTo(new File(changeDirectory+"/"+fileCollection.get(i).getFile_name())))
			        {
			            // if file copied successfully then delete the original file
			        	currentfile.delete();
			            LOGGER.info("File moved successfully");
			        }
			        else
			        {
			        	LOGGER.info("Failed to move the file");
			        }
			        
			        // SAVE THE BANNER FILE RECORD 
			        
			}
		
		}
			
		adminDao.deleteAllTempFile(session);
		
		return status;
	}
	
	public List<IndexBannerModel> getAllBannerFileList()
	{
		return adminDao.getAllBannerFileList();
	}
	
	public List<CaseModel> getCaseRecord(String condition)
	{
		return adminDao.getCaseRecord(condition);
	}
	
	public String saveCaseDetail(FormBean formBean,MultipartHttpServletRequest request,HttpSession session)
	{
		String status = "";
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
 	    String storePath = fileResource.getString("caseDirectory");
 	    
		CaseModel caseModel = new CaseModel();
		
		caseModel.setCase_title(formBean.getContentString1());
		caseModel.setCase_desc(request.getParameter("caseDesc"));
		caseModel.setCase_cr_date(new Date());
		caseModel.setCase_status("Active");
		
		if(adminDao.saveCaseDetail(caseModel).equals("success"))
		{
			List<TempFileUploadModel> fileCollection = adminDao.getCurrentUploadedFiles(session);
			
			int caseId = adminDao.getCaseId(caseModel);
			caseModel.setCase_id(caseId);
			
			storePath = storePath +"/"+ caseId;
			
			for(int i=0;i<fileCollection.size();i++)
			{
				CaseFileModel caseFileModel = new CaseFileModel();
				
				caseFileModel.setCaseModel(caseModel);
				caseFileModel.setCr_date(new Date());
				caseFileModel.setFile_name(fileCollection.get(i).getFile_name());
				caseFileModel.setFile_type(fileCollection.get(i).getFile_type());
				
				status = adminDao.getSaveCaseFileModel(caseFileModel);
			
				if(status.equals("success"))
				{
					  
					 // TRANSFER THE FILE TO THE ACTUAL BANNER LOCATION
	                
						String tempDirectory = fileResource.getString("tempPortalFileDirectory");
						tempDirectory = tempDirectory+"/"+fileCollection.get(i).getFile_name();
						
						String changeDirectory = storePath;
						File changeFolder = new File(changeDirectory);
						changeFolder.mkdirs();
						
						File currentfile = new File(tempDirectory);
				         
				        // renaming the file and moving it to a new location
				        if(currentfile.renameTo(new File(changeDirectory+"/"+fileCollection.get(i).getFile_name())))
				        {
				            // if file copied successfully then delete the original file
				        	currentfile.delete();
				            LOGGER.info("File moved successfully");
				        }
				        else
				        {
				        	LOGGER.info("Failed to move the file");
				        }
				        
				        if(caseFileModel.getFile_type().equals("Video"))
	                    {
	                    	videoRender.convertImageFromVideo(storePath,fileCollection.get(i).getFile_name());
	                    }
	                    else if(caseFileModel.getFile_type().equals("Pdf"))
	                    {
	                    	videoRender.saveFirstPageThumbnail(storePath,fileCollection.get(i).getFile_name());
	                    }
				        
				        // SAVE THE BANNER FILE RECORD 
				        
				}
			}
		}
		
		
		adminDao.deleteAllTempFile(session);
		
		return status;
	}
	
	public List<CaseUserCommentModel> getUserCaseComment(int caseId)
	{
		return adminDao.getUserCaseComment(caseId);
	}
	
	
	public String changeCaseStatus(HttpServletRequest request)
	{
		return adminDao.changeCaseStatus(request);
	}
	
	public String deleteCaseDetails(HttpServletRequest request)
	{
		return adminDao.deleteCaseDetails(request);
	}
	
	public List<IndexBannerModel> getBannerRecord(int bannerId)
	{
		return adminDao.getBannerRecord(bannerId);
	}
	
	public String changeBannerStatus(HttpServletRequest request)
	{
		return adminDao.changeBannerStatus(request);
	}
	
	public String deleteBanner(HttpServletRequest request)
	{
		return adminDao.deleteBanner(request);
	}
	
	public String savePortalContent(PortalContentModel prtlContent)
	{
		prtlContent.setContent_cr_date(new Date());
		prtlContent.setContent_status("Active");
		
		return adminDao.savePortalContent(prtlContent);
	}
	
	public List<PortalContentModel> getContentList()
	{
		return adminDao.getContentList();
	}
	
	public List<PortalContentModel> getContentDetails(String condition,String contentFor,String contentType)
	{
		return adminDao.getContentDetails(condition,contentFor,contentType);
	}
	
	public String updatePortalContent(PortalContentModel prtlContent)
	{
		return adminDao.updatePortalContent(prtlContent);
	}
	
	public String deleteContent(int contentId)
	{
		return adminDao.deleteContent(contentId);
	}
	
	
	public List<EventApplicationModel> getEventRegRecord()
	{
		return adminDao.getEventRegRecord();
	}
	
	public List<EventApplicationModel> getPreConRegRecord()
	{
		return adminDao.getPreConRegRecord();
	}
	
	public List<AbstractSubmissionModel> getAbstractDocRecord()
	{
		return adminDao.getAbstractDocRecord();
	}
	
	
	public List<BulkMailEmailModel> getBulkMailEmailId()
	{
		return adminDao.getBulkMailEmailId();
	}
	
	public String bulkMailSendStatus() throws IOException
	{
		List<BulkMailEmailModel> emailList = adminDao.getBulkMailEmailId();
		
		
		
		// GENERATE FAILED MAIL LIST IN EXCEL SHEET
		   
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	String reportDirectory = fileResource.getString("mailStatusDirectory");		
	 	
	 	String fileName = "Email_sent_status.xls";
	 	
	 	fileName = fileName.replaceAll(" ","_");
	 	
	 	File exlDirectory = new File(reportDirectory);
        
        if(!exlDirectory.exists())
    	 {
       	 	exlDirectory.mkdirs();
    	 }
        
        File excelReport = new File(reportDirectory + "/" + fileName);
        
	 	WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
        
        WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
        int j = 1;
        
    	try {    		 
             // Add the created Cells to the heading of the sheet
             
             writableSheet.addCell(new Label(0, 0, "SERIAL NO"));
             writableSheet.addCell(new Label(1, 0, "EMAIL ID"));
             writableSheet.addCell(new Label(2, 0, "TIME"));
             writableSheet.addCell(new Label(3, 0, "STATUS"));             
	    } 
    	catch (Exception e) {	        	 
	        	 LOGGER.error("IOException : Exception rais while Event Report generate."+e.getMessage());
	             e.printStackTrace();
	    }
    	
    	
		SendBulkMail email = new SendBulkMail();
		int c = 0;
		
		for(int i=0;i<emailList.size();i++)
		{
			try
			{				
				if(emailList.get(i).getEmail_id().contains(";"))
				{
					String emailPattern = emailList.get(i).getEmail_id();
					String emailArray[] = emailPattern.split(";");
					
					if(emailArray.length==2)
					{
						email.bulkmail(emailArray[0], "RetNetIndia Event Invitation");
						email.bulkmail(emailArray[1], "RetNetIndia Event Invitation");
					}
					
					c = c + 1;
					
				}
				else
				{					
					email.bulkmail(emailList.get(i).getEmail_id(), "RetNetIndia Event Invitation");
					c = c + 1;
				}
				
				System.out.println("Send-"+c);
			}
			catch(Exception e)
			{
				try
				{
					 writableSheet.addCell(new Number(0, j, j));
	            	 writableSheet.addCell(new Label(1, j, emailList.get(i).getEmail_id()));
	            	 //writableSheet.addCell(new Label(2, j, eModel.get(i).getApplicant_email()));
	            	
	            	 DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	            	 Date sentTimeStamp = new Date();        
	            	 String reportDate = df.format(sentTimeStamp);

	            	 writableSheet.addCell(new Label(2, j,reportDate));	 
	            	 writableSheet.addCell(new Label(3, j, "Failed-"+"Error -message-:: "+e.getMessage()));           	
	            	 
	            	 j = j + 1;
		             
				}
				catch(Exception reportExcep)
				{
					 LOGGER.error("IOException : Exception rais while Event Report generate."+e.getMessage());
		             e.printStackTrace();
				}
				
				LOGGER.error("Error message="+e.getMessage());
				LOGGER.error("Failed email id ="+emailList.get(i).getEmail_id()+"         Time=="+new Date());
			}			
		}
		
           
        try {
			writableWorkbook.write();
			writableWorkbook.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
          

        
		String status = c +"";
		
		return status;		
	}
	
	
	
	
	
	
	
	
	
}
