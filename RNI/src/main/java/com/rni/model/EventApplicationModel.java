package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="event_application_record")
public class EventApplicationModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="application_id")
	private int application_id;
	
	@Column(name="applicant_name")
	private String applicant_name;
	
	@Column(name="applicant_address")
	private String applicant_address;
	
	@Column(name="applicant_email")
	private String applicant_email;
	
	@Column(name="applicant_mobile")
	private String applicant_mobile;
		
	@Column(name="applicant_city")
	private String applicant_city;
	
	@Column(name="applicant_state")
	private String applicant_state;
	
	@Column(name="applicant_pincode")
	private String applicant_pincode;
	
	@Column(name="applicant_affiliation")
	private String applicant_affiliation;
	
	@Column(name="applicant_council_no")
	private String applicant_council_no;
	
	@Column(name="applicant_amount")
	private float applicant_amount;
	
	@Column(name="applied_date")
	private Date applied_date;
	
	@Column(name="applicant_ip_address")
	private String applicant_ip_address;
	
	@Column(name="applicant_session_id")
	private String applicant_session_id;
	
	@Column(name="applicant_status")
	private String applicant_status;
	
	@Column(name="applicant_transaction_id")
	private String applicant_transaction_id;
	
	@Column(name="applicant_order_id")
	private String applicant_order_id;
	
	@Column(name="applicant_transaction_reason")
	private String applicant_transaction_reason;
	
	@Column(name="applicant_transaction_error_code")
	private String applicant_transaction_error_code;
	
	@Column(name="applicant_transaction_payment_mode")
	private String applicant_transaction_payment_mode;
	
	@Column(name="applicant_food_preference")
	private String applicant_food_preference;
	
	@Column(name="event_type")
	private String event_type;
	
	@Column(name="registration_category")
	private String registration_category;
	
	@Column(name="room_partner_name")
	private String room_partner_name;
	
	@Column(name="room_partner_mobile")
	private String room_partner_mobile;
	
	@Column(name="room_partner_name_2")
	private String room_partner_name_2;
	
	@Column(name="room_partner_mobile_2")
	private String room_partner_mobile_2;
	
	
	@ManyToOne
    @JoinColumn(name="user_id")
    private UserRegisterModel user;
	   
	@ManyToOne
    @JoinColumn(name="event_id")
    private EventModel event;

	
	public EventApplicationModel(){
		
	}
	

	public EventApplicationModel(int application_id, String applicant_name, String applicant_address,
			String applicant_email, String applicant_mobile, String applicant_city, String applicant_state,
			String applicant_pincode, String applicant_affiliation, String applicant_council_no, float applicant_amount,
			Date applied_date, String applicant_ip_address, String applicant_session_id, String applicant_status,
			String applicant_transaction_id, String applicant_order_id, String applicant_transaction_reason,
			String applicant_transaction_error_code, String applicant_transaction_payment_mode,
			String applicant_food_preference, String event_type, String registration_category, String room_partner_name,
			String room_partner_mobile,String room_partner_name_2,String room_partner_mobile_2, UserRegisterModel user, EventModel event) {
		super();
		this.application_id = application_id;
		this.applicant_name = applicant_name;
		this.applicant_address = applicant_address;
		this.applicant_email = applicant_email;
		this.applicant_mobile = applicant_mobile;
		this.applicant_city = applicant_city;
		this.applicant_state = applicant_state;
		this.applicant_pincode = applicant_pincode;
		this.applicant_affiliation = applicant_affiliation;
		this.applicant_council_no = applicant_council_no;
		this.applicant_amount = applicant_amount;
		this.applied_date = applied_date;
		this.applicant_ip_address = applicant_ip_address;
		this.applicant_session_id = applicant_session_id;
		this.applicant_status = applicant_status;
		this.applicant_transaction_id = applicant_transaction_id;
		this.applicant_order_id = applicant_order_id;
		this.applicant_transaction_reason = applicant_transaction_reason;
		this.applicant_transaction_error_code = applicant_transaction_error_code;
		this.applicant_transaction_payment_mode = applicant_transaction_payment_mode;
		this.applicant_food_preference = applicant_food_preference;
		this.event_type = event_type;
		this.registration_category = registration_category;
		this.room_partner_name = room_partner_name;
		this.room_partner_mobile = room_partner_mobile;
		this.room_partner_name_2 = room_partner_name_2;
		this.room_partner_mobile_2 = room_partner_mobile_2;
		this.user = user;
		this.event = event;
	}


	public int getApplication_id() {
		return application_id;
	}

	public void setApplication_id(int application_id) {
		this.application_id = application_id;
	}

	public String getApplicant_name() {
		return applicant_name;
	}

	public void setApplicant_name(String applicant_name) {
		this.applicant_name = applicant_name;
	}

	public String getApplicant_address() {
		return applicant_address;
	}

	public void setApplicant_address(String applicant_address) {
		this.applicant_address = applicant_address;
	}

	public String getApplicant_ip_address() {
		return applicant_ip_address;
	}

	public void setApplicant_ip_address(String applicant_ip_address) {
		this.applicant_ip_address = applicant_ip_address;
	}

	public String getApplicant_session_id() {
		return applicant_session_id;
	}

	public void setApplicant_session_id(String applicant_session_id) {
		this.applicant_session_id = applicant_session_id;
	}

	public String getApplicant_email() {
		return applicant_email;
	}

	public void setApplicant_email(String applicant_email) {
		this.applicant_email = applicant_email;
	}

	public String getApplicant_mobile() {
		return applicant_mobile;
	}

	public void setApplicant_mobile(String applicant_mobile) {
		this.applicant_mobile = applicant_mobile;
	}

	public String getApplicant_city() {
		return applicant_city;
	}

	public void setApplicant_city(String applicant_city) {
		this.applicant_city = applicant_city;
	}

	public String getApplicant_state() {
		return applicant_state;
	}

	public void setApplicant_state(String applicant_state) {
		this.applicant_state = applicant_state;
	}

	public String getApplicant_pincode() {
		return applicant_pincode;
	}

	public void setApplicant_pincode(String applicant_pincode) {
		this.applicant_pincode = applicant_pincode;
	}

	public String getApplicant_affiliation() {
		return applicant_affiliation;
	}

	public void setApplicant_affiliation(String applicant_affiliation) {
		this.applicant_affiliation = applicant_affiliation;
	}

	public String getApplicant_council_no() {
		return applicant_council_no;
	}

	public void setApplicant_council_no(String applicant_council_no) {
		this.applicant_council_no = applicant_council_no;
	}

	public float getApplicant_amount() {
		return applicant_amount;
	}

	public void setApplicant_amount(float applicant_amount) {
		this.applicant_amount = applicant_amount;
	}

	public Date getApplied_date() {
		return applied_date;
	}

	public void setApplied_date(Date applied_date) {
		this.applied_date = applied_date;
	}

	public UserRegisterModel getUser() {
		return user;
	}

	public void setUser(UserRegisterModel user) {
		this.user = user;
	}

	public EventModel getEvent() {
		return event;
	}

	public void setEvent(EventModel event) {
		this.event = event;
	}

	public String getApplicant_status() {
		return applicant_status;
	}

	public void setApplicant_status(String applicant_status) {
		this.applicant_status = applicant_status;
	}

	public String getApplicant_transaction_id() {
		return applicant_transaction_id;
	}

	public void setApplicant_transaction_id(String applicant_transaction_id) {
		this.applicant_transaction_id = applicant_transaction_id;
	}

	public String getApplicant_order_id() {
		return applicant_order_id;
	}

	public void setApplicant_order_id(String applicant_order_id) {
		this.applicant_order_id = applicant_order_id;
	}

	public String getApplicant_transaction_reason() {
		return applicant_transaction_reason;
	}

	public void setApplicant_transaction_reason(String applicant_transaction_reason) {
		this.applicant_transaction_reason = applicant_transaction_reason;
	}

	public String getApplicant_transaction_error_code() {
		return applicant_transaction_error_code;
	}

	public void setApplicant_transaction_error_code(String applicant_transaction_error_code) {
		this.applicant_transaction_error_code = applicant_transaction_error_code;
	}

	public String getApplicant_transaction_payment_mode() {
		return applicant_transaction_payment_mode;
	}

	public void setApplicant_transaction_payment_mode(String applicant_transaction_payment_mode) {
		this.applicant_transaction_payment_mode = applicant_transaction_payment_mode;
	}


	public String getApplicant_food_preference() {
		return applicant_food_preference;
	}


	public void setApplicant_food_preference(String applicant_food_preference) {
		this.applicant_food_preference = applicant_food_preference;
	}


	public String getEvent_type() {
		return event_type;
	}


	public void setEvent_type(String event_type) {
		this.event_type = event_type;
	}


	public String getRegistration_category() {
		return registration_category;
	}


	public void setRegistration_category(String registration_category) {
		this.registration_category = registration_category;
	}

	

	public String getRoom_partner_name() {
		return room_partner_name;
	}


	public void setRoom_partner_name(String room_partner_name) {
		this.room_partner_name = room_partner_name;
	}


	public String getRoom_partner_mobile() {
		return room_partner_mobile;
	}


	public void setRoom_partner_mobile(String room_partner_mobile) {
		this.room_partner_mobile = room_partner_mobile;
	}

	public String getRoom_partner_name_2() {
		return room_partner_name_2;
	}

	public void setRoom_partner_name_2(String room_partner_name_2) {
		this.room_partner_name_2 = room_partner_name_2;
	}

	public String getRoom_partner_mobile_2() {
		return room_partner_mobile_2;
	}

	public void setRoom_partner_mobile_2(String room_partner_mobile_2) {
		this.room_partner_mobile_2 = room_partner_mobile_2;
	}


	@Override
	public String toString() {
		return "EventApplicationModel [application_id=" + application_id + ", applicant_name=" + applicant_name
				+ ", applicant_address=" + applicant_address + ", applicant_email=" + applicant_email
				+ ", applicant_mobile=" + applicant_mobile + ", applicant_city=" + applicant_city + ", applicant_state="
				+ applicant_state + ", applicant_pincode=" + applicant_pincode + ", applicant_affiliation="
				+ applicant_affiliation + ", applicant_council_no=" + applicant_council_no + ", applicant_amount="
				+ applicant_amount + ", applied_date=" + applied_date + ", applicant_ip_address=" + applicant_ip_address
				+ ", applicant_session_id=" + applicant_session_id + ", applicant_status=" + applicant_status
				+ ", applicant_transaction_id=" + applicant_transaction_id + ", applicant_order_id="
				+ applicant_order_id + ", applicant_transaction_reason=" + applicant_transaction_reason
				+ ", applicant_transaction_error_code=" + applicant_transaction_error_code
				+ ", applicant_transaction_payment_mode=" + applicant_transaction_payment_mode
				+ ", applicant_food_preference=" + applicant_food_preference + ", event_type=" + event_type
				+ ", registration_category=" + registration_category + ", user=" + user + ", event=" + event + "]";
	}
	
	

	
	
}
