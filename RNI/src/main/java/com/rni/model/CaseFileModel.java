package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.rni.model.CaseModel;

@Entity
@Table(name="case_file")
public class CaseFileModel {

	@Id
    @GeneratedValue
    @Column(name="case_file_id")
    private int case_file_id;
	
	@Column(name="file_type")
    private String file_type;
	
	@Column(name="file_name")
    private String file_name;
     
    @Column(name="cr_date")
    private Date cr_date;
    
    @ManyToOne
    @JoinColumn(name="case_id")
    private CaseModel caseModel;

    public CaseFileModel(){
    
    }
    
	public CaseFileModel(int case_file_id, String file_type, String file_name, Date cr_date, CaseModel caseModel) {
		super();
		this.case_file_id = case_file_id;
		this.file_type = file_type;
		this.file_name = file_name;
		this.cr_date = cr_date;
		this.caseModel = caseModel;
	}

	public int getCase_file_id() {
		return case_file_id;
	}

	public void setCase_file_id(int case_file_id) {
		this.case_file_id = case_file_id;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public CaseModel getCaseModel() {
		return caseModel;
	}

	public void setCaseModel(CaseModel caseModel) {
		this.caseModel = caseModel;
	}

	
}
