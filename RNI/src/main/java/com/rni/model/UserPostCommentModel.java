package com.rni.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name="user_post_comment")
public class UserPostCommentModel implements Serializable {

	@Id
    @GeneratedValue
    @Column(name="comment_id")
    private int comment_id;
	
	@Column(name="comment_message")
    private String comment_message;
     
    @Column(name="commentt_status")
    private String commentt_status;
    
    @Column(name="comment_cr_date")
    private Date comment_cr_date;
    
    @Column(name="comment_contain")
    private String comment_contain;
    
    @ManyToOne
    @JoinColumn(name="post_id")
    @JsonBackReference
    private UserPostModel post;
    
    @ManyToOne
    @JoinColumn(name="user_id")
    @JsonBackReference
    private UserRegisterModel user;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy="comment", cascade = CascadeType.ALL)
    private Set<UserPostCommentFileModel> commentFile;
   
    public UserPostCommentModel(){
    	
    }


	public UserPostCommentModel(int comment_id, String comment_message, String commentt_status, Date comment_cr_date,
			String comment_contain, UserPostModel post, UserRegisterModel user,Set<UserPostCommentFileModel> commentFile) {
		super();
		this.comment_id = comment_id;
		this.comment_message = comment_message;
		this.commentt_status = commentt_status;
		this.comment_cr_date = comment_cr_date;
		this.comment_contain = comment_contain;
		this.post = post;
		this.user = user;
		this.commentFile = commentFile;
	}

	
	public int getComment_id() {
		return comment_id;
	}


	public void setComment_id(int comment_id) {
		this.comment_id = comment_id;
	}


	public String getComment_message() {
		return comment_message;
	}


	public void setComment_message(String comment_message) {
		this.comment_message = comment_message;
	}


	public String getCommentt_status() {
		return commentt_status;
	}


	public void setCommentt_status(String commentt_status) {
		this.commentt_status = commentt_status;
	}

	public Date getComment_cr_date() {
		return comment_cr_date;
	}

	public void setComment_cr_date(Date comment_cr_date) {
		this.comment_cr_date = comment_cr_date;
	}

	public UserPostModel getPost() {
		return post;
	}

	public void setPost(UserPostModel post) {
		this.post = post;
	}

	public UserRegisterModel getUser() {
		return user;
	}

	public void setUser(UserRegisterModel user) {
		this.user = user;
	}


	public Set<UserPostCommentFileModel> getCommentFile() {
		return commentFile;
	}


	public void setCommentFile(Set<UserPostCommentFileModel> commentFile) {
		this.commentFile = commentFile;
	}


	public String getComment_contain() {
		return comment_contain;
	}


	public void setComment_contain(String comment_contain) {
		this.comment_contain = comment_contain;
	}

}
