package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="event_images")
public class EventImageModel {

	@Id
    @GeneratedValue
    @Column(name="image_id")
    private int image_id;
	
	@Column(name="file_type")
    private String file_type;
	
	@Column(name="file_name")
    private String file_name;
     
    @Column(name="cr_date")
    private Date cr_date;
    
    @ManyToOne
    @JoinColumn(name="event_id")
    private EventModel event;

    public EventImageModel() {
    	
    }
    
	public EventImageModel(int image_id, String file_type, String file_name, Date cr_date, EventModel event) {
		super();
		this.image_id = image_id;
		this.file_type = file_type;
		this.file_name = file_name;
		this.cr_date = cr_date;
		this.event = event;
	}

	public int getImage_id() {
		return image_id;
	}

	public void setImage_id(int image_id) {
		this.image_id = image_id;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public EventModel getEvent() {
		return event;
	}

	public void setEvent(EventModel event) {
		this.event = event;
	}
    
    
    
}
