package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="abstract_submission")
public class AbstractSubmissionModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="abstract_doc_id")
	private int abstract_doc_id;
	
	@Column(name="abstract_title")
	private String abstract_title;
	
	@Column(name="sender_name")
	private String sender_name;
	
	@Column(name="sender_email")
	private String sender_email;
	
	@Column(name="sender_mobile")
	private String sender_mobile;
		
	@Column(name="sender_file_name")
	private String sender_file_name;
	
	@Column(name="sender_file_type")
	private String sender_file_type;
	
	@Column(name="uploaded_date")
	private Date uploaded_date;

	
	
	public int getAbstract_doc_id() {
		return abstract_doc_id;
	}

	public void setAbstract_doc_id(int abstract_doc_id) {
		this.abstract_doc_id = abstract_doc_id;
	}

	public String getAbstract_title() {
		return abstract_title;
	}

	public void setAbstract_title(String abstract_title) {
		this.abstract_title = abstract_title;
	}

	public String getSender_name() {
		return sender_name;
	}

	public void setSender_name(String sender_name) {
		this.sender_name = sender_name;
	}

	public String getSender_email() {
		return sender_email;
	}

	public void setSender_email(String sender_email) {
		this.sender_email = sender_email;
	}

	public String getSender_mobile() {
		return sender_mobile;
	}

	public void setSender_mobile(String sender_mobile) {
		this.sender_mobile = sender_mobile;
	}

	public String getSender_file_name() {
		return sender_file_name;
	}

	public void setSender_file_name(String sender_file_name) {
		this.sender_file_name = sender_file_name;
	}

	public String getSender_file_type() {
		return sender_file_type;
	}

	public void setSender_file_type(String sender_file_type) {
		this.sender_file_type = sender_file_type;
	}

	public Date getUploaded_date() {
		return uploaded_date;
	}

	public void setUploaded_date(Date uploaded_date) {
		this.uploaded_date = uploaded_date;
	}

	
}
