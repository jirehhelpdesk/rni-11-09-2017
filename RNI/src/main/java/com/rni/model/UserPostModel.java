package com.rni.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonManagedReference;

import javax.persistence.JoinColumn;
 

@Entity
@Table(name="user_post")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonAutoDetect(getterVisibility=JsonAutoDetect.Visibility.NONE)
public class UserPostModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue
    @Column(name="post_id")
    private int post_id;
	
	@Column(name="post_title")
    private String post_title;
     
    @Column(name="post_desc")
    private String post_desc;
     
    @Column(name="post_publish_type")
    private String post_publish_type;
     
    @Column(name="post_contain")
    private String post_contain;
    
    @Column(name="post_status")
    private String post_status;
    
    @Column(name="download_privilage")
    private String download_privilage;
    
    @Column(name="post_cr_date")
    private Date post_cr_date;
    
    @Column(name="post_view")
    private long post_view;
    
    @Column(name="post_like")
    private long post_like;
    
    @Column(name="post_dislike")
    private long post_dislike;
    
    @Column(name="feed_of_discussion")
    private String feed_of_discussion;
    
    
    @ManyToOne
    @JoinColumn(name="user_id")
    private UserRegisterModel user;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy="post", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<UserPostCommentModel> comment;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy="post", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<UserPostFile> postFile;
   
    @OneToMany(fetch = FetchType.EAGER, mappedBy="post", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<UserPostInterestModel> postInterest;
    
   
    
    public UserPostModel() {
    	
    }

	


	public UserPostModel(int post_id, String post_title, String post_desc, String post_publish_type,
			String post_contain, String post_status, String download_privilage, Date post_cr_date, long post_view,
			long post_like, long post_dislike, UserRegisterModel user, Set<UserPostCommentModel> comment,
			Set<UserPostFile> postFile, Set<UserPostInterestModel> postInterest) {
		super();
		this.post_id = post_id;
		this.post_title = post_title;
		this.post_desc = post_desc;
		this.post_publish_type = post_publish_type;
		this.post_contain = post_contain;
		this.post_status = post_status;
		this.download_privilage = download_privilage;
		this.post_cr_date = post_cr_date;
		this.post_view = post_view;
		this.post_like = post_like;
		this.post_dislike = post_dislike;
		this.user = user;
		this.comment = comment;
		this.postFile = postFile;
		this.postInterest = postInterest;
	}




	public int getPost_id() {
		return post_id;
	}

	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}

	public String getPost_title() {
		return post_title;
	}

	public void setPost_title(String post_title) {
		this.post_title = post_title;
	}

	public String getPost_desc() {
		return post_desc;
	}

	public void setPost_desc(String post_desc) {
		this.post_desc = post_desc;
	}

	public String getPost_publish_type() {
		return post_publish_type;
	}

	public void setPost_publish_type(String post_publish_type) {
		this.post_publish_type = post_publish_type;
	}

	
	public String getPost_contain() {
		return post_contain;
	}

	public void setPost_contain(String post_contain) {
		this.post_contain = post_contain;
	}

	public Set<UserPostFile> getPostFile() {
		return postFile;
	}

	public void setPostFile(Set<UserPostFile> postFile) {
		this.postFile = postFile;
	}

	public String getPost_status() {
		return post_status;
	}

	public void setPost_status(String post_status) {
		this.post_status = post_status;
	}

	public Date getPost_cr_date() {
		return post_cr_date;
	}

	public void setPost_cr_date(Date post_cr_date) {
		this.post_cr_date = post_cr_date;
	}
	
	public long getPost_view() {
		return post_view;
	}

	public void setPost_view(long post_view) {
		this.post_view = post_view;
	}

	public UserRegisterModel getUser() {
		return user;
	}

	public void setUser(UserRegisterModel user) {
		this.user = user;
	}

	public Set<UserPostCommentModel> getComment() {
		return comment;
	}

	public void setComment(Set<UserPostCommentModel> comment) {
		this.comment = comment;
	}

	public long getPost_like() {
		return post_like;
	}

	public void setPost_like(long post_like) {
		this.post_like = post_like;
	}

	public long getPost_dislike() {
		return post_dislike;
	}

	public void setPost_dislike(long post_dislike) {
		this.post_dislike = post_dislike;
	}

	public String getDownload_privilage() {
		return download_privilage;
	}

	public void setDownload_privilage(String download_privilage) {
		this.download_privilage = download_privilage;
	}




	public String getFeed_of_discussion() {
		return feed_of_discussion;
	}




	public void setFeed_of_discussion(String feed_of_discussion) {
		this.feed_of_discussion = feed_of_discussion;
	}




	public Set<UserPostInterestModel> getPostInterest() {
		return postInterest;
	}




	public void setPostInterest(Set<UserPostInterestModel> postInterest) {
		this.postInterest = postInterest;
	}
    
	
	
}
