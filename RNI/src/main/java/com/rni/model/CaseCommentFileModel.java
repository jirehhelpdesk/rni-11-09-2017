package com.rni.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="case_comment_file")
public class CaseCommentFileModel {

	@Id
    @GeneratedValue
    @Column(name="case_comment_file_id")
    private int case_comment_file_id;
	
    @Column(name="case_comment_file")
    private String case_comment_file;
    
    @Column(name="case_comment_file_type")
    private String case_comment_file_type;
    
    @Column(name="case_comment_file_cr_date")
    private Date case_comment_file_cr_date;
    
    @ManyToOne
    @JoinColumn(name="case_comment_id")
    private CaseUserCommentModel caseComment;

    
    public CaseCommentFileModel(){
    	
    }


	public CaseCommentFileModel(int case_comment_file_id, String case_comment_file, String case_comment_file_type,
			Date case_comment_file_cr_date, CaseUserCommentModel caseComment) {
		super();
		this.case_comment_file_id = case_comment_file_id;
		this.case_comment_file = case_comment_file;
		this.case_comment_file_type = case_comment_file_type;
		this.case_comment_file_cr_date = case_comment_file_cr_date;
		this.caseComment = caseComment;
	}


	public int getCase_comment_file_id() {
		return case_comment_file_id;
	}


	public void setCase_comment_file_id(int case_comment_file_id) {
		this.case_comment_file_id = case_comment_file_id;
	}


	public String getCase_comment_file() {
		return case_comment_file;
	}


	public void setCase_comment_file(String case_comment_file) {
		this.case_comment_file = case_comment_file;
	}


	public String getCase_comment_file_type() {
		return case_comment_file_type;
	}


	public void setCase_comment_file_type(String case_comment_file_type) {
		this.case_comment_file_type = case_comment_file_type;
	}


	public Date getCase_comment_file_cr_date() {
		return case_comment_file_cr_date;
	}


	public void setCase_comment_file_cr_date(Date case_comment_file_cr_date) {
		this.case_comment_file_cr_date = case_comment_file_cr_date;
	}


	public CaseUserCommentModel getCaseComment() {
		return caseComment;
	}


	public void setCaseComment(CaseUserCommentModel caseComment) {
		this.caseComment = caseComment;
	}
  
    
}
