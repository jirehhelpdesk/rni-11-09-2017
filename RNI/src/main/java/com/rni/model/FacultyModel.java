package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="faculty_details")
public class FacultyModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="faculty_id")
	private int faculty_id;
	
	@Column(name="faculty_name")
	private String faculty_name;
	
	@Column(name="faculty_email_id")
	private String faculty_email_id;
	
	@Column(name="faculty_phone")
	private String faculty_phone;
		
	@Column(name="faculty_speciality")
	private String faculty_speciality;
	
	@Column(name="faculty_about_us")
	private String faculty_about_us;

	@Column(name="faculty_photo")
	private String faculty_photo;

	@Column(name="faculty_cr_date")
	private Date faculty_cr_date;
	
	
	public int getFaculty_id() {
		return faculty_id;
	}

	public void setFaculty_id(int faculty_id) {
		this.faculty_id = faculty_id;
	}

	public String getFaculty_name() {
		return faculty_name;
	}

	public void setFaculty_name(String faculty_name) {
		this.faculty_name = faculty_name;
	}

	public String getFaculty_email_id() {
		return faculty_email_id;
	}

	public void setFaculty_email_id(String faculty_email_id) {
		this.faculty_email_id = faculty_email_id;
	}

	public String getFaculty_phone() {
		return faculty_phone;
	}

	public void setFaculty_phone(String faculty_phone) {
		this.faculty_phone = faculty_phone;
	}

	public String getFaculty_speciality() {
		return faculty_speciality;
	}

	public void setFaculty_speciality(String faculty_speciality) {
		this.faculty_speciality = faculty_speciality;
	}

	public String getFaculty_about_us() {
		return faculty_about_us;
	}

	public void setFaculty_about_us(String faculty_about_us) {
		this.faculty_about_us = faculty_about_us;
	}

	public String getFaculty_photo() {
		return faculty_photo;
	}

	public void setFaculty_photo(String faculty_photo) {
		this.faculty_photo = faculty_photo;
	}

	public Date getFaculty_cr_date() {
		return faculty_cr_date;
	}

	public void setFaculty_cr_date(Date faculty_cr_date) {
		this.faculty_cr_date = faculty_cr_date;
	}
	
}
