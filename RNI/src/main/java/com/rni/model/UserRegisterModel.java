package com.rni.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name="user_register")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonAutoDetect(getterVisibility=JsonAutoDetect.Visibility.NONE)
public class UserRegisterModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id")
	private int user_id;
	
	@Column(name="first_name")
	private String first_name;
	
	@Column(name="last_name")
	private String last_name;
	
	@Column(name="email_id")
	private String email_id;
		
	@Column(name="mobile_no")
	private String mobile_no;
	
	@Column(name="password")
	private String password;
	
	@Column(name="cr_date")
	private Date cr_date;

	@Column(name="status")
	private String status;
	
	@Column(name="profession")
	private String profession;
	
	@Column(name="subscription_type")
	private String subscription_type;
	
	@Column(name="feeds_update")
	private String feeds_update;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="user", cascade = CascadeType.ALL)
	private Set<UserPostModel> post;
	
	@OneToMany(mappedBy="user")
    private Set<UserPostCommentModel> comment;
	
	@OneToMany(mappedBy="user")
    private Set<EventApplicationModel> evnApplication;
	
	@OneToOne(mappedBy="user")
    private UserProfileModel profile;
	
	@OneToMany(mappedBy="user")
    private Set<UserPostInterestModel> postInterest;
	
	@OneToMany(mappedBy="user")
    private Set<CaseUserCommentModel> caseComment;
	
	
	public UserRegisterModel(){
		
	}
	

	public UserRegisterModel(int user_id, String first_name, String last_name, String email_id, String mobile_no,
			String password, Date cr_date, String status, String profession,String subscription_type,String feeds_update, Set<UserPostModel> post,
			Set<UserPostCommentModel> comment, Set<EventApplicationModel> evnApplication,Set<CaseUserCommentModel> caseComment,
			UserProfileModel profile) {
		super();
		this.user_id = user_id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.email_id = email_id;
		this.mobile_no = mobile_no;
		this.password = password;
		this.cr_date = cr_date;
		this.status = status;
		this.profession = profession;
		this.subscription_type = subscription_type;
		this.feeds_update = feeds_update;
		this.post = post;
		this.comment = comment;
		this.evnApplication = evnApplication;
		this.profile = profile;
		this.caseComment = caseComment;
	}






	public Set<UserPostModel> getPost() {
		return post;
	}





	public void setPost(Set<UserPostModel> post) {
		this.post = post;
	}





	public Set<UserPostCommentModel> getComment() {
		return comment;
	}





	public void setComment(Set<UserPostCommentModel> comment) {
		this.comment = comment;
	}





	public int getUser_id() {
		return user_id;
	}


	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}


	public String getFirst_name() {
		return first_name;
	}


	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}


	public String getLast_name() {
		return last_name;
	}


	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}


	public String getEmail_id() {
		return email_id;
	}


	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}


	public String getMobile_no() {
		return mobile_no;
	}


	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Date getCr_date() {
		return cr_date;
	}


	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getProfession() {
		return profession;
	}


	public void setProfession(String profession) {
		this.profession = profession;
	}


	public Set<EventApplicationModel> getEvnApplication() {
		return evnApplication;
	}


	public void setEvnApplication(Set<EventApplicationModel> evnApplication) {
		this.evnApplication = evnApplication;
	}


	public UserProfileModel getProfile() {
		return profile;
	}


	public void setProfile(UserProfileModel profile) {
		this.profile = profile;
	}


	public String getSubscription_type() {
		return subscription_type;
	}


	public void setSubscription_type(String subscription_type) {
		this.subscription_type = subscription_type;
	}


	public String getFeeds_update() {
		return feeds_update;
	}


	public void setFeeds_update(String feeds_update) {
		this.feeds_update = feeds_update;
	}


	public Set<CaseUserCommentModel> getCaseComment() {
		return caseComment;
	}


	public void setCaseComment(Set<CaseUserCommentModel> caseComment) {
		this.caseComment = caseComment;
	}
	
	
	
}
