package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="committee_details")
public class CommitteeMemberModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="committee_member_id")
	private int committee_member_id;
	
	@Column(name="member_name")
	private String member_name;
	
	@Column(name="member_email_id")
	private String member_email_id;
	
	@Column(name="member_phone")
	private String member_phone;
		
	@Column(name="member_speciality")
	private String member_speciality;
	
	@Column(name="member_about_us")
	private String member_about_us;

	@Column(name="member_photo")
	private String member_photo;

	@Column(name="member_cr_date")
	private Date member_cr_date;
	
	
	
	public int getCommittee_member_id() {
		return committee_member_id;
	}

	public void setCommittee_member_id(int committee_member_id) {
		this.committee_member_id = committee_member_id;
	}

	public String getMember_name() {
		return member_name;
	}

	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}

	public String getMember_email_id() {
		return member_email_id;
	}

	public void setMember_email_id(String member_email_id) {
		this.member_email_id = member_email_id;
	}

	public String getMember_phone() {
		return member_phone;
	}

	public void setMember_phone(String member_phone) {
		this.member_phone = member_phone;
	}

	public String getMember_speciality() {
		return member_speciality;
	}

	public void setMember_speciality(String member_speciality) {
		this.member_speciality = member_speciality;
	}

	public String getMember_about_us() {
		return member_about_us;
	}

	public void setMember_about_us(String member_about_us) {
		this.member_about_us = member_about_us;
	}

	public String getMember_photo() {
		return member_photo;
	}

	public void setMember_photo(String member_photo) {
		this.member_photo = member_photo;
	}

	public Date getMember_cr_date() {
		return member_cr_date;
	}

	public void setMember_cr_date(Date member_cr_date) {
		this.member_cr_date = member_cr_date;
	}

	
}
