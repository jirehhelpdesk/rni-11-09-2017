package com.rni.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="event_details")
public class EventModel {

	@Id
    @GeneratedValue
    @Column(name="event_id")
    private int event_id;
	
	@Column(name="event_code")
    private String event_code;
     
    @Column(name="event_title")
    private String event_title;
     
    @Column(name="event_desc")
    private String event_desc;
     
    @Column(name="event_location")
    private String event_location;
    
    @Column(name="event_location_map")
    private String event_location_map;
    
    @Column(name="event_charge")
    private String event_charge;
    
    @Column(name="event_charge_fee")
    private float event_charge_fee;
    
    @Column(name="event_start_date")
    private Date event_start_date;
    
    @Column(name="event_end_date")
    private Date event_end_date;
    
    @Column(name="event_cr_date")
    private Date event_cr_date;
    
    @Column(name="event_status")
    private String event_status;
    
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy="event", cascade = CascadeType.ALL)
    private Set<EventImageModel> image;

    @OneToMany(fetch = FetchType.EAGER, mappedBy="event", cascade = CascadeType.ALL)
    private Set<EventApplicationModel> evnApplication;
	
    public EventModel() {
    	
    }
    
	

	public EventModel(int event_id, String event_code, String event_title, String event_desc, String event_location,
			String event_location_map, String event_charge, float event_charge_fee, Date event_start_date,
			Date event_end_date, Date event_cr_date, String event_status, Set<EventImageModel> image,
			Set<EventApplicationModel> evnApplication) {
		super();
		this.event_id = event_id;
		this.event_code = event_code;
		this.event_title = event_title;
		this.event_desc = event_desc;
		this.event_location = event_location;
		this.event_location_map = event_location_map;
		this.event_charge = event_charge;
		this.event_charge_fee = event_charge_fee;
		this.event_start_date = event_start_date;
		this.event_end_date = event_end_date;
		this.event_cr_date = event_cr_date;
		this.event_status = event_status;
		this.image = image;
		this.evnApplication = evnApplication;
	}



	public int getEvent_id() {
		return event_id;
	}

	public void setEvent_id(int event_id) {
		this.event_id = event_id;
	}

	public String getEvent_code() {
		return event_code;
	}

	public void setEvent_code(String event_code) {
		this.event_code = event_code;
	}

	public String getEvent_title() {
		return event_title;
	}

	public void setEvent_title(String event_title) {
		this.event_title = event_title;
	}

	public String getEvent_desc() {
		return event_desc;
	}

	public void setEvent_desc(String event_desc) {
		this.event_desc = event_desc;
	}

	public String getEvent_location() {
		return event_location;
	}

	public void setEvent_location(String event_location) {
		this.event_location = event_location;
	}
	
	public String getEvent_location_map() {
		return event_location_map;
	}

	public void setEvent_location_map(String event_location_map) {
		this.event_location_map = event_location_map;
	}

	public String getEvent_charge() {
		return event_charge;
	}

	public void setEvent_charge(String event_charge) {
		this.event_charge = event_charge;
	}

	public float getEvent_charge_fee() {
		return event_charge_fee;
	}

	public void setEvent_charge_fee(float event_charge_fee) {
		this.event_charge_fee = event_charge_fee;
	}

	public Date getEvent_start_date() {
		return event_start_date;
	}

	public void setEvent_start_date(Date event_start_date) {
		this.event_start_date = event_start_date;
	}

	public Date getEvent_end_date() {
		return event_end_date;
	}

	public void setEvent_end_date(Date event_end_date) {
		this.event_end_date = event_end_date;
	}

	public Date getEvent_cr_date() {
		return event_cr_date;
	}

	public void setEvent_cr_date(Date event_cr_date) {
		this.event_cr_date = event_cr_date;
	}

	public Set<EventImageModel> getImage() {
		return image;
	}

	public void setImage(Set<EventImageModel> image) {
		this.image = image;
	}

	public Set<EventApplicationModel> getEvnApplication() {
		return evnApplication;
	}

	public void setEvnApplication(Set<EventApplicationModel> evnApplication) {
		this.evnApplication = evnApplication;
	}



	public String getEvent_status() {
		return event_status;
	}



	public void setEvent_status(String event_status) {
		this.event_status = event_status;
	}
    
    
	
}
