package com.rni.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="case_user_comment")
public class CaseUserCommentModel {

	@Id
    @GeneratedValue
    @Column(name="case_comment_id")
    private int case_comment_id;
	
	@Column(name="case_comment_message")
    private String case_comment_message;
     
    @Column(name="case_commentt_status")
    private String case_commentt_status;
    
    @Column(name="case_comment_cr_date")
    private Date case_comment_cr_date;
    
    @Column(name="case_comment_contain")
    private String case_comment_contain;
    
    @ManyToOne
    @JoinColumn(name="case_id")
    private CaseModel caseModel;
    
    @ManyToOne
    @JoinColumn(name="user_id")
    private UserRegisterModel user;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy="caseComment", cascade = CascadeType.ALL)
    private Set<CaseCommentFileModel> caseCommentFile;
   
    public CaseUserCommentModel(){
    	
    }

	public CaseUserCommentModel(int case_comment_id, String case_comment_message, String case_commentt_status,
			Date case_comment_cr_date, String case_comment_contain, CaseModel caseModel, UserRegisterModel user,
			Set<CaseCommentFileModel> caseCommentFile) {
		super();
		this.case_comment_id = case_comment_id;
		this.case_comment_message = case_comment_message;
		this.case_commentt_status = case_commentt_status;
		this.case_comment_cr_date = case_comment_cr_date;
		this.case_comment_contain = case_comment_contain;
		this.caseModel = caseModel;
		this.user = user;
		this.caseCommentFile = caseCommentFile;
	}

	
	public int getCase_comment_id() {
		return case_comment_id;
	}

	public void setCase_comment_id(int case_comment_id) {
		this.case_comment_id = case_comment_id;
	}

	public String getCase_comment_message() {
		return case_comment_message;
	}

	public void setCase_comment_message(String case_comment_message) {
		this.case_comment_message = case_comment_message;
	}

	public String getCase_commentt_status() {
		return case_commentt_status;
	}

	public void setCase_commentt_status(String case_commentt_status) {
		this.case_commentt_status = case_commentt_status;
	}

	public Date getCase_comment_cr_date() {
		return case_comment_cr_date;
	}

	public void setCase_comment_cr_date(Date case_comment_cr_date) {
		this.case_comment_cr_date = case_comment_cr_date;
	}

	public String getCase_comment_contain() {
		return case_comment_contain;
	}

	public void setCase_comment_contain(String case_comment_contain) {
		this.case_comment_contain = case_comment_contain;
	}

	public CaseModel getCaseModel() {
		return caseModel;
	}

	public void setCaseModel(CaseModel caseModel) {
		this.caseModel = caseModel;
	}

	public UserRegisterModel getUser() {
		return user;
	}

	public void setUser(UserRegisterModel user) {
		this.user = user;
	}

	public Set<CaseCommentFileModel> getCaseCommentFile() {
		return caseCommentFile;
	}

	public void setCaseCommentFile(Set<CaseCommentFileModel> caseCommentFile) {
		this.caseCommentFile = caseCommentFile;
	}

    
    
}
