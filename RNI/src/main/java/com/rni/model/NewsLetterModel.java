package com.rni.model;


import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name="newsletter_details")
public class NewsLetterModel {
    
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="news_id")
	private int news_id;
	
	@Column(name="user_count")
	private long user_count;
	
	@Column(name="news_sent_date")
	private Date news_sent_date;
	
	@Column(name="sent_user_type")
	private String sent_user_type;
	
	@Column(name="sent_type")
	private String sent_type;
	
	@Column(name="news_matter")
	private String news_matter;

	
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="news", cascade = CascadeType.ALL)
	private List<NewsLetterAttachedFile> attachFile;
	
	public NewsLetterModel(){
		
	}
	
	

	public NewsLetterModel(int news_id, long user_count, Date news_sent_date, String sent_user_type, String sent_type,
			String news_matter, List<NewsLetterAttachedFile> attachFile) {
		super();
		this.news_id = news_id;
		this.user_count = user_count;
		this.news_sent_date = news_sent_date;
		this.sent_user_type = sent_user_type;
		this.sent_type = sent_type;
		this.news_matter = news_matter;
		this.attachFile = attachFile;
	}



	public int getNews_id() {
		return news_id;
	}

	public void setNews_id(int news_id) {
		this.news_id = news_id;
	}

	public long getUser_count() {
		return user_count;
	}

	public void setUser_count(long user_count) {
		this.user_count = user_count;
	}

	public Date getNews_sent_date() {
		return news_sent_date;
	}

	public void setNews_sent_date(Date news_sent_date) {
		this.news_sent_date = news_sent_date;
	}

	public String getSent_type() {
		return sent_type;
	}

	public void setSent_type(String sent_type) {
		this.sent_type = sent_type;
	}

	public String getNews_matter() {
		return news_matter;
	}

	public void setNews_matter(String news_matter) {
		this.news_matter = news_matter;
	}



	public String getSent_user_type() {
		return sent_user_type;
	}



	public void setSent_user_type(String sent_user_type) {
		this.sent_user_type = sent_user_type;
	}



	public List<NewsLetterAttachedFile> getAttachFile() {
		return attachFile;
	}



	public void setAttachFile(List<NewsLetterAttachedFile> attachFile) {
		this.attachFile = attachFile;
	}


}
