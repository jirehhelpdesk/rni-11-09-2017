package com.rni.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_post_file")
public class UserPostFile implements Serializable {

	@Id
    @GeneratedValue
    @Column(name="post_file_id")
    private int post_file_id;
	
    @Column(name="post_file_type")
    private String post_file_type;
    
    @Column(name="post_file_name")
    private String post_file_name;
    
    @Column(name="post_file_render")
    private String post_file_render;
    
    @Column(name="file_cr_date")
    private Date file_cr_date;
    
	@ManyToOne
    @JoinColumn(name="post_id")
    private UserPostModel post;

	public UserPostFile(){
		
	}
			
	public UserPostFile(int post_file_id, String post_file_type, String post_file_name, String post_file_render,
			Date file_cr_date, UserPostModel post) {
		super();
		this.post_file_id = post_file_id;
		this.post_file_type = post_file_type;
		this.post_file_name = post_file_name;
		this.post_file_render = post_file_render;
		this.file_cr_date = file_cr_date;
		this.post = post;
	}

	public int getPost_file_id() {
		return post_file_id;
	}

	public void setPost_file_id(int post_file_id) {
		this.post_file_id = post_file_id;
	}

	public String getPost_file_type() {
		return post_file_type;
	}

	public void setPost_file_type(String post_file_type) {
		this.post_file_type = post_file_type;
	}

	public String getPost_file_name() {
		return post_file_name;
	}

	public void setPost_file_name(String post_file_name) {
		this.post_file_name = post_file_name;
	}

	public String getPost_file_render() {
		return post_file_render;
	}

	public void setPost_file_render(String post_file_render) {
		this.post_file_render = post_file_render;
	}

	public Date getFile_cr_date() {
		return file_cr_date;
	}

	public void setFile_cr_date(Date file_cr_date) {
		this.file_cr_date = file_cr_date;
	}

	public UserPostModel getPost() {
		return post;
	}

	public void setPost(UserPostModel post) {
		this.post = post;
	}
    
	
	
	
}
