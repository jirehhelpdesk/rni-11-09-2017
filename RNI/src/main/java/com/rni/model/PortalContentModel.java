package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="portal_content")
public class PortalContentModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="content_id")
	private int content_id;
	
	@Column(name="content_for")
	private String content_for;
	
	@Column(name="content_heading")
	private String content_heading;
	
	@Column(name="content_desc")
	private String content_desc;
		
	@Column(name="content_img")
	private String content_img;
	
	@Column(name="content_cr_date")
	private Date content_cr_date;

	@Column(name="content_status")
	private String content_status;

	
	
	public int getContent_id() {
		return content_id;
	}

	public void setContent_id(int content_id) {
		this.content_id = content_id;
	}

	public String getContent_for() {
		return content_for;
	}

	public void setContent_for(String content_for) {
		this.content_for = content_for;
	}

	public String getContent_heading() {
		return content_heading;
	}

	public void setContent_heading(String content_heading) {
		this.content_heading = content_heading;
	}

	public String getContent_desc() {
		return content_desc;
	}

	public void setContent_desc(String content_desc) {
		this.content_desc = content_desc;
	}

	public String getContent_img() {
		return content_img;
	}

	public void setContent_img(String content_img) {
		this.content_img = content_img;
	}

	public Date getContent_cr_date() {
		return content_cr_date;
	}

	public void setContent_cr_date(Date content_cr_date) {
		this.content_cr_date = content_cr_date;
	}

	public String getContent_status() {
		return content_status;
	}

	public void setContent_status(String content_status) {
		this.content_status = content_status;
	}
	
}
