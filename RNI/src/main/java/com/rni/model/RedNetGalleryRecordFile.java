package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="gallery_file")
public class RedNetGalleryRecordFile {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="gallery_file_id")
	private int gallery_file_id;
	
	@Column(name="cr_date")
	private Date cr_date;
	
	@Column(name="file_name")
	private String file_name;
	
	@Column(name="file_type")
	private String file_type;
	
	@ManyToOne
    @JoinColumn(name="gallery_id")
    private RedNetGalleryRecord gallery;

	
	public RedNetGalleryRecordFile(){
		
	}
	
	public RedNetGalleryRecordFile(int gallery_file_id, Date cr_date, String file_name, String file_type,
			RedNetGalleryRecord gallery) {
		super();
		this.gallery_file_id = gallery_file_id;
		this.cr_date = cr_date;
		this.file_name = file_name;
		this.file_type = file_type;
		this.gallery = gallery;
	}

	public int getGallery_file_id() {
		return gallery_file_id;
	}

	public void setGallery_file_id(int gallery_file_id) {
		this.gallery_file_id = gallery_file_id;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public RedNetGalleryRecord getGallery() {
		return gallery;
	}

	public void setGallery(RedNetGalleryRecord gallery) {
		this.gallery = gallery;
	}

	
	
	
	
	
}
