package com.rni.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="gallery_details")
public class RedNetGalleryRecord {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="gallery_id")
	private int gallery_id;
	
	@Column(name="cr_date")
	private Date cr_date;
	
	@Column(name="gallery_text")
	private String gallery_text;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="gallery", cascade = CascadeType.ALL)
	private List<RedNetGalleryRecordFile> galleryFile;

	
	public RedNetGalleryRecord(){
		
	}
	
	public RedNetGalleryRecord(int gallery_id, Date cr_date, String gallery_text,
			List<RedNetGalleryRecordFile> galleryFile) {
		super();
		this.gallery_id = gallery_id;
		this.cr_date = cr_date;
		this.gallery_text = gallery_text;
		this.galleryFile = galleryFile;
	}

	public int getGallery_id() {
		return gallery_id;
	}

	public void setGallery_id(int gallery_id) {
		this.gallery_id = gallery_id;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getGallery_text() {
		return gallery_text;
	}

	public void setGallery_text(String gallery_text) {
		this.gallery_text = gallery_text;
	}

	public List<RedNetGalleryRecordFile> getGalleryFile() {
		return galleryFile;
	}

	public void setGalleryFile(List<RedNetGalleryRecordFile> galleryFile) {
		this.galleryFile = galleryFile;
	}
	
	
	
	
}
