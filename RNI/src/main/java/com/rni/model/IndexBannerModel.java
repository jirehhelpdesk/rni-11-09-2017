package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="banner_file_record")
public class IndexBannerModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="banner_id")
	private int banner_id;
	
	@Column(name="banner_file_type")
	private String banner_file_type;
	
	@Column(name="banner_file_name")
	private String banner_file_name;
	
	@Column(name="banner_status")
	private String banner_status;
		
	@Column(name="banner_cr_date")
	private Date banner_cr_date;

	
	public int getBanner_id() {
		return banner_id;
	}

	public void setBanner_id(int banner_id) {
		this.banner_id = banner_id;
	}

	public String getBanner_file_type() {
		return banner_file_type;
	}

	public void setBanner_file_type(String banner_file_type) {
		this.banner_file_type = banner_file_type;
	}

	public String getBanner_file_name() {
		return banner_file_name;
	}

	public void setBanner_file_name(String banner_file_name) {
		this.banner_file_name = banner_file_name;
	}

	public String getBanner_status() {
		return banner_status;
	}

	public void setBanner_status(String banner_status) {
		this.banner_status = banner_status;
	}

	public Date getBanner_cr_date() {
		return banner_cr_date;
	}

	public void setBanner_cr_date(Date banner_cr_date) {
		this.banner_cr_date = banner_cr_date;
	}
	
	
}
