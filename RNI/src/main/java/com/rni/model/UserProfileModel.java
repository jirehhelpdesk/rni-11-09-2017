package com.rni.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_profile")
public class UserProfileModel implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="profile_id")
	private int profile_id;
	
	@Column(name="dob")
	private Date dob;
	
	@Column(name="age")
	private int age;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="under_graduation")
	private String under_graduation;
	
	@Column(name="under_graduation_institute")
	private String under_graduation_institute;
	
	@Column(name="under_graduation_university")
	private String under_graduation_university;
	
	@Column(name="under_graduation_place")
	private String under_graduation_place;
	
	@Column(name="under_graduation_state")
	private String under_graduation_state;
	
	
	@Column(name="post_graduation")
	private String post_graduation;
	
	@Column(name="post_graduation_institute")
	private String post_graduation_institute;
	
	@Column(name="post_graduation_university")
	private String post_graduation_university;
	
	@Column(name="post_graduation_place")
	private String post_graduation_place;
	
	@Column(name="post_graduation_state")
	private String post_graduation_state;
	
	
	@Column(name="fellowship")
	private String fellowship;
	
	@Column(name="fellowship_sub_speciality")
	private String fellowship_sub_speciality;
	
	@Column(name="fellowship_institute")
	private String fellowship_institute;
	
	@Column(name="fellowship_university")
	private String fellowship_university;
	
	@Column(name="fellowship_place")
	private String fellowship_place;
	
	@Column(name="fellowship_state")
	private String fellowship_state;
	
	
	@Column(name="additional_qualification")
	private String additional_qualification;
	
	@Column(name="practice_at")
	private String practice_at;
	
	@Column(name="practice_as")
	private String practice_as;
	
	@Column(name="year_of_experience")
	private int year_of_experience;
	
	@Column(name="phone_number")
	private String phone_number;
	
	@Column(name="website_url")
	private String website_url;
	
	@Column(name="pincode")
	private String pincode;
	
	@Column(name="area_of_interest")
	private String area_of_interest;
	
	@Column(name="other_area_of_interest")
	private String other_area_of_interest;
	
	@Column(name="profile_photo")
	private String profile_photo;
	
	@Column(name="cr_date")
	private Date cr_date;

	@Column(name="update_date")
	private Date update_date;

	@OneToOne
    @JoinColumn(name="user_id")
    private UserRegisterModel user;

	
	public UserProfileModel(){
		
	}

	
	public UserProfileModel(int profile_id, Date dob, int age, String gender, String under_graduation,
			String under_graduation_institute, String under_graduation_university, String under_graduation_place,
			String under_graduation_state, String post_graduation, String post_graduation_institute,
			String post_graduation_university, String post_graduation_place, String post_graduation_state,
			String fellowship, String fellowship_sub_speciality, String fellowship_institute,
			String fellowship_university, String fellowship_place, String fellowship_state,
			String additional_qualification, String practice_at, String practice_as, int year_of_experience,
			String phone_number, String website_url, String pincode, String area_of_interest,
			String other_area_of_interest, String profile_photo, Date cr_date, Date update_date,
			UserRegisterModel user) {
		super();
		this.profile_id = profile_id;
		this.dob = dob;
		this.age = age;
		this.gender = gender;
		this.under_graduation = under_graduation;
		this.under_graduation_institute = under_graduation_institute;
		this.under_graduation_university = under_graduation_university;
		this.under_graduation_place = under_graduation_place;
		this.under_graduation_state = under_graduation_state;
		this.post_graduation = post_graduation;
		this.post_graduation_institute = post_graduation_institute;
		this.post_graduation_university = post_graduation_university;
		this.post_graduation_place = post_graduation_place;
		this.post_graduation_state = post_graduation_state;
		this.fellowship = fellowship;
		this.fellowship_sub_speciality = fellowship_sub_speciality;
		this.fellowship_institute = fellowship_institute;
		this.fellowship_university = fellowship_university;
		this.fellowship_place = fellowship_place;
		this.fellowship_state = fellowship_state;
		this.additional_qualification = additional_qualification;
		this.practice_at = practice_at;
		this.practice_as = practice_as;
		this.year_of_experience = year_of_experience;
		this.phone_number = phone_number;
		this.website_url = website_url;
		this.pincode = pincode;
		this.area_of_interest = area_of_interest;
		this.other_area_of_interest = other_area_of_interest;
		this.profile_photo = profile_photo;
		this.cr_date = cr_date;
		this.update_date = update_date;
		this.user = user;
	}


	public int getProfile_id() {
		return profile_id;
	}


	public void setProfile_id(int profile_id) {
		this.profile_id = profile_id;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getUnder_graduation() {
		return under_graduation;
	}


	public void setUnder_graduation(String under_graduation) {
		this.under_graduation = under_graduation;
	}


	public String getPost_graduation() {
		return post_graduation;
	}


	public void setPost_graduation(String post_graduation) {
		this.post_graduation = post_graduation;
	}


	public String getFellowship() {
		return fellowship;
	}


	public void setFellowship(String fellowship) {
		this.fellowship = fellowship;
	}


	public String getAdditional_qualification() {
		return additional_qualification;
	}


	public void setAdditional_qualification(String additional_qualification) {
		this.additional_qualification = additional_qualification;
	}


	public String getPractice_at() {
		return practice_at;
	}


	public void setPractice_at(String practice_at) {
		this.practice_at = practice_at;
	}


	public String getPractice_as() {
		return practice_as;
	}


	public void setPractice_as(String practice_as) {
		this.practice_as = practice_as;
	}


	public int getYear_of_experience() {
		return year_of_experience;
	}


	public void setYear_of_experience(int year_of_experience) {
		this.year_of_experience = year_of_experience;
	}


	public String getPhone_number() {
		return phone_number;
	}


	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}


	public String getWebsite_url() {
		return website_url;
	}


	public void setWebsite_url(String website_url) {
		this.website_url = website_url;
	}


	public String getPincode() {
		return pincode;
	}


	public void setPincode(String pincode) {
		this.pincode = pincode;
	}


	public String getArea_of_interest() {
		return area_of_interest;
	}


	public void setArea_of_interest(String area_of_interest) {
		this.area_of_interest = area_of_interest;
	}

	public String getOther_area_of_interest() {
		return other_area_of_interest;
	}


	public void setOther_area_of_interest(String other_area_of_interest) {
		this.other_area_of_interest = other_area_of_interest;
	}


	public String getProfile_photo() {
		return profile_photo;
	}


	public void setProfile_photo(String profile_photo) {
		this.profile_photo = profile_photo;
	}


	public Date getCr_date() {
		return cr_date;
	}


	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}


	public UserRegisterModel getUser() {
		return user;
	}


	public void setUser(UserRegisterModel user) {
		this.user = user;
	}


	public Date getUpdate_date() {
		return update_date;
	}


	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}


	public String getUnder_graduation_institute() {
		return under_graduation_institute;
	}


	public void setUnder_graduation_institute(String under_graduation_institute) {
		this.under_graduation_institute = under_graduation_institute;
	}


	public String getUnder_graduation_university() {
		return under_graduation_university;
	}


	public void setUnder_graduation_university(String under_graduation_university) {
		this.under_graduation_university = under_graduation_university;
	}


	public String getUnder_graduation_place() {
		return under_graduation_place;
	}


	public void setUnder_graduation_place(String under_graduation_place) {
		this.under_graduation_place = under_graduation_place;
	}


	public String getUnder_graduation_state() {
		return under_graduation_state;
	}


	public void setUnder_graduation_state(String under_graduation_state) {
		this.under_graduation_state = under_graduation_state;
	}


	public String getPost_graduation_institute() {
		return post_graduation_institute;
	}


	public void setPost_graduation_institute(String post_graduation_institute) {
		this.post_graduation_institute = post_graduation_institute;
	}


	public String getPost_graduation_university() {
		return post_graduation_university;
	}


	public void setPost_graduation_university(String post_graduation_university) {
		this.post_graduation_university = post_graduation_university;
	}


	public String getPost_graduation_place() {
		return post_graduation_place;
	}


	public void setPost_graduation_place(String post_graduation_place) {
		this.post_graduation_place = post_graduation_place;
	}


	public String getPost_graduation_state() {
		return post_graduation_state;
	}


	public void setPost_graduation_state(String post_graduation_state) {
		this.post_graduation_state = post_graduation_state;
	}


	public String getFellowship_sub_speciality() {
		return fellowship_sub_speciality;
	}


	public void setFellowship_sub_speciality(String fellowship_sub_speciality) {
		this.fellowship_sub_speciality = fellowship_sub_speciality;
	}


	public String getFellowship_institute() {
		return fellowship_institute;
	}


	public void setFellowship_institute(String fellowship_institute) {
		this.fellowship_institute = fellowship_institute;
	}


	public String getFellowship_university() {
		return fellowship_university;
	}


	public void setFellowship_university(String fellowship_university) {
		this.fellowship_university = fellowship_university;
	}


	public String getFellowship_place() {
		return fellowship_place;
	}


	public void setFellowship_place(String fellowship_place) {
		this.fellowship_place = fellowship_place;
	}


	public String getFellowship_state() {
		return fellowship_state;
	}


	public void setFellowship_state(String fellowship_state) {
		this.fellowship_state = fellowship_state;
	}
	
	
	
}
