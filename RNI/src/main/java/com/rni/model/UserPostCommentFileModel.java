package com.rni.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="user_post_comment_file")
public class UserPostCommentFileModel implements Serializable {

	@Id
    @GeneratedValue
    @Column(name="comment_file_id")
    private int comment_file_id;
	
    @Column(name="comment_file")
    private String comment_file;
    
    @Column(name="comment_file_type")
    private String comment_file_type;
    
    @Column(name="comment_file_render")
    private String comment_file_render;
    
    @Column(name="comment_file_cr_date")
    private Date comment_file_cr_date;
    
    @ManyToOne
    @JoinColumn(name="comment_id")
    private UserPostCommentModel comment;

    
    public UserPostCommentFileModel(){
    	
    }
    
	public UserPostCommentFileModel(int comment_file_id, String comment_file, String comment_file_type,
			String comment_file_render, Date comment_file_cr_date, UserPostCommentModel comment) {
		super();
		this.comment_file_id = comment_file_id;
		this.comment_file = comment_file;
		this.comment_file_type = comment_file_type;
		this.comment_file_render = comment_file_render;
		this.comment_file_cr_date = comment_file_cr_date;
		this.comment = comment;
	}

	public int getComment_file_id() {
		return comment_file_id;
	}

	public void setComment_file_id(int comment_file_id) {
		this.comment_file_id = comment_file_id;
	}

	public String getComment_file() {
		return comment_file;
	}

	public void setComment_file(String comment_file) {
		this.comment_file = comment_file;
	}

	public String getComment_file_type() {
		return comment_file_type;
	}

	public void setComment_file_type(String comment_file_type) {
		this.comment_file_type = comment_file_type;
	}

	public String getComment_file_render() {
		return comment_file_render;
	}

	public void setComment_file_render(String comment_file_render) {
		this.comment_file_render = comment_file_render;
	}

	public Date getComment_file_cr_date() {
		return comment_file_cr_date;
	}

	public void setComment_file_cr_date(Date comment_file_cr_date) {
		this.comment_file_cr_date = comment_file_cr_date;
	}

	public UserPostCommentModel getComment() {
		return comment;
	}

	public void setComment(UserPostCommentModel comment) {
		this.comment = comment;
	}
    
}
