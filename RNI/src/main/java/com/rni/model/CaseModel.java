package com.rni.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="case_details")
public class CaseModel {

	@Id
    @GeneratedValue
    @Column(name="case_id")
    private int case_id;
	
	@Column(name="case_title")
    private String case_title;
     
    @Column(name="case_desc")
    private String case_desc;
    
    @Column(name="case_cr_date")
    private Date case_cr_date;
    
    @Column(name="case_status")
    private String case_status;
    
    @Column(name="case_of_the_month")
    private String case_of_the_month;
    
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy="caseModel", cascade = CascadeType.ALL)
    private List<CaseFileModel> caseFile;

    @OneToMany(fetch = FetchType.EAGER, mappedBy="caseModel", cascade = CascadeType.ALL)
    private Set<CaseUserCommentModel> caseUserComment;

    
    public CaseModel(){
    	
    }
    
	public CaseModel(int case_id, String case_title, String case_desc, Date case_cr_date, String case_status,
			String case_of_the_month, List<CaseFileModel> caseFile, Set<CaseUserCommentModel> caseUserComment) {
		super();
		this.case_id = case_id;
		this.case_title = case_title;
		this.case_desc = case_desc;
		this.case_cr_date = case_cr_date;
		this.case_status = case_status;
		this.case_of_the_month = case_of_the_month;
		this.caseFile = caseFile;
		this.caseUserComment = caseUserComment;
	}

	
	public int getCase_id() {
		return case_id;
	}

	public void setCase_id(int case_id) {
		this.case_id = case_id;
	}

	public String getCase_title() {
		return case_title;
	}

	public void setCase_title(String case_title) {
		this.case_title = case_title;
	}

	public String getCase_desc() {
		return case_desc;
	}

	public void setCase_desc(String case_desc) {
		this.case_desc = case_desc;
	}

	public Date getCase_cr_date() {
		return case_cr_date;
	}

	public void setCase_cr_date(Date case_cr_date) {
		this.case_cr_date = case_cr_date;
	}

	public String getCase_status() {
		return case_status;
	}

	public void setCase_status(String case_status) {
		this.case_status = case_status;
	}

	public String getCase_of_the_month() {
		return case_of_the_month;
	}

	public void setCase_of_the_month(String case_of_the_month) {
		this.case_of_the_month = case_of_the_month;
	}

	public List<CaseFileModel> getCaseFile() {
		return caseFile;
	}

	public void setCaseFile(List<CaseFileModel> caseFile) {
		this.caseFile = caseFile;
	}

	public Set<CaseUserCommentModel> getCaseUserComment() {
		return caseUserComment;
	}

	public void setCaseUserComment(Set<CaseUserCommentModel> caseUserComment) {
		this.caseUserComment = caseUserComment;
	}
	
}
