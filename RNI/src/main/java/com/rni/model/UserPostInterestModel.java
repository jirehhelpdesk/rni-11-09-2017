package com.rni.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_post_interest")
public class UserPostInterestModel implements Serializable {

	@Id
    @GeneratedValue
    @Column(name="interest_id")
    private int interest_id;
	
	@Column(name="interest")
    private String interest;
    
    @Column(name="interest_date")
    private Date interest_date;
    
    @ManyToOne
    @JoinColumn(name="user_id")
    private UserRegisterModel user;
     
    @ManyToOne
    @JoinColumn(name="post_id")
    private UserPostModel post;
    
    public UserPostInterestModel(){
    	
    }
    

	public UserPostInterestModel(int interest_id, String interest, Date interest_date,
			UserRegisterModel user, UserPostModel post) {
		super();
		this.interest_id = interest_id;
		this.interest = interest;
		this.interest_date = interest_date;
		this.user = user;
		this.post = post;
	}


	public int getInterest_id() {
		return interest_id;
	}


	public void setInterest_id(int interest_id) {
		this.interest_id = interest_id;
	}


	public String getInterest() {
		return interest;
	}


	public void setInterest(String interest) {
		this.interest = interest;
	}


	public Date getInterest_date() {
		return interest_date;
	}


	public void setInterest_date(Date interest_date) {
		this.interest_date = interest_date;
	}

	public UserRegisterModel getUser() {
		return user;
	}


	public void setUser(UserRegisterModel user) {
		this.user = user;
	}


	public UserPostModel getPost() {
		return post;
	}


	public void setPost(UserPostModel post) {
		this.post = post;
	}
    
    
    
    
}
