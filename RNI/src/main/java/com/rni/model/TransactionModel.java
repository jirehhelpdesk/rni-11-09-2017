package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="transaction_details")
public class TransactionModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="transaction_id")
	private int transaction_id;
	
	@Column(name="transaction_amount")
	private float transaction_amount;
	
	@Column(name="transaction_date")
	private Date transaction_date;
	
	@Column(name="ip_address")
	private String ip_address;
	
	@Column(name="session_id")
	private String session_id;
	
	@Column(name="transaction_status")
	private String transaction_status;
	
	@OneToOne
    @JoinColumn(name="application_id")
    private EventApplicationModel application;

	public TransactionModel(){
		
	}

	public TransactionModel(int transaction_id, float transaction_amount, Date transaction_date, String ip_address,
			String session_id, String transaction_status, EventApplicationModel application) {
		super();
		this.transaction_id = transaction_id;
		this.transaction_amount = transaction_amount;
		this.transaction_date = transaction_date;
		this.ip_address = ip_address;
		this.session_id = session_id;
		this.transaction_status = transaction_status;
		this.application = application;
	}


	public int getTransaction_id() {
		return transaction_id;
	}


	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}


	public float getTransaction_amount() {
		return transaction_amount;
	}


	public void setTransaction_amount(float transaction_amount) {
		this.transaction_amount = transaction_amount;
	}


	public Date getTransaction_date() {
		return transaction_date;
	}


	public void setTransaction_date(Date transaction_date) {
		this.transaction_date = transaction_date;
	}


	public String getIp_address() {
		return ip_address;
	}


	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}


	public String getSession_id() {
		return session_id;
	}


	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}


	public String getTransaction_status() {
		return transaction_status;
	}


	public void setTransaction_status(String transaction_status) {
		this.transaction_status = transaction_status;
	}


	public EventApplicationModel getApplication() {
		return application;
	}


	public void setApplication(EventApplicationModel application) {
		this.application = application;
	}
	
	
	
}
