package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="newsletter_attach_file")
public class NewsLetterAttachedFile {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="news_attach_id")
	private int news_attach_id;
	
	@Column(name="file_name")
	private String file_name;
	
	@Column(name="cr_date")
	private Date cr_date;
	
	@ManyToOne
    @JoinColumn(name="news_id")
    private NewsLetterModel news;

	
	public NewsLetterAttachedFile(){
		
	}
	
	public NewsLetterAttachedFile(int news_attach_id, String file_name, Date cr_date, NewsLetterModel news) {
		super();
		this.news_attach_id = news_attach_id;
		this.file_name = file_name;
		this.cr_date = cr_date;
		this.news = news;
	}

	public int getNews_attach_id() {
		return news_attach_id;
	}

	public void setNews_attach_id(int news_attach_id) {
		this.news_attach_id = news_attach_id;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public NewsLetterModel getNews() {
		return news;
	}

	public void setNews(NewsLetterModel news) {
		this.news = news;
	}

}
