package com.rni.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="temp_file")
public class TempFileUploadModel {

	@Id
    @GeneratedValue
    @Column(name="temp_id")
    private int temp_id;
	
	@Column(name="file_type")
    private String file_type;
	
	@Column(name="file_name")
    private String file_name;
     
	@Column(name="upload_from")
    private String upload_from;
    
	@Column(name="upload_for")
    private String upload_for;
     
    @Column(name="cr_date")
    private Date cr_date;

    @Column(name="session_id")
    private String session_id;
     
    
	public int getTemp_id() {
		return temp_id;
	}

	public void setTemp_id(int temp_id) {
		this.temp_id = temp_id;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public Date getCr_date() {
		return cr_date;
	}

	public void setCr_date(Date cr_date) {
		this.cr_date = cr_date;
	}

	public String getUpload_from() {
		return upload_from;
	}

	public void setUpload_from(String upload_from) {
		this.upload_from = upload_from;
	}

	public String getUpload_for() {
		return upload_for;
	}

	public void setUpload_for(String upload_for) {
		this.upload_for = upload_for;
	}

	public String getSession_id() {
		return session_id;
	}

	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}
	
	
	
}
