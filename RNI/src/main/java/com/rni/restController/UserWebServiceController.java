package com.rni.restController;

import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.SysexMessage;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.rni.bean.UserBean;
import com.rni.bean.UserPostBean;
import com.rni.model.UserPostInterestModel;
import com.rni.model.UserPostModel;
import com.rni.service.AdminService;
import com.rni.service.RestfullService;
import com.rni.service.UserService;



@RestController
@RequestMapping("/user")
public class UserWebServiceController {
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RestfullService restService;
	
	static final Logger logger = Logger.getLogger(UserWebServiceController.class);
	
	 
	@RequestMapping(value = "json/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody UserBean getBuyerById(@PathVariable("userId") int userId) {
		
		UserBean user = null;
		
		try 
		{			
			user = userService.getUserDetails(userId);
		} 
		catch (Exception e) 
		{			
			e.printStackTrace();			
		}
		
		return user;
	}
	
	@RequestMapping(value = "xml/{userId}.xml", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody UserBean getBuyerByIdInXML(@PathVariable("userId") int userId) {
		
		UserBean user = null;
		
		try 
		{			
			user = userService.getUserDetails(userId);
		} 
		catch (Exception e) 
		{			
			e.printStackTrace();			
		}
		
		return user;
	} 
	
	
	// FEED LIST JSON
	
	@RequestMapping(value = "allFeeds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<UserPostModel> getAllFeeds() {
		
		List<UserPostModel> feedList = new ArrayList<UserPostModel>();
		
		try{
			feedList = restService.getAllPost();
		}
		catch(Exception e)
		{
			Log.error("All feeds json response ="+e.getMessage());
		}
		
		return feedList;
	}
	
	@RequestMapping(value = "postDetail/{post_id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody UserPostModel getPostDetail(@PathVariable("post_id") int post_id) {
		
		UserPostModel postDetail = restService.getPostInformation(post_id);
		
		return postDetail;
	}
	
	
	
}
