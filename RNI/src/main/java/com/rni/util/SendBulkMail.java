package com.rni.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.rni.dao.AdminDao;
import com.rni.model.BulkMailEmailModel;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class SendBulkMail {

	private static final Logger LOGGER = Logger.getLogger(SendBulkMail.class);
	
	@Autowired
	private AdminDao adminDao;
	
	public String bulkmail(String emailId,String subject)
	{		 
		
		    String resetStatus = "";
		  
		    ResourceBundle smsresource = ResourceBundle.getBundle("resources/emailConfig");
			
		    String auth=smsresource.getString("mail.smtp.auth");
			String starttls = smsresource.getString("mail.smtp.starttls.enable");
			String host = smsresource.getString("mail.smtp.host");			
			String port = smsresource.getString("mail.smtp.port");
			String emailusername = smsresource.getString("emailusername");
			String emailpassword = smsresource.getString("emailpassword");
 
		    String from = emailusername;
		    final String username = from;//change accordingly
		    final String password = emailpassword;//change accordingly
		    Properties props = new Properties();
		      
			props.put("mail.smtp.auth", auth);
			props.put("mail.smtp.starttls.enable", starttls);
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", port);
			
			Session session = Session.getInstance(props,
					  new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username, password);
						}
					  });
	 
			try {
	 
				Message message = new MimeMessage(session);
				
				message.setFrom(new InternetAddress(from));
				message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(emailId));
				message.setSubject(subject);
				
				StringWriter writer = new StringWriter();
				try {
					IOUtils.copy(new FileInputStream(new File("D:/JIREH JAVA  PROJECTS/Project-793-RETNETINDIA/email-temp/index.html")), writer);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				message.setContent(writer.toString(), "text/html");
			
				Transport.send(message);
	 
				resetStatus = "success";	
				
			} catch (MessagingException e) {
				
				  resetStatus = "failed";
	 			  System.out.println("Due to certain reason it has not worked");
	 			  e.printStackTrace();	
	 		 	
			}
	      
	      return resetStatus;
	}
	

	public static void main(String arg[])
	{				
		SendBulkMail email = new SendBulkMail();
		
		System.out.println("Result Status=="+email.bulkmail("prakashbabu@jirehsol.com", "RetNetIndia Event Invitation"));
		
		System.out.println("Result Status=="+email.bulkmail("divyansh.mishra@gmail.com", "RetNetIndia Event Invitation"));
		
		//System.out.println("Result Status=="+email.bulkmail("abinash.raula@jirehsol.com", "RetNetIndia Event Invitation"));
		
	}
	
}
