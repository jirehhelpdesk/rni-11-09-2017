package com.rni.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.rni.model.EventApplicationModel;
import com.rni.service.AdminServiceImpl;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class ReportingExcel {
	
	private static final Logger LOGGER = Logger.getLogger(ReportingExcel.class);
	
	
	
	public String generateEventReportInExcel(List<EventApplicationModel> eModel)
	{		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	String reportDirectory = fileResource.getString("eventDirectory");		
	 	
	 	reportDirectory = reportDirectory + "/" + eModel.get(0).getEvent().getEvent_id();
	 	
	 	String fileName = eModel.get(0).getEvent().getEvent_code()+".xls";
	 	String status = "";
	 	
    	try {
    		 
             File exlDirectory = new File(reportDirectory);
             if(!exlDirectory.exists())
         	 {
            	 exlDirectory.mkdirs();
         	 }
             
             File excelReport = new File(reportDirectory + "/" + fileName);
             
             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
             
             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
        	 
             //Add the created Cells to the heading of the sheet
             
             writableSheet.addCell(new Label(0, 0, "SERIAL NO"));
             writableSheet.addCell(new Label(1, 0, "FULL NAME"));
             writableSheet.addCell(new Label(2, 0, "EMAIL ID"));
             writableSheet.addCell(new Label(3, 0, "PHONE NO"));
             writableSheet.addCell(new Label(4, 0, "CITY"));
             writableSheet.addCell(new Label(5, 0, "STATE"));
             writableSheet.addCell(new Label(6, 0, "PINCODE"));
             writableSheet.addCell(new Label(7, 0, "COUNCIL NO"));
             writableSheet.addCell(new Label(8, 0, "AFFILITION"));
             writableSheet.addCell(new Label(9, 0, "ADDRESS"));
             writableSheet.addCell(new Label(10, 0, "AMOUNT"));
             writableSheet.addCell(new Label(11, 0, "APPLIED DATE"));
             writableSheet.addCell(new Label(12, 0, "STATUS"));
             
             int j = 1;
             
             for(int i=0;i<eModel.size();i++)
             {  	
            	 writableSheet.addCell(new Number(0, j, j));
            	 writableSheet.addCell(new Label(1, j, eModel.get(i).getApplicant_name()));
            	 writableSheet.addCell(new Label(2, j, eModel.get(i).getApplicant_email()));
            	 writableSheet.addCell(new Label(3, j, eModel.get(i).getApplicant_mobile()));
            	 writableSheet.addCell(new Label(4, j, eModel.get(i).getApplicant_city()));
            	 writableSheet.addCell(new Label(5, j, eModel.get(i).getApplicant_state()));	            	 
            	 writableSheet.addCell(new Label(6, j, eModel.get(i).getApplicant_pincode()));    
            	 writableSheet.addCell(new Label(7, j, eModel.get(i).getApplicant_council_no()));    
            	 writableSheet.addCell(new Label(8, j, eModel.get(i).getApplicant_affiliation()));   
            	 writableSheet.addCell(new Label(9, j, eModel.get(i).getApplicant_address()));   
            	 writableSheet.addCell(new Number(10, j, eModel.get(i).getApplicant_amount()));   
            	 
            	 DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            	 Date appliedDate = eModel.get(i).getApplied_date();        
            	 String reportDate = df.format(appliedDate);

            	 writableSheet.addCell(new Label(11, j,reportDate));	 
            	 writableSheet.addCell(new Label(12, j, eModel.get(i).getApplicant_status()));           	
            	 
            	 j = j + 1;
             }
                	             
             writableWorkbook.write();
             writableWorkbook.close();
             
             LOGGER.info("Event Report generated successfully.");
             
             status = "Complete";
             
	         } catch (IOException e) {
	        	 
	        	 status = "Error";
	        	 LOGGER.error("IOException : Exception rais while Event Report generate."+e.getMessage());
	             e.printStackTrace();
	         } catch (RowsExceededException e) {
	        	 
	        	 status = "Error";
	        	 LOGGER.error("RowsExceededException : Exception rais while Event Report generate."+e.getMessage());
	             e.printStackTrace();
	         } catch (WriteException e) {
	        	 
	        	 status = "Error";
	        	 LOGGER.error("WriteException : Exception rais while Event Report generate."+e.getMessage());
	        	 
	             e.printStackTrace();
	         }
    	   	    
    	if(status.equals("Complete"))
    	{
    		return fileName;
    	}
    	else
    	{
    		return status;
    	}
		
	}
	
	
	
	// REPORT EVENT BEFORE LAUNCH 
	
	public String generateEventBeforeLaunchRpt(List<EventApplicationModel> eModel)
	{		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	String reportDirectory = fileResource.getString("eventDirectory");		
	 	
	 	reportDirectory = reportDirectory + "/EventReportBeforeLaunch";
	 	
	 	String fileName = "10thRetNet_Event_Reg.xls";
	 	String status = "";
	 	
    	try {
    		 
             File exlDirectory = new File(reportDirectory);
             if(!exlDirectory.exists())
         	 {
            	 exlDirectory.mkdirs();
         	 }
             
             File excelReport = new File(reportDirectory + "/" + fileName);
             
             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
             
             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
        	 
             //Add the created Cells to the heading of the sheet
             
             writableSheet.addCell(new Label(0, 0, "SERIAL NO"));
             writableSheet.addCell(new Label(1, 0, "FULL NAME"));
             writableSheet.addCell(new Label(2, 0, "EMAIL ID"));
             writableSheet.addCell(new Label(3, 0, "PHONE NO"));
             writableSheet.addCell(new Label(4, 0, "CITY"));
             writableSheet.addCell(new Label(5, 0, "STATE"));
             writableSheet.addCell(new Label(6, 0, "PINCODE"));
             writableSheet.addCell(new Label(7, 0, "COUNCIL NO"));
             writableSheet.addCell(new Label(8, 0, "AFFILITION"));
             writableSheet.addCell(new Label(9, 0, "ADDRESS"));
             writableSheet.addCell(new Label(10, 0, "AMOUNT"));
             writableSheet.addCell(new Label(11, 0, "APPLIED DATE"));
             writableSheet.addCell(new Label(12, 0, "STATUS"));
             writableSheet.addCell(new Label(13, 0, "REGISTRATION TYPE"));
             writableSheet.addCell(new Label(14, 0, "FOOD PREFERENCE"));
             writableSheet.addCell(new Label(15, 0, "TRANSACTION_ID"));
             writableSheet.addCell(new Label(16, 0, "ROOM SHARE-PARTNER NAME_1"));
             writableSheet.addCell(new Label(17, 0, "ROOM SHARE-PARTNER MOBILE_1"));
             writableSheet.addCell(new Label(18, 0, "ROOM SHARE-PARTNER NAME_2"));
             writableSheet.addCell(new Label(19, 0, "ROOM SHARE-PARTNER MOBILE_2"));
             
             int j = 1;
             
             for(int i=0;i<eModel.size();i++)
             {  	
            	 writableSheet.addCell(new Number(0, j, j));
            	 writableSheet.addCell(new Label(1, j, eModel.get(i).getApplicant_name()));
            	 writableSheet.addCell(new Label(2, j, eModel.get(i).getApplicant_email()));
            	 writableSheet.addCell(new Label(3, j, eModel.get(i).getApplicant_mobile()));
            	 writableSheet.addCell(new Label(4, j, eModel.get(i).getApplicant_city()));
            	 writableSheet.addCell(new Label(5, j, eModel.get(i).getApplicant_state()));	            	 
            	 writableSheet.addCell(new Label(6, j, eModel.get(i).getApplicant_pincode()));    
            	 writableSheet.addCell(new Label(7, j, eModel.get(i).getApplicant_council_no()));    
            	 writableSheet.addCell(new Label(8, j, eModel.get(i).getApplicant_affiliation()));   
            	 writableSheet.addCell(new Label(9, j, eModel.get(i).getApplicant_address()));   
            	 writableSheet.addCell(new Number(10, j, eModel.get(i).getApplicant_amount()));   
            	 
            	 DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            	 Date appliedDate = eModel.get(i).getApplied_date();        
            	 String reportDate = df.format(appliedDate);

            	 writableSheet.addCell(new Label(11, j,reportDate));	 
            	 writableSheet.addCell(new Label(12, j, eModel.get(i).getApplicant_status()));      
            	 writableSheet.addCell(new Label(13, j, eModel.get(i).getRegistration_category()));      
            	 writableSheet.addCell(new Label(14, j, eModel.get(i).getApplicant_food_preference()));      
            	 writableSheet.addCell(new Label(15, j, eModel.get(i).getApplicant_transaction_id()));      
            	 writableSheet.addCell(new Label(16, j, eModel.get(i).getRoom_partner_name()));      
            	 writableSheet.addCell(new Label(17, j, eModel.get(i).getRoom_partner_mobile()));      
            	 writableSheet.addCell(new Label(18, j, eModel.get(i).getRoom_partner_name_2()));      
            	 writableSheet.addCell(new Label(19, j, eModel.get(i).getRoom_partner_mobile_2()));      
            	 
            	 j = j + 1;
             }
                	             
             writableWorkbook.write();
             writableWorkbook.close();
             
             LOGGER.info("Event Report generated successfully.");
             
             status = "Complete";
             
	         } catch (IOException e) {
	        	 
	        	 status = "Error";
	        	 LOGGER.error("IOException : Exception rais while Event Report generate."+e.getMessage());
	             e.printStackTrace();
	         } catch (RowsExceededException e) {
	        	 
	        	 status = "Error";
	        	 LOGGER.error("RowsExceededException : Exception rais while Event Report generate."+e.getMessage());
	             e.printStackTrace();
	         } catch (WriteException e) {
	        	 
	        	 status = "Error";
	        	 LOGGER.error("WriteException : Exception rais while Event Report generate."+e.getMessage());
	        	 
	             e.printStackTrace();
	         }
    	   	    
    	if(status.equals("Complete"))
    	{
    		return fileName;
    	}
    	else
    	{
    		return status;
    	}
		
	}
	
	
	/*public static void main(String[] args)
	{		
		generateConsolidatedReport obj = new generateConsolidatedReport();
		
	    System.out.println(obj.createConsolidatedReport());
	}*/

}
