package com.rni.util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.rni.model.BulkMailEmailModel;
import com.rni.model.EventApplicationModel;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class BulkEmailSentStatusReport {

	private static final Logger LOGGER = Logger.getLogger(BulkEmailSentStatusReport.class);
	
	public String generateEventReportInExcel(List<BulkMailEmailModel> emailList)
	{		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
	 	String reportDirectory = fileResource.getString("mailStatusDirectory");		
	 	
	 	String fileName = new Date()+"_Email_sent_status.xls";
	 	String status = "";
	 	
    	try {
    		 
             File exlDirectory = new File(reportDirectory);
             
             if(!exlDirectory.exists())
         	 {
            	 exlDirectory.mkdirs();
         	 }
             
             File excelReport = new File(reportDirectory + "/" + fileName);
             
             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
             
             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
        	 
             // Add the created Cells to the heading of the sheet
             
             writableSheet.addCell(new Label(0, 0, "SERIAL NO"));
             writableSheet.addCell(new Label(1, 0, "EMAIL ID"));
             writableSheet.addCell(new Label(2, 0, "TIME"));
             writableSheet.addCell(new Label(3, 0, "STATUS"));
             
             int j = 1;
             
             for(int i=0;i<emailList.size();i++)
             {  	
            	 writableSheet.addCell(new Number(0, j, j));
            	 writableSheet.addCell(new Label(1, j, emailList.get(i).getEmail_id()));
            	 //writableSheet.addCell(new Label(2, j, eModel.get(i).getApplicant_email()));
            	
            	 DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            	 Date sentTimeStamp = new Date();        
            	 String reportDate = df.format(sentTimeStamp);

            	 writableSheet.addCell(new Label(11, j,reportDate));	 
            	 //writableSheet.addCell(new Label(12, j, eModel.get(i).getApplicant_status()));           	
            	 
            	 j = j + 1;
             }
                	             
             writableWorkbook.write();
             writableWorkbook.close();
             
             LOGGER.info("Event Report generated successfully.");
             
             status = "Complete";
             
	         } catch (IOException e) {
	        	 
	        	 status = "Error";
	        	 LOGGER.error("IOException : Exception rais while Event Report generate."+e.getMessage());
	             e.printStackTrace();
	         } catch (RowsExceededException e) {
	        	 
	        	 status = "Error";
	        	 LOGGER.error("RowsExceededException : Exception rais while Event Report generate."+e.getMessage());
	             e.printStackTrace();
	         } catch (WriteException e) {
	        	 
	        	 status = "Error";
	        	 LOGGER.error("WriteException : Exception rais while Event Report generate."+e.getMessage());
	        	 
	             e.printStackTrace();
	         }
    	   	    
    	if(status.equals("Complete"))
    	{
    		return fileName;
    	}
    	else
    	{
    		return status;
    	}
		
	}
	
}
