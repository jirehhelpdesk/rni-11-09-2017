package com.rni.util;

import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FrameGrabber.Exception;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;


import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.rendering.PDFRenderer;


public class ImageRenderUtil{
    
	/*public static void main(String []args) throws IOException, Exception
    {
		ImageRenderUtil obj = new ImageRenderUtil();
		
		obj.convertImageFromVideo("C:/Users/server/Downloads", "Rb_prob_final_video.mp4");
		
        //System.out.println("Status="+obj.convertImageFromVideo("C:/RetNetIndia/User/Post/8/6","Birla-Tax-Relife-SIP.pdf"));
        
        //obj.saveFirstPageThumbnail("C:/RetNetIndia/User/Post/8/6","Birla-Tax-Relife-SIP.pdf");
    }
	*/
	
	public String convertImageFromVideo(String videoPath,String fileName)
	{
		String status = "";
		
		FFmpegFrameGrabber frameGrabber = new FFmpegFrameGrabber(videoPath+"/"+fileName);
		
        try {
        	
			frameGrabber.start();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        IplImage i;
        try {

            i = frameGrabber.grab();
            BufferedImage  bi = i.getBufferedImage();
            
            int pos = fileName.lastIndexOf(".");
            String renderFileName = fileName.substring(0, pos);
            
            try {
				ImageIO.write(bi,"png", new File(videoPath+"/"+renderFileName+".png"));
				status = "success";
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				status = "failed";
				e.printStackTrace();
			}
            frameGrabber.stop();
            
            status = "success";
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            
            status = "failed";
        }
        
        
        return status;
	}
	
	public String saveFirstPageThumbnail(String path,String fileName) {
        
		String status = "";
		
		String totalFilePath = path + "/" + fileName;
		
		try
		{
		 
		  //Loading an existing PDF document
	      File file = new File(totalFilePath);
	      PDDocument document = PDDocument.load(file);
	       
	      //Instantiating the PDFRenderer class
	      PDFRenderer renderer = new PDFRenderer(document);

	      //Rendering an image from the PDF document
	      BufferedImage image = renderer.renderImage(0);
	      
	      int pos = fileName.lastIndexOf(".");
	      String renderFileName = fileName.substring(0, pos);
	      
	      renderFileName = renderFileName + ".png";
	      
	      //Writing the image to a file
	      ImageIO.write(image, "PNG", new File(path + "/" + renderFileName));
	       
	      System.out.println("Image created");
	       
	      //Closing the document
	      document.close();
	      
	      status = "success";
	      
		 }
	     catch(IOException e)
		 {
			status = "failed";
			e.printStackTrace();
		 }
		
		/*		
		try
		{
			PDDocument document = PDDocument.load(totalFilePath);
	        List<PDPage> pages = document.getDocumentCatalog().getAllPages();
	        PDPage page = pages.get(0); //first page screen shot
	        BufferedImage bufferedImage = page.convertToImage();
	        
	        int pos = fileName.lastIndexOf(".");
	        String renderFileName = fileName.substring(0, pos);
	        
	        File outputFile = new File(path+"/"+renderFileName+".png");
	        ImageIO.write(bufferedImage, "png", outputFile);
	        
	        status = "success";
		}
		catch(IOException e)
		{
			status = "failed";
			e.printStackTrace();
		}*/
		
		return status;
    }
}

