package com.rni.util;

import java.util.ResourceBundle;

public class EmailNotificationUtil {


	public String emailNotification(String fullName,String emailId,String fileName,String link,String action,String reasonFor,String extraInfo)
	{		
		         
		         // Send Email to User that register in the block
		         
				 ResourceBundle resource = ResourceBundle.getBundle(fileName);
				 	
					String defaultUrl = resource.getString("defaultUrl");
					String mailSubject = resource.getString("mailSubject");
			
					String headerText = resource.getString("headerText");
					String headerLogo = defaultUrl + resource.getString("headerLogo");
			
					String bodyHeading = resource.getString("bodyHeading");
					String bodyMessage1 = resource.getString("bodyMessage1");
					String bodyMessage2 = resource.getString("bodyMessage2");
			
					String footerMessage = resource.getString("footerMessage");
			
					String facebookicon = defaultUrl + resource.getString("facebookicon");
					String twittericon = defaultUrl + resource.getString("twittericon");
					String googleicon = defaultUrl + resource.getString("googleicon");
					String linkedicon = defaultUrl + resource.getString("linkedicon");
			
					String facebookLink = resource.getString("facebookLink");
					String twitterLink = resource.getString("twitterLink");
					String googlePlus = resource.getString("googlePlus");
					String linkedIn = resource.getString("linkedIn");
			   	 
					StringBuilder text = new StringBuilder();
				        			        
				        
					    /* Sending Emails to Register user */
					       
				        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
				        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
				       
				        text.append("<body marginwidth='0' style='font-family:Verdana, Geneva, sans-serif;' marginheight='0' topmargin='0' leftmargin='0'>");
				       
				        text.append("<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>");
				        
						        text.append("<tr>");
						        
						          text.append("<td width='100%' valign='top' bgcolor='' style='padding-top:20px;'>");
							       
						            text.append("<table width='600' border='0' cellpadding='0' cellspacing='0' align='center' style='margin:0 auto;'>");					        
							         
						              text.append("<tr style='background-color:#29363E;'>");
							            
						                text.append("<td>");
								       
								            text.append("<table border='0' cellpadding='0' cellspacing='0' align='left' class='title'>");
									        text.append("<tr><td>");
									        text.append("<a href='"+defaultUrl+"' target='_'><img src='"+headerLogo+"' style='height: 58px;width: 141px;'/><span style='color:#fff;font-size: 44px;'>RetNet India</span></a>");
									        text.append("</td></tr>");
									        text.append("</table>");
								        
								        text.append("</td>");
								        
						              text.append("</tr>");
						             
						              text.append("<tr><td style='text-align: center;'height='3'></td></tr>");
						             
						              text.append("<tr><td height='30' width='100%' bgcolor='#515151' style='vertical-align: top;background-color:#21a5b2;' valign='top'>");
						              text.append("<a  style='text-decoration: none; font-size: 20px;color:#fff;font-family: Verdana, Geneva, sans-serif;'>"+bodyHeading+"</a>");
						              text.append("</td></tr>");
						              						              
						            text.append("</table>");
						        
						        text.append("<table bgcolor='#f5f5f5'  width='600'  cellspacing='0' cellpadding='0' border='0' align='center' style='height:auto;margin-top:3px;text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;' >");
						        text.append("<tbody>");
						        text.append("<tr>");
						        text.append("<td width='100%' valign='middle' style='text-align: left; font-family:cursive,Arial,sans-serif; font-size: 14px; color: rgb(63, 67, 69); line-height: 24px;' t-style='not6Text' mc:edit='41' object='text-editable'>");
						        text.append("<p style='font-family:cursive,Arial,sans-serif; font-weight: normal;'>");
						        text.append("Dear "+fullName+",");
						        text.append("</p><br>");
						        text.append("<p style='font-family:cursive,Arial,sans-serif; font-weight: normal;'>");
						        
						        if(reasonFor.equals("Abstract"))
						        {
						        	text.append(bodyMessage1+" <b>'"+extraInfo+"'</b> "+resource.getString("bodyMessage1half"));
						        }
						        else
						        {
						        	text.append(bodyMessage1);
						        }
						        text.append("</p>");
						        text.append("<p style='font-family:cursive,Arial,sans-serif; font-weight: normal;'>");
						        text.append(bodyMessage2);
						        text.append("</p>");
						        
						       
						        if(reasonFor.equals("Credential"))
						        {
						        	String credentialArray[] = extraInfo.split("/");
						        	text.append("<p style='font-family:cursive,Arial,sans-serif; font-weight: normal;'>");
							        text.append("<b>Username</b> - "+ credentialArray[0] +"<br>");
							        text.append("<b>Password</b> - "+ credentialArray[1]);
							        text.append("</p>");
						        }
						        
						        
						        if(reasonFor.equals("Order"))
						        {
						        	  String orderInfo[] = extraInfo.split("/");
						        	 
						        	  /* Body Part-3 */
								          
					                      text.append("<tr><td><table width='100%' cellpadding='5' cellspacing='0px'>");
					                      text.append("<tr><td colspan='6' style='border: 1px solid #ccc; background-color: #00b6f5; color: #fff;'><strong>Event confirmation</strong></td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Name : </strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>"+fullName+"</td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Email Id :</strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>"+emailId+"</td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Location :</strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>"+orderInfo[0]+"</td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Status :</strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>"+orderInfo[2]+"</td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Transaction id :</strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>"+orderInfo[3]+"</td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Registration type :</strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>"+orderInfo[4]+"</td></tr>");
					                      
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Currency :</strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>INR</td></tr>");
					                     
					                      text.append("</table>");
					                      text.append("</td></tr>");
					                      
					                      text.append("<tr><td><p style='border-top:1px solid #ccc;margin-top:0px;'></p></td></tr>");
					                      
					                      text.append("<tr><td>"); 
					                      text.append("<table width='100%' cellspacing='0px' cellpadding='5' style='margin-top: -10px;'>");
					                      text.append("<tbody>");
					                    
					                      /*text.append("<tr style='float: right; text-align: right;'>");
					                      text.append("<td style='border: 0px solid #ccc;width:130px;'><strong>Amount :</strong></td>");
					                      text.append("<td style='border: 0px solid rgb(204, 204, 204); width: 100px;'>"+session.getAttribute("kitCharge")+"</td>");
					                      text.append("</tr>");
					                      
					                      text.append("<tr><td></td></tr>");*/
					                      
					                      text.append("<tr style='float: right; text-align: right;'>");
					                      text.append("<td style='border: 0px solid #ccc;width:200px;'><strong>Registration Charge :</strong></td>"); 
					                      text.append("<td style='border: 0px solid rgb(204, 204, 204); width: 100px;'>"+orderInfo[1]+"</td>");
					                      text.append("</tr>");
					                      
					                      text.append("<tr><td><p style='border-top:1px solid #ccc;margin-top: -10px;'></p></td></tr>");
					                      
					                      text.append("</tbody>");			                      
					                      text.append("</table>");
					                      
					                      text.append("</td></tr>");
					                    
							         /* End of Body Part-3 */
						        }
						        
						        text.append("</td></tr>");
						        
						        if(!link.equals("NotRequired"))
						        {
							        text.append("<tr><td align='center'>");
							        System.out.println("LINK--"+link);
							        text.append("<a href='"+link+"' style='font-size:14px;font-weight:bold;text-align:center;text-decoration:none;font-family:Verdana, Geneva, sans-serif;-webkit-text-size-adjust:none;padding: 4px 6px;color:#fff !important;background-color:#21a5b2;'>"+action+"</a>");
							        
							        text.append("</td></tr>");
						        }
						        
						        
						        text.append("<tr><td>&nbsp;&nbsp;</td></tr>");
						        
						        text.append("</tbody></table>");
						        
						        
						        if(reasonFor.equals("Abstract"))
						        {
						        	text.append("<table align='center' width='600' border='0' cellpadding='0' cellspacing='0' style='padding: 0; background: #cecece;' bgcolor='#cecece' data-module='Section_2' data-bg='Section_2_bg_image' data-bgcolor='Section_2_bg_color' data-thumb='thumbnails/2.jpg'>");
						        	text.append("<tbody><tr>");
						        	text.append("<td align='center' valign='top'>");
						        	text.append("<table width='600' border='0' cellspacing='0' cellpadding='0' class='p100' style='mso-table-rspace: 0pt; mso-table-lspace: 0pt; border-collapse: collapse;'>");
						        	text.append("<tbody><tr>");
						        	text.append("<td background='http://retnetindia.com/static/extEmail/images/Footer-IMG.jpg' align='center' valign='top' style='background-position: top center;background-size: cover;height:800px;width: 800px;' data-bg='Section_2_bg_image_in' data-bgcolor='Section_2_bg_color_in'><div> </div></td>");
						        	text.append("</tr></tbody>");
						        	text.append("</table></td>");
						        	text.append("</tr>");
						        	text.append("</tbody></table>");
						       }
						        
						        						        
						        text.append("<table width='600' border='0' cellpadding='0' cellspacing='0' align='center' style='margin:0 auto;'>");
						        text.append("<tr><td height='3'></td></tr>");
						        text.append("<tr><td align='center' style='font-size:15px;' bgcolor='#e1e1e1' >");
						        text.append("<a target='_' href='"+facebookLink+"'><img src='"+facebookicon+"'></a>&nbsp;&nbsp;&nbsp;");
						        text.append("<a target='_' href='"+twitterLink+"'><img src='"+twittericon+"'></a>&nbsp;&nbsp;&nbsp;");
						        text.append("<a target='_' href='"+googlePlus+"'><img src='"+googleicon+"'></a>&nbsp;&nbsp;&nbsp;");
						        text.append("<a target='_' href='"+linkedIn+"'><img src='"+linkedicon+"'></a>");
						        text.append("</td></tr>");
						        text.append("<tr><td height='3'></td></tr>");
						        text.append("<tr><td bgcolor='#e1e1e1' align='center'>"+footerMessage+"</td></tr>");
						        text.append("</table>");
						        
						        text.append("<table width='600' border='0' cellpadding='0' cellspacing='0' align='center'  style='margin:0 auto;background-color: #29363E;'>");
						        text.append("<tr><td>");
						        text.append("<table width='600' border='0' cellpadding='0' cellspacing='0' align='center' style='margin:0 auto;font-size: 12px;'>");
						        text.append("<tr><td style='padding:0px;line-height:20px;font-size:10px;color:#ffffff;text-align:center;'>");
						        text.append("&#169; 2018 RetNet India&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
						        text.append("<span>");					      
						        text.append("</span>");
						        text.append("<br>");
						        text.append(" RetNet India <span>Design and Developed by <a href='http://jirehsol.co.in/' target='_' style='text-decoration:underline;color:#ffffff;'>Jireh</a>.</span>");
						        text.append("</td></tr></table>");
						        
						        
						    text.append("</td></tr><table>");
						      
						    text.append("</td></tr>");    
						    
				        text.append("</table>");
				        
				        text.append("</body>");
				        text.append("</html>");
				       
				        /* END OF BODY PART */
				        
				       
				        return text.toString();
				      
	}
	
	
	/*public static void main(String arg[])
	{
		EmailNotificationUtil obj = new EmailNotificationUtil();
		//(String fullName,String emailId,String fileName,String link,String action,String reasonFor,String extraInfo)
		String status = obj.emailNotification("Abinash", "abinash.raula@jirehsol.com", "resources/wellcomeRegistration","http://localhost:8081/AOneTerminal/", "Go to Home","","");
	
		System.out.println("result="+status);
	}*/
	
	
}
