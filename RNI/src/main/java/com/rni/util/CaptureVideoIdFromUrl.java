package com.rni.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CaptureVideoIdFromUrl {

	
	/*public static void main(String ar[])
	{
		CaptureVideoIdFromUrl obj = new CaptureVideoIdFromUrl();
		
		System.out.println("EmbedId="+obj.getYoutubeEmbedIdFromUrl("https://www.youtube.com/watch?v=K4wEI5zhHB0"));
		
	}*/
	
	public String getYoutubeEmbedIdFromUrl(String url)
	{
		String youTubeVideoUrl = "https://www.youtube.com/embed/";
		
		String embedId = "";
		String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

	    Pattern compiledPattern = Pattern.compile(pattern);
	    Matcher matcher = compiledPattern.matcher(url);

	    if(matcher.find()){
	    	embedId = matcher.group();
	    }
	    
	    return youTubeVideoUrl+embedId;
	}
}
