package com.rni.scheduler;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.rni.dao.AdminDao;
import com.rni.dao.UserDao;
import com.rni.model.EventModel;
import com.rni.service.UserServiceImpl;

public class VideoRenderScheduler {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private AdminDao adminDao;
	
	private static final Logger LOGGER = Logger.getLogger(VideoRenderScheduler.class);
	
	/*@Scheduled(fixedRate=900000)// 15 minutes */	
	
	/*@Scheduled(fixedRate=1800000)// 30 minutes */	
	
	/*@Scheduled(fixedRate=3600000)// 1 hour */	
	
	@Scheduled(fixedRate=3600000)
    public void renderVideoFile() {
       
		String location = displayIt(new File("C:/RetNetIndia"));
	
    }
	
	
	public String displayIt(File node){

		
		String location = "";
		
		if(node.isDirectory()){
			String[] subNote = node.list();
			for(String filename : subNote){
				
				String flName = filename;
				
				displayIt(new File(node, filename));
				
				location = node.getAbsoluteFile().toString();
			}
		}
		
		System.out.println("location="+location);
		
		return location;

	}
	 
}
