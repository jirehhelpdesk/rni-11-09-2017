package com.rni.scheduler;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.rni.dao.AdminDao;
import com.rni.dao.UserDao;
import com.rni.model.EventModel;
import com.rni.service.UserServiceImpl;

public class EventStatusScheduler {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private AdminDao adminDao;
	
	private static final Logger LOGGER = Logger.getLogger(EventStatusScheduler.class);
	
	/*@Scheduled(fixedRate=900000)// 15 minutes */	
	
	/*@Scheduled(fixedRate=1800000)// 30 minutes */	
	
	/*@Scheduled(fixedRate=3600000)// 1 hour */	
	
	@Scheduled(fixedRate=3600000)
    public void updateEventStatus() {
        
		List<EventModel> eventList = userDao.getEventList();
		Date today = new Date();
		String eventStatus = "";
		
        for(int i=0;i<eventList.size();i++)
        {
        	EventModel eventModel = new EventModel();
    	   
        	Date eventStartDate = eventList.get(i).getEvent_start_date();
    	    eventModel.setEvent_id(eventList.get(i).getEvent_id());
    	    
			if(eventStartDate.compareTo(today)>0)
			{																		
				//  System.out.println("eventStartDate is after today");
				LOGGER.info("eventStartDate is after today");
				eventModel.setEvent_status("Open");
			}
			else if(eventStartDate.compareTo(today)<0)
			{																		
				// System.out.println("eventStartDate is before today");
				
				eventModel.setEvent_status("Expired");
			}
			else
			{
				// System.out.println("eventStartDate is equal to today");
				
				eventModel.setEvent_status("Closed");
			}		
			
			try{
				eventStatus = userDao.updateEventStatus(eventModel);
				
				if(eventStatus.equals("success"))
				{
					eventModel = null;
					LOGGER.info("Scheduler update on event status success."+new Date());
				}
				else
				{
					eventModel = null;
					LOGGER.error("Scheduler update on event status failed."+new Date());
				}
			}
			catch(Exception e)
			{
				
				LOGGER.error("Exception: Scheduler update on event status failed."+new Date());
			}			
        }
    }
	 
}
