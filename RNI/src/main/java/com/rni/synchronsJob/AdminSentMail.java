package com.rni.synchronsJob;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;

import com.rni.dao.AdminDao;
import com.rni.model.EmailModel;
import com.rni.model.NewsLetterModel;
import com.rni.service.AdminServiceImpl;
import com.rni.util.EmailNotificationUtil;
import com.rni.util.EmailSentUtil;
import com.rni.util.ImageRenderUtil;
import com.rni.util.PasswordGenerator;
import com.rni.util.UniquePatternGeneration;

public class AdminSentMail {

	
	@Autowired
	private AdminDao adminDao;
	
	PasswordGenerator pwGenerator = new PasswordGenerator();
	
	ImageRenderUtil videoRender = new ImageRenderUtil();
	
	EmailNotificationUtil emailUtil = new EmailNotificationUtil();
	
	private static final Logger LOGGER = Logger.getLogger(AdminSentMail.class);
	
	
	@Async
	public String sendNewsLetterMail(List<String> emailIdList,String subject,String fileNameList)
	{
		String status = "";
		
		List<String> attachFiles = new ArrayList<String>();
		
		if(fileNameList.contains("/"))
		{
			fileNameList = fileNameList.substring(0, fileNameList.length()-1);
		}
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/RetNetFileDirectories");
 	    String filePath = fileResource.getString("newsDirectory");
 	    
 	    // ARRANGE THE File Array
 	    
 	    String fileNameListArray[] = fileNameList.split("/");
 	    
 	    for(int i=0;i<fileNameListArray.length;i++)
 	    {
 	    	attachFiles.add(filePath +"/"+ fileNameListArray[i]);
 	    }
 	    
 	    // SENDING MAIL TO THE USER 
 	    
 	    String repositoryFile = "resources/newsLetter";

		ResourceBundle resource = ResourceBundle.getBundle(repositoryFile);
	 	 
		String mailSubject = subject;
		String defaultUrl = resource.getString("defaultUrl");
		
		UniquePatternGeneration generatorObject = new UniquePatternGeneration();
		
		String link = defaultUrl;
		
		String reasonFor = "NotRequired";
		
		String extraInfo = "NotRequired";
			
		String emailBody = emailUtil.emailNotification("User","support@retnetindia.com",repositoryFile,link,"RetnetIndia",reasonFor,extraInfo);
			
		String mailStatus = "";
		
		/* SENDING MAIL */
	    
		for(int i=0;i<emailIdList.size();i++)
		{
			
			EmailSentUtil emailStatus = new EmailSentUtil();
 	        
			try {
				mailStatus = emailStatus.sentEmailWithAttachment(emailIdList.get(i), mailSubject, emailBody, attachFiles);
				
				LOGGER.info("sent mail to "+emailIdList.get(i));
			} 
			catch (Exception e) {
				
				// TODO Auto-generated catch block
				mailStatus = "failed";
				LOGGER.error("Some exception arise while sending mail");
				e.printStackTrace();
			}
			
  
		    /* END OF SENDING MAIL */
					
			
			if(!mailStatus.equals("success"))
			{				
				EmailModel emailDetail = new EmailModel();
				
				emailDetail.setEmail_id(emailIdList.get(i));
				emailDetail.setEmail_subject(mailSubject);
				emailDetail.setEmail_content(emailBody);
				emailDetail.setEmail_status("failed");
				emailDetail.setCr_date(new Date());
				
				status = adminDao.getSaveEmailDetails(emailDetail);
			}
			
		}
			
		return status;
	}
	
	
	
	
}
